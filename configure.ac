AC_INIT(tucnak, 4.36) AC_CONFIG_HEADER([config.h])

AC_CONFIG_SRCDIR(src/main.c)
AC_CANONICAL_TARGET
AM_INIT_AUTOMAKE
m4_ifdef([AM_SILENT_RULES], [AM_SILENT_RULES([yes])])


dnl Determine default prefix
test x$prefix = "xNONE" && prefix="$ac_default_prefix"


if test -f "/etc/debian_version"; then
    echo "TIP: If following test fails, try to run: apt-get install libc6-dev"
fi
if test -f "/etc/SuSE-release"; then
    echo "TIP: If following test fails, try to run: yast -i gcc"
fi

dnl Locate required external software
AC_PROG_CC
tucnak_cc_ver=$($CC -dumpversion)

# --disable-opt
opt='-O2'
AC_ARG_ENABLE(opt, [  --disable-opt           Disable compiling optimisation (-O0)], disable_opt=yes,disable_opt=no )
if test x"$disable_opt" = x"yes"; then        
    CFLAGS=$(echo "$CFLAGS"|sed 's/-O2/-O0/')
    opt='-O0'
fi



# check for package type
pkg="bin"
AC_ARG_WITH([pkg], [AS_HELP_STRING([--with-pkg], [Set package identifier for automatic update (default: bin)])], 
    [
        if test x"$with_pkg" != x""; then
            pkg="$with_pkg"
        fi
    ],[])
AC_DEFINE_UNQUOTED([PKG], ["$pkg"], [Package identifier for automatic update])
    


AC_MSG_CHECKING([whether linker supports -z now])
OLDLDFLAGS=$LDFLAGS
LDFLAGS="$LDFLAGS -z now"
AC_LINK_IFELSE(
    [AC_LANG_PROGRAM([],[])],
    [tucnak_ld_z_now=yes],
    [tucnak_ld_z_now=no
     LDFLAGS=$OLDLDFLAGS])
AC_MSG_RESULT([$tucnak_ld_z_now])


AC_MSG_CHECKING([whether linker supports -rdynamic])
OLDLDFLAGS=$LDFLAGS
LDFLAGS="$LDFLAGS -rdynamic"
AC_LINK_IFELSE(
    [AC_LANG_PROGRAM([],[])],
    [tucnak_ld_rdynamic=yes],
    [tucnak_ld_rdynamic=no
     LDFLAGS=$OLDLDFLAGS])
AC_MSG_RESULT([$tucnak_ld_rdynamic])


AC_MSG_CHECKING([whether linker supports -Wl,-Map (GNU)])
tucnak_ld_wlmap=unchecked
OLDLDFLAGS=$LDFLAGS
LDFLAGS="$LDFLAGS -Wl,-Map,tucnak.map"
AC_LINK_IFELSE(
    [AC_LANG_PROGRAM([],[])],
    [tucnak_ld_wlmap=yes
     tucnak_ld_flags="-Wl,-Map,tucnak.map"],
    [tucnak_ld_wlmap=no
     LDFLAGS=$OLDLDFLAGS])

if test x"$tucnak_ld_wlmap" = x"no"; then
    AC_MSG_CHECKING([whether linker supports -Wl,-map (BSD)])
    tucnak_ld_wlmap=unchecked
    OLDLDFLAGS=$LDFLAGS
    LDFLAGS="$LDFLAGS -Wl,-map,tucnak.map"
    AC_LINK_IFELSE(
        [AC_LANG_PROGRAM([],[])],
        [tucnak_ld_wlmap=yes
         tucnak_ld_flags="-Wl,-map,tucnak.map"],
        [tucnak_ld_wlmap=no
         LDFLAGS=$OLDLDFLAGS])
fi    

rm -f tucnak.map
AC_MSG_RESULT([$tucnak_ld_wlmap])


AC_PROG_INSTALL


dnl Check for system header files
AC_HEADER_STDC
AC_CHECK_HEADERS(arpa/inet.h ctype.h dirent.h dlfcn.h errno.h fcntl.h grp.h iconv.h inttypes.h libgen.h linux/ppdev.h locale.h math.h netdb.h netinet/in.h netinet/ip.h nl_types.h png.h process.h pty.h pwd.h regex.h signal.h stdarg.h stdint.h stdio.h time.h unistd.h sys/ioctl.h sys/kd.h sys/param.h sys/select.h sys/signal.h sys/socket.h sys/soundcard.h sys/stat.h sys/time.h sys/types.h sys/vfs.h sys/wait.h termios.h utime.h winsock2.h)
AC_HEADER_TIME

tucnak_libc_ver=""
iprig_libc_ver="cross"
if test "$cross_compiling" = yes; then
    iprig_libc_ver=$(cat $TOPDIR/staging_dir_$target_cpu/uclibc_version)
    iprig_cc_ver=$(cat $TOPDIR/staging_dir_$target_cpu/gcc_version)
else
AC_TRY_RUN([
#include <features.h>
#include <stdio.h>
#ifdef __CYGWIN__
#include <cygwin/version.h>
#endif
FILE *f;
int main(void){
    f=fopen("conftest.libc.out", "wt");
    if (!f) return -1;
#ifdef __GLIBC__
    fprintf(f, "glibc %d.%d (%s)\n", __GLIBC__, __GLIBC_MINOR__, gnu_get_libc_version());
#endif
#ifdef __CYGWIN__
    fprintf(f, "cygwin %d.%d.%d, api %d.%d\n", 
    CYGWIN_VERSION_DLL_MAJOR/1000,
    CYGWIN_VERSION_DLL_MAJOR%1000,
    CYGWIN_VERSION_DLL_MINOR,
    CYGWIN_VERSION_API_MAJOR,
    CYGWIN_VERSION_API_MINOR);
#endif    
    fclose(f);
    return 0;
}                                                                
],[ tucnak_libc_ver=$(cat conftest.libc.out)], []) 
rm -f conftest.libc.out
fi

AC_CHECK_LIB(m, sin,   LIBM="-lm")
AC_CHECK_LIB(m, cos,   LIBM="-lm")
AC_CHECK_LIB(m, tan,   LIBM="-lm")
AC_CHECK_LIB(m, acos,  LIBM="-lm")
AC_CHECK_LIB(m, round, LIBM="-lm")
LIBS="$LIBS $LIBM"

#AC_CHECK_LIB(ws2_32, [WSAStartup], [LIBS="$LIBS -lws2_32"])
if test "x$ac_cv_header_winsock2_h" = "xyes"; then
	LIBS="$LIBS -lws2_32 -lsetupapi"
fi

tucnak_mlibs=""
tucnak_mdebs=""
tucnak_mrpms=""
tucnak_olibs=""
tucnak_odebs=""
tucnak_orpms=""


# check for make
AC_CHECK_PROG(HAVE_MAKE, make, [yes], [no])
if test x"$HAVE_MAKE" = x"no"; then
    tucnak_mlibs="$tucnak_mlibs make"
    tucnak_mdebs="$tucnak_mdebs make"
    tucnak_mrpms="$tucnak_mrpms make"
fi



# check for #pkg-config
AC_CHECK_PROG(HAVE_PKGCONFIG, pkg-config, [yes], [no])
if test x"$HAVE_PKGCONFIG" = x"no"; then
    tucnak_mlibs="$tucnak_mlibs pkg-config"
    tucnak_mdebs="$tucnak_mdebs pkg-config"
    tucnak_mrpms="$tucnak_mrpms pkg-config"
fi
    

#
# check for libzia
#
tucnak_libzia_ver=""
tucnak_libzia_settings=""
AC_MSG_CHECKING([for libzia])


PKG_CHECK_MODULES(zia, libzia, [libzia_via_pkg=1], [libzia_via_pkg=0])
LIBZIA_CFLAGS=$zia_CFLAGS
LIBZIA_LIBS=$zia_LIBS

if test -d "../libzia"; then
    echo "existuje ../libzia"
    tucnak_libzia_ver="$(sh ../libzia/bin/zia-config --version) in $(readlink -f ../libzia/bin/zia-config)"
    tucnak_libzia_settings="../libzia/settings"
    LIBZIA_CFLAGS="$(bash ../libzia/bin/zia-config --cflags2) -I$(readlink -f ../libzia/include) -I$(readlink -f ../libzia/include/unix)"
    LIBZIA_LIBS="-L$(readlink -f ../libzia/src/.libs) -Wl,-Bstatic -lzia -Wl,-Bdynamic $(bash ../libzia/bin/zia-config --libs2)"
    AC_MSG_RESULT([yes; static in ../libzia/src])
else
    echo "neexistuje ../libzia"

    if test x"$libzia_via_pkg" = x"1"; then
        tucnak_libzia_ver="$($PKG_CONFIG --modversion 'libzia') via pkg-config in $($PKG_CONFIG --variable=prefix 'libzia')"
        tucnak_libzia_settings="$(zia-config --prefix)/share/libzia/$target/settings"
        LIBZIA_CFLAGS=$($PKG_CONFIG --cflags 'libzia') 
        LIBZIA_LIBS=$($PKG_CONFIG --libs 'libzia') 
        AC_MSG_RESULT([yes  dynamic in $($PKG_CONFIG --variable=prefix 'libzia')])
    else
        AC_MSG_ERROR([no  libzia not found via pkg-config])
    fi    

#    tucnak_libzia_ver="$(zia-config --version) $(which zia-config)"
#    tucnak_libzia_settings="$(zia-config --prefix)/share/libzia/settings"
#    LIBZIA_CFLAGS="$(zia-config --cflags)"
#    LIBZIA_LIBS=$(zia-config --libs)
#    AC_MSG_RESULT([yes; dynamic in $(zia-config --prefix)])
fi

echo "LIBZIA_CFLAGS='$LIBZIA_CFLAGS'"
echo "LIBZIA_LIBS='$LIBZIA_LIBS'"


# check for glib and gthreads
tucnak_glib_ver=""
PKG_CHECK_MODULES(GLIB, 
            glib-2.0 > 2.0.0, 
            [ AC_DEFINE([HAVE_GLIB], [1], [glib])
              tucnak_glib="yes" 
              tucnak_glib_ver=$($PKG_CONFIG --modversion 'glib-2.0 > 2.0.0')],
            [ tucnak_mlibs="$tucnak_mlibs libglib2"
              tucnak_mdebs="$tucnak_mdebs libglib2.0-dev"
              tucnak_glib="no"])


if test x"$tucnak_glib" = x"yes"; then
    PKG_CHECK_MODULES(GTHREAD, 
                      gthread-2.0 > 2.0.0, 
                      AC_DEFINE([HAVE_GTHREAD2], [1], [gthread2]), 
                      AC_MSG_ERROR([glib is installed but gthread not ?]))
fi    



# AC_ARG_WITH([sdl], AS_HELP_STRING([--without-sdl], [Build without sdl library for graphics support (default: test)]))
# tucnak_sdl="disabled"
#
# if test "x$with_sdl" != "xno"; then
# # check for SDL
# SDL_VERSION=1.2.0
# tucnak_sdl_ver="";
# AM_PATH_SDL($SDL_VERSION,
#             [ AC_DEFINE([HAVE_SDL], [1], [libSDL])
#               tucnak_sdl="yes"
#               tucnak_sdl_ver=$(sdl-config --version)],
#             [ tucnak_olibs="$tucnak_olibs libsdl"
#               tucnak_odebs="$tucnak_odebs libsdl-dev" 
#               tucnak_sdl="no"]
#             )
# 
# 

#export PKG_CONFIG=/usr/bin/pkg-config


#AC_ARG_WITH([png], AS_HELP_STRING([--without-png], [Build without PNG support (default: test)]))
#
#if test "x$with_png" != "xno"; then
## check for libpng
#tucnak_libpng_ver=""
#PKG_CHECK_MODULES(LIBPNG, libpng > 1.2.0,
#            [ AC_DEFINE([HAVE_LIBPNG], [1], [libpng]) 
#              tucnak_libpng_ver=$(pkg-config --modversion 'libpng > 1.2.0') ], 
#            [ tucnak_olibs="$tucnak_olibs libpng"
#              tucnak_odebs="$tucnak_odebs libpng12-dev"])
#fi

# check for iconv
# if test x"$tucnak_sdl" = x"yes"; then
    AC_MSG_CHECKING(for iconv_open())
    save_LIBS="$LIBS"
    LIBS="$LIBS -liconv"
    ICONV_LIBS=""
    tucnak_iconv="no"
    AC_TRY_LINK(
    [   
#ifdef HAVE_ICONV_H
# include <iconv.h>
#endif
    ],
    [ 
        iconv_open("fr", "to");
    ],
    [   # ok
        #echo " __OK "
        AC_MSG_RESULT(yes; with -liconv) 
        AC_DEFINE([HAVE_ICONV],[1],[iconv with -liconv])
        ICONV_LIBS="-liconv"
        tucnak_iconv="libiconv"
    ],
    [   # failed
        #echo " __FAIL "
        LIBS="$save_LIBS"
        AC_TRY_LINK(
        [
#ifdef HAVE_ICONV_H
# include <iconv.h>
#endif
        ], 
        [   
            iconv_open("fr", "to");
        ],
        [   
            #echo " __OK "
            AC_MSG_RESULT(yes) 
            AC_DEFINE([HAVE_ICONV],[1],[iconv in libc])
            ICONV_LIBS=""
            tucnak_iconv="libc"
        ],
        [   # failed
            #echo " __FAIL "
            AC_MSG_RESULT(no)
            tucnak_mlibs="$tucnak_mlibs libiconv"
            tucnak_mdebs="$tucnak_mdebs libc6-dev"
            tucnak_iconv="no"
            LIBS="$save_LIBS"
        ])
#    ])   


#else
#   tucnak_iconv="not needed"
#fi    
    

AC_ARG_WITH([gpm], AS_HELP_STRING([--with-gpm], [Build without GPM support (default: without)]))
tucnak_libgpm="disabled"

if test "x$with_gpm" == "xyes"; then
# check for libgpm
tucnak_libgpm="no"
tucnak_old_LIBS=$LIBS
tucnak_libgpm_ver=""
AC_CHECK_LIB(gpm, Gpm_Open)
if ! test x"$LIBS" = x"$tucnak_old_LIBS"; then
    tucnak_libgpm="yes"
    
    AC_TRY_RUN([
    #include <gpm.h>
    #include <stdio.h>
    FILE *f;
    int main(void){
        int w;
        f=fopen("conftest.libgpm.out", "wt");
        if (!f) return -1;
        fprintf(f, "%s\n", Gpm_GetLibVersion(&w));
        fclose(f);
        return 0;
    }],
    [ tucnak_libgpm_ver=$(cat conftest.libgpm.out)], 
    [ tucnak_olibs="$tucnak_olibs libgpmg1"
      tucnak_odebs="$tucnak_odebs libgpmg1-dev" 
      tucnak_orpms="$tucnak_orpms gpm-devel"]) 
    rm -f conftest.libgpm.out
fi    
fi

AC_ARG_WITH([sndfile], AS_HELP_STRING([--without-sndfile], [Build without sndfile library (default: test)]))
tucnak_sndfile="disabled"
if test "x$with_sndfile" != "xno"; then
# check for libsndfile
libsndfile_LIBS=$LIBS
libsndfile_CFLAGS=$CFLAGS
PKG_CHECK_MODULES(SNDFILE, sndfile >= 1.0.2, 
            [ AC_DEFINE_UNQUOTED([HAVE_SNDFILE], [1], [Set to 1 if you have libsndfile.])
              AC_SUBST(SNDFILE_CFLAGS)
              AC_SUBST(SNDFILE_LIBS)
              tucnak_libsndfile_ver=$(pkg-config --modversion 'sndfile >= 1.0.2')
              tucnak_sndfile="yes"
              ],
            [ tucnak_olibs="$tucnak_mlibs libsndfile"
              tucnak_odebs="$tucnak_mdebs libsndfile-dev"
              tucnak_orpms="$tucnak_mrpms libsndfile-devel"
              tucnak_sndfile="no"])
fi


AC_ARG_WITH([alsa], AS_HELP_STRING([--without-alsa], [Build without alsa library for audio i/o (default: test)]))
tucnak_alsa="disabled"

if test "x$with_alsa" != "xno"; then
# check for alsa
tucnak_alsa_ver=""
AC_MSG_CHECKING(for alsa)
AC_EGREP_CPP([AP_maGiC_VALUE],
[
#include <alsa/asoundlib.h>
#if defined(SND_LIB_MAJOR) && defined(SND_LIB_MINOR)
#if SND_LIB_MAJOR>0 || (SND_LIB_MAJOR==0 && SND_LIB_MINOR>=6)
AP_maGiC_VALUE
#endif
#endif
],[
#    have_alsa_final=yes
    ALSA_LIB="-lasound"
    AC_MSG_RESULT([yes])
    AC_DEFINE([HAVE_ALSA],[1], [Alsa installed]) 
    tucnak_alsa="yes"
    
    AC_TRY_RUN([
    #include <alsa/asoundlib.h>
    #include <stdio.h>
    FILE *f;
    int main(void){
        f=fopen("conftest.alsa.out", "wt");
        if (!f) return -1;
        fprintf(f, "%s\n", SND_LIB_VERSION_STR);
        fclose(f);
        return 0;
    }
    ],[ tucnak_alsa_ver=$(cat conftest.alsa.out)], []) 
    rm -f conftest.alsa.out
],[
    AC_MSG_RESULT([no])
    tucnak_alsa="no"
    tucnak_olibs="$tucnak_olibs libasound"
    tucnak_odebs="$tucnak_odebs libasound-dev"
    tucnak_orpms="$tucnak_orpms alsa-devel"]
)              
fi

##
## check for libftdi
##
#AC_ARG_WITH([ftdi], AS_HELP_STRING([--without-ftdi], [Build without FTDI support for usb to serial converter (default: test)]))
#tucnak_ftdi="disabled"
#
#if test "x$with_ftdi" != "xno"; then
## check for libftdi
#tucnak_ftdi_ver=""
#PKG_CHECK_MODULES(LIBFTDI, libftdi >= 0.11, 
#            [ AC_DEFINE([HAVE_LIBFTDI], [1], [libftdi installed])
#              tucnak_ftdi_ver=$($PKG_CONFIG --modversion 'libftdi >= 0.7')
#              echo "libftdi version $tucnak_ftdi_ver"
#              tucnak_ftdi="yes" 
#              LIBS="$LIBS -lusb"
#              ], 
#            [ tucnak_ftdi="no" 
#              tucnak_olibs="$tucnak_olibs libftdi1"
#              tucnak_odebs="$tucnak_odebs libftdi-dev" ])
#
#fi

#AS_HELP_STRING([--with-hamlib=path], [Build with Hamlib from fiven path - hamlib configure prefix])
AC_ARG_WITH([hamlib], AS_HELP_STRING([--without-hamlib], [Build without Hamlib support (default: test)]))
echo withval=$withval
echo with_hamlib=$with_hamlib
tucnak_hamlib="disabled"

if test "x$with_hamlib" != "xno"; then
    if test "x$with_hamlib" == "x"; then
        # check for hamlib using pkgconfig
        echo 'check for hamlib using pkgconfig'
        tucnak_hamlib_ver=""
        PKG_CHECK_MODULES(HAMLIB, 
                hamlib, 
                [ AC_DEFINE([HAVE_HAMLIB], [1], [hamlib])
                  tucnak_hamlib="yes" 
                  tucnak_hamlib_ver=$($PKG_CONFIG --modversion 'hamlib') ], 
                [ tucnak_olibs="$tucnak_olibs libhamlib"
                  tucnak_odebs="$tucnak_odebs libhamlib-dev" 
                  tucnak_hamlib="no" ])
    else
        # use path from with_hamlib
        echo 'use path from with_hamlib'
        HAMLIB_CFLAGS="-I$with_hamlib/hamlib/include"
        HAMLIB_LIBS="-L$with_hamlib/lib -lhamlib"
        AC_CHECK_LIB(hamlib, rig_init, 
                [ AC_DEFINE([HAVE_HAMLIB], [1], [hamlib])
                  tucnak_hamlib="yes" 
                  tucnak_hamlib_ver="unknown from $with_hamlib" ], 
                [ tucnak_hamlib="no" ])
    fi
fi




# check for fftw3
AC_ARG_WITH([fftw3], AS_HELP_STRING([--without-fftw3], [Build without fftw3 library (default: test)]))
tucnak_fftw3="disabled"
if test "x$with_fftw3" != "xno"; then
    AC_CHECK_LIB(fftw3, fftw_execute, 
        [ AC_DEFINE([HAVE_LIBFFTW3], [1], [fftw3 installed])
          FFTW3_LIBS="-lfftw3" 
          tucnak_fftw3="yes" ],
        [ tucnak_olibs="$tucnak_olibs libfftw3"
          tucnak_odebs="$tucnak_odebs libfftw3-dev" 
          tucnak_orpms="$tucnak_orpms fftw3-devel" 
          tucnak_fftw3="no" ])
fi    


# check for portaudio
AC_ARG_WITH([portaudio], AS_HELP_STRING([--without-portaudio], [Build without portaudio library (default: test)]))
tucnak_portaudio="disabled"
if test "x$with_portaudio" != "xno"; then
    AC_CHECK_LIB(portaudio, Pa_WriteStream, 
        [ AC_DEFINE([HAVE_PORTAUDIO], [1], [portaudio installed])
          PORTAUDIO_LIBS="-lportaudio" 
          tucnak_portaudio="yes" ],
        [ tucnak_olibs="$tucnak_olibs libportaudio"
          tucnak_odebs="$tucnak_odebs portaudio19-dev" 
          tucnak_orpms="$tucnak_orpms portaudio-devel" 
          tucnak_portaudio="no" ])
fi    

tucnak_librtlsdr_ver=""
PKG_CHECK_MODULES(LIBRTLSDR, 
        librtlsdr, 
        [ AC_DEFINE([HAVE_LIBRTLSDR], [1], [librtlsdr])
          tucnak_librtlsdr="yes" 
          tucnak_librtlsdr_ver=$($PKG_CONFIG --modversion 'librtlsdr') ], 
        [ tucnak_olibs="$tucnak_olibs librtlsdr"
          tucnak_odebs="$tucnak_odebs librtlsdr-dev" 
          tucnak_librtlsdr="no" ])


# check for ddir
tucnak_ddir="/usr/lib/tucnak"
if test -n "$DEB_HOST_MULTIARCH"; then
    tucnak_ddir="/usr/lib/$DEB_HOST_MULTIARCH/tucnak"
fi    
AC_DEFINE_UNQUOTED([DDIR], ["$tucnak_ddir"], [Directory for tucnak.d])



# check for library errors
if test -n "$tucnak_mlibs$tucnak_olibs"; then
    echo ""
    echo ""
    echo "********"
    if test -n "$tucnak_mlibs"; then
        echo "Missing mandatory libraries: $tucnak_mlibs"
    fi
    if test -n "$tucnak_olibs"; then
        echo "Missing optional libraries:  $tucnak_olibs"
    fi
    if test -f "/etc/debian_version"; then
        echo "Try to run: apt-get install $tucnak_mdebs $tucnak_odebs"
    fi
    if test -f "/etc/SuSE-release"; then
        echo "Try to run: yast -i $tucnak_mrpms $tucnak_orpms"
    fi
    echo "********"
    if test -n "$tucnak_mlibs"; then
        AC_MSG_ERROR([missing mandatory library/libraries])
    fi
    
fi
# removed $GLIB_CFLAGS, $GLIB_LIBS   
CFLAGS="$CFLAGS $GTHREAD_CFLAGS $SNDFILE_CFLAGS $HAMLIB_CFLAGS $LIBRTLSDR_CFLAGS"
LIBS="$LIBS $GTHREAD_LIBS $ICONV_LIBS $SNDFILE_LIBS $ALSA_LIB $HAMLIB_LIBS $FFTW3_LIBS $PORTAUDIO_LIBS $LIBRTLSDR_LIBS"

AC_C_BIGENDIAN([tucnak_endian="big"],[tucnak_endian="little"],[tucnak_endian="unknown"])
AC_MSG_CHECKING([for long long])
AC_TRY_COMPILE(, [unsigned long long a;], have_long_long=yes, have_long_long=no)
AC_MSG_RESULT($have_long_long)
test "$have_long_long" = yes && AC_DEFINE([HAVE_LONG_LONG], [1], [long long])

AC_MSG_CHECKING([for uint32_t])
AC_TRY_COMPILE([                                                                                                        
#include <sys/types.h>                                                                                                  
#ifdef HAVE_STDINT_H                                                                                                    
#include <stdint.h>                                                                                                     
#endif                                                                                                                  
#ifdef HAVE_INTTYPES_H                                                                                                  
#include <inttypes.h>                                                                                                   
#endif                                                                                                                  
], [uint32_t a;], have_uint32_t=yes, have_uint32_t=no)                                                                  
AC_MSG_RESULT($have_uint32_t)                                                                                           
test "$have_uint32_t" = yes && AC_DEFINE([HAVE_UINT32_T], [1], [uinf32_t])                                                                 
              
AC_MSG_CHECKING([for typeof])
AC_TRY_COMPILE(, [int a;
typeof(a) b;], have_typeof=yes, have_typeof=no)
AC_MSG_RESULT($have_typeof)
test "$have_typeof" = yes && AC_DEFINE([HAVE_TYPEOF], [1], [have operator typeof])

AC_CHECK_SIZEOF(char)                                                                                                
AC_CHECK_SIZEOF(short)                                                                                               
AC_CHECK_SIZEOF(int)                                                                                                 
AC_CHECK_SIZEOF(long)                                                                                                
test "$have_long_long" = yes && AC_CHECK_SIZEOF(long long, 8)      
AC_CHECK_SIZEOF(void *)

AC_CHECK_FUNCS(gmtime_r, AC_DEFINE([HAVE_GMTIME_R], [1], [have function gmtime_r]))

AC_CHECK_FUNCS([rig_send_voice_mem rig_send_morse rig_stop_morse])




#check for F_TLOCK in fnctl.h
AC_CACHE_CHECK([for F_TLOCK in sys/file.h],tucnak_cv_HAVE_F_TLOCK, [
    AC_TRY_COMPILE([#include <sys/file.h>],[int a = F_TLOCK],
        tucnak_cv_HAVE_F_TLOCK=yes,tucnak_cv_HAVE_F_TLOCK=no)])
if test x"$tucnak_cv_HAVE_F_TLOCK" = x"yes"; then
    AC_DEFINE([HAVE_F_TLOCK], [1], [T_LOCK])
fi


#check for swradio
#AC_CACHE_CHECK([for V4L2_BUF_TYPE_SDR_CAPTURE],tucnak_cv_HAVE_SWRADIO, [
#    AC_TRY_COMPILE([#include <linux/videodev2.h>],[int a = V4L2_BUF_TYPE_SDR_CAPTURE],
#        tucnak_cv_HAVE_SWRADIO=yes,tucnak_cv_HAVE_SWRADIO=no)])
#if test x"$tucnak_cv_HAVE_SWRADIO" = x"yes"; then
#    AC_DEFINE([HAVE_SWRADIO], [1], [SWRADIO])
#fi


AC_CHECK_PROG(HAVE_RPM, rpm, "yes", "no")

HAVE_RPM_PACKAGING=no
if test x$HAVE_RPM = xyes ; then
  AC_MSG_CHECKING(for rpm build directories)
  RPM_SOURCESDIR=$(rpm --showrc | grep "^sourcedir" | \
    sed -e 's/.*: //' 2> /dev/null)
  if test x$RPM_SPECDIR = x ; then
    # Red Hat  : /usr/src/redhat
    # Mandrake : /usr/src/RPM
    # SuSE     : /usr/src/packages
    # Debian   : /usr/src/rpm
    for TEST_DIR in /usr/src/redhat /usr/src/RPM /usr/src/packages /usr/src/rpm ; do
      if test -d $TEST_DIR/SPECS -a -d $TEST_DIR/SOURCES -a \
              -d $TEST_DIR/RPMS -a -d $TEST_DIR/SRPMS -a \
              -d $TEST_DIR/BUILD ; then
        RPM_SOURCESDIR=$TEST_DIR/SOURCES
      fi
    done
  fi
  if test x$RPM_SOURCESDIR = x ; then
    AC_MSG_RESULT(not found)
    RPM_SOURCESDIR=""
  else
    AC_MSG_RESULT($RPM_SOURCESDIR)
    HAVE_RPM_PACKAGING=yes
  fi

  if test -f /etc/mandrake-release ; then 
      RPM_RELEASE="mdk"
  else    
      RPM_RELEASE=""
  fi      
  
fi
AC_SUBST(HAVE_RPM_PACKAGING)
AC_SUBST(RPM_SOURCESDIR)
AC_SUBST(RPM_RELEASE)




#dnl check for ioctl(TIOCLINUX)
AC_CACHE_CHECK([for TIOCLINUX],tucnak_cv_HAVE_TIOCLINUX,[
AC_TRY_RUN([
#define AUTOCONF_TEST 1
#include <sys/ioctl.h>
#include "confdefs.h"
int cmp(const struct dirent **a, const struct dirent **b){return 0;}
int main(){
    char c[2];
    c[0]=c[1]=0;
    exit(0);
    ioctl(1,TIOCLINUX,c);
}],
tucnak_cv_HAVE_TIOCLINUX=yes,tucnak_cv_HAVE_TIOCLINUX=no,tucnak_cv_HAVE_TIOCLINUX=cross)])
if test x"$tucnak_cv_HAVE_TIOCLINUX" = x"yes"; then
    AC_DEFINE([HAVE_TIOCLINUX], [], [ioctl TIOCLINUX])
fi



#dnl Check for socklen_t
AC_CACHE_CHECK([for SOCKLEN_T],tucnak_cv_HAVE_SOCKLEN_T,[
AC_TRY_RUN([
#define AUTOCONF_TEST 1
#include <sys/socket.h>
#include "confdefs.h"
int main(){
    socklen_t tmp;
    exit(0);
}],
tucnak_cv_HAVE_SOCKLEN_T=yes,tucnak_cv_HAVE_SOCKLEN_T=no,tucnak_cv_HAVE_SOCKLEN_T=cross)])

if test x"$tucnak_cv_HAVE_SOCKLEN_T" = x"yes"; then
    AC_DEFINE([HAVE_SOCKLEN_T], [], [socklen_t])
fi


    

# --enable-pedantic
AC_ARG_ENABLE(pedantic, [  --enable-pedantic       Enable pedantic gcc checking(-Wall etc.)], enable_pedantic=yes,enable_pedantic=no )
dnl echo "pedantic=$enable_pedantic"
if test x"$enable_pedantic" = x"yes"; then
    #CFLAGS="$CFLAGS -Wall -Wpedantic"
    CFLAGS="$CFLAGS -Wall -Werror -Wpointer-arith -Wstrict-prototypes -Wunused -Wunused-function -Wunused-label -Wunused-value -Wunused-variable -Wformat -Wno-format-extra-args -Wformat-security -Wdeprecated-declarations " #-Wstringop-truncation -Wformat-overflow=2
fi

# --enable-leak-debug
AC_ARG_ENABLE(leak-debug, [  --enable-leak-debug     Enable memory leak debug], enable_leak_debug=yes,enable_leak_debug=no )
if test x"$enable_leak_debug" = x"yes"; then
    AC_DEFINE_UNQUOTED([LEAK_DEBUG], [], [Memory leak debug])
    AC_DEFINE_UNQUOTED([LEAK_DEBUG_LIST], [], [Memory leak debug list])

fi

# --enable-instr
AC_ARG_ENABLE(instr, [  --enable-instr          Enable instrument call trace], 
    [ CFLAGS="$CFLAGS -finstrument-functions"
      enable_instr=yes ],
    [ enable_instr=no])


dnl ebuild for Gentoo
dnl rm -f "pkg/$PACKAGE_NAME-*.ebuild"
dnl cp "pkg/$PACKAGE_NAME.ebuild" "pkg/$PACKAGE_NAME-$PACKAGE_VERSION.ebuild"

if test x"$tucnak_ld_wlmap" = x"yes"; then
    LIBS="$LIBS $tucnak_ld_flags"
fi    

CFLAGS="$CFLAGS $LIBZIA_CFLAGS"
echo "LIBZIA_LIBS='$LIBZIA_LIBS'"
echo "LIBS='$LIBS'"
LIBS="$LIBZIA_LIBS $LIBS"
echo "LIBS='$LIBS'"

CFLAGS=$(echo $CFLAGS | sed 's/ \+/ /g')
LDFLAGS=$(echo $LDFLAGS | sed 's/ \+/ /g')
LIBS=$(echo $LIBS | sed 's/ \+/ /g')

TUCNAK_LDADD=$LIBS
LIBS=""
AC_SUBST(TUCNAK_LDADD)
TUCNAK_CFLAGS=$CFLAGS

sharedir="$prefix/share/$PACKAGE_NAME"
AC_DEFINE_UNQUOTED([SHAREDIR], ["$sharedir"], [Shared directory])

VERSIONCODE=$(echo $VERSION | sed 's/\.//')
AC_SUBST(VERSIONCODE)

AC_OUTPUT(Makefile android/AndroidManifest.xml android/Makefile android/nagup android/src/Makefile android/tucnak-package data/Makefile doc/Makefile intl/Makefile pkg/tucnak.spec pkg/Makefile share/Makefile share/applications/Makefile share/pixmaps/Makefile src/Makefile src/msvcver.h srcup win32/Makefile win32/nagup.bat win32/tucnak-cygwin.nsi win32/tucnak-msvc.nsi)

chmod +x srcup svnver.sh android/tucnak-package

echo ""
(echo "------ Tucnak settings: --------";
echo "   version: $PACKAGE_NAME-$PACKAGE_VERSION"; 
echo "    prefix: $prefix"; 
echo "       pkg: $pkg";
echo "    CFLAGS: $CFLAGS";
echo "   LDFLAGS: $LDFLAGS";
echo "      LIBS: $TUCNAK_LDADD";
echo "    libzia: yes $tucnak_libzia_ver";
echo "     iconv: $tucnak_iconv";
echo "    libgpm: $tucnak_libgpm $tucnak_libgpm_ver";
echo "libsndfile: $tucnak_sndfile $tucnak_libsndfile_ver";
echo "      alsa: $tucnak_alsa $tucnak_alsa_ver";
echo " libhamlib: $tucnak_hamlib $tucnak_hamlib_ver";
echo "  libfftw3: $tucnak_fftw3";
echo " portaudio: $tucnak_portaudio";
echo " librtlsdr: $tucnak_librtlsdr $tucnak_librtlsdr_ver";
echo "     ppdev: $ac_cv_header_linux_ppdev_h";
echo "      ddir: $tucnak_ddir";
echo "  pedantic: $enable_pedantic";
echo "      opts: $opt") | tee settings
echo ""

(echo "char *txt_settings[]=";
cat settings | sed 's/\(.*\)/"\1\\n"/';
echo ";") > src/settings.c
