/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2022 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __SSBD_H
#define __SSBD_H

#include "header.h"
#ifdef VOIP
#include "voip.h"
#endif

struct qso;

#define CE_NONE             0
#define CE_ONLY_STRFTIME    1

//#define SSBBUFFER_LEN2 512 //4096



struct ssbd {
    int recording;
    gchar *pfilename, *rfilename;
#ifdef HAVE_SNDFILE    
    GThread *thread;
    int proc_break;
    int channels;			// number of sound channels. Modified only in main thread, readed in worker threads
    
    FILE *f;
    gchar *callsign;
    gint serno;
    short *buffer;
    SNDFILE *sndfile;
    int norecshowed;
    int midlevel, maxlevel, cntlevel, loglevel, oldloglevel;
    MUTEX_DEFINE(loglevel); 
    time_t recstop;
#ifdef Z_HAVE_SDL    
    SDL_Surface *norecicon, *playicon, *recicon;
#else
    void *norecicon, *playicon, *recicon;
#endif    
    int code;
    int swontoptype;
    int playedf;			// frames
    int seek;
    MUTEX_DEFINE(seek);
    char *pl_pcmfile;
#ifdef Z_HAVE_SDL
	short *scope_buf;		// buffer for scope draw, set in main thread
	int scope_samples;		// length of buffer in samples
	int scope_i;			// position when filling in samples
	int scope_w;			// scope width in frames
	int scope_show;			// 0 = filling buffer in thread, 1 = displaying in main thread
#endif
#ifdef VOIP
	struct voip *voip;
#endif
	struct dsp *dsp;
#endif
};


extern struct ssbd *gssbd;

struct ssbd *init_ssbd(void);
void free_ssbd(struct ssbd *ssbd, int terminate);
void ssbd_abort(struct ssbd *ssbd, int abort_rec);
int ssbd_recording(struct ssbd *ssbd);

#ifdef HAVE_SNDFILE

int ssbd_play_file(struct ssbd *ssbd, gchar *filename);
int ssbd_rec_file(struct ssbd *ssbd);
int ssbd_callsign(struct ssbd *ssbd, char *call);
void ssbd_play_read_handler(struct ssbd *ssbd, gchar *str);
void ssbd_rec_read_handler(struct ssbd *ssbd, gchar *str);

gpointer ssbd_play_thread_func(gpointer data);
gpointer ssbd_rec_thread_func(gpointer data);
void ssbd_thread_create(struct ssbd *ssbd, GThreadFunc thread_func);
void ssbd_thread_join(struct ssbd *ssbd);
void ssbd_watchdog(struct ssbd *ssbd, int start_rec);
void ssbd_play_last_sample(struct ssbd *ssbd, struct qso *qso);

#define SSBDRECORDING (gssbd->recording)
#else
#define SSBDRECORDING 0
#endif


#endif
