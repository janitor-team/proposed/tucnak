/*
    Tucnak - SOTA contest log
    Copyright (C) 2002-2020  Ladislav Vaiz <ok1zia@nagano.cz>;
    2020 Michal OK2MUF

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __SOTA_H
#define __SOTA_H

char* get_sota_mode_ps(int mode);
int export_all_bands_sota(void);
gchar* get_sota_log_filename(void);

#endif
