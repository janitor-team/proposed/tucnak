/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2021  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/


#include "header.h"

#include "bfu.h"
#include "control.h"
#include "cwdb.h"
#include "ebw.h"
#include "edi.h"
#include "excdb.h"
#include "fifo.h"
#include "hf.h"
#include "inputln.h"
#include "kbd.h"
#include "main.h"
#include "menu.h"
#include "namedb.h"
#include "net.h"
#include "qrvdb.h"
#include "qsodb.h"
#include "session.h"
#include "stats.h"
#include "subwin.h"
#include "terminal.h"
#include "tregex.h"
#include "trig.h"
#include "zosk.h"

/*********************** DUPLICATE CALLSIGN *********************************/

void duplicate_callsign_fn(struct dialog_data *dlg)
{
    struct terminal *term = dlg->win->term;
    int max = 0, min = 0;
    int w, rw;
    int y = -1;

    max_buttons_width(term, dlg->items +0, 1, &max);
    min_buttons_width(term, dlg->items +0, 1, &min);
    max_buttons_width(term, dlg->items +1, 1, &max);
    min_buttons_width(term, dlg->items +1, 1, &min);
    max_buttons_width(term, dlg->items +2, 1, &max);
    min_buttons_width(term, dlg->items +2, 1, &min);
    max_buttons_width(term, dlg->items +3, 1, &max);
    min_buttons_width(term, dlg->items +3, 1, &min);
    
    w = dlg->win->term->x * 9 / 10 - 2 * DIALOG_LB;
    if (w > max) w = max;
    if (w < min) w = min;
    if (w > dlg->win->term->x - 2 * DIALOG_LB) w = dlg->win->term->x - 2 * DIALOG_LB;
    if (w < 1) w = 1;
    
    rw = 0;
    y++;
    dlg_format_buttons(NULL, term, dlg->items +0, 1, 0, &y, w, &rw, AL_CENTER);
    dlg_format_buttons(NULL, term, dlg->items +1, 1, 0, &y, w, &rw, AL_CENTER);
    dlg_format_buttons(NULL, term, dlg->items +2, 1, 0, &y, w, &rw, AL_CENTER);
    dlg_format_buttons(NULL, term, dlg->items +3, 1, 0, &y, w, &rw, AL_CENTER);
    y++;
    
    w = rw;
    dlg->xw = w + 2 * DIALOG_LB;
    dlg->yw = y + 2 * DIALOG_TB;

    
    center_dlg(dlg);
    draw_dlg(dlg);
    y = dlg->y + DIALOG_TB;
    y++;
    dlg_format_buttons(term, term, dlg->items +0, 1, dlg->x + DIALOG_LB, &y, w, NULL, AL_CENTER);
    dlg_format_buttons(term, term, dlg->items +1, 1, dlg->x + DIALOG_LB, &y, w, NULL, AL_CENTER);
    dlg_format_buttons(term, term, dlg->items +2, 1, dlg->x + DIALOG_LB, &y, w, NULL, AL_CENTER);
    dlg_format_buttons(term, term, dlg->items +3, 1, dlg->x + DIALOG_LB, &y, w, NULL, AL_CENTER);
    y++;
    
    
    /*  dlg_format_text(term, term, contest_options1_msg[3], dlg->x + DIALOG_LB, &y, w, NULL, COLOR_DIALOG_TEXT, AL_LEFT);*/
 /*   dlg_format_field(term, term, &dlg->items[3], dlg->x + DIALOG_LB, &y, w, NULL, AL_LEFT);*/
 /*   y++;*/
/*  dlg_format_text(term, term, contest_options1_msg[4], dlg->x + DIALOG_LB, &y, w, NULL, COLOR_DIALOG_TEXT, AL_LEFT);
    dlg_format_field(term, term, &dlg->items[4], dlg->x + DIALOG_LB, &y, w, NULL, AL_LEFT);
    dlg_format_text(term, term, contest_options1_msg[4], dlg->x + DIALOG_LB, &y, w, NULL, COLOR_DIALOG_TEXT, AL_LEFT);
    dlg_format_field(term, term, &dlg->items[4], dlg->x + DIALOG_LB, &y, w, NULL, AL_LEFT);
    y++;*/
}

int dlg_clear_tmpqsos(struct dialog_data *dlg, struct dialog_item_data *di){

    clear_tmpqsos(aband,1);
    return cancel_dialog(dlg,di);
}

/* todo prototyp */
int dlg_edit_qso(struct dialog_data *dlg, struct dialog_item_data *di){
    edit_qso((struct qso *)dlg->dlg->refresh_data);

    clear_tmpqsos(aband, 1);
    return cancel_dialog(dlg,di);
}

int dlg_mark_as_error(struct dialog_data *dlg, struct dialog_item_data *di){
    struct qso *qso;             
    
    qso = (struct qso*) dlg->dlg->refresh_data;
    qso_mark_as_error(aband, atoi(qso->qsonrs)-1);
    recalc_stats(aband);
    
	aband->dupe_qso = NULL;
    ADD_TMPQSO_STRING(aband, callsign, qso->callsign, 1, ucallsign);
    wkd_tmpqso(aband, WT_CALLSIGN, qso->callsign);

    return cancel_dialog(dlg,di);
}

int dlg_store_as_dupe(struct dialog_data *dlg, struct dialog_item_data *di){
    struct qso *qso;             
   
	aband->dupe_qso = NULL;
    
    qso = (struct qso*) dlg->dlg->refresh_data;

    aband->tmpqsos[0].dupe = 1; 
    ADD_TMPQSO_STRING(aband, callsign, qso->callsign, 1, ucallsign);
    TMPQ.ucallsign=1;
    if (!TMPQ.ulocator) add_tmpqso_locator(aband, qso->locator, 0, 0); 
     
    wkd_tmpqso(aband, WT_CALLSIGN, qso->callsign);
    wkd_tmpqso(aband, WT_LOCATOR, qso->locator);
    
    return cancel_dialog(dlg,di);
}


void duplicate_callsign(struct qso *qso)
{
    struct dialog *d;
    int i;

#ifdef Z_HAVE_SDL
	if (gses->osk){
		zosk_free(gses->osk);
		gses->osk = NULL;
		sdl_force_redraw();
	}
#endif

    if (!(d = g_malloc(sizeof(struct dialog) + 55 * sizeof(struct dialog_item)))) return;
    memset(d, 0, sizeof(struct dialog) + 55 * sizeof(struct dialog_item));
    d->title = VTEXT(T_DUPE_QSO);
    d->fn = duplicate_callsign_fn;
    d->refresh = (void (*)(void *))dlg_clear_tmpqsos;
    d->refresh_data = (void *)qso;
    
    d->items[i=0].type = D_BUTTON;
    d->items[i].gid  = B_ENTER;
    d->items[i].fn   = dlg_clear_tmpqsos; 
    d->items[i].text = VTEXT(T_CLEAR_THIS_QSO);
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid  = B_ENTER;
    d->items[i].fn   = dlg_edit_qso; 
    d->items[i].text = VTEXT(T_EDIT_OLD_QSO);
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid  = B_ENTER;
    d->items[i].fn   = dlg_mark_as_error; 
    d->items[i].text = VTEXT(T_MARK_OLD_QSO);
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid  = B_ENTER;
    d->items[i].fn   = dlg_store_as_dupe; 
    d->items[i].text = VTEXT(T_STORE_AS_DUPE);
    
    d->items[++i].type = D_END;
    do_dialog(d, getml(d, NULL));
}
                               


/**************************** EDIT ***********************************/

void menu_add_error(void *arg){
    add_error(aband, "");
} 

/*********************** CHOP - Change operator *************************/

static char op[EQSO_LEN];
static int global_operator;

char *chop_msg[] = {
    CTEXT(T_CALLSIGN), /* 0 */
    CTEXT(T_GLOBAL_OPERATOR),
    "", /* OK */  /* 9 */
    "", /* Cancel */
};

void chop(struct band *band, char *op){
    gchar *c;
    
    g_free(band->operator_);
    band->operator_ = fixsemi(g_strdup(op));
    c = g_strconcat("O ", op, NULL);
    add_swap(band, c);
    g_free(c);  
    wkd_tmpqso(band, WT_OPERATOR, op);
}

void refresh_chop(void *xxx){
    int i;

    z_str_uc(op);
    /*if (!regcmp(op,"^([0-9][A-Z]|[A-Z]{1,2}[0-9]?)[0-9]{1,4}[A-Z]{1,4}$")==0){
        errbox(VTEXT(T_BAD_CALL), 0);
        return;
    } */
    cfg->global_operator = global_operator;
    
    if (global_operator){
        for (i=0; i<ctest->bands->len; i++){
            struct band *b;

            b=(struct band *)g_ptr_array_index(ctest->bands, i);
            chop(b, op);
        }
    }else{
        chop(aband, op);
    }
    
    redraw_later();
    
}

void chop_fn(struct dialog_data *dlg)
{
    struct terminal *term = dlg->win->term;
    int max = 0, min = 0;
    int w, rw;
    int y = -1;

    max_group_width(term, chop_msg + 0, dlg->items + 0, 1, &max);
    min_group_width(term, chop_msg + 0, dlg->items + 0, 1, &min);
    max_group_width(term, chop_msg + 1, dlg->items + 1, 1, &max);
    min_group_width(term, chop_msg + 1, dlg->items + 1, 1, &min);
    max_buttons_width(term, dlg->items + 2, 2, &max);
    min_buttons_width(term, dlg->items + 2, 2, &min);
    
    w = dlg->win->term->x * 9 / 10 - 2 * DIALOG_LB;
    if (w > max) w = max;
    if (w < min) w = min;
    if (w > dlg->win->term->x - 2 * DIALOG_LB - 8 ) w = dlg->win->term->x - 2 * DIALOG_LB - 8;
    if (w < 1) w = 1;
    
    rw = 0;
    y ++;
    dlg_format_group(NULL, term, chop_msg + 0, dlg->items + 0, 1, 0, &y, w, &rw);
    y++;
    dlg_format_group(NULL, term, chop_msg + 1, dlg->items + 1, 1, 0, &y, w, &rw);
    y++;
    dlg_format_buttons(NULL, term, dlg->items + 2, 2, 0, &y, w, &rw, AL_LEFT);
    
    
    w = rw;
    dlg->xw = w + 2 * DIALOG_LB;
    dlg->yw = y + 2 * DIALOG_TB;

    
    center_dlg(dlg);
    draw_dlg(dlg);
    y = dlg->y + DIALOG_TB;
    y++;
    dlg_format_group(term, term, chop_msg + 0, dlg->items + 0, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    y++;
    dlg_format_group(term, term, chop_msg + 1, dlg->items + 1, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    y++;
    dlg_format_buttons(term, term, dlg->items + 2, 2, dlg->x + DIALOG_LB, &y, w, NULL, AL_LEFT);
    
}


void menu_chop(void *arg){
    struct dialog *d;
    int i;
    
    if (!ctest || !aband) return;
    
    fixsemi(aband->operator_);
    safe_strncpy0(op, aband->operator_, EQSO_LEN);
    global_operator=cfg->global_operator;

    
    if (!(d = (struct dialog *)g_malloc(sizeof(struct dialog) + 10 * sizeof(struct dialog_item)))) return;
    memset(d, 0, sizeof(struct dialog) + 10 * sizeof(struct dialog_item));
    d->title = VTEXT(T_CHOP);
    d->fn = chop_fn;
    d->refresh = (void (*)(void *))refresh_chop;
    
    d->items[i=0].type = D_FIELD;
    d->items[i].dlen = EQSO_LEN;
    d->items[i].data = op;
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&global_operator;
    

    d->items[++i].type = D_BUTTON;
    d->items[i].gid = B_ENTER;
    d->items[i].fn = ok_dialog;
    d->items[i].text = VTEXT(T_OK);
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid = B_ESC;
    d->items[i].fn = cancel_dialog;
    d->items[i].text = VTEXT(T_CANCEL);
    
    d->items[++i].type = D_END;
    do_dialog(d, getml(d, NULL));


}
/*********************** SKEDQRG - Change QRG for sked *************************/

void do_skedqrg(struct session *ses, char *qrg){
    gchar *c;

    if (!qrg) return;

    g_free(aband->skedqrg);
    aband->skedqrg = fixsemi(g_strdup(qrg));
    c = g_strconcat("G ", qrg, NULL);
    add_swap(aband, c);
    g_free(c);  
    c = g_strdup_printf("RG %s;%s\n",aband->pband,qrg);
    rel_write_all(c);
    g_free(c);  
    redraw_later();
}

void menu_skedqrg(void *arg){
    static char qrg[20];
    
    if (!ctest || !aband) return;
    
    fixsemi(aband->skedqrg);
    safe_strncpy0(qrg, aband->skedqrg, 20);

    input_field(NULL, VTEXT(T_SKED_QRG), VTEXT(T_N_SKED_QRG), 
               VTEXT(T_OK), VTEXT(T_CANCEL), NULL, 
               NULL, 20, qrg, 0, 0, NULL,
               (void (*)(void *, char *)) do_skedqrg, NULL, 0);

}

/******************** FILL OPERATORS **************************/
// arg==0 -> fill only when empty, don't save (from edi export)
// arg==1 -> overwrite ops, save (from menu)

void menu_fillop(void *arg){
    struct band *b;
    struct qso *q;
    int i,j,k;
    gchar *c;
    GString *gs,*gs1,*gs2;
    GPtrArray *calls;

    if (!ctest) return;
    for (i=0; i<ctest->bands->len; i++){
        b = (struct band *)g_ptr_array_index(ctest->bands,i);

		if (GPOINTER_TO_INT(arg) == 0 && strlen(b->mope1) != 0) continue;
		
        calls=g_ptr_array_new();
        for (j=0;j<b->qsos->len;j++){
            q=(struct qso *)g_ptr_array_index(b->qsos,j);

            for (k=0;k<calls->len;k++){
                if (strcmp(q->operator_,(char*)g_ptr_array_index(calls,k))==0) break;
            }
            if (k==calls->len)
                g_ptr_array_add(calls, q->operator_);
            
        }

        zg_ptr_array_qsort(calls, z_compare_string);
        gs1=g_string_sized_new(100);
        gs2=g_string_sized_new(100);
        gs=gs1;
        for (k=0; k<calls->len;k++){
            c=(char*)g_ptr_array_index(calls, k);
            if (gs->len+2+strlen(c) >= 50) gs=gs2;
            g_string_append(gs, ";");
            g_string_append(gs, c);
        }
        g_ptr_array_free(calls,TRUE); /* don't free items! */
        
        if (gs1->len) g_string_erase(gs1,0,1);
        if (gs2->len) g_string_erase(gs2,0,1);
        
        g_free(b->mope1);
        b->mope1=g_strdup(gs1->str);
        g_string_free(gs1,TRUE);
        
        g_free(b->mope2);
        b->mope2=g_strdup(gs2->str);
        g_string_free(gs2,TRUE);
    }
	if (GPOINTER_TO_INT(arg) != 0){
		save_all_bands_txt(0);
	}
}

/******************** RECALC ALL QRB and QTF  **************************/

void menu_recalc_qrb(void *arg){
	if (ctest) recalc_all_qrbqtf(ctest);
    qrv_recalc_wkd(qrv);
    qrv_recalc_qrbqtf(qrv);
    qrv_recalc_gst(qrv);
}

/******************** UPDATE C_W ******************************/

void menu_cw_update_contest(void *arg){
    gchar *s;
    int ret;
    
    update_cw_from_ctest(cw, ctest);
    s = g_strconcat(tucnak_dir, "/tucnakcw", NULL);
	z_wokna(s);
    ret=save_cw_into_file(cw, s);        
    if (ret){
        errbox(VTEXT(T_CANT_WRITE), ret);
    }else{
        log_addf(VTEXT(T_SAVED_S), s);
    }
    g_free(s);
}

void menu_cw_update_band(void *arg){
    gchar *s;
    int ret;
    
    update_cw_from_band(cw, aband);
    s = g_strconcat(tucnak_dir, "/tucnakcw", NULL);
	z_wokna(s);
    ret=save_cw_into_file(cw, s);        
    if (ret){
        errbox(VTEXT(T_CANT_WRITE), ret);
    }else{
        log_addf(VTEXT(T_SAVED_S), s);
    }
    g_free(s);
}

/******************** UPDATE EXC ******************************/

void menu_exc_update_contest(void *arg){
    gchar *s, *ename;
    int ret;
    
	
	if (!ctest) return;
    dbg("menu_exc_update_contest(%s)\n", ctest->excname);
    if (!ctest->excname || !*ctest->excname) return;

    ename = g_strdup(ctest->excname);
    z_str_lc(ename);
    
    update_exc_from_ctest(excdb, ctest);
    s = g_strconcat(tucnak_dir, "/tucnakexc", ename, NULL);
	z_wokna(s);
    ret=save_exc_into_file(excdb, s);        
    if (ret){
        errbox(VTEXT(T_CANT_WRITE), ret);
    }else{
        log_addf(VTEXT(T_SAVED_S), s);
    }
    g_free(s);
}

void menu_exc_update_band(void *arg){
    gchar *s, *ename;
    int ret;
    
	if (!ctest) return;
    dbg("menu_exc_update_band(%s)\n", ctest->excname);
    if (!ctest->excname || !*ctest->excname) return;

    ename = g_strdup(ctest->excname);
    z_str_lc(ename);
    update_exc_from_band(excdb, aband);
    s = g_strconcat(tucnak_dir, "/tucnakexc", ename, NULL);
	z_wokna(s);
    ret=save_exc_into_file(excdb, s);        
    if (ret){
        errbox(VTEXT(T_CANT_WRITE), ret);
    }else{
        log_addf(VTEXT(T_SAVED_S), s);
    }
    g_free(s);
}

void menu_import_ebw(void *arg){
    int ret;
    
    ret=read_ebw_files(cw, namedb);
	if (ret!=0) return;
    
	save_cw(cw, 1);
    save_namedb(namedb, 1);
}

/******************** AUTOSAVE *******************************/

#define AS_LEN 5

/*char as_disk_aq_str[AS_LEN], as_disk_am_str[AS_LEN];
gint as_disk_fsync;*/
char as_floppy_aq_str[AS_LEN], as_floppy_am_str[AS_LEN];
gint as_floppy_fsync;
gint as_mount;
char as_floppy_path[MAX_STR_LEN];
char as_mount_cmd[MAX_STR_LEN];

                                                   
void refresh_autosave(void *xxx)
{
/*    STORE_SINT(cfg,as_disk_aq);
    STORE_SINT(cfg,as_disk_am);
    STORE_INT (cfg,as_disk_fsync); */
    STORE_SINT(cfg,as_floppy_aq);
    STORE_SINT(cfg,as_floppy_am);
    STORE_INT (cfg,as_floppy_fsync);
    STORE_INT (cfg,as_mount);
    STORE_STR(cfg,as_floppy_path);
    STORE_STR(cfg,as_mount_cmd);

    dbg("refresh_autosave\n");
}

char *autosave_msg[] = {
#if 0
    CTEXT(T_SAVE_AQ), /* 0 */
    CTEXT(T_SAVE_AM),
    CTEXT(T_FSYNC),
#endif    
    CTEXT(T_SAVE_AQF), /* 0 */
    CTEXT(T_SAVE_AMF),
    CTEXT(T_FSYNC),                 
    
    CTEXT(T_DO_MOUNT),
    CTEXT(T_FLOPPY_PATH),     /* 5 */
    CTEXT(T_MOUNT_CMD), 
    "", /* OK */  /* 9 */
    "", /* Cancel */
};

void autosave_fn(struct dialog_data *dlg)
{
    struct terminal *term = dlg->win->term;
    int max = 0, min = 0;
    int w, rw;
    int y = -1;

    max_group_width(term, autosave_msg + 0, dlg->items + 0, 1, &max);
    min_group_width(term, autosave_msg + 0, dlg->items + 0, 1, &min);
    max_group_width(term, autosave_msg + 1, dlg->items + 1, 1, &max);
    min_group_width(term, autosave_msg + 1, dlg->items + 1, 1, &min);
    max_group_width(term, autosave_msg + 2, dlg->items + 2, 1, &max);
    min_group_width(term, autosave_msg + 2, dlg->items + 2, 1, &min);
    max_group_width(term, autosave_msg + 3, dlg->items + 3, 1, &max);
    min_group_width(term, autosave_msg + 3, dlg->items + 3, 1, &min);
    max_group_width(term, autosave_msg + 4, dlg->items + 4, 1, &max);
    min_group_width(term, autosave_msg + 4, dlg->items + 4, 1, &min);
    max_group_width(term, autosave_msg + 5, dlg->items + 5, 1, &max);
    min_group_width(term, autosave_msg + 5, dlg->items + 5, 1, &min);
 /*   max_group_width(term, autosave_msg + 6, dlg->items + 6, 1, &max);
    min_group_width(term, autosave_msg + 6, dlg->items + 6, 1, &min);
    max_group_width(term, autosave_msg + 7, dlg->items + 7, 1, &max);
    min_group_width(term, autosave_msg + 7, dlg->items + 7, 1, &min);
    max_group_width(term, autosave_msg + 8, dlg->items + 8, 1, &max);
    min_group_width(term, autosave_msg + 8, dlg->items + 8, 1, &min);
 */   
    max_buttons_width(term, dlg->items + 6, 2, &max);
    min_buttons_width(term, dlg->items + 6, 2, &min);
    
    w = dlg->win->term->x * 9 / 10 - 2 * DIALOG_LB;
    if (w > max) w = max;
    if (w < min) w = min;
    if (w > dlg->win->term->x - 2 * DIALOG_LB - 8 ) w = dlg->win->term->x - 2 * DIALOG_LB - 8;
    if (w < 1) w = 1;
    
    rw = 0;
    y ++;
    dlg_format_group(NULL, term, autosave_msg + 0, dlg->items + 0, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, autosave_msg + 1, dlg->items + 1, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, autosave_msg + 2, dlg->items + 2, 1, 0, &y, w, &rw);
    y++;
    dlg_format_group(NULL, term, autosave_msg + 3, dlg->items + 3, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, autosave_msg + 4, dlg->items + 4, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, autosave_msg + 5, dlg->items + 5, 1, 0, &y, w, &rw);
/*    y++;
    dlg_format_group(NULL, term, autosave_msg + 6, dlg->items + 6, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, autosave_msg + 7, dlg->items + 7, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, autosave_msg + 8, dlg->items + 8, 1, 0, &y, w, &rw);
    */
    y++;
    dlg_format_buttons(NULL, term, dlg->items + 6, 2, 0, &y, w, &rw, AL_LEFT);
    
    
    w = rw;
    dlg->xw = w + 2 * DIALOG_LB;
    dlg->yw = y + 2 * DIALOG_TB;

    
    center_dlg(dlg);
    draw_dlg(dlg);
    y = dlg->y + DIALOG_TB;
    y++;
    dlg_format_group(term, term, autosave_msg + 0, dlg->items + 0, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, autosave_msg + 1, dlg->items + 1, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, autosave_msg + 2, dlg->items + 2, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    y++;
    dlg_format_group(term, term, autosave_msg + 3, dlg->items + 3, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, autosave_msg + 4, dlg->items + 4, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, autosave_msg + 5, dlg->items + 5, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
/*    y++;
    dlg_format_group(term, term, autosave_msg + 6, dlg->items + 6, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, autosave_msg + 7, dlg->items + 7, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, autosave_msg + 8, dlg->items + 8, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    */
    y++;
    dlg_format_buttons(term, term, dlg->items + 6, 2, dlg->x + DIALOG_LB, &y, w, NULL, AL_LEFT);
    
}

void menu_autosave(void *arg)
{
    struct dialog *d;
    int i;

    dbg("menu_autosave\n");
 /*   snprint(as_disk_aq_str,AS_LEN, cfg->as_disk_aq);
    snprint(as_disk_am_str, AS_LEN, cfg->as_disk_am);
    as_disk_fsync = cfg->as_disk_fsync;*/
    g_snprintf(as_floppy_aq_str, AS_LEN, "%d", cfg->as_floppy_aq);
    g_snprintf(as_floppy_am_str, AS_LEN, "%d", cfg->as_floppy_am);
    as_floppy_fsync = cfg->as_floppy_fsync;
    as_mount = cfg->as_mount;
    safe_strncpy0(as_floppy_path, cfg->as_floppy_path, MAX_STR_LEN); 
    safe_strncpy0(as_mount_cmd, cfg->as_mount_cmd, MAX_STR_LEN); 
    

    if (!(d = (struct dialog *)g_malloc(sizeof(struct dialog) + 55 * sizeof(struct dialog_item)))) return;
    memset(d, 0, sizeof(struct dialog) + 55 * sizeof(struct dialog_item));
    d->title = VTEXT(T_AUTOSAVE_OPTS);
    d->fn = autosave_fn;
    d->refresh = (void (*)(void *))refresh_autosave;

    
/*    d->items[i=0].type = D_FIELD;
    d->items[i].dlen = AS_LEN;
    d->items[i].data = as_disk_aq_str;
    
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = AS_LEN;
    d->items[i].data = as_disk_am_str;
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (void *)&as_disk_fsync;
    
  */  
    d->items[i=0].type = D_FIELD;
    d->items[i].dlen = AS_LEN;
    d->items[i].data = as_floppy_aq_str;
    d->items[i].fn   = check_number;
    d->items[i].gid  = 0;
    d->items[i].gnum = 10000;
    
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = AS_LEN;
    d->items[i].data = as_floppy_am_str;
    d->items[i].fn   = check_number;
    d->items[i].gid  = 0;
    d->items[i].gnum = 10000;
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&as_floppy_fsync;
    

    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&as_mount;
    
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = MAX_STR_LEN;
    d->items[i].data = as_floppy_path;
    d->items[i].maxl = 20;
    
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = MAX_STR_LEN;
    d->items[i].data = as_mount_cmd;
    d->items[i].maxl = 20;
    
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid = B_ENTER;
    d->items[i].fn = ok_dialog;
    d->items[i].text = VTEXT(T_OK);
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid = B_ESC;
    d->items[i].fn = cancel_dialog;
    d->items[i].text = VTEXT(T_CANCEL);
    
    d->items[++i].type = D_END;
    do_dialog(d, getml(d, NULL));
}


/******************** ADD SUBWIN *******************************/

int asw_type;
char asw_cmd[MAX_STR_LEN];
char asw_respawn_str[MAX_STR_LEN];

                                                   
void refresh_add_sw(void *arg){
	int where_ = -1;
    struct subwin *asw = (struct subwin *)arg;
	struct subwin *newsw;


    dbg("refresh_add_sw\n");
	{
		zg_ptr_array_foreach(struct subwin *, subwin, gses->subwins)
		{
			if (subwin != asw) continue;
			where_ = subwin_i;
			break;
		}
	}
	
	newsw = new_subwin((enum sw_type)asw_type, where_);
	if (newsw){
    	struct event ev = { EV_RESIZE, 0, 0, 0, 0, 0};

		if (gses->ontop == asw) gses->ontop = newsw;
		if (asw){
			newsw->ontop = asw->ontop;
			newsw->ontop2 = asw->ontop2;
			newsw->focused = asw->focused;
		}
		sw_default_func(newsw, &ev, 1);
		
	}
	if (asw) free_subwin(asw);
    
}


void menu_add_subwin(void *arg){
    struct dialog *d;
    int i, change;

	change = GPOINTER_TO_INT(arg);
	
	if (!change){
		asw_type = SWT_SHELL;
	}
    safe_strncpy0(asw_cmd, "/bin/sh", MAX_STR_LEN);
    g_snprintf(asw_respawn_str, MAX_STR_LEN, "%d", 10);
    
    d = g_new0(struct dialog, 20);
	if (!change){
		d->title = VTEXT(T_ADD_SUBWIN);
	}else{
		d->title = VTEXT(T_CHANGE_TYPE);
		d->refresh_data = gses->ontop;
	}
    d->fn = dlg_pf_fn;
    d->refresh = refresh_add_sw;
    
	i = -1;
	d->y0 = 1;

    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_KST;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&asw_type;
    d->items[i].msg = CTEXT(T_KST);
    d->items[i].wrap = 1;

    d->items[++i].type = D_CHECKBOX;  
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_SHELL;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&asw_type;
    d->items[i].msg = CTEXT(T_SHELL);
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_QSOS;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&asw_type;
    d->items[i].msg = CTEXT(T_QSOS);
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_HF;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&asw_type;
    d->items[i].msg = CTEXT(T_HF);
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_QRV;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&asw_type;
    d->items[i].msg = CTEXT(T_QRV);
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_LOG;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&asw_type;
    d->items[i].msg = CTEXT(T_LOG);
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_PLAYER;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&asw_type;
    d->items[i].msg = CTEXT(T_SW_PLAYER);
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_CHART;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&asw_type;
    d->items[i].msg = CTEXT(T_SW_CHART);
    d->items[i].wrap = 1;
    

    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_SKED;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&asw_type;
    d->items[i].msg = CTEXT(T_SKEDS);
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_TALK;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&asw_type;
    d->items[i].msg = CTEXT(T_TALK);
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_DXC;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&asw_type;
    d->items[i].msg = CTEXT(T_DXC);
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_CHECKBOX; 
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_UNFI;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&asw_type;
    d->items[i].msg = CTEXT(T_UNFI);
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_STAT;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&asw_type;
    d->items[i].msg = CTEXT(T_STAT);
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_MAP;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&asw_type;
    d->items[i].msg = CTEXT(T_MAP);
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_SCOPE;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&asw_type;
    d->items[i].msg = CTEXT(T_SCOPE);
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_SDR;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&asw_type;
    d->items[i].msg = CTEXT(T_SDR);
    d->items[i].wrap = 1;
    
    /*d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 1;
    d->items[i].gnum = SWT_SWAP;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (void *)&asw_type;
    d->items[i].msg = CTEXT(T_SWAP);
    d->items[i].wrap = 1;  */

    if (!change){
		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = asw_cmd;
		d->items[i].msg = CTEXT(T_CMD);
		d->items[i].wrap = 1;
	}

    
    /*d->items[++i].type = D_FIELD;
    d->items[i].dlen = MAX_STR_LEN;
    d->items[i].data = asw_respawn_str;
    d->items[i].msg = CTEXT(T_RESPAWN);
    d->items[i].wrap = 1; */

	d->items[i].wrap++;
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid = B_ENTER;
    d->items[i].fn = ok_dialog;
    d->items[i].text = VTEXT(T_OK);
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid = B_ESC;
    d->items[i].fn = cancel_dialog;
    d->items[i].text = VTEXT(T_CANCEL);
    d->items[i].align = AL_BUTTONS;
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_END;
    do_dialog(d, getml(d, NULL));
}


/********************* CLOSE SUBWIN ***********************/

void menu_close_subwin(void *arg){
    struct subwin *sw;
    

    sw = find_sw_ontop();
    if (!sw) return;

	g_ptr_array_remove(gses->subwins, sw);

    free_subwin(sw);
	if (sw == gses->ontop2)
		sw_totop_next(0, 1);
    sw_totop_next(0, 0);
    redraw_later();
    
}

/********************* SPLIT SUBWIN ***********************/

void menu_split_subwin(void *arg){
	cfg->splitheight = 10;
	resize_terminal(NULL);
}

void menu_remove_split(void *arg){
	cfg->splitheight = 0;
	resize_terminal(NULL);
}

/******************* UNFINISHED REMARK *************/

void do_unfinished(struct session *ses, char *remark){
    GString *gs;

    if (!ctest || !aband) return;

    gs = g_string_sized_new(80);
    
    if (TMPQ.callsign && TMPQ.ucallsign){
        if (strlen(remark)>10){
            gchar *c;
            remark[17]='\0';
            c = g_strdup_printf("%s %s", remark, TMPQ.callsign);
            g_string_append_printf(gs, "%-24s", c);
            g_free(c);
        }else{
            g_string_append_printf(gs, "%-10s %-13s", remark, TMPQ.callsign); 
        }
    }else{
        remark[24]='\0';
        g_string_append_printf(gs, "%-24s",remark);
    }

            
    g_string_append_printf(gs, "%5s%4s %5s%4s",
        TMPQ.rsts?TMPQ.rsts:"---", 
        TMPQ.qsonrs?TMPQ.qsonrs: "---",
        TMPQ.rstr?TMPQ.rstr:"---", 
        TMPQ.qsonrr?TMPQ.qsonrr:"---");

    if (TMPQ.locator && TMPQ.ulocator){
        g_string_append_printf(gs, "    %-8s %7d %3d", TMPQ.locator, (int)TMPQ.qrb, TMPQ.qtf);
    }
#ifdef HAVE_HAMLIB
	if (gtrigs->trigs->len > 0){
        g_string_append_printf(gs, "  %1.1f", gtrigs->qrg/1000.0);
    }
#endif

    fifo_addf(aband->unfi, gs->str);
    g_string_free(gs, 1);

    add_error(aband, remark);/* before clear_tmpqsos! */
    
    clear_tmpqsos(aband, 1);
    clear_inputline(aband->il);
    get_cw_qs(aband->il->cdata); /* clears */
    get_band_qs(aband, aband->il->cdata);
    get_hf_dxc(aband->il->cdata);
    redraw_later();
    

}

    
void menu_unfinished(void *arg){
    static char remark[40];
    int max;

    if (TMPQ.callsign && TMPQ.ucallsign)
        max=1+17;
    else
        max=1+24;
    
    if (!ctest || !aband) return;
    
    strcpy(remark, "");

    input_field(NULL, VTEXT(T_UNFI_QSO), VTEXT(T_N_REMARK), 
               VTEXT(T_OK), VTEXT(T_CANCEL), NULL, 
               NULL, max, remark, 0, 0, NULL,
               (void (*)(void *, char *)) do_unfinished, NULL, 0);
    
}


