/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __HTTPD_H
#define __HTTPD_H

#include "header.h"

struct httpd{
    int port;
    int sock;
    int refresh;
    GPtrArray *conns;
};

struct httpconn{
    int sock;
    GString *request;
    GString *response;
	int response_i;
	struct zbinbuf *response_zbb;
	struct sockaddr_in peer;
};

extern struct httpd *httpd;

struct httpd *init_httpd(void);
void free_httpd(struct httpd *httpd);
void httpd_accept_handler(void *arg);
void httpd_read_handler(void *arg);
void httpd_write_handler(void *arg);
void httpd_header(struct httpconn *conn, int status, char *contenttype);
void httpd_get(struct httpconn *conn);

#endif
