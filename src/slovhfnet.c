/*
    slovhfnet.c - Upload claimed live score to slovhf.net
    Copyright (C) 2017-2020 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"

#include "slovhfnet.h"

#include <libzia.h>

#include "bfu.h"
#include "dwdb.h"
#include "edi.h"
#include "fifo.h"
#include "main.h"
#include "menu.h"
#include "subwin.h"
#include "stats.h"
#include "tregex.h"



struct slovhfnet *init_slovhfnet(void){
	struct slovhfnet *slovhf = g_new0(struct slovhfnet, 1);

	slovhf->http = zhttp_init();
	return slovhf;
}

void free_slovhfnet(struct slovhfnet *slovhf){

	if (slovhf->http) zhttp_free(slovhf->http);
	g_free(slovhf);
}


void slovhfnet_upload(void *arg){
	GString *gs = g_string_new("");
	GString *gs2 = g_string_new("");
	int i;
	struct band *b;
	char *c;

	if (gses->slovhf) return;
	if (!ctest) return;

	if (!cfg->slovhf_user || !*cfg->slovhf_user){
	    msg_box(NULL, VTEXT(T_ERROR), AL_CENTER, TRANSLATE("Undefined SLOVHF user. Please fill it in menu Contest defaults."), NULL, 1, VTEXT(T_OK), NULL, B_ENTER | B_ESC);
		return;
	}

	for (i = 0; i < ctest->bands->len; i++){
		b = (struct band *)g_ptr_array_index(ctest->bands, i);
		if (b->stats->nqsos == 0) continue;
		g_string_append_printf(gs, "%-7s  %4d  %6d\n", b->pband, b->stats->nqsos, b->stats->ntotal);
	}

	
	g_string_append_printf(gs2, TRANSLATE("Contest: %s\n"), ctest->tname);
	g_string_append_printf(gs2, TRANSLATE("Call: %s\n"), ctest->pcall);
	g_string_append_printf(gs2, TRANSLATE("User: %s\n\n"), cfg->slovhf_user);
	g_string_append_printf(gs2, "%-7s  %-4s  %-6s\n", "Band", "QSOs", "Points"); 
	g_string_insert(gs, 0, gs2->str);
	g_string_free(gs2, TRUE);
    g_string_append_printf(gs, TRANSLATE("\nUpload report to slovhf.net?"));


	c = g_strdup(gs->str);
	g_string_free(gs, TRUE);

	msg_box(getml(c, NULL), VTEXT(T_INFO), AL_LEFT, 
		c, NULL, 2, 
		VTEXT(T_YES), slovhfnet_do_upload, B_ENTER,
		VTEXT(T_NO), NULL, B_ESC);

}

void slovhfnet_do_upload(void *arg){
	struct slovhfnet *slovhf = init_slovhfnet();
	gses->slovhf = slovhf;

	g_free(gses->slovhf->status);gses->slovhf->status = NULL;
	slovhfnet_next_band(slovhf);
}

void slovhfnet_next_band(struct slovhfnet *slovhf){
	struct band *b;
	GString *gs;
	int qrgindex = 0;

	for (; slovhf->bandi < ctest->bands->len; slovhf->bandi++){
		b = (struct band *)g_ptr_array_index(ctest->bands, slovhf->bandi);
		if (b->stats->nqsos > 0) break;
	}
	if (slovhf->bandi >= ctest->bands->len){
		//log_addf(TRANSLATE("All bands uploaded"), slovhf->http->errorstr);
		free_slovhfnet(slovhf); gses->slovhf = NULL;
		log_addf(TRANSLATE("Live score uploaded"));
		return;
	}

	g_free(slovhf->status);slovhf->status = g_strdup_printf(VTEXT(T_UPLOADING_BAND_S), b->bandname);
	log_addf(VTEXT(T_UPLOADING_BAND_S), b->bandname);
//http://slovhf.net/rezults.php?call={callsign}&lokator={sent_wwl}&qso={valid_qsos}&km={total_score}&dxcall={odx_call}&dxlokator={odx_wwl}&dxqrb={odx_qrb}&qrgindex={qrgindex}&gazda={slovhf.net_username}&pid=tucnak
// qrgindex
//	  1: '50 MHz';
//    2: '70 MHz';
//    3: '145 MHz';
//    4: '435 MHz';
//    5: '1,3 GHz';
//    6: '2,3 GHz';
//    7: '3,4 GHz';
//    8: '5,7 GHz';
//    9: '10 GHz';
//    10: '24 GHz';
//    11: '47 GHz';
//    12: '76 GHz';
//    13: '120 GHz';
//    14: '144 GHz';
//    15: '248 GHz';*/
	if (b->qrg_min >= 241000000) qrgindex = 15;
	else if (b->qrg_min >= 142000000) qrgindex = 14;
	else if (b->qrg_min >= 120000000) qrgindex = 13;
	else if (b->qrg_min >= 75500000) qrgindex = 12;
	else if (b->qrg_min >= 47000000) qrgindex = 11;
	else if (b->qrg_min >= 24000000) qrgindex = 10;
	else if (b->qrg_min >= 10000000) qrgindex = 9;
	else if (b->qrg_min >= 5650000) qrgindex = 8;
	else if (b->qrg_min >= 3400000) qrgindex = 7;
	else if (b->qrg_min >= 2300000) qrgindex = 6;
	else if (b->qrg_min >= 1240000) qrgindex = 5;
	else if (b->qrg_min >= 430000) qrgindex = 4;
	else if (b->qrg_min >= 144000) qrgindex = 3;
	else if (b->qrg_min >= 70025) qrgindex = 2;
	else if (b->qrg_min >= 50000) qrgindex = 1;
	

	gs = g_string_sized_new(200);
	zg_string_eprintfa("", gs, "http://slovhf.net/rezults.php");
	//zg_string_eprintfa("", gs, "http://ok1zia.nagano.cz/tmp/rezults.php");
	zg_string_eprintfa("u", gs, "?call=%s", ctest->pcall); 
	zg_string_eprintfa("u", gs, "&lokator=%s", ctest->pwwlo); 
	zg_string_eprintfa("u", gs, "&qso=%d", b->stats->nqsos); 
	zg_string_eprintfa("u", gs, "&km=%d", b->stats->ntotal); 
	zg_string_eprintfa("u", gs, "&qrgindex=%d", qrgindex); 
	zg_string_eprintfa("u", gs, "&gazda=%s", cfg->slovhf_user); 
	zg_string_eprintfa("u", gs, "&pid=tucnak"); 


	zhttp_get(slovhf->http, zsel, gs->str, slovhfnet_sent, slovhf);
}

void slovhfnet_sent(struct zhttp *http){
	struct slovhfnet *slovhf = (struct slovhfnet*)http->arg;

    if (http->errorstr){
		g_free(slovhf->status);slovhf->status = g_strdup_printf(TRANSLATE("Can't upload score: %s"), http->errorstr);
		zhttp_free(slovhf->http); slovhf->http = NULL;
		free_slovhfnet(slovhf); gses->slovhf = NULL;
		return;
	}

	//log_addf("OK");

	slovhf->bandi++;
	slovhfnet_next_band(slovhf);
}

void slovhfnet_abort(void *arg){
	if (!gses->slovhf) return;
	free_slovhfnet(gses->slovhf);
	gses->slovhf = NULL;
}


/*void slovhfnet_confirmed(struct zhttp *http){
	struct slovhfnet *slovhf = (struct slovhfnet*)http->arg;

    if (http->errorstr){
		g_free(slovhf->status);slovhf->status = g_strdup_printf(VTEXT(T_CANT_CONFIRM_LOG), http->errorstr);
		zhttp_free(slovhf->http); slovhf->http = NULL;//free_slovhfnet(slovhf);
		return;
	}

}

void slovhfnet_ask_claimed(struct zhttp *http){
	struct slovhfnet *slovhf = (struct slovhfnet*)http->arg;
	char *url;

	g_free(slovhf->status);slovhf->status = g_strdup(VTEXT(T_DOWNLOADING_CLAIMED_SCORES));

	url = g_strdup_printf("http://www.slovhf.net/getlog/gentxtsql.php?zavod=%d&country=%d", slovhf->ctestid, slovhf->country);
	
	zhttp_get(http, zsel, url, slovhfnet_claimed, http->arg);
	g_free(url);
}

void slovhfnet_claimed(struct zhttp *http){
	struct slovhfnet *slovhf = (struct slovhfnet*)http->arg;
	char *html, *c;
	GString *gs;

    if (http->errorstr){
		g_free(slovhf->status);slovhf->status = g_strdup_printf(VTEXT(T_CANT_GET_CLAIMED_SCORES), http->errorstr);
		zhttp_free(slovhf->http); slovhf->http = NULL;//free_slovhfnet(slovhf);
		return;
	}


	
	html = (char *)g_malloc(http->response->len + 1);
	zbinbuf_getstr(http->response, http->dataofs, html, http->response->len - http->dataofs + 1);

	gs = g_string_new(html);
	c = z_strcasestr(gs->str, "<pre>");
	if (c != NULL) g_string_erase(gs, 0, (c - gs->str) + 5);
	c = z_strcasestr(gs->str, "</pre>");
	if (c != NULL) g_string_truncate(gs, c - gs->str);

	fifo_add_lines(glog, gs->str);

	g_string_free(gs, TRUE);
	g_free(html);

	g_free(slovhf->status);slovhf->status = g_strdup(VTEXT(T_LOGS_UPLOADED));
	zhttp_free(slovhf->http); slovhf->http = NULL;

}


void slovhfnet_info(void *arg1){
	struct refresh *r;
	char *c, *button;
	GString *gs;

	if (!gses || !gses->slovhf) return;

	if (term->windows.next->handler	== menu_func){
		zselect_timer_new(zsel, RESOURCE_INFO_REFRESH, slovhfnet_info, NULL);
	}else{
		r = (struct refresh*)g_malloc(sizeof(struct refresh));
		r->win = NULL;
		r->fn = slovhfnet_info;
		r->data = NULL;
		r->timer = -1;

		gs = g_string_new("");
		if (gses->slovhf->status != NULL) g_string_append_printf(gs, "%s", gses->slovhf->status);
		if (gses->slovhf->status != NULL && gses->slovhf->http != NULL) g_string_append(gs, "\n\n");
		if (gses->slovhf->http != NULL){
			g_string_append(gs, VTEXT(T_HTTP_STATUS));
			zhttp_status(gses->slovhf->http, gs);
			button = VTEXT(T_CANCEL);
		}else{
			button = VTEXT(T_OK);
		}
		c = g_strdup(gs->str);
		g_string_free(gs, TRUE);

		msg_box(getml(c, NULL), VTEXT(T_LOG_UPLOAD), AL_LEFT, c, r, 1, button, slovhfnet_abort, B_ENTER | B_ESC);
		r->win = term->windows.next;
		((struct dialog_data *)r->win->data)->dlg->abort = refresh_abort;
		r->timer = zselect_timer_new(zsel, RESOURCE_INFO_REFRESH, (void (*)(void *))refresh, r);
	}
}	 */
