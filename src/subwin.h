/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2016 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __SUBWIN_H
#define __SUBWIN_H

#include "header.h"
#include "terminal.h"
#include "ac.h"

struct band;

#define BANDMAPW 22
#define BANDMAPMINW (75+BANDMAPW)
#define KSTQRVW1 23
#define KSTQRVW2 (23+13)
#define QSOS_LEN (sw->allqsos?ctest->allqsos->len:aband->qsos->len)

enum sw_type { SWT_QSOS, SWT_LOG, SWT_SHELL, SWT_TALK, 
               SWT_DXC, SWT_SKED, SWT_SWAP, SWT_UNFI,
               SWT_STAT, SWT_MAP, SWT_SCOPE, SWT_QRV,
               SWT_HF, SWT_PLAYER, SWT_CHART, SWT_KST,
			   SWT_UPD, SWT_SDR};

struct subwin {
    gchar *title;
    enum sw_type type;
    gint x,y,w,ww,h,hh;
    gint offset,cur,ho, offset2;
    gint ontop, ontop2,focused;
    gint titl1,titl2;

    int  (*kbd_func)(struct subwin *, struct event *ev, int fw);
    int  (*mouse_func)(struct subwin *, struct event *ev, int fw);
    void (*redraw)(struct subwin *, struct band *band, int flags);
    void (*check_bounds)(struct subwin *);
    void (*raise)(struct subwin *);
    GPtrArray *lines;
    int eol, maxlen, unread;
    
    struct inputln *il; /* optional */
    
      /* subwins having pipes */
	gint read_fd, write_fd; 
	struct zserial *zser;
	iconv_t iconv;
#ifdef Z_UNIX
    //pid_t pid;
    struct winsize ws;
#endif
    GPtrArray *high; /* of gchar* */

    gchar *command;
    gint respawn_time;

    /* fifo is foreign key mostly to global variable */
    struct fifo *fifo;

    /* for graphics subwins (SWT_MAP) */
#ifdef Z_HAVE_SDL    
    SDL_Surface *l1map;
//    SDL_Surface *l2rot;
    SDL_Surface *screen;
    MUTEX_DEFINE(screen);
//    GMutex *screen_mutex;
//#ifdef LEAK_DEBUG_LIST    
//    char *screen_file;
//    int screen_line;
//#endif
    int ox, oy;
    int zoom;
    int gdirty;
    SDL_Rect map, info;
    gchar *pwwlo;
    struct qso *minqso;
    //gchar *minqrvcall;
    int scope_mode;
    int fft_wf_y, fft_wf_h;  
    int fft_sp_y, fft_sp_h;
    int fft_wf_x2;
    int fft_sp_x2;
    
    double antchar[360];
    int beamwidth;
    double gst[360];

    int mint, maxt, minv, maxv;
    int ch_left, ch_right, ch_top, ch_bottom;
    char *ch_op;
    int mx, my, ch_mouse_value;
	int showlines, showwwls;
#endif    
    GPtrArray *chbands; // of struct chband
#ifdef HAVE_SNDFILE
    int pl_rate;
    int pl_channels;
    char *pl_shapebuf;	// for all samples
    int pl_shape_frames;
    int pl_total_frames;      
    int pl_pxlen;
    int pl_ofs;
#ifdef Z_HAVE_SDL    
    SDL_Surface *pl_preview_screen;
    int pl_preview_w;
#endif
#endif
    int sock;
	struct zbinbuf *wrbuf;
    //int kst_srv;
	struct zasyncdns *adns;
	int chat; // unix chat, not KST chat
	int first_shus;
	char *callunder;
	char *refunder;
	char *acunder;
	int uhel;
	GPtrArray* callsunder;
	int callunderx, callundery;
	double qrg; // kHz
	char *pattern;
	int dxc_from_net;
	int timer_id;
	GHashTable *kstusers;  // of qrv_item (foreign key, can be NULL)
	GHashTable *kstoldusers;
	int shus;	// 0 = none, 1 = from timer with filter, 2 = from operator
	int kst_y, kst_offset;
	int allqsos;
	char *player_filename;
	int dxc_offset;
	int olddragx, olddragy; // buď v terminálových nebo grafických souřadnicích
	int ac_plotinfo;
	struct zchart *chart;
	GPtrArray *buttons; // of struct button
	struct button *sdr_ssb, *sdr_cw, *sdr_usb, *sdr_lsb, *sdr_iqcomp;
	int sdr_by;
	int side_top, side_bott;
	int shaking;
	struct timeval kst_latest_data;
};

struct config_subwin{
    int nr;
    enum sw_type type;
    gchar *command;
    gchar *autorun;
    int respawn_time;
};


struct subwin *new_subwin(enum sw_type type, int where);
void free_subwins(void);
void free_subwin(struct subwin *sw);
struct subwin *sw_raise_or_new(enum sw_type type);

int sw_add_line(struct subwin *sw, gchar *line, int eol);
int sw_add_block(struct subwin *sw, gchar *data);
int sw_printf(struct subwin *sw, const char *m, ...);


int sw_default_func(struct subwin *sw, struct event *ev, int fw);
int sw_all_func(struct event *ev, int fw);
int sw_focus_func(struct event *ev, int fw);
int sw_ontop_func(struct event *ev, int fw);
struct subwin *find_sw_ontop(void);
void sw_set_focus(void); 
void sw_unset_focus(void);
struct subwin *sw_set_ontop(int n, int set2); /* n abs */
struct subwin *sw_totop_next(int n, int set2); /* n = +-1 */
void sw_set_unread(struct fifo *fifo);
void sw_unset_unread(struct fifo *fifo);
struct config_subwin *get_config_sw_by_number(GPtrArray *sws, int nr);

void sw_default_redraw(struct subwin *sw, int flags);
void sw_check_len(struct subwin *sw);
void draw_titles(int y, int ontop2);
int sw_line_is_highlighted(struct subwin *sw, gchar *c);


int  sw_qsos_kbd_func(struct subwin *sw, struct event *ev, int fw);
int  sw_qsos_mouse_func(struct subwin *sw, struct event *ev, int fw);
void sw_qsos_redraw(struct subwin *sw, struct band *band, int flags);
void sw_qsos_check_bounds(struct subwin *sw);
int show_qs(void);
void sw_qs_redraw(void);

int  sw_fifo_kbd_func(struct subwin *sw, struct event *ev, int fw);
int  sw_fifo_mouse_func(struct subwin *sw, struct event *ev, int fw);
void sw_fifo_redraw(struct subwin *sw, struct band *band, int flags);
void sw_fifo_check_bounds(struct subwin *sw);
  
int  sw_shell_mouse_func(struct subwin *sw, struct event *ev, int fw);
void sw_shell_redraw(struct subwin *sw, struct band *band, int flags);
void sw_shell_check_bounds(struct subwin *sw);
void sw_shell_enter(void *enterdata, gchar *str, int cq);
int  sw_shell_run(struct subwin *sw, char *cmd);
void sw_shell_read_handler(void *);
void sw_shell_write_handler(void *);
void sw_shell_exception_handler(void *);
void sw_shell_kill(struct subwin *sw);
void sw_shell_match(gpointer acall, gpointer nic, gpointer astr);
int sw_shell_hihglight(struct subwin *sw, char *str);
#ifdef HAVE_PTY_H
int sw_pty_run(struct subwin *sw, char *cmd);
#endif
#ifdef Z_UNIX
int sw_pipe_run(struct subwin *sw, char *cmd);
#endif
  
void sw_talk_enter(void *enterdata, gchar *str, int);
void sw_talk_read(char *, int ignoredupe);

  
int  sw_unfi_kbd_func(struct subwin *sw, struct event *ev, int fw);
int  sw_unfi_mouse_func(struct subwin *sw, struct event *ev, int fw);
void sw_unfi_redraw(struct subwin *sw, struct band *band, int flags);
void sw_unfi_check_bounds(struct subwin *sw);

int  sw_stat_kbd_func(struct subwin *sw, struct event *ev, int fw);
int  sw_stat_mouse_func(struct subwin *sw, struct event *ev, int fw);
void sw_stat_redraw(struct subwin *sw, struct band *band, int flags);
void sw_stat_check_bounds(struct subwin *sw);
void sw_stat_raise(struct subwin *sw);

void clip_char(struct subwin *sw, int xrel, int yrel, unsigned c);
void clip_color(struct subwin *sw, int xrel, int yrel, unsigned c);
void clip_printf(struct subwin *sw, int xrel, int yrel, short color, char *fmt, ...);

void sw_export_lines(struct subwin *sw, char *fname);

extern int sw_shake_tmo;
void sw_shake(struct subwin *sw, int down);
int sw_accel_dy(struct subwin *sw, struct event *ev);
void sw_highlight(struct subwin *sw, const char *text, const char *needle, int color, int i, int whole);
void sw_set_gdirty(enum sw_type type);


#endif
