/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2006  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"
#include "cwdaemon.h"
#include "cwwindow.h"
#include "kbd.h"
#include "terminal.h"
#include "rc.h"       // added for slashkey replacement ( cfg->slasheky )
#include "kbdbind.h" // added for F_key support


//static struct window *cwwindow;

static void draw_cwwindow(struct cwwin_data *cwwdata){
    fill_area(cwwdata->x-2, cwwdata->y-1, cwwdata->w+4, cwwdata->h+2, COL_INV); 
    draw_frame(cwwdata->x, cwwdata->y, cwwdata->w, cwwdata->h, COL_INV, 1); 
    print_text(cwwdata->x + 5, cwwdata->y, -1, VTEXT(T__CW_), COL_NORM);
    print_text(cwwdata->x + 5, cwwdata->y + cwwdata->h -1 , -1, VTEXT(T_CWW_KEYS), COL_NORM);

    print_text(cwwdata->x + 3, cwwdata->y +2 , cwwdata->w-4, cwwdata->text->str , COL_INV);
        
    /*dbg("draw_cwwindow: set_cursor %d %d \n", cwwdata->x +
	 * strlen(cwwdata->text), cwwdata->y+2, 0, 0);*/
    set_cursor(cwwdata->x + cwwdata->text->len + 3, cwwdata->y+2, 
                     cwwdata->x + cwwdata->text->len + 3 , cwwdata->y+2);
    
}

void cwwindow_func(struct window *win, struct event *ev, int fwd){
    //struct terminal *term;
    struct cwwin_data *cwwdata;
	int nn; 
    
    struct cq *cq;  // added  for F-Key macro readout
    gchar *raw; // added  for F-Key macro readout
    
    //term = win->term;
    cwwdata = (struct cwwin_data *)win->data;
    
//    dbg("cwwindow_func [%d,%d,%d,%d]\n",ev->ev,ev->x,ev->y,ev->b);

    switch (ev->ev) {
        case EV_INIT:
            win->data = cwwdata = (struct cwwin_data *)g_malloc(sizeof(struct cwwin_data));
            memset(win->data, 0, sizeof(struct cwwin_data));
			cwwdata->text = g_string_sized_new(100);

        case EV_RESIZE:
            cwwdata->x = 10;
            cwwdata->y = (ev->y - 6) / 2;
            cwwdata->w = ev->x - 20;
            cwwdata->h = 5;
            
        case EV_REDRAW:
            draw_cwwindow(cwwdata);
            break;
        case EV_ABORT:
            /* mem_free(cwwdata);  already in terminal.c:196 (delete_window) */ 
			g_string_free(cwwdata->text, 1);
            break;
        case EV_MOUSE:
            break;
        case EV_KBD:
            //dbg("\nTest:%d\n",(ev->x));
	    switch(ev->x){
                case KBD_ESC:
                case KBD_TAB:
                    /*delete_window_ev(win, (void *)win->next != &win->term->windows && 
                            win->next->handler == cwwindow_func ? ev : NULL);*/
                    delete_window_ev(win, ev);
                    break;
                case KBD_ENTER: /* delete window, CW text play on */
                    ev->x=KBD_TAB;
                    delete_window_ev(win, ev);
                    break;
                case KBD_PGUP:
					cwdaemon_qrq(cwda, 2);
					redraw_later();
					break;  
                case KBD_PGDN:
					cwdaemon_qrs(cwda, 2);
					redraw_later();
                    break;  
               
		
		//F-Keytest  part start
		case KBD_F5:
		    dbg("\nF5_key detected\n");
		    cq = (struct cq *)g_ptr_array_index(cfg->cqs, 0);  // macro F 5 read
		   if (!cq->cw_str) {
			 dbg("Error in cq structure\n");
		     break;
		    }
		    raw = convert_cq(cq);  //convert macro to text 
		     if (!raw) {
			 dbg("Error in raw structure\n");
			 break;
		     }
		    //dbg("\nCW-Marco insert:%s\n",raw);
		    g_string_append (cwwdata->text, raw); // adding text to display buffer
		    g_string_append (cwwdata->text, " "); //adding Space 
		    cwda->text(cwda, raw);//adding text to cw buffer
		    cwda->text(cwda," ");//adding Space 
		    //window scrolling
		    nn = cwwdata->text->len - (cwwdata->w - 7);
		    if (nn>0) g_string_erase(cwwdata->text, 0, nn);
		    draw_cwwindow(cwwdata); 
		    break;
		
		case KBD_F6:
		    dbg("\nF6_key detected\n");
		    cq = (struct cq *)g_ptr_array_index(cfg->cqs, 1);  // macro F 6 read
		    if (!cq->cw_str) {
			 dbg("Error in cq structure\n");
		     break;
		    }
		    raw = convert_cq(cq);  //convert macro to text 
		     raw = convert_cq(cq);  //convert macro to text 
		     if (!raw) {
			 dbg("Error in raw structure\n");
			 break;
		     }
		     //dbg("\nCW-Marco insert:%s\n",raw);
		    g_string_append (cwwdata->text, raw); // adding text to cw buffer
		    g_string_append (cwwdata->text, " "); //adding Space 
		    cwda->text(cwda, raw);//adding text to cw buffer
		    cwda->text(cwda," ");//adding Space 
		    //window scrolling
		    nn = cwwdata->text->len - (cwwdata->w - 7);
		    if (nn>0) { g_string_erase(cwwdata->text, 0, nn);draw_cwwindow(cwwdata); }
		    break;
		
		case KBD_F7:
		    dbg("\nF7_key detected\n");
		    cq = (struct cq *)g_ptr_array_index(cfg->cqs, 2);  // macro F 7 read
		     if (!cq->cw_str) {
			 dbg("Error in cq structure\n");
		     break;
		    }
		    raw = convert_cq(cq);  //convert macro to text 
		     if (!raw) {
			 dbg("Error in raw structure\n");
			 break;
		     }
		    //dbg("\nCW-Marco insert:%s\n",raw);
		    g_string_append (cwwdata->text, raw); // adding text to cw buffer
		    g_string_append (cwwdata->text, " "); //adding Space 
		    cwda->text(cwda, raw);//adding text to cw buffer
		    cwda->text(cwda," ");//adding Space 
		    //window scrolling
		    nn = cwwdata->text->len - (cwwdata->w - 7);
		    if (nn>0) g_string_erase(cwwdata->text, 0, nn);
		    draw_cwwindow(cwwdata);
		   break;
		
		case KBD_F8:
		    dbg("\nF8_key detected\n");
		    cq = (struct cq *)g_ptr_array_index(cfg->cqs, 3);  // macro F 8 read
		    if (!cq->cw_str) {
			 dbg("Error in cq structure\n");
		     break;
		    }
		    raw = convert_cq(cq);  //convert macro to text 
		     raw = convert_cq(cq);  //convert macro to text 
		     if (!raw) {
			 dbg("Error in raw structure\n");
			 break;
		     }
		     //dbg("\nCW-Macro insert:%s\n",raw);
		    g_string_append (cwwdata->text, raw); // adding text to cw buffer
		    g_string_append (cwwdata->text, " "); //adding Space 
		    cwda->text(cwda, raw);//adding text to cw buffer
		    cwda->text(cwda," ");//adding Space 
		    //window scrolling
		    nn = cwwdata->text->len - (cwwdata->w - 7);
		    if (nn>0) g_string_erase(cwwdata->text, 0, nn);
		    draw_cwwindow(cwwdata);
		    break;
		
		case KBD_F11:
		    dbg("\nF11_key detected\n");
		    cq = (struct cq *)g_ptr_array_index(cfg->cqs, 4);  // macro F 11 read
		    if (!cq->cw_str) {
			 dbg("Error in cq structure\n");
		     break;
		    }
		    raw = convert_cq(cq);  //convert macro to text 
		     raw = convert_cq(cq);  //convert macro to text 
		     if (!raw) {
			 dbg("Error in raw structure\n");
			 break;
		     }
		     //dbg("\nCW-Marco insert:%s\n",raw);
		    g_string_append (cwwdata->text, raw); // adding text to cw buffer
		    g_string_append (cwwdata->text, " "); //adding Space 
		    cwda->text(cwda, raw);//adding text to cw buffer
		    cwda->text(cwda," ");//adding Space 
		    //window scrolling
		    nn = cwwdata->text->len - (cwwdata->w - 7);
		    if (nn>0) g_string_erase(cwwdata->text, 0, nn);
		    draw_cwwindow(cwwdata);
		    break;
		
		case KBD_F12:
		    dbg("\nF12_key detected\n");
		    cq = (struct cq *)g_ptr_array_index(cfg->cqs, 5);  // macro F 12 read
		    if (!cq->cw_str) {
			 dbg("Error in cq structure\n");
		     break;
		    }
		    raw = convert_cq(cq);  //convert macro to text 
		     raw = convert_cq(cq);  //convert macro to text 
		     if (!raw) {
			 dbg("Error in raw structure\n");
			 break;
		     }
		     //dbg("\nCW-Marco insert:%s\n",raw);
		    g_string_append (cwwdata->text, raw); // adding text to cw buffer
		    g_string_append (cwwdata->text, " "); //adding Space 
		    cwda->text(cwda, raw);//adding text to cw buffer
		    cwda->text(cwda," ");//adding Space 
		    //window scrolling
		    nn = cwwdata->text->len - (cwwdata->w - 7);
		    if (nn>0) g_string_erase(cwwdata->text, 0, nn);
		    draw_cwwindow(cwwdata);
		    break;    
		    
		//F-Keytest part stop
		
		
		
		
		default:    
                  
		    // slashkey replace patch start
		    if (cfg->slashkey && *cfg->slashkey){
                        if (ev->x == '/') ev->x = *cfg->slashkey;
                        else if (ev->x == *cfg->slashkey) ev->x = '/';
                    }
		    //slashkey replace patch end
		    
		    if (ev->x == KBD_BS && ev->y == 0){
                        if (cwda && cwda->back) cwda->back(cwda);
                        if (cwwdata->text->len > 0)
                            g_string_truncate(cwwdata->text, cwwdata->text->len - 1);
                        redraw_later();
                        break;
                    }
                   
		    if (ev->x >= ' ' && ev->x < 256 && ev->y == 0) {
						char s[2];
						int n;
                        dbg("key=%c\n", ev->x);
			
						g_string_append_c(cwwdata->text, (char)ev->x); 
						n = cwwdata->text->len - (cwwdata->w - 7);
						if (n>0) g_string_erase(cwwdata->text, 0, n);
						s[0]=(char)ev->x;
						s[1]='\0';
                        //cwdaemon_text(cwda, s);
                        if (cwda && cwda->text) cwda->text(cwda, s);
                        draw_cwwindow(cwwdata);
                        break;
                    }
                    break;
            }
            break;
    }
}

