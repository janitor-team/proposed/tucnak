/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2020 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __DWDB_H
#define __DWDB_H

struct cw;
extern struct dw *dw;
struct qso;
#include <glib.h>

struct dw_item{
    gchar *wwl0, *wwl1;   /* center of activity (optional),  */
                            /* center of dxcc (geometric)     */
    GHashTable *wwls;      /* all wwls valid for this dxcc */
    int waz, itu;
    gchar *continent;
    gchar *dxcname;
	double latitude, longitude;
};

struct wd_item{
    gchar *dxc0, *dxc1;
};

struct dw{
    GHashTable *pd;  /* key=prefix "DF", value=dxcc (main prefix) "DL" */
    GHashTable *dw;  /* key=dxcc "DL", value=dw_item */
    GHashTable *wd;  /* key=big wwl, "JN69" */
};


struct dw *init_dw(void);
void free_dw(struct dw *dw);

gint get_pd_size(struct dw *dw);
gint get_dw_size(struct dw *dw);
gint get_wd_size(struct dw *dw);

void add_pd(struct dw *dw, gchar *prefix, gchar *dxc);
struct dw_item *add_dxc(struct dw *dw, gchar *dxc, gchar *wwl0, gchar *wwl1, int waz, int itu, gchar *continent, gchar *dxcname, double latitude, double longitude);
void add_dw(struct dw_item *dxci, gchar *dxc, gchar *wwl);
void add_wd(struct dw *dw, gchar *wwl, gchar *dxc);
    
int load_dw_from_file(struct dw *dw, gchar *filename);
int load_cty_from_file(struct dw *dw, gchar *filename);
void read_dw_files(struct dw *dw);


struct dw_item *get_dw_item_by_call(struct dw *dw, const char *call);
struct dw_item *get_dw_base_item_by_call(struct dw *dw, const char *call);
int wwl_is_ok_by_call(struct dw *dw, gchar *wwl, gchar *call);
int get_susp(struct cw *cw, struct dw *dw, gchar *call, gchar *wwl, int ambig);
int get_susp_call(struct cw *cw, struct dw *dw, gchar *call, gchar *wwl);
int get_susp_loc(struct cw *cw, struct dw *dw, gchar *call, gchar *wwl);
gchar *find_wwl_by_dxc_or_pref(struct dw *dw, gchar *call);
gchar *find_dxc_by_wwl(struct dw *dw, gchar *wwl);
char *get_dxcc(struct dw *dw, char *buf, const char *call);
char *get_pref(char *buf, const char *call);

int load_dw_from_mem(struct dw *dw, const char *file, const long int len);
int load_cty_from_mem(struct dw *dw, const char *file, const long int len);
void call_info(void *arg);
void menu_qso_check(void *arg);
enum suspcall qso_info(struct qso *qso, GString *gs, GString *gs2);



#endif
