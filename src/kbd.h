/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __KBD_H
#define __KBD_H

#define BM_BUTT     3
#define BM_EBUTT    7
#define B_LEFT      1
#define B_MIDDLE    2
#define B_RIGHT     3
#define B_WHUP      4
#define B_WHDOWN    5
#define BM_ACT      0x38
#define B_DOWN      0
#define B_UP        8
#define B_DRAG      0x10
#define B_MOVE      0x20
#define B_CLICK     0x40
#define BM_BUTCL    0x47

#define KBD_ENTER   0x100
#define KBD_BS      0x101
#define KBD_TAB     0x102
#define KBD_ESC     0x103
#define KBD_LEFT    0x104
#define KBD_RIGHT   0x105
#define KBD_UP      0x106
#define KBD_DOWN    0x107
#define KBD_INS     0x108
#define KBD_DEL     0x109
#define KBD_HOME    0x10a
#define KBD_END     0x10b
#define KBD_PGUP    0x10c
#define KBD_PGDN    0x10d

#define KBD_F1      0x120
#define KBD_F2      0x121
#define KBD_F3      0x122
#define KBD_F4      0x123
#define KBD_F5      0x124
#define KBD_F6      0x125
#define KBD_F7      0x126
#define KBD_F8      0x127
#define KBD_F9      0x128
#define KBD_F10     0x129
#define KBD_F11     0x12a
#define KBD_F12     0x12b


#define KBD_CTRL_C  0x200

#define KBD_SHIFT   1
#define KBD_CTRL    2
#define KBD_ALT     4

void handle_trm(int, int, int, int, int);
void free_all_itrms(void);
void resize_terminal(void *);
void dispatch_special( char *);
void kbd_ctrl_c(void);
int is_blocked(void);
void itrm_safe_abort(void);


#endif
