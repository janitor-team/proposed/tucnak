/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __TSDL_H
#define __TSDL_H

#include "header.h"

#ifdef Z_HAVE_SDL

#define SDL_USERDATA_IN_FILTEREVENTx

#define FONT_W zsdl->font_w
#define FONT_H zsdl->font_h 

#if defined(Z_MSC_MINGW) || defined(Z_MACOS)
#define EVENTS_IN_SAME_THREAD
#endif

/*struct tsdl_setvideomode{
	int w, h, bpp, flags;
	SDL_Surface *screen;
	int done;
};*/

struct tsdl{
    SDL_Surface *screen;
	int fullscreen;
	SDL_Surface *icon, *mast, *home, *zoomin, *zoomout, *key, *xfer;
    int bpp;
    SDL_Color colors[256];
    SDL_Rect termarea;
    int gr[16];
    int termcol[16];
    int yellow, yellow2, red, green, magenta;
    int cursor;
    GThread *event_thread;
    int event_thread_break;
    int mouse_drag, drag_buttons;
    int eventpipestate, resizeevents, resize_w, resize_h;
    MUTEX_DEFINE(eventpipestate);
/*    GMutex *eventpipestate_mutex;
#ifdef LEAK_DEBUG_LIST
    char *eventpipestate_file;
    int eventpipestate_line;
#endif        */
    gchar *title;
    int mouse_x, mouse_y;
    int eventpipe[2];
    iconv_t iconvhandle;
#ifdef EVENTS_IN_SAME_THREAD
	int event_timer_id;
#endif
	int progress_w;
    int screen_w, screen_h;
	int window_w, window_h;
	int old_mouse_x, old_mouse_y;
    int altsym;
	int ignore_progress;
};


extern struct tsdl *sdl;
extern struct zzsdl *zsdl;

struct tsdl *init_sdl(void);
void sdl_stop_event_thread(void);
void free_sdl(void);
SDL_Rect *new_rect(int x, int y, int w, int h);
void free_rect(SDL_Rect *rect);
void sdl_pre_info(void);
void sdl_info(GString *gs);
int sdl_setvideomode(int w, int h, int init);
void sdl_redraw_screen(void);
int sdl_get_terminal_size(int, int *, int *);
int sdl_attach_terminal(int, int, int);
gpointer sdl_event_thread(gpointer handle);
#ifdef SDL_USERDATA_IN_FILTEREVENT
int sdl_filter_events(void *userdata, const SDL_Event *ev);
#else    
int sdl_filter_events(const SDL_Event *ev);
#endif    

int sdl_set_title(gchar *title);
void sdl_screenshot(int topwindow);
void zsdl_test_bars(SDL_Surface *surface, int ofs);
#endif

void progress(char *m, ...);
void sdl_force_redraw(void);
void tsdl_change_font(char how);
void tsdl_optimal_font(void);

#endif
