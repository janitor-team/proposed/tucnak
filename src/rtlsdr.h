/*
    Tucnak - VHF contest log
    Copyright (C) 2018 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __RTLSDR_H
#define __RTLSDR_H

#include "header.h"
#include "dsp.h"

#ifdef HAVE_LIBRTLSDR

#include <rtl-sdr.h>

int rtlsdr_open2(struct dsp *dsp, int rec);
int rtlsdr_close2(struct dsp *dsp);
int rtlsdr_write(struct dsp *dsp, void *data, int frames);
int rtlsdr_read(struct dsp *dsp, void *data, int frames);
int rtlsdr_set_sdr_format(struct dsp *dsp, int frames, int speed, int rec);

#endif


#endif

