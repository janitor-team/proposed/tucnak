/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __WIKI_H
#define __WIKI_H

#include "header.h"
struct band;
struct zhttp;

struct wiki{
	struct zhttp *http;
	char *apiurl;
	char *status;
	char *edittoken;
	char *text;
	GPtrArray *images;
	int done;
};

void wiki_header(GString *gs);
void wiki_band_header(GString *gs, struct band *b, int flags);
void wiki_footer(GString *gs);
void wiki_complete(GString *gs, int flags);
int export_all_bands_wiki(void);

void wiki_status(struct wiki *wiki, char *fmt, ...);
void wiki_upload(void *arg);
void wiki_info(void *arg1);
void wiki_schedule(struct wiki *wiki);

#endif
