/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __SESSION_H
#define __SESSION_H

#include <libzia.h>
#include <header.h>
#include <terminal.h>

#define TMPQ aband->tmpqsos[0]

struct inputln;
struct band; 
struct skedwin_data;
struct zhttp;
struct vhfcontestnet;
struct slovhfnet;
struct wiki;

/* also used for ctest->cq->type */
enum modes{ /* TX_RX */
    MOD_NONE = 0,
    MOD_SSB_SSB,
    MOD_CW_CW,
    MOD_SSB_CW,
    MOD_CW_SSB,
    MOD_AM_AM,
    MOD_FM_FM,
    MOD_RTTY_RTTY,
    MOD_SSTV_SSTV,
    MOD_ATV_ATV /* 9 */
};

enum extcq { EC_NONE, EC_ODD, EC_EVEN, EC_1ST, EC_2ND };

struct session {
    struct window *win;                                             
    int id;
    int exit_query;
    
    GPtrArray *subwins; 
    struct subwin *ontop, *ontop2;
    gint focused, height1, height2, y1, y2;
    
    gint timer_id;
    struct inputln *il;  /* inputline without contest for C_W testing ETC.. */
    GPtrArray *qs;
    MUTEX_DEFINE(qs);
//    GMutex *qs_mutex;
//#ifdef LEAK_DEBUG_LIST    
//    char *qs_file;
//    int qs_line;
//#endif                                               
    GThread *qs_thread;
    int qs_thread_break;
    int qs_max_matches;
    gchar *qs_str;     /* modified only from main thread without cwqs worker thread, no mutex needed */
    gchar *qs_master;
    enum modes mode;  
    gint last_cq_timer_id;
    struct cq *last_cq;
    struct cq *ac_cq;
    gint extcq_break_timer_id;
    enum extcq extcq;
    int tx;
#ifdef Z_HAVE_SDL    
    SDL_Surface *icon, *oldicon;
#else
    void *icon, *oldicon;
#endif    
    GHashTable *hicalls; /* highlighted callsigns in shell */
    int tune;
    int redraw_timer_id;
#ifdef Z_HAVE_SDL
    int update_rotar_timer_id;
    int bat_timer_id;
#endif
    struct skedwin_data *skedwdata;
    int skedw_timer_id;
    struct dw_item *adwi;
    char *adxc;
	char *asunriseset;
    int aqrb, aqtf;
#ifdef FALL
    int fall_id;
#endif
	int sel_start, sel_stop, sel_min, sel_max;
	struct zhttp *update;
#ifdef Z_HAVE_SDL
	struct zosk *osk;
#endif
	struct vhfcontestnet *vcn;
	struct slovhfnet *slovhf;
	struct wiki *wiki;
	double myh, myw;
	char *callunder;
};


extern struct session *gses;
extern struct window *gseswin;

struct session *create_session(struct window *win);
void *create_session_info(int, char *, int *);
void rxtx(void);
void esc(void);
void rx(void);
int preferred_func(struct event *ev);
void win_func(struct window *, struct event *, int);

void call_ctx_menu(int evx, int evy);
void draw_root_window(void);
void send_event(struct event *ev);
void add_date_time(struct band *band);
void fix_date_time(struct band *band);
void process_input(void *enterdata,char *text, int cq);

void draw_time(void);
void time_func(void *);
void print_tucnak(int x, int y);
void net_timer_redraw(void *);
void clear_hicalls(void);
int load_hicalls_from_file(char *filename);
int save_hicalls_to_file(char *filename);
void after_callsign(struct band *band,char *c, char *oldtmpqcall);
void after_exc(struct band *band,char *c);
void after_locator(struct band *band,char *c);
void after_nr(struct band *band,char *c);
#ifdef FALL
void fall_func(void *arg);
#endif
//gboolean free_hicalls(gpointer key, gpointer value, gpointer user_data);
void process_input_no_contest(void *enterdata, gchar *text, int cq);
void update_hw(void);


#endif
