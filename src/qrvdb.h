/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __QRVDB_H
#define __QRVDB_H

#include "header.h"
struct qso;
struct subwin;
struct event;
struct band;


#define QRVSSIZE 30

struct qrv_item{
    gchar *call;
    gchar *wwl;
    int bands_qrv;        // qrv on band
    int wkd[32];          // 0  not wkd or no info about
                          // <0 wkd by others
                          // >0 wkd by me, maybe also by others
    gchar *text;
    time_t kst_time;         // time when spot was caught on KST
    
    int bands_wkd;        // wkd in this contest
    int kx,ky;
    double qrb;
    int qtf;
    double weight;

    // AS
    time_t ac_start;
    int ac_interval;
    int ac_n;
	//int ac_drawn; 
};

struct qrvdb{
    ZPtrArray *qrvs;      // of struct qrv_item
    MUTEX_DEFINE(qrvs);
    GHashTable *hash;

    int def_bands;
    double antchar[360];
    int beamwidth;
    double gst[360];
    int (*sort)(const void*, const void *); 
    int (*sort2)(const void*, const void *); 
    int showall;
    char search[QRVSSIZE];
	int sortdir;
};

extern struct qrvdb *qrv;

struct qrvdb *init_qrv(void);
void clear_qrv(struct qrvdb *qrvdb);
void free_qrv(struct qrvdb *qrvdb);

struct qrv_item *qrv_add(gchar *call, gchar *wwl, int qrv_int, int wkd[32], const gchar *text, time_t kst_time);
struct qrv_item *qrv_kst_add(char *call, char *wwl, char *fullname);

void qrv_delete(char *call, int bi);
int load_qrv_files(struct qrvdb *qrvdb);
int save_qrv_to_file(char *filename);

int load_qrv_from_file(struct qrvdb *qrvdb, gchar *filename);
void load_one_qrv(struct qrvdb *qrvdb, gchar *s);

void qrv_set_wkd(struct qrvdb *qrvdb, struct qso *qso);
void qrv_recalc_wkd(struct qrvdb *qrvdb);

void qrv_compute_qrbqtf(struct qrv_item *q);
void qrv_recalc_qrbqtf(struct qrvdb *qrvdb); 
void qrv_recalc_gst(struct qrvdb *qrvdb); 

int qrv_skip(struct qrv_item *qi, int bi, int setdrawn);
void sw_qrv_seek(struct subwin *sw, int value);

struct qrv_item *qrv_get(struct qrvdb *qrvdb, char *call);
int qrv_compare_ac_start(const void *a, const void *b);
int qrv_compare_ac_int(const void *a, const void *b);
int qrv_compare_ac_n(const void *a, const void *b);
int qrv_compare_call(const void *a, const void *b);
int qrv_compare_wwl(const void *a, const void *b);
int qrv_compare_wkd(const void *a, const void *b);
int qrv_compare_qrb(const void *a, const void *b);
int qrv_compare_qtf(const void *a, const void *b);
int qrv_compare_kst_time(const void *a, const void *b);
int qrv_compare_text(const void *a, const void *b);

void qrv_touch(struct qrvdb *qrvdb, char *call);

void edit_qrv(void *arg);

int sw_qrv_kbd_func(struct subwin *sw, struct event *ev, int fw);
int sw_qrv_mouse_func(struct subwin *sw, struct event *ev, int fw);
void sw_qrv_redraw(struct subwin *sw, struct band *band, int flags);
void sw_qrv_check_bounds(struct subwin *sw);
void sw_qrv_raise(struct subwin *sw);
void sw_qrv_sort(struct qrvdb *qrvdb);
void qrv_read_line(char *);

int sw_qrv_color(char *call);

struct zstring *qrv_format(struct qrv_item *qi);
void qrv_sort(int (*fce)(const void*, const void *));

void menu_qrv_add_contest(void *menudata, void *itdata);


#endif
