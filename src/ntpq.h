/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __NTPQ_H
#define __NTPQ_H

#ifdef Z_UNIX

struct ntpq {
    pid_t pid;
    int rfd;
    GString *buf;
    int good, oldgood;
    int timer;
};

extern struct ntpq *ntpq;

struct ntpq *init_ntpq(void);
void free_ntpq(struct ntpq *ntpq);
void ntpq_timer(void *arg);
void ntpq_read_handler(void *arg);

#endif
#endif
