/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __PA_H
#define __PA_H

#include "header.h"
#include "dsp.h"

#ifdef HAVE_PORTAUDIO

extern int pa_initialised;
void pa_info(GString *gs);

#ifdef HAVE_SNDFILE
int pa_set_format(struct dsp *dsp, SF_INFO *sfinfo, int rec);
#endif
int pa_open2(struct dsp *dsp, int rec);
int pa_close2(struct dsp *dsp);
int pa_write2(struct dsp *dsp, void *data, int len);
int pa_read2(struct dsp *dsp, void *data, int len);
int pa_reset2(struct dsp *dsp);
int pa_sync2(struct dsp *dsp);
int pa_get_sources(GString *labels);
int pa_set_sdr_format(struct dsp *dsp, int frames, int speed, int rec);

#endif

#endif
