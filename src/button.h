/*
    Tucnak - VHF contest log
    Copyright (C) 2012-2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.


*/

#include "header.h"

#ifndef __BUTTON_H
#define __BUTTON_H


struct button{
	int w, h;
#ifdef Z_HAVE_SDL
    SDL_Surface *surface;
#endif
    void (*fn)(void *);
    void *data;
    int x, y; 
	int font_h, fcolor, bcolor;
	char *text;
};

struct button *init_button(
#ifdef Z_HAVE_SDL
        SDL_Surface *screen, 
#endif
        int x, int y, void (*fn)(void *), void *data);
void free_button(struct button *b);

void buttons_clear(GPtrArray *buttons);
#ifdef Z_HAVE_SDL
void buttons_redraw(GPtrArray *buttons, SDL_Surface *dst);
int buttons_mouse(GPtrArray *buttons, int mb, int mx, int my);
void button_bitmap(struct button *b, SDL_Surface *surface);
void button_draw(struct button *b, SDL_Surface *dst);
#endif

void button_text(struct button *b, int font_h, int fcolor, int bcolor, char *text);
void button_free(struct button *b);


#endif
