/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2022  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"
#include "bfu.h"
#include "cwdaemon.h"
#include "dsp.h"
#include "fifo.h"
#include "qsodb.h"
#include "main.h"
#include "menu.h"
#include "rc.h"
#include "rotar.h"
#include "trig.h"
#include "tsdl.h"
#include "session.h"
#include "subwin.h"



struct config *cfg;
int opt_g=0, opt_i=0, opt_m=0, opt_s=0, opt_t=0, debug_keyboard=0, opt_x=0;
long long tl;
char *opt_tucnakrc = NULL;
	
    
enum {
    SYMBOL_OBSOLETE     = G_TOKEN_LAST + 10000,
    SYMBOL_MY_FIRST     = G_TOKEN_LAST + 1,
    
    SYMBOL_PCALL        = G_TOKEN_LAST + 1,
    SYMBOL_PWWLO        = G_TOKEN_LAST + 2,
    SYMBOL_PEXCH        = G_TOKEN_LAST + 3,
    SYMBOL_PADR1        = G_TOKEN_LAST + 4,
    SYMBOL_PADR2        = G_TOKEN_LAST + 5,
    SYMBOL_PCLUB        = G_TOKEN_LAST + 6,
    SYMBOL_RNAME        = G_TOKEN_LAST + 7,
    SYMBOL_RCALL        = G_TOKEN_LAST + 8,
    SYMBOL_RADR1        = G_TOKEN_LAST + 9,
    SYMBOL_RADR2        = G_TOKEN_LAST + 10,
    SYMBOL_RPOCO        = G_TOKEN_LAST + 11,
    SYMBOL_RCITY        = G_TOKEN_LAST + 12,
    SYMBOL_RCOUN        = G_TOKEN_LAST + 13,
    SYMBOL_RPHON        = G_TOKEN_LAST + 14,
    SYMBOL_RHBBS        = G_TOKEN_LAST + 15,
    SYMBOL_RS           = G_TOKEN_LAST + 16,
    SYMBOL_RST          = G_TOKEN_LAST + 17,
    SYMBOL_QSOP_M       = G_TOKEN_LAST + 18,
    SYMBOL_TOTAL_M      = G_TOKEN_LAST + 19,
    
    SYMBOL_CQ           = G_TOKEN_LAST + 20,
    SYMBOL_CW_STR       = G_TOKEN_LAST + 21,
    SYMBOL_CW_SPEED     = G_TOKEN_LAST + 22,
	SYMBOL_CW_REPEAT    = G_TOKEN_LAST + 23,
	SYMBOL_CW_BREAKABLE = G_TOKEN_LAST + 24,
    SYMBOL_CW_TS        = G_TOKEN_LAST + 25,
    SYMBOL_CW_ALLOWUND  = G_TOKEN_LAST + 26,
    SYMBOL_SSB_FILE     = G_TOKEN_LAST + 27,
    SYMBOL_SSB_TS       = G_TOKEN_LAST + 28,
	SYMBOL_SSB_REPEAT   = G_TOKEN_LAST + 29,
	SYMBOL_SSB_BREAKABLE = G_TOKEN_LAST + 30,
    
    SYMBOL_STARTBAND    = G_TOKEN_LAST + 40,
    SYMBOL_PBAND        = G_TOKEN_LAST + 41,
    SYMBOL_BANDCHAR     = G_TOKEN_LAST + 42,
    SYMBOL_QRV          = G_TOKEN_LAST + 43,
    SYMBOL_READONLY     = G_TOKEN_LAST + 44,
    SYMBOL_PSECT        = G_TOKEN_LAST + 45,
    SYMBOL_STXEQ        = G_TOKEN_LAST + 46,
    SYMBOL_SPOWE        = G_TOKEN_LAST + 47,
    SYMBOL_SRXEQ        = G_TOKEN_LAST + 48,
    SYMBOL_SANTE        = G_TOKEN_LAST + 49,
    SYMBOL_SANTH        = G_TOKEN_LAST + 50,
    SYMBOL_MOPE1        = G_TOKEN_LAST + 51,
    SYMBOL_MOPE2        = G_TOKEN_LAST + 52,
    SYMBOL_REMARKS      = G_TOKEN_LAST + 53,
    
    SYMBOL_OK_SECTION_SINGLE    = G_TOKEN_LAST + 54,
    SYMBOL_OK_SECTION_MULTI     = G_TOKEN_LAST + 55,
    SYMBOL_ADIFBAND             = G_TOKEN_LAST + 56,
    SYMBOL_CBRBAND              = G_TOKEN_LAST + 57,
    SYMBOL_STFBAND              = G_TOKEN_LAST + 58,
    SYMBOL_QRG_MIN      = G_TOKEN_LAST + 59,
    SYMBOL_QRG_MAX      = G_TOKEN_LAST + 60,
    SYMBOL_BANDMULTI    = G_TOKEN_LAST + 61,
    SYMBOL_SKEDQRG      = G_TOKEN_LAST + 62,
    SYMBOL_BAND_LO      = G_TOKEN_LAST + 63,
    SYMBOL_BAND_SW      = G_TOKEN_LAST + 64,
    SYMBOL_OPSECT       = G_TOKEN_LAST + 65,
    SYMBOL_WWLRADIUS    = G_TOKEN_LAST + 66,
    
    SYMBOL_AS_DAQ       = G_TOKEN_LAST + 70,
    SYMBOL_AS_DAM       = G_TOKEN_LAST + 71,
    SYMBOL_AS_DFS       = G_TOKEN_LAST + 72,
    SYMBOL_AS_FAQ       = G_TOKEN_LAST + 73,
    SYMBOL_AS_FAM       = G_TOKEN_LAST + 74,
    SYMBOL_AS_FFS       = G_TOKEN_LAST + 75,
    SYMBOL_AS_FMO       = G_TOKEN_LAST + 76,
    SYMBOL_AS_FPA       = G_TOKEN_LAST + 77,
    SYMBOL_AS_FMC       = G_TOKEN_LAST + 78,
    
    SYMBOL_IF_IGNORE      = G_TOKEN_LAST + 80,
    SYMBOL_IP_IGNORE      = G_TOKEN_LAST + 81,
    SYMBOL_IP_ANNOUNCE    = G_TOKEN_LAST + 82,
	SYMBOL_REMOTE_ENABLE  = G_TOKEN_LAST + 83,
    SYMBOL_REMOTE_HOST    = G_TOKEN_LAST + 84,
    SYMBOL_REMOTE_PORT    = G_TOKEN_LAST + 85,
    SYMBOL_REMOTE_PASS    = G_TOKEN_LAST + 86,
	SYMBOL_MASTERPRIORITY = G_TOKEN_LAST + 87,
    SYMBOL_TRACE_BCAST    = G_TOKEN_LAST + 88,
    SYMBOL_TRACE_SOCK     = G_TOKEN_LAST + 89,
    SYMBOL_TRACE_RECV     = G_TOKEN_LAST + 90,
    SYMBOL_TRACE_SEND     = G_TOKEN_LAST + 91,
    SYMBOL_TRACE_QSOS     = G_TOKEN_LAST + 92,
    SYMBOL_TRACE_RIG      = G_TOKEN_LAST + 93,
    SYMBOL_TRACE_SDEV     = G_TOKEN_LAST + 94,
    SYMBOL_TRACE_KEYS     = G_TOKEN_LAST + 95,
    SYMBOL_LANGUAGE       = G_TOKEN_LAST + 96,

    SYMBOL_CWDA_TYPE     = G_TOKEN_LAST + 100,
    SYMBOL_CWDA_DEVICE   = G_TOKEN_LAST + 101,
    SYMBOL_CWDA_HOST     = G_TOKEN_LAST + 102,
    SYMBOL_CWDA_UDP      = G_TOKEN_LAST + 103,
    SYMBOL_CWDA_IOPORT   = G_TOKEN_LAST + 104,
    SYMBOL_CWDA_SPEED    = G_TOKEN_LAST + 105,
    SYMBOL_CWDA_WEIGHT   = G_TOKEN_LAST + 106,
    SYMBOL_CWDA_MINWPM   = G_TOKEN_LAST + 107,
    SYMBOL_CWDA_MAXWPM   = G_TOKEN_LAST + 108,
    SYMBOL_CWDA_SPK      = G_TOKEN_LAST + 109,
    SYMBOL_CWDA_LEADIN   = G_TOKEN_LAST + 110,
    SYMBOL_CWDA_TAIL     = G_TOKEN_LAST + 111,
    SYMBOL_CWDA_VID      = G_TOKEN_LAST + 112,
    SYMBOL_CWDA_PID      = G_TOKEN_LAST + 113,
    SYMBOL_CWDA_AUTGIVE  = G_TOKEN_LAST + 114,
    SYMBOL_WK_WK2        = G_TOKEN_LAST + 115,
    SYMBOL_WK_USEPOT     = G_TOKEN_LAST + 116,
    SYMBOL_WK_USEBUT     = G_TOKEN_LAST + 117,
    SYMBOL_WK_KEYMODE    = G_TOKEN_LAST + 118,
    SYMBOL_WK_SWAP       = G_TOKEN_LAST + 119,

    SYMBOL_SSBD_TYPE       = G_TOKEN_LAST + 120,
    SYMBOL_SSBD_RECORD     = G_TOKEN_LAST + 121,
    SYMBOL_SSBD_MAXMIN     = G_TOKEN_LAST + 122,
    SYMBOL_SSBD_DISKFREE   = G_TOKEN_LAST + 123,
    SYMBOL_SSBD_FORMAT     = G_TOKEN_LAST + 124,
    SYMBOL_SSBD_CHANNELS   = G_TOKEN_LAST + 125,
    SYMBOL_SSBD_SAMPLERATE = G_TOKEN_LAST + 126,
    SYMBOL_SSBD_PLEV       = G_TOKEN_LAST + 127,
    SYMBOL_SSBD_RLEV       = G_TOKEN_LAST + 128,
    SYMBOL_SSBD_TEMPLATE   = G_TOKEN_LAST + 129,

    SYMBOL_SSBD_PCM_PLAY   = G_TOKEN_LAST + 130,
    SYMBOL_SSBD_PCM_REC    = G_TOKEN_LAST + 131,
    SYMBOL_SSBD_PERIOD_T   = G_TOKEN_LAST + 132,
    SYMBOL_SSBD_BUFFER_T   = G_TOKEN_LAST + 133,
    SYMBOL_SSBD_ALSA_MIXER = G_TOKEN_LAST + 134,
    SYMBOL_SSBD_ALSA_SRC   = G_TOKEN_LAST + 135,

    SYMBOL_SSBD_DSP        = G_TOKEN_LAST + 136,
    SYMBOL_SSBD_MAXFRAG    = G_TOKEN_LAST + 137,
    SYMBOL_SSBD_MIXER      = G_TOKEN_LAST + 138,
    SYMBOL_SSBD_OSS_SRC    = G_TOKEN_LAST + 139,
    SYMBOL_SSBD_RECSRC     = G_TOKEN_LAST + 140,

    SYMBOL_SSBD_HOST       = G_TOKEN_LAST + 141,
    SYMBOL_SSBD_UDP        = G_TOKEN_LAST + 142,
    SYMBOL_SSBD_PA_PLAY    = G_TOKEN_LAST + 143,
    SYMBOL_SSBD_PA_REC     = G_TOKEN_LAST + 144,

    SYMBOL_SSBD_PIPE       = G_TOKEN_LAST + 145,
    SYMBOL_SSBD_COMMAND    = G_TOKEN_LAST + 146,
    
    SYMBOL_TERM              = G_TOKEN_LAST + 150,
    SYMBOL_TERM_MODE         = G_TOKEN_LAST + 151,
    SYMBOL_TERM_M11_HACK     = G_TOKEN_LAST + 152,
    SYMBOL_TERM_RESTRICT_852 = G_TOKEN_LAST + 153,
    SYMBOL_TERM_BLOCK_CURSOR = G_TOKEN_LAST + 154,
    SYMBOL_TERM_COL          = G_TOKEN_LAST + 155,
    SYMBOL_TERM_UTF_8_IO     = G_TOKEN_LAST + 156,
    SYMBOL_TERM_CHARSET      = G_TOKEN_LAST + 157,

    SYMBOL_ROTAR            = G_TOKEN_LAST + 160,
    SYMBOL_ROT_DESC         = G_TOKEN_LAST + 161,
    SYMBOL_ROT_TYPE         = G_TOKEN_LAST + 162,
    SYMBOL_ROT_FILENAME     = G_TOKEN_LAST + 163,
    SYMBOL_ROT_HOSTNAME     = G_TOKEN_LAST + 164,
    SYMBOL_ROT_PORT         = G_TOKEN_LAST + 165,
    SYMBOL_ROT_VID          = G_TOKEN_LAST + 166,
    SYMBOL_ROT_PID          = G_TOKEN_LAST + 167,
    SYMBOL_ROT_SERIAL       = G_TOKEN_LAST + 168,
    SYMBOL_ROT_TIMEOUT_MS   = G_TOKEN_LAST + 169,
    SYMBOL_ROT_BEAMWIDTH    = G_TOKEN_LAST + 170,
    SYMBOL_ROT_SADDR        = G_TOKEN_LAST + 171,
	SYMBOL_ROT_MODEL        = G_TOKEN_LAST + 172,
	SYMBOL_ROT_SPEED        = G_TOKEN_LAST + 173,
    SYMBOL_ROT_REM_ROTSTR   = G_TOKEN_LAST + 174,
	SYMBOL_ROT_POLL_MS		= G_TOKEN_LAST + 175,
	SYMBOL_ROT_OFFSET		= G_TOKEN_LAST + 176,
    
    
    SYMBOL_SW                = G_TOKEN_LAST + 180,
    SYMBOL_SW_TYPE           = G_TOKEN_LAST + 181,
    SYMBOL_SW_COMMAND        = G_TOKEN_LAST + 182,
    SYMBOL_SW_AUTORUN        = G_TOKEN_LAST + 183,
    SYMBOL_SW_RESPAWN_TIME   = G_TOKEN_LAST + 184,
    
	SYMBOL_RIG                = G_TOKEN_LAST + 190,
	SYMBOL_RIG_ENABLED        = G_TOKEN_LAST + 191, 
	SYMBOL_RIG_DESC			  = G_TOKEN_LAST + 192,
    SYMBOL_RIG_FILENAME       = G_TOKEN_LAST + 193,
    SYMBOL_RIG_MODEL          = G_TOKEN_LAST + 194,
    SYMBOL_RIG_SPEED          = G_TOKEN_LAST + 195,
	SYMBOL_RIG_HANDSHAKE_NONE = G_TOKEN_LAST + 196,
    SYMBOL_RIG_CIVADDR        = G_TOKEN_LAST + 197,
    SYMBOL_RIG_LO             = G_TOKEN_LAST + 198,
    SYMBOL_RIG_SSBCW_SHIFT    = G_TOKEN_LAST + 199,
    SYMBOL_RIG_POLL_MS        = G_TOKEN_LAST + 200,
    SYMBOL_RIG_QRG_R2T        = G_TOKEN_LAST + 201,
    SYMBOL_RIG_QRG_T2R        = G_TOKEN_LAST + 202,
    SYMBOL_RIG_MODE_T2R       = G_TOKEN_LAST + 203,
    SYMBOL_RIG_CLR_RIT        = G_TOKEN_LAST + 204,
	SYMBOL_RIG_PTT_T2R        = G_TOKEN_LAST + 205,
    SYMBOL_RIG_VERBOSE        = G_TOKEN_LAST + 206,

    SYMBOL_LOGLINES          = G_TOKEN_LAST + 210,
    SYMBOL_SKEDCOUNT         = G_TOKEN_LAST + 212,
    SYMBOL_TAKEOFF           = G_TOKEN_LAST + 213,
    SYMBOL_OPERATOR          = G_TOKEN_LAST + 214,
    SYMBOL_GLOBAL_OPERATOR   = G_TOKEN_LAST + 215,
    SYMBOL_GFX_X             = G_TOKEN_LAST + 216,
    SYMBOL_GFX_Y             = G_TOKEN_LAST + 217,
    SYMBOL_FONTHEIGHT        = G_TOKEN_LAST + 218,
    SYMBOL_SLASHKEY          = G_TOKEN_LAST + 219,
    SYMBOL_NTPQ              = G_TOKEN_LAST + 220,
    SYMBOL_DSSAVER           = G_TOKEN_LAST + 221,
	SYMBOL_SPLITHEIGHT       = G_TOKEN_LAST + 222,
	SYMBOL_FULLSCREEN        = G_TOKEN_LAST + 223,
	SYMBOL_MAXIMIZED         = G_TOKEN_LAST + 224,
	SYMBOL_USETOUCH			 = G_TOKEN_LAST + 225,
	SYMBOL_TOUCHPOS			 = G_TOKEN_LAST + 226,
	SYMBOL_ALTSYMS			 = G_TOKEN_LAST + 227,
	SYMBOL_NOLOCKS			 = G_TOKEN_LAST + 228,
	SYMBOL_PORTRAIT			 = G_TOKEN_LAST + 229,
	SYMBOL_REVERSE			 = G_TOKEN_LAST + 230,
	SYMBOL_INVERSE			 = G_TOKEN_LAST + 231,
	SYMBOL_ADIF_EXPORT_MODE_T2R	 = G_TOKEN_LAST + 232,
	SYMBOL_LOGDIR_PATH       = G_TOKEN_LAST + 233,

	SYMBOL_DXC_HOST			 = G_TOKEN_LAST + 240,
	SYMBOL_DXC_PORT			 = G_TOKEN_LAST + 241,
	SYMBOL_DXC_USER			 = G_TOKEN_LAST + 242,
	SYMBOL_DXC_PASS			 = G_TOKEN_LAST + 243,
    SYMBOL_KST_USER			 = G_TOKEN_LAST + 244,
	SYMBOL_KST_PASS			 = G_TOKEN_LAST + 245,
	SYMBOL_KST_NAME			 = G_TOKEN_LAST + 246,
	SYMBOL_KST_MINQRB		 = G_TOKEN_LAST + 247,
	SYMBOL_KST_MAXQRB		 = G_TOKEN_LAST + 248,
	SYMBOL_SLOVHF_USER		 = G_TOKEN_LAST + 249,

    SYMBOL_HTTPD_ENABLE      = G_TOKEN_LAST + 250,
    SYMBOL_HTTPD_PORT        = G_TOKEN_LAST + 251,
    SYMBOL_HTTPD_SHOW_PRIV   = G_TOKEN_LAST + 252,
    SYMBOL_HTTPD_REFRESH     = G_TOKEN_LAST + 253,
    SYMBOL_HTTPD_SHOW_LINKS  = G_TOKEN_LAST + 254,

    SYMBOL_AC_ENABLE         = G_TOKEN_LAST + 260,
    SYMBOL_AC_URL            = G_TOKEN_LAST + 261,
    SYMBOL_AC_KFACTOR        = G_TOKEN_LAST + 262,
    SYMBOL_AC_ARWIDTH        = G_TOKEN_LAST + 263,
    SYMBOL_AC_MINELEV        = G_TOKEN_LAST + 264,
    SYMBOL_AC_MAXELEV        = G_TOKEN_LAST + 265,
    SYMBOL_AC_MINDUR         = G_TOKEN_LAST + 266,
    SYMBOL_AC_MAXDELTA       = G_TOKEN_LAST + 267,
    SYMBOL_AC_MINALT         = G_TOKEN_LAST + 268,
    
	SYMBOL_SDR_ENABLE        = G_TOKEN_LAST + 270,
	SYMBOL_SDR_CW            = G_TOKEN_LAST + 271,
	SYMBOL_SDR_LSB           = G_TOKEN_LAST + 272,
	SYMBOL_SDR_SPEED         = G_TOKEN_LAST + 273,
	SYMBOL_SDR_AF_SPEED      = G_TOKEN_LAST + 274,
	SYMBOL_SDR_BLOCK         = G_TOKEN_LAST + 275,
	SYMBOL_SDR_ZERO          = G_TOKEN_LAST + 276,
	SYMBOL_SDR_CW_LOW        = G_TOKEN_LAST + 277,
	SYMBOL_SDR_CW_HIGH       = G_TOKEN_LAST + 278,
	SYMBOL_SDR_SSB_LOW       = G_TOKEN_LAST + 279,
	SYMBOL_SDR_SSB_HIGH      = G_TOKEN_LAST + 280,
	SYMBOL_SDR_REC_DSP_TYPE  = G_TOKEN_LAST + 281,
	SYMBOL_SDR_PLAY_DSP_TYPE = G_TOKEN_LAST + 282,
	SYMBOL_SDR_PA_REC        = G_TOKEN_LAST + 283,
	SYMBOL_SDR_PA_PLAY       = G_TOKEN_LAST + 284,
	SYMBOL_SDR_PCM_REC       = G_TOKEN_LAST + 285,
	SYMBOL_SDR_PCM_PLAY      = G_TOKEN_LAST + 286,
	SYMBOL_SDR_SNDFILENAME   = G_TOKEN_LAST + 287,
	SYMBOL_SDR_REMOTERX      = G_TOKEN_LAST + 288,
	SYMBOL_SDR_AF_FILENAME   = G_TOKEN_LAST + 289,
	SYMBOL_SDR_IQCOMP        = G_TOKEN_LAST + 290,

	SYMBOL_WIKI_URL          = G_TOKEN_LAST + 300,
	SYMBOL_WIKI_USER         = G_TOKEN_LAST + 301,
	SYMBOL_WIKI_PASS         = G_TOKEN_LAST + 302,
	//SYMBOL_WIKI_PAGE         = G_TOKEN_LAST + 303,
	SYMBOL_WIKI_MAP          = G_TOKEN_LAST + 304,
	SYMBOL_WIKI_CHART        = G_TOKEN_LAST + 305,
	//SYMBOL_WIKI_OVERWRITE    = G_TOKEN_LAST + 306,

    SYMBOL_RAIN_ENABLE        = G_TOKEN_LAST + 306,
    SYMBOL_RAIN_METEOX        = G_TOKEN_LAST + 307,
    SYMBOL_RAIN_WETTERONLINE  = G_TOKEN_LAST + 308,
    SYMBOL_RAIN_CHMI          = G_TOKEN_LAST + 309,
	SYMBOL_RAIN_WEATHERONLINE = G_TOKEN_LAST + 310,
	SYMBOL_RAIN_RAINVIEWER    = G_TOKEN_LAST + 311,
    SYMBOL_RAIN_DEBUG         = G_TOKEN_LAST + 312,
	SYMBOL_RAIN_MAXQRB        = G_TOKEN_LAST + 313,
	SYMBOL_RAIN_MINCOLOR      = G_TOKEN_LAST + 314,
	SYMBOL_RAIN_MINSCPDIST    = G_TOKEN_LAST + 315,
    
    SYMBOL_SOTA_USER         = G_TOKEN_LAST + 316,
    SYMBOL_SOTA_PASS         = G_TOKEN_LAST + 317,

    SYMBOL_MY_LAST            = G_TOKEN_LAST + 318
};

void add_symbols(GScanner *scanner){
    g_scanner_scope_add_symbol(scanner,0, "pcall",GINT_TO_POINTER(SYMBOL_PCALL)); 
    g_scanner_scope_add_symbol(scanner,0, "pwwlo",GINT_TO_POINTER(SYMBOL_PWWLO)); 
    g_scanner_scope_add_symbol(scanner,0, "pexch",GINT_TO_POINTER(SYMBOL_PEXCH)); 
    
    g_scanner_scope_add_symbol(scanner,0, "padr1",GINT_TO_POINTER(SYMBOL_PADR1)); 
    g_scanner_scope_add_symbol(scanner,0, "padr2",GINT_TO_POINTER(SYMBOL_PADR2)); 
    g_scanner_scope_add_symbol(scanner,0, "pclub",GINT_TO_POINTER(SYMBOL_PCLUB)); 
    
    g_scanner_scope_add_symbol(scanner,0, "rname",GINT_TO_POINTER(SYMBOL_RNAME)); 
    g_scanner_scope_add_symbol(scanner,0, "rcall",GINT_TO_POINTER(SYMBOL_RCALL)); 
    g_scanner_scope_add_symbol(scanner,0, "radr1",GINT_TO_POINTER(SYMBOL_RADR1)); 
    g_scanner_scope_add_symbol(scanner,0, "radr2",GINT_TO_POINTER(SYMBOL_RADR2)); 
    g_scanner_scope_add_symbol(scanner,0, "rpoco",GINT_TO_POINTER(SYMBOL_RPOCO)); 
    g_scanner_scope_add_symbol(scanner,0, "rcity",GINT_TO_POINTER(SYMBOL_RCITY)); 
    g_scanner_scope_add_symbol(scanner,0, "rcoun",GINT_TO_POINTER(SYMBOL_RCOUN)); 
    g_scanner_scope_add_symbol(scanner,0, "rphon",GINT_TO_POINTER(SYMBOL_RPHON)); 
    g_scanner_scope_add_symbol(scanner,0, "rhbbs",GINT_TO_POINTER(SYMBOL_RHBBS)); 
    
    g_scanner_scope_add_symbol(scanner,0,"default_rs",  GINT_TO_POINTER(SYMBOL_RS)); 
    g_scanner_scope_add_symbol(scanner,0,"default_rst", GINT_TO_POINTER(SYMBOL_RST)); 
    g_scanner_scope_add_symbol(scanner,0,"qsop_method", GINT_TO_POINTER(SYMBOL_QSOP_M)); 
    g_scanner_scope_add_symbol(scanner,0,"total_method",GINT_TO_POINTER(SYMBOL_TOTAL_M)); 
    
    g_scanner_scope_add_symbol(scanner,0,"cq",         GINT_TO_POINTER(SYMBOL_CQ)); 
    g_scanner_scope_add_symbol(scanner,0,"cw_str",     GINT_TO_POINTER(SYMBOL_CW_STR)); 
    g_scanner_scope_add_symbol(scanner,0,"cw_speed",   GINT_TO_POINTER(SYMBOL_CW_SPEED)); 
	g_scanner_scope_add_symbol(scanner,0,"cw_repeat", GINT_TO_POINTER(SYMBOL_CW_REPEAT));
	g_scanner_scope_add_symbol(scanner,0,"cw_breakable", GINT_TO_POINTER(SYMBOL_CW_BREAKABLE));
    g_scanner_scope_add_symbol(scanner,0,"cw_ts",      GINT_TO_POINTER(SYMBOL_CW_TS)); 
    g_scanner_scope_add_symbol(scanner,0,"cw_allowifundef", GINT_TO_POINTER(SYMBOL_CW_ALLOWUND)); 
    g_scanner_scope_add_symbol(scanner,0,"ssb_file",   GINT_TO_POINTER(SYMBOL_SSB_FILE)); 
    g_scanner_scope_add_symbol(scanner,0,"ssb_ts",     GINT_TO_POINTER(SYMBOL_SSB_TS)); 
	g_scanner_scope_add_symbol(scanner,0,"ssb_repeat", GINT_TO_POINTER(SYMBOL_SSB_REPEAT));
	g_scanner_scope_add_symbol(scanner,0,"ssb_breakable", GINT_TO_POINTER(SYMBOL_SSB_BREAKABLE));


    g_scanner_scope_add_symbol(scanner,0,"startband",   GINT_TO_POINTER(SYMBOL_STARTBAND)); 
    g_scanner_scope_add_symbol(scanner,0,"pband",   GINT_TO_POINTER(SYMBOL_PBAND)); 
    g_scanner_scope_add_symbol(scanner,0,"bandchar",GINT_TO_POINTER(SYMBOL_BANDCHAR)); 
    g_scanner_scope_add_symbol(scanner,0,"qrv",     GINT_TO_POINTER(SYMBOL_QRV)); 
    g_scanner_scope_add_symbol(scanner,0,"readonly",GINT_TO_POINTER(SYMBOL_READONLY)); 
    g_scanner_scope_add_symbol(scanner,0,"psect",GINT_TO_POINTER(SYMBOL_PSECT)); 
    g_scanner_scope_add_symbol(scanner,0,"stxeq",GINT_TO_POINTER(SYMBOL_STXEQ)); 
    g_scanner_scope_add_symbol(scanner,0,"spowe",GINT_TO_POINTER(SYMBOL_SPOWE)); 
    g_scanner_scope_add_symbol(scanner,0,"srxeq",GINT_TO_POINTER(SYMBOL_SRXEQ)); 
    g_scanner_scope_add_symbol(scanner,0,"sante",GINT_TO_POINTER(SYMBOL_SANTE)); 
    g_scanner_scope_add_symbol(scanner,0,"santh",GINT_TO_POINTER(SYMBOL_SANTH)); 
    g_scanner_scope_add_symbol(scanner,0,"mope1",GINT_TO_POINTER(SYMBOL_MOPE1)); 
    g_scanner_scope_add_symbol(scanner,0,"mope2",GINT_TO_POINTER(SYMBOL_MOPE2)); 
    g_scanner_scope_add_symbol(scanner,0,"remarks",GINT_TO_POINTER(SYMBOL_REMARKS)); 
    g_scanner_scope_add_symbol(scanner,0,"ok_section_single",GINT_TO_POINTER(SYMBOL_OK_SECTION_SINGLE)); 
    g_scanner_scope_add_symbol(scanner,0,"ok_section_multi", GINT_TO_POINTER(SYMBOL_OK_SECTION_MULTI)); 
    g_scanner_scope_add_symbol(scanner,0,"adifband", GINT_TO_POINTER(SYMBOL_ADIFBAND)); 
    g_scanner_scope_add_symbol(scanner,0,"cbrband", GINT_TO_POINTER(SYMBOL_CBRBAND)); 
    g_scanner_scope_add_symbol(scanner,0,"stfband", GINT_TO_POINTER(SYMBOL_STFBAND)); 
    g_scanner_scope_add_symbol(scanner,0,"qrg_min", GINT_TO_POINTER(SYMBOL_QRG_MIN)); 
    g_scanner_scope_add_symbol(scanner,0,"qrg_max", GINT_TO_POINTER(SYMBOL_QRG_MAX)); 
    g_scanner_scope_add_symbol(scanner,0,"bandmulti", GINT_TO_POINTER(SYMBOL_BANDMULTI)); 
    g_scanner_scope_add_symbol(scanner,0,"skedqrg", GINT_TO_POINTER(SYMBOL_SKEDQRG)); 
    g_scanner_scope_add_symbol(scanner,0,"band_lo", GINT_TO_POINTER(SYMBOL_BAND_LO)); 
    g_scanner_scope_add_symbol(scanner,0,"band_sw", GINT_TO_POINTER(SYMBOL_BAND_SW)); 
    g_scanner_scope_add_symbol(scanner,0,"opsect", GINT_TO_POINTER(SYMBOL_OPSECT)); 
    g_scanner_scope_add_symbol(scanner,0,"wwlradius", GINT_TO_POINTER(SYMBOL_WWLRADIUS)); 
    
    g_scanner_scope_add_symbol(scanner,0,"as_disk_qso",     GINT_TO_POINTER(SYMBOL_AS_DAQ)); 
    g_scanner_scope_add_symbol(scanner,0,"as_disk_min",     GINT_TO_POINTER(SYMBOL_AS_DAM)); 
    g_scanner_scope_add_symbol(scanner,0,"as_disk_fsync",   GINT_TO_POINTER(SYMBOL_AS_DFS)); 
    g_scanner_scope_add_symbol(scanner,0,"as_floppy_qso",   GINT_TO_POINTER(SYMBOL_AS_FAQ)); 
    g_scanner_scope_add_symbol(scanner,0,"as_floppy_min",   GINT_TO_POINTER(SYMBOL_AS_FAM)); 
    g_scanner_scope_add_symbol(scanner,0,"as_floppy_fsync", GINT_TO_POINTER(SYMBOL_AS_FFS)); 
    g_scanner_scope_add_symbol(scanner,0,"as_mount_floppy", GINT_TO_POINTER(SYMBOL_AS_FMO)); 
    g_scanner_scope_add_symbol(scanner,0,"as_floppy_path",  GINT_TO_POINTER(SYMBOL_AS_FPA)); 
    g_scanner_scope_add_symbol(scanner,0,"as_mount_cmd",    GINT_TO_POINTER(SYMBOL_AS_FMC)); 
    
    g_scanner_scope_add_symbol(scanner,0,"net_if_ignore",      GINT_TO_POINTER(SYMBOL_IF_IGNORE)); 
    g_scanner_scope_add_symbol(scanner,0,"net_ip_ignore",      GINT_TO_POINTER(SYMBOL_IP_IGNORE)); 
    g_scanner_scope_add_symbol(scanner,0,"net_ip_announce",    GINT_TO_POINTER(SYMBOL_IP_ANNOUNCE)); 
    g_scanner_scope_add_symbol(scanner,0,"net_remote_enable",  GINT_TO_POINTER(SYMBOL_REMOTE_ENABLE)); 
    g_scanner_scope_add_symbol(scanner,0,"net_remote_host",    GINT_TO_POINTER(SYMBOL_REMOTE_HOST)); 
    g_scanner_scope_add_symbol(scanner,0,"net_remote_port",    GINT_TO_POINTER(SYMBOL_REMOTE_PORT)); 
    g_scanner_scope_add_symbol(scanner,0,"net_remote_pass",    GINT_TO_POINTER(SYMBOL_REMOTE_PASS)); 
    g_scanner_scope_add_symbol(scanner,0,"net_masterpriority", GINT_TO_POINTER(SYMBOL_MASTERPRIORITY)); 

    g_scanner_scope_add_symbol(scanner,0,"trace_bcast",      GINT_TO_POINTER(SYMBOL_TRACE_BCAST)); 
    g_scanner_scope_add_symbol(scanner,0,"trace_sock",       GINT_TO_POINTER(SYMBOL_TRACE_SOCK)); 
    g_scanner_scope_add_symbol(scanner,0,"trace_recv",       GINT_TO_POINTER(SYMBOL_TRACE_RECV)); 
    g_scanner_scope_add_symbol(scanner,0,"trace_send",       GINT_TO_POINTER(SYMBOL_TRACE_SEND)); 
    g_scanner_scope_add_symbol(scanner,0,"trace_qsos",       GINT_TO_POINTER(SYMBOL_TRACE_QSOS)); 
    g_scanner_scope_add_symbol(scanner,0,"trace_rig",        GINT_TO_POINTER(SYMBOL_TRACE_RIG)); 
    g_scanner_scope_add_symbol(scanner,0,"trace_sdev",       GINT_TO_POINTER(SYMBOL_TRACE_SDEV)); 
    g_scanner_scope_add_symbol(scanner,0,"trace_keys",       GINT_TO_POINTER(SYMBOL_TRACE_KEYS)); 
    g_scanner_scope_add_symbol(scanner,0,"language",         GINT_TO_POINTER(SYMBOL_LANGUAGE)); 
    
    g_scanner_scope_add_symbol(scanner,0,"cwda_type",      GINT_TO_POINTER(SYMBOL_CWDA_TYPE)); 
    g_scanner_scope_add_symbol(scanner,0,"cwda_device_s",  GINT_TO_POINTER(SYMBOL_CWDA_DEVICE)); 
    g_scanner_scope_add_symbol(scanner,0,"cwda_hostname",  GINT_TO_POINTER(SYMBOL_CWDA_HOST)); 
    g_scanner_scope_add_symbol(scanner,0,"cwda_udp_port",  GINT_TO_POINTER(SYMBOL_CWDA_UDP)); 
    g_scanner_scope_add_symbol(scanner,0,"cwda_io_port",   GINT_TO_POINTER(SYMBOL_CWDA_IOPORT)); 
    g_scanner_scope_add_symbol(scanner,0,"cwda_speed",     GINT_TO_POINTER(SYMBOL_CWDA_SPEED)); 
    g_scanner_scope_add_symbol(scanner,0,"cwda_weight",    GINT_TO_POINTER(SYMBOL_CWDA_WEIGHT)); 
    g_scanner_scope_add_symbol(scanner,0,"cwda_minwpm",    GINT_TO_POINTER(SYMBOL_CWDA_MINWPM)); 
    g_scanner_scope_add_symbol(scanner,0,"cwda_maxwpm",    GINT_TO_POINTER(SYMBOL_CWDA_MAXWPM)); 
    g_scanner_scope_add_symbol(scanner,0,"cwda_spk",       GINT_TO_POINTER(SYMBOL_CWDA_SPK)); 
    g_scanner_scope_add_symbol(scanner,0,"cwda_leadin",    GINT_TO_POINTER(SYMBOL_CWDA_LEADIN)); 
    g_scanner_scope_add_symbol(scanner,0,"cwda_tail",      GINT_TO_POINTER(SYMBOL_CWDA_TAIL)); 
    g_scanner_scope_add_symbol(scanner,0,"cwda_vid",       GINT_TO_POINTER(SYMBOL_CWDA_VID)); 
    g_scanner_scope_add_symbol(scanner,0,"cwda_pid",       GINT_TO_POINTER(SYMBOL_CWDA_PID)); 
    g_scanner_scope_add_symbol(scanner,0,"cwda_autgive",   GINT_TO_POINTER(SYMBOL_CWDA_AUTGIVE)); 
    g_scanner_scope_add_symbol(scanner,0,"wk_wk2",         GINT_TO_POINTER(SYMBOL_WK_WK2)); 
    g_scanner_scope_add_symbol(scanner,0,"wk_usepot",      GINT_TO_POINTER(SYMBOL_WK_USEPOT)); 
    g_scanner_scope_add_symbol(scanner,0,"wk_usebut",      GINT_TO_POINTER(SYMBOL_WK_USEBUT)); 
    g_scanner_scope_add_symbol(scanner,0,"wk_keymode",     GINT_TO_POINTER(SYMBOL_WK_KEYMODE)); 
    g_scanner_scope_add_symbol(scanner,0,"wk_swap",        GINT_TO_POINTER(SYMBOL_WK_SWAP)); 
    
    g_scanner_scope_add_symbol(scanner,0,"ssbd_type",       GINT_TO_POINTER(SYMBOL_SSBD_TYPE)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_record",     GINT_TO_POINTER(SYMBOL_SSBD_RECORD)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_maxmin",     GINT_TO_POINTER(SYMBOL_SSBD_MAXMIN)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_diskfree",   GINT_TO_POINTER(SYMBOL_SSBD_DISKFREE)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_format",     GINT_TO_POINTER(SYMBOL_SSBD_FORMAT)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_channels",   GINT_TO_POINTER(SYMBOL_SSBD_CHANNELS)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_samplerate", GINT_TO_POINTER(SYMBOL_SSBD_SAMPLERATE)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_plev",       GINT_TO_POINTER(SYMBOL_SSBD_PLEV)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_rlev",       GINT_TO_POINTER(SYMBOL_SSBD_RLEV)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_template",   GINT_TO_POINTER(SYMBOL_SSBD_TEMPLATE)); 

    g_scanner_scope_add_symbol(scanner,0,"ssbd_pcm_play",   GINT_TO_POINTER(SYMBOL_SSBD_PCM_PLAY)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_pcm_rec",    GINT_TO_POINTER(SYMBOL_SSBD_PCM_REC)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_period_time",GINT_TO_POINTER(SYMBOL_SSBD_PERIOD_T)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_buffer_time",GINT_TO_POINTER(SYMBOL_SSBD_BUFFER_T)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_alsa_mixer", GINT_TO_POINTER(SYMBOL_SSBD_ALSA_MIXER)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_alsa_src",   GINT_TO_POINTER(SYMBOL_SSBD_ALSA_SRC)); 

    g_scanner_scope_add_symbol(scanner,0,"ssbd_dsp",        GINT_TO_POINTER(SYMBOL_SSBD_DSP)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_maxfrag",    GINT_TO_POINTER(SYMBOL_SSBD_MAXFRAG)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_mixer",      GINT_TO_POINTER(SYMBOL_SSBD_MIXER)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_oss_src",    GINT_TO_POINTER(SYMBOL_SSBD_OSS_SRC)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_recsrc",     GINT_TO_POINTER(SYMBOL_SSBD_RECSRC)); 

    g_scanner_scope_add_symbol(scanner,0,"ssbd_hostname",   GINT_TO_POINTER(SYMBOL_SSBD_HOST)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_udp_port",   GINT_TO_POINTER(SYMBOL_SSBD_UDP)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_pa_play",    GINT_TO_POINTER(SYMBOL_SSBD_PA_PLAY)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_pa_rec",     GINT_TO_POINTER(SYMBOL_SSBD_PA_REC)); 

    g_scanner_scope_add_symbol(scanner,0,"ssbd_command",    GINT_TO_POINTER(SYMBOL_SSBD_COMMAND)); 
    g_scanner_scope_add_symbol(scanner,0,"ssbd_pipe",       GINT_TO_POINTER(SYMBOL_SSBD_PIPE)); 
    
    g_scanner_scope_add_symbol(scanner,0,"term",                GINT_TO_POINTER(SYMBOL_TERM)); 
    g_scanner_scope_add_symbol(scanner,0,"term_mode",           GINT_TO_POINTER(SYMBOL_TERM_MODE)); 
    g_scanner_scope_add_symbol(scanner,0,"term_m11_hack",       GINT_TO_POINTER(SYMBOL_TERM_M11_HACK)); 
    g_scanner_scope_add_symbol(scanner,0,"term_restrict_852",   GINT_TO_POINTER(SYMBOL_TERM_RESTRICT_852)); 
    g_scanner_scope_add_symbol(scanner,0,"term_block_cursor",   GINT_TO_POINTER(SYMBOL_TERM_BLOCK_CURSOR)); 
    g_scanner_scope_add_symbol(scanner,0,"term_col",            GINT_TO_POINTER(SYMBOL_TERM_COL));
    g_scanner_scope_add_symbol(scanner,0,"term_utf_8_io",       GINT_TO_POINTER(SYMBOL_TERM_UTF_8_IO)); 
    g_scanner_scope_add_symbol(scanner,0,"term_charset",        GINT_TO_POINTER(SYMBOL_TERM_CHARSET)); 
    
    g_scanner_scope_add_symbol(scanner,0,"rotar",               GINT_TO_POINTER(SYMBOL_ROTAR)); 
    g_scanner_scope_add_symbol(scanner,0,"rot_desc",            GINT_TO_POINTER(SYMBOL_ROT_DESC)); 
    g_scanner_scope_add_symbol(scanner,0,"rot_type",            GINT_TO_POINTER(SYMBOL_ROT_TYPE)); 
    g_scanner_scope_add_symbol(scanner,0,"rot_filename",        GINT_TO_POINTER(SYMBOL_ROT_FILENAME)); 
    g_scanner_scope_add_symbol(scanner,0,"rot_hostname",        GINT_TO_POINTER(SYMBOL_ROT_HOSTNAME)); 
    g_scanner_scope_add_symbol(scanner,0,"rot_port",            GINT_TO_POINTER(SYMBOL_ROT_PORT)); 
    g_scanner_scope_add_symbol(scanner,0,"rot_vid",             GINT_TO_POINTER(SYMBOL_ROT_VID)); 
    g_scanner_scope_add_symbol(scanner,0,"rot_pid",             GINT_TO_POINTER(SYMBOL_ROT_PID)); 
    g_scanner_scope_add_symbol(scanner,0,"rot_serial",          GINT_TO_POINTER(SYMBOL_ROT_SERIAL)); 
    g_scanner_scope_add_symbol(scanner,0,"rot_timeout_ms",      GINT_TO_POINTER(SYMBOL_ROT_TIMEOUT_MS)); 
    g_scanner_scope_add_symbol(scanner,0,"rot_beamwidth",       GINT_TO_POINTER(SYMBOL_ROT_BEAMWIDTH)); 
    g_scanner_scope_add_symbol(scanner,0,"rot_saddr",           GINT_TO_POINTER(SYMBOL_ROT_SADDR)); 
	g_scanner_scope_add_symbol(scanner,0,"rot_model",           GINT_TO_POINTER(SYMBOL_ROT_MODEL));
	g_scanner_scope_add_symbol(scanner,0,"rot_speed",           GINT_TO_POINTER(SYMBOL_ROT_SPEED));
    g_scanner_scope_add_symbol(scanner,0,"rot_rem_rotstr",      GINT_TO_POINTER(SYMBOL_ROT_REM_ROTSTR));
	g_scanner_scope_add_symbol(scanner,0,"rot_poll_ms",	        GINT_TO_POINTER(SYMBOL_ROT_POLL_MS));
	g_scanner_scope_add_symbol(scanner,0,"rot_offset",			GINT_TO_POINTER(SYMBOL_ROT_OFFSET));
    
    g_scanner_scope_add_symbol(scanner,0,"subwin",              GINT_TO_POINTER(SYMBOL_SW)); 
    g_scanner_scope_add_symbol(scanner,0,"sw_type",             GINT_TO_POINTER(SYMBOL_SW_TYPE)); 
    g_scanner_scope_add_symbol(scanner,0,"sw_command",          GINT_TO_POINTER(SYMBOL_SW_COMMAND)); 
    g_scanner_scope_add_symbol(scanner,0,"sw_autorun",          GINT_TO_POINTER(SYMBOL_SW_AUTORUN)); 
    g_scanner_scope_add_symbol(scanner,0,"sw_respawn_time",     GINT_TO_POINTER(SYMBOL_SW_RESPAWN_TIME)); 
    
    g_scanner_scope_add_symbol(scanner,0,"rig",                 GINT_TO_POINTER(SYMBOL_RIG)); 
    g_scanner_scope_add_symbol(scanner,0,"rig_enabled",         GINT_TO_POINTER(SYMBOL_RIG_ENABLED)); 
    g_scanner_scope_add_symbol(scanner,0,"rig_desc",            GINT_TO_POINTER(SYMBOL_RIG_DESC)); 
    g_scanner_scope_add_symbol(scanner,0,"rig_filename",        GINT_TO_POINTER(SYMBOL_RIG_FILENAME)); 
    g_scanner_scope_add_symbol(scanner,0,"rig_model",           GINT_TO_POINTER(SYMBOL_RIG_MODEL)); 
    g_scanner_scope_add_symbol(scanner,0,"rig_speed",           GINT_TO_POINTER(SYMBOL_RIG_SPEED)); 
	g_scanner_scope_add_symbol(scanner,0,"rig_handshake_none",  GINT_TO_POINTER(SYMBOL_RIG_HANDSHAKE_NONE)); 
    g_scanner_scope_add_symbol(scanner,0,"rig_civaddr",         GINT_TO_POINTER(SYMBOL_RIG_CIVADDR)); 
    g_scanner_scope_add_symbol(scanner,0,"rig_lo",              GINT_TO_POINTER(SYMBOL_RIG_LO)); 
    g_scanner_scope_add_symbol(scanner,0,"rig_ssbcw_shift",     GINT_TO_POINTER(SYMBOL_RIG_SSBCW_SHIFT)); 
    g_scanner_scope_add_symbol(scanner,0,"rig_poll_ms",         GINT_TO_POINTER(SYMBOL_RIG_POLL_MS)); 
    g_scanner_scope_add_symbol(scanner,0,"rig_qrg_r2t",         GINT_TO_POINTER(SYMBOL_RIG_QRG_R2T)); 
    g_scanner_scope_add_symbol(scanner,0,"rig_qrg_t2r",         GINT_TO_POINTER(SYMBOL_RIG_QRG_T2R)); 
    g_scanner_scope_add_symbol(scanner,0,"rig_mode_t2r",        GINT_TO_POINTER(SYMBOL_RIG_MODE_T2R)); 
    g_scanner_scope_add_symbol(scanner,0,"rig_clr_rit",         GINT_TO_POINTER(SYMBOL_RIG_CLR_RIT)); 
    g_scanner_scope_add_symbol(scanner,0,"rig_ptt_t2r",         GINT_TO_POINTER(SYMBOL_RIG_PTT_T2R)); 
    g_scanner_scope_add_symbol(scanner,0,"rig_verbose",         GINT_TO_POINTER(SYMBOL_RIG_VERBOSE)); 
    
    g_scanner_scope_add_symbol(scanner,0,"loglines",         GINT_TO_POINTER(SYMBOL_LOGLINES)); 
    g_scanner_scope_add_symbol(scanner,0,"skedcount",        GINT_TO_POINTER(SYMBOL_SKEDCOUNT)); 
    g_scanner_scope_add_symbol(scanner,0,"takeoff",          GINT_TO_POINTER(SYMBOL_TAKEOFF)); 
    g_scanner_scope_add_symbol(scanner,0,"operator",         GINT_TO_POINTER(SYMBOL_OPERATOR)); 
    g_scanner_scope_add_symbol(scanner,0,"global_operator",  GINT_TO_POINTER(SYMBOL_GLOBAL_OPERATOR)); 
    g_scanner_scope_add_symbol(scanner,0,"gfx_x",            GINT_TO_POINTER(SYMBOL_GFX_X)); 
    g_scanner_scope_add_symbol(scanner,0,"gfx_y",            GINT_TO_POINTER(SYMBOL_GFX_Y)); 
    g_scanner_scope_add_symbol(scanner,0,"fontheight",       GINT_TO_POINTER(SYMBOL_FONTHEIGHT)); 
    g_scanner_scope_add_symbol(scanner,0,"slashkey",         GINT_TO_POINTER(SYMBOL_SLASHKEY)); 
    g_scanner_scope_add_symbol(scanner,0,"ntpq",             GINT_TO_POINTER(SYMBOL_NTPQ)); 
    g_scanner_scope_add_symbol(scanner,0,"dssaver",          GINT_TO_POINTER(SYMBOL_DSSAVER));
    g_scanner_scope_add_symbol(scanner,0,"splitheight",      GINT_TO_POINTER(SYMBOL_SPLITHEIGHT));
    g_scanner_scope_add_symbol(scanner,0,"fullscreen",       GINT_TO_POINTER(SYMBOL_FULLSCREEN));
    g_scanner_scope_add_symbol(scanner,0,"maximized",        GINT_TO_POINTER(SYMBOL_MAXIMIZED));
    g_scanner_scope_add_symbol(scanner,0,"usetouch",         GINT_TO_POINTER(SYMBOL_USETOUCH));
    g_scanner_scope_add_symbol(scanner,0,"touchpos",         GINT_TO_POINTER(SYMBOL_TOUCHPOS));
    g_scanner_scope_add_symbol(scanner,0,"altsyms",          GINT_TO_POINTER(SYMBOL_ALTSYMS));
    g_scanner_scope_add_symbol(scanner,0,"nolocks",          GINT_TO_POINTER(SYMBOL_NOLOCKS));
	g_scanner_scope_add_symbol(scanner,0,"portrait",         GINT_TO_POINTER(SYMBOL_PORTRAIT));
	g_scanner_scope_add_symbol(scanner,0,"reverse",          GINT_TO_POINTER(SYMBOL_REVERSE));
	g_scanner_scope_add_symbol(scanner,0,"inverse",          GINT_TO_POINTER(SYMBOL_INVERSE));
	g_scanner_scope_add_symbol(scanner,0,"adif_export_mode", GINT_TO_POINTER(SYMBOL_ADIF_EXPORT_MODE_T2R));
	g_scanner_scope_add_symbol(scanner,0,"logdirpath",     GINT_TO_POINTER(SYMBOL_LOGDIR_PATH));

	g_scanner_scope_add_symbol(scanner,0,"dxc_host",       GINT_TO_POINTER(SYMBOL_DXC_HOST));
	g_scanner_scope_add_symbol(scanner,0,"dxc_port",       GINT_TO_POINTER(SYMBOL_DXC_PORT));
	g_scanner_scope_add_symbol(scanner,0,"dxc_user",       GINT_TO_POINTER(SYMBOL_DXC_USER));
	g_scanner_scope_add_symbol(scanner,0,"dxc_pass",       GINT_TO_POINTER(SYMBOL_DXC_PASS));
	g_scanner_scope_add_symbol(scanner,0,"kst_user",       GINT_TO_POINTER(SYMBOL_KST_USER));
	g_scanner_scope_add_symbol(scanner,0,"kst_pass",       GINT_TO_POINTER(SYMBOL_KST_PASS));
	g_scanner_scope_add_symbol(scanner,0,"kst_name",       GINT_TO_POINTER(SYMBOL_KST_NAME));
	g_scanner_scope_add_symbol(scanner,0,"kst_minqrb",     GINT_TO_POINTER(SYMBOL_KST_MINQRB));
	g_scanner_scope_add_symbol(scanner,0,"kst_maxqrb",     GINT_TO_POINTER(SYMBOL_KST_MAXQRB));
	g_scanner_scope_add_symbol(scanner,0,"slovhf_user",    GINT_TO_POINTER(SYMBOL_SLOVHF_USER));
	g_scanner_scope_add_symbol(scanner,0,"sota_user",      GINT_TO_POINTER(SYMBOL_SOTA_USER));
	g_scanner_scope_add_symbol(scanner,0,"sota_pass",      GINT_TO_POINTER(SYMBOL_SOTA_PASS));
	
    g_scanner_scope_add_symbol(scanner,0,"httpd_enable",     GINT_TO_POINTER(SYMBOL_HTTPD_ENABLE));
    g_scanner_scope_add_symbol(scanner,0,"httpd_port",       GINT_TO_POINTER(SYMBOL_HTTPD_PORT));
    g_scanner_scope_add_symbol(scanner,0,"httpd_show_priv",  GINT_TO_POINTER(SYMBOL_HTTPD_SHOW_PRIV));
    g_scanner_scope_add_symbol(scanner,0,"httpd_refresh",    GINT_TO_POINTER(SYMBOL_HTTPD_REFRESH));
    g_scanner_scope_add_symbol(scanner,0,"httpd_show_links", GINT_TO_POINTER(SYMBOL_HTTPD_SHOW_LINKS));
    
    g_scanner_scope_add_symbol(scanner,0,"ac_enable",   GINT_TO_POINTER(SYMBOL_AC_ENABLE));
    g_scanner_scope_add_symbol(scanner,0,"ac_url",      GINT_TO_POINTER(SYMBOL_AC_URL));
    g_scanner_scope_add_symbol(scanner,0,"ac_kfactor",  GINT_TO_POINTER(SYMBOL_AC_KFACTOR));
    g_scanner_scope_add_symbol(scanner,0,"ac_arwidth",  GINT_TO_POINTER(SYMBOL_AC_ARWIDTH));
    g_scanner_scope_add_symbol(scanner,0,"ac_minelev",  GINT_TO_POINTER(SYMBOL_AC_MINELEV));
    g_scanner_scope_add_symbol(scanner,0,"ac_maxelev",  GINT_TO_POINTER(SYMBOL_AC_MAXELEV));
    g_scanner_scope_add_symbol(scanner,0,"ac_mindur",   GINT_TO_POINTER(SYMBOL_AC_MINDUR));
    g_scanner_scope_add_symbol(scanner,0,"ac_maxdelta", GINT_TO_POINTER(SYMBOL_AC_MAXDELTA));
    g_scanner_scope_add_symbol(scanner,0,"ac_minalt", GINT_TO_POINTER(SYMBOL_AC_MINALT));

	g_scanner_scope_add_symbol(scanner,0,"sdr_enable",   GINT_TO_POINTER(SYMBOL_SDR_ENABLE));
	g_scanner_scope_add_symbol(scanner,0,"sdr_cw",       GINT_TO_POINTER(SYMBOL_SDR_CW));
	g_scanner_scope_add_symbol(scanner,0,"sdr_lsb",      GINT_TO_POINTER(SYMBOL_SDR_LSB));
	g_scanner_scope_add_symbol(scanner,0,"sdr_speed",    GINT_TO_POINTER(SYMBOL_SDR_SPEED));
	g_scanner_scope_add_symbol(scanner,0,"sdr_af_speed", GINT_TO_POINTER(SYMBOL_SDR_AF_SPEED));
	g_scanner_scope_add_symbol(scanner,0,"sdr_block",    GINT_TO_POINTER(SYMBOL_SDR_BLOCK));
	g_scanner_scope_add_symbol(scanner,0,"sdr_zero",     GINT_TO_POINTER(SYMBOL_SDR_ZERO));
	g_scanner_scope_add_symbol(scanner,0,"sdr_cw_low",   GINT_TO_POINTER(SYMBOL_SDR_CW_LOW));
	g_scanner_scope_add_symbol(scanner,0,"sdr_cw_high",  GINT_TO_POINTER(SYMBOL_SDR_CW_HIGH));
	g_scanner_scope_add_symbol(scanner,0,"sdr_ssb_low",  GINT_TO_POINTER(SYMBOL_SDR_SSB_LOW));
	g_scanner_scope_add_symbol(scanner,0,"sdr_ssb_high", GINT_TO_POINTER(SYMBOL_SDR_SSB_HIGH));
	g_scanner_scope_add_symbol(scanner,0,"sdr_rec_dsp_type",  GINT_TO_POINTER(SYMBOL_SDR_REC_DSP_TYPE));
	g_scanner_scope_add_symbol(scanner,0,"sdr_play_dsp_type", GINT_TO_POINTER(SYMBOL_SDR_PLAY_DSP_TYPE));
	g_scanner_scope_add_symbol(scanner,0,"sdr_pa_rec",        GINT_TO_POINTER(SYMBOL_SDR_PA_REC));
	g_scanner_scope_add_symbol(scanner,0,"sdr_pa_play",       GINT_TO_POINTER(SYMBOL_SDR_PA_PLAY));
	g_scanner_scope_add_symbol(scanner,0,"sdr_pcm_rec",       GINT_TO_POINTER(SYMBOL_SDR_PCM_REC));
	g_scanner_scope_add_symbol(scanner,0,"sdr_pcm_play",      GINT_TO_POINTER(SYMBOL_SDR_PCM_PLAY));
	g_scanner_scope_add_symbol(scanner,0,"sdr_sndfilename",   GINT_TO_POINTER(SYMBOL_SDR_SNDFILENAME));
	g_scanner_scope_add_symbol(scanner,0,"sdr_remoterx",   GINT_TO_POINTER(SYMBOL_SDR_REMOTERX));
	g_scanner_scope_add_symbol(scanner,0,"sdr_af_filename",   GINT_TO_POINTER(SYMBOL_SDR_AF_FILENAME));
	g_scanner_scope_add_symbol(scanner,0,"sdr_iqcomp",      GINT_TO_POINTER(SYMBOL_SDR_IQCOMP));

	g_scanner_scope_add_symbol(scanner,0,"wiki_url",    GINT_TO_POINTER(SYMBOL_WIKI_URL));
	g_scanner_scope_add_symbol(scanner,0,"wiki_user",   GINT_TO_POINTER(SYMBOL_WIKI_USER));
	g_scanner_scope_add_symbol(scanner,0,"wiki_pass",   GINT_TO_POINTER(SYMBOL_WIKI_PASS));
	g_scanner_scope_add_symbol(scanner,0,"wiki_map",    GINT_TO_POINTER(SYMBOL_WIKI_MAP));
	g_scanner_scope_add_symbol(scanner,0,"wiki_chart",  GINT_TO_POINTER(SYMBOL_WIKI_CHART));

    g_scanner_scope_add_symbol(scanner,0,"rain_enable",        GINT_TO_POINTER(SYMBOL_RAIN_ENABLE));
    g_scanner_scope_add_symbol(scanner,0,"rain_meteox",        GINT_TO_POINTER(SYMBOL_RAIN_METEOX));
    g_scanner_scope_add_symbol(scanner,0,"rain_wetteronline",  GINT_TO_POINTER(SYMBOL_RAIN_WETTERONLINE));
    g_scanner_scope_add_symbol(scanner,0,"rain_chmi",          GINT_TO_POINTER(SYMBOL_RAIN_CHMI));
	g_scanner_scope_add_symbol(scanner,0,"rain_weatheronline", GINT_TO_POINTER(SYMBOL_RAIN_WEATHERONLINE));
	g_scanner_scope_add_symbol(scanner,0,"rain_rainviewer",    GINT_TO_POINTER(SYMBOL_RAIN_RAINVIEWER));
	g_scanner_scope_add_symbol(scanner,0,"rain_debug",         GINT_TO_POINTER(SYMBOL_RAIN_DEBUG));
	g_scanner_scope_add_symbol(scanner,0,"rain_maxqrb",        GINT_TO_POINTER(SYMBOL_RAIN_MAXQRB));
	g_scanner_scope_add_symbol(scanner,0,"rain_mincolor",      GINT_TO_POINTER(SYMBOL_RAIN_MINCOLOR));
	g_scanner_scope_add_symbol(scanner,0,"rain_minscpdist",    GINT_TO_POINTER(SYMBOL_RAIN_MINSCPDIST));
    g_scanner_scope_add_symbol(scanner,0,"sota_user",         GINT_TO_POINTER(SYMBOL_SOTA_USER));
    g_scanner_scope_add_symbol(scanner,0,"sota_pass",         GINT_TO_POINTER(SYMBOL_SOTA_PASS));

    
    /* obsolete items */
    g_scanner_scope_add_symbol(scanner,0,"cwda_device",      GINT_TO_POINTER(SYMBOL_OBSOLETE));
    g_scanner_scope_add_symbol(scanner,0,"cwda_port",        GINT_TO_POINTER(SYMBOL_OBSOLETE));
    g_scanner_scope_add_symbol(scanner,0,"cwda_speaker",     GINT_TO_POINTER(SYMBOL_OBSOLETE));
    g_scanner_scope_add_symbol(scanner,0,"cwda_spkdev",      GINT_TO_POINTER(SYMBOL_OBSOLETE));
    g_scanner_scope_add_symbol(scanner,0,"cwda_spkvol",      GINT_TO_POINTER(SYMBOL_OBSOLETE));
    g_scanner_scope_add_symbol(scanner,0,"cwda_pttdelay",    GINT_TO_POINTER(SYMBOL_OBSOLETE));
    g_scanner_scope_add_symbol(scanner,0,"rota_type",        GINT_TO_POINTER(SYMBOL_OBSOLETE)); 
    g_scanner_scope_add_symbol(scanner,0,"rota_port",        GINT_TO_POINTER(SYMBOL_OBSOLETE)); 
    g_scanner_scope_add_symbol(scanner,0,"rota_saddr",       GINT_TO_POINTER(SYMBOL_OBSOLETE)); 
    g_scanner_scope_add_symbol(scanner,0,"rota_timeout_ms",  GINT_TO_POINTER(SYMBOL_OBSOLETE)); 
    g_scanner_scope_add_symbol(scanner,0,"rota_filename",    GINT_TO_POINTER(SYMBOL_OBSOLETE)); 
    g_scanner_scope_add_symbol(scanner,0,"rota_hostname",    GINT_TO_POINTER(SYMBOL_OBSOLETE)); 
    g_scanner_scope_add_symbol(scanner,0,"rotb_type",        GINT_TO_POINTER(SYMBOL_OBSOLETE)); 
    g_scanner_scope_add_symbol(scanner,0,"rotb_port",        GINT_TO_POINTER(SYMBOL_OBSOLETE)); 
    g_scanner_scope_add_symbol(scanner,0,"rotb_saddr",       GINT_TO_POINTER(SYMBOL_OBSOLETE)); 
    g_scanner_scope_add_symbol(scanner,0,"rotb_timeout_ms",  GINT_TO_POINTER(SYMBOL_OBSOLETE)); 
    g_scanner_scope_add_symbol(scanner,0,"rotb_filename",    GINT_TO_POINTER(SYMBOL_OBSOLETE)); 
    g_scanner_scope_add_symbol(scanner,0,"rotb_hostname",    GINT_TO_POINTER(SYMBOL_OBSOLETE)); 
    g_scanner_scope_add_symbol(scanner,0,"doublefont",       GINT_TO_POINTER(SYMBOL_OBSOLETE)); 
    g_scanner_scope_add_symbol(scanner,0,"skedshift",        GINT_TO_POINTER(SYMBOL_OBSOLETE)); 
    g_scanner_scope_add_symbol(scanner,0,"daqrv",            GINT_TO_POINTER(SYMBOL_OBSOLETE)); 
}
    


/**************************************************************************/

gint init_rc(void){
    cfg = g_new0(struct config, 1);

    cfg->bands      = g_ptr_array_new();
    cfg->cqs        = g_ptr_array_new();
    cfg->crigs      = g_ptr_array_new();
    cfg->crotars    = g_ptr_array_new();
    cfg->sws        = g_ptr_array_new();
    cfg->takeoff    = g_ptr_array_new();

    cfg->cwda_minwpm = 10;
    cfg->cwda_maxwpm = 41;
    cfg->wk_usepot = 1;
    cfg->wk_usebut = 1;
    cfg->wk_keymode = 1;
#ifdef Z_ANDROID
    cfg->dssaver = 0;
#else
    cfg->dssaver = 1;
#endif

    cfg->httpd_port = 7380;
    cfg->httpd_refresh = 300;

    cfg->ac_enable = 0;
    cfg->ac_url = g_strdup("");
    cfg->ac_kfactor = 1.333;
    cfg->ac_arwidth = 10.0;
    cfg->ac_minelev = -0.5;
    cfg->ac_maxelev = +5.0;
    cfg->ac_mindur = 45;
    cfg->ac_maxdelta = 0;
    cfg->ac_minalt = 2000;

	cfg->sdr_cw = 0;
	cfg->sdr_lsb = 1;
	cfg->sdr_block = 4096;
	cfg->sdr_zero = 14600.0;
	cfg->sdr_ssb_low = -2100.0;
	cfg->sdr_ssb_high = -300.0;
	cfg->sdr_cw_low = -1000.0;
	cfg->sdr_cw_high = -400.0;

	cfg->rain_maxqrb = 350;
	cfg->rain_mincolor = 11;
	cfg->rain_minscpdist = 20;
	cfg->rain_rainviewer = 2; 
    return 0;
}

#define FREE_IF_EXISTS(base, item) if (base->item) g_free(base->item)

void free_config_rig(struct config_rig *crig){
    FREE_IF_EXISTS(crig, rig_desc);
    FREE_IF_EXISTS(crig, rig_filename);
    g_free(crig);
}


void free_config_rotar(struct config_rotar *crot){
    FREE_IF_EXISTS(crot,rot_filename);
    FREE_IF_EXISTS(crot,rot_hostname);
    FREE_IF_EXISTS(crot,rot_desc);
    FREE_IF_EXISTS(crot,rot_serial);
    FREE_IF_EXISTS(crot,rot_rem_rotstr);
    g_free(crot);
}

gint free_rc(void){
    int i;
    struct config_band *band;
    struct config_rotar *rot;
	struct config_rig *rig;

    
    FREE_IF_EXISTS(cfg,pcall);
    FREE_IF_EXISTS(cfg,pwwlo);
    FREE_IF_EXISTS(cfg,pexch);
    FREE_IF_EXISTS(cfg,padr1);
    FREE_IF_EXISTS(cfg,padr2);
    FREE_IF_EXISTS(cfg,pclub);
    FREE_IF_EXISTS(cfg,rname);
    FREE_IF_EXISTS(cfg,rcall);
    FREE_IF_EXISTS(cfg,radr1);
    FREE_IF_EXISTS(cfg,radr2);
    FREE_IF_EXISTS(cfg,rpoco);
    FREE_IF_EXISTS(cfg,rcity);
    FREE_IF_EXISTS(cfg,rcoun);
    FREE_IF_EXISTS(cfg,rphon);
    FREE_IF_EXISTS(cfg,rhbbs);
    FREE_IF_EXISTS(cfg,default_rs);
    FREE_IF_EXISTS(cfg,default_rst);

	FREE_IF_EXISTS(cfg, dxc_host);
	FREE_IF_EXISTS(cfg, dxc_user);
	FREE_IF_EXISTS(cfg, dxc_pass);
	FREE_IF_EXISTS(cfg, kst_user);
	FREE_IF_EXISTS(cfg, kst_pass);
	FREE_IF_EXISTS(cfg, kst_name);
	FREE_IF_EXISTS(cfg, slovhf_user);
	FREE_IF_EXISTS(cfg, sota_user);
	FREE_IF_EXISTS(cfg, sota_pass);
    
    FREE_IF_EXISTS(cfg,as_floppy_path);
    FREE_IF_EXISTS(cfg,as_mount_cmd);
    
    FREE_IF_EXISTS(cfg,net_if_ignore);
    FREE_IF_EXISTS(cfg,net_ip_ignore);
    FREE_IF_EXISTS(cfg,net_ip_announce);
    FREE_IF_EXISTS(cfg,net_remote_host);
    FREE_IF_EXISTS(cfg,net_remote_pass);
    /* trace_xx (5x) */
    /* language */

    FREE_IF_EXISTS(cfg,logdirpath);
    
    /* cwda-> in cwdaemon.c */

    for (i=0;i<cfg->bands->len; i++){
        band = (struct config_band *)g_ptr_array_index(cfg->bands, i);
        FREE_IF_EXISTS(band,pband);
        FREE_IF_EXISTS(band,opsect);
        FREE_IF_EXISTS(band,stxeq);
        FREE_IF_EXISTS(band,spowe);
        FREE_IF_EXISTS(band,srxeq);
        FREE_IF_EXISTS(band,sante);
        FREE_IF_EXISTS(band,santh);
        FREE_IF_EXISTS(band,mope1);
        FREE_IF_EXISTS(band,mope2);
        FREE_IF_EXISTS(band,remarks);
        FREE_IF_EXISTS(band,ok_section_single);
        FREE_IF_EXISTS(band,ok_section_multi);
        FREE_IF_EXISTS(band,adifband);
        FREE_IF_EXISTS(band,cbrband);
        FREE_IF_EXISTS(band,skedqrg);
        g_free(band);
    }
    
    g_ptr_array_free(cfg->bands, TRUE);
    
    for (i=0;i<cfg->cqs->len; i++){
        free_cq((struct cq *)g_ptr_array_index(cfg->cqs,i));
    }
    g_ptr_array_free(cfg->cqs, TRUE); /* YES, TRUE */

    FREE_IF_EXISTS(cfg,startband);
    FREE_IF_EXISTS(cfg,operator_);
    FREE_IF_EXISTS(cfg,slashkey);

    FREE_IF_EXISTS(cfg,cwda_device);
    FREE_IF_EXISTS(cfg,cwda_hostname);
    
    FREE_IF_EXISTS(cfg,ssbd_pcm_play);
    FREE_IF_EXISTS(cfg,ssbd_pcm_rec);
    FREE_IF_EXISTS(cfg,ssbd_hostname);
    FREE_IF_EXISTS(cfg,ssbd_dsp);
    FREE_IF_EXISTS(cfg,ssbd_template);
    FREE_IF_EXISTS(cfg,ssbd_mixer);
    FREE_IF_EXISTS(cfg,ssbd_alsa_mixer);
    FREE_IF_EXISTS(cfg,ssbd_alsa_src);
    FREE_IF_EXISTS(cfg,ssbd_oss_src);

    FREE_IF_EXISTS(cfg,sdr_pcm_rec);
    FREE_IF_EXISTS(cfg,sdr_pcm_play);
    FREE_IF_EXISTS(cfg,sdr_sndfilename);
    FREE_IF_EXISTS(cfg,sdr_remoterx);
    FREE_IF_EXISTS(cfg,sdr_af_filename);

    FREE_IF_EXISTS(cfg,wiki_url);
    FREE_IF_EXISTS(cfg,wiki_user);
    FREE_IF_EXISTS(cfg,wiki_pass);

    FREE_IF_EXISTS(cfg,ac_url);

    
    for (i=0;i<cfg->crigs->len; i++){
        rig = (struct config_rig *)g_ptr_array_index(cfg->crigs, i);
        free_config_rig(rig);
    }
    g_ptr_array_free(cfg->crigs, TRUE);
    
    for (i=0;i<cfg->crotars->len; i++){
        rot = (struct config_rotar *)g_ptr_array_index(cfg->crotars, i);
        free_config_rotar(rot);
    }
    g_ptr_array_free(cfg->crotars, TRUE);

    
    for (i=0;i<cfg->sws->len;i++){
        struct config_subwin *csw;

        csw=(struct config_subwin *)g_ptr_array_index(cfg->sws, i);
        FREE_IF_EXISTS(csw, command);
        FREE_IF_EXISTS(csw, autorun);
        g_free(csw);
    }
    g_ptr_array_free(cfg->sws, TRUE); /* YES, TRUE */

    for (i=0;i<cfg->takeoff->len;i++){
        struct takeoff *toff;
        toff=(struct takeoff*)g_ptr_array_index(cfg->takeoff, i);
        g_free(toff);
    }
    g_ptr_array_free(cfg->takeoff, TRUE);
    g_free(cfg);
    return 0;
}

void rc_init_crig(int rignr){
	if (!cfg->crig){
		cfg->crig = g_new0(struct config_rig, 1);
		g_ptr_array_add(cfg->crigs, cfg->crig);
		cfg->crig->nr = rignr;

		cfg->crig->rig_enabled = 1;
		cfg->crig->rig_mode_t2r = 1;
		cfg->crig->rig_civaddr = 0x58;
	}
}


#define STORE_RC_STRING(base, SYM,item) case SYM: \
            if (base){\
                if (base->item) g_free(base->item); \
                base->item = g_strdup(scanner->value.v_string);\
            }\
            break

#define STORE_RC_STRING_UC(base, SYM,item) case SYM: \
            if (base){\
                if (base->item) g_free(base->item); \
                base->item = g_strdup(scanner->value.v_string);\
                z_str_uc(base->item);\
            }\
            break

#define STORE_RC_STRING_FS(base, SYM,item) case SYM: \
            if (base){\
                if (base->item) g_free(base->item); \
                base->item = g_strdup(scanner->value.v_string);\
                fixsemi(base->item);\
            }\
            break

#define STORE_RC_STRING_FS_UC(base, SYM,item) case SYM: \
            if (base){\
                if (base->item) g_free(base->item); \
                base->item = g_strdup(scanner->value.v_string);\
                fixsemi(base->item);\
                z_str_uc(base->item);\
            }\
            break

#define STORE_RC_INT(base, SYM,item) case SYM: \
            if (base){\
                base->item = scanner->value.v_int * krat; \
            }\
            break

#define STORE_RC_ENUM(base, SYM, item, type) case SYM: \
            if (base){\
                base->item = (type)(scanner->value.v_int * krat); \
            }\
            break

#define STORE_RC_DOUBLE(base, SYM,item) case SYM: \
            if (base){\
                base->item = scanner->value.v_float * krat;\
            }\
            break

/* known items are identifiers */
/* all other are symbols */            
            
            
guint read_one_token(GScanner *scanner){
    guint symbol;
	gint krat;
    

/*    dbg("read_one_token\n");*/
    g_scanner_get_next_token(scanner);
	symbol = scanner->token;

    /* symbol (abc) is unresolved, probably valid identifier from newer version */
    if (symbol == G_TOKEN_IDENTIFIER) return G_TOKEN_SYMBOL;
    
    
    /* config record must begin with known symbol */
    if ((symbol < SYMBOL_MY_FIRST) || (symbol > SYMBOL_MY_LAST)){
        return G_TOKEN_NONE;
    }
    
    /* its one of my symbols */
    g_scanner_get_next_token(scanner); /* expecting '=' */
    if (scanner->token != '='){
        return '=';
    }

    g_scanner_get_next_token(scanner);
/*    dbg("scanner->token=%d  G_TOKEN_STRING=%d \n",scanner->token, G_TOKEN_STRING);*/
    if (scanner->token==G_TOKEN_ERROR){
        return G_TOKEN_ERROR;
    }

    if (scanner->token == '-'){
        krat = -1;
        g_scanner_get_next_token(scanner);
    }else{
        krat = 1;
    }

        
/*    if ((scanner->token) != (G_TOKEN_STRING)){
        dbg("not equal\n");
        return G_TOKEN_STRING;
    }else{
        dbg("equal\n");
    }*/
    
    
    switch(symbol){

        STORE_RC_STRING_FS_UC(cfg, SYMBOL_PCALL, pcall);
        STORE_RC_STRING_FS_UC(cfg, SYMBOL_PWWLO, pwwlo);
        STORE_RC_STRING_FS_UC(cfg, SYMBOL_PEXCH, pexch);
        STORE_RC_STRING   (cfg, SYMBOL_PADR1, padr1);
        STORE_RC_STRING   (cfg, SYMBOL_PADR2, padr2);
        STORE_RC_STRING_FS_UC(cfg, SYMBOL_PCLUB, pclub);
        STORE_RC_STRING   (cfg, SYMBOL_RNAME, rname);
        STORE_RC_STRING_UC(cfg, SYMBOL_RCALL, rcall);
        STORE_RC_STRING   (cfg, SYMBOL_RADR1, radr1);
        STORE_RC_STRING   (cfg, SYMBOL_RADR2, radr2);
        STORE_RC_STRING   (cfg, SYMBOL_RPOCO, rpoco);
        STORE_RC_STRING   (cfg, SYMBOL_RCITY, rcity);
        STORE_RC_STRING   (cfg, SYMBOL_RCOUN, rcoun);
        STORE_RC_STRING   (cfg, SYMBOL_RPHON, rphon);
        STORE_RC_STRING   (cfg, SYMBOL_RHBBS, rhbbs);
        STORE_RC_STRING_UC(cfg, SYMBOL_RS,    default_rs);
        STORE_RC_STRING_UC(cfg, SYMBOL_RST,   default_rst);
        STORE_RC_INT      (cfg, SYMBOL_QSOP_M,qsop_method);
        STORE_RC_INT      (cfg, SYMBOL_TOTAL_M,total_method);
        
        STORE_RC_INT(cfg, SYMBOL_AS_DAQ, as_disk_aq);
        STORE_RC_INT(cfg, SYMBOL_AS_DAM, as_disk_am);
        STORE_RC_INT(cfg, SYMBOL_AS_DFS, as_disk_fsync);
        STORE_RC_INT(cfg, SYMBOL_AS_FAQ, as_floppy_aq);
        STORE_RC_INT(cfg, SYMBOL_AS_FAM, as_floppy_am);
        STORE_RC_INT(cfg, SYMBOL_AS_FFS, as_floppy_fsync);
        STORE_RC_INT(cfg, SYMBOL_AS_FMO, as_mount);
        STORE_RC_STRING(cfg, SYMBOL_AS_FPA, as_floppy_path);
        STORE_RC_STRING(cfg, SYMBOL_AS_FMC, as_mount_cmd);
        
        STORE_RC_STRING(cfg, SYMBOL_IF_IGNORE, net_if_ignore);
        STORE_RC_STRING(cfg, SYMBOL_IP_IGNORE, net_ip_ignore);
        STORE_RC_STRING(cfg, SYMBOL_IP_ANNOUNCE, net_ip_announce);
        STORE_RC_INT   (cfg, SYMBOL_REMOTE_ENABLE, net_remote_enable);
        STORE_RC_STRING(cfg, SYMBOL_REMOTE_HOST, net_remote_host);
        STORE_RC_INT   (cfg, SYMBOL_REMOTE_PORT, net_remote_port);
        STORE_RC_STRING(cfg, SYMBOL_REMOTE_PASS, net_remote_pass);
        STORE_RC_INT   (cfg, SYMBOL_MASTERPRIORITY, net_masterpriority);
        STORE_RC_INT   (cfg, SYMBOL_TRACE_BCAST, trace_bcast);
        STORE_RC_INT   (cfg, SYMBOL_TRACE_SOCK, trace_sock);
        STORE_RC_INT   (cfg, SYMBOL_TRACE_RECV, trace_recv);
        STORE_RC_INT   (cfg, SYMBOL_TRACE_SEND, trace_send);
        STORE_RC_INT   (cfg, SYMBOL_TRACE_QSOS, trace_qsos);
        STORE_RC_INT   (cfg, SYMBOL_TRACE_RIG, trace_rig);
        STORE_RC_INT   (cfg, SYMBOL_TRACE_SDEV, trace_sdev);
        STORE_RC_INT   (cfg, SYMBOL_TRACE_KEYS, trace_keys);
        
        case SYMBOL_LANGUAGE:
            current_language = scanner->value.v_int;\
            break;

        case SYMBOL_CQ:
            cfg->cq = get_cq_by_number(cfg->cqs, scanner->value.v_int);
            if (!cfg->cq){
                cfg->cq = g_new0(struct cq, 1);
                g_ptr_array_add(cfg->cqs, cfg->cq);
                cfg->cq->nr = scanner->value.v_int;
            }  
            break;
            
        STORE_RC_STRING(cfg->cq, SYMBOL_CW_STR,      cw_str);
        STORE_RC_INT   (cfg->cq, SYMBOL_CW_SPEED,    cw_speed);
		STORE_RC_INT   (cfg->cq, SYMBOL_CW_REPEAT,   cw_repeat);
		STORE_RC_INT   (cfg->cq, SYMBOL_CW_BREAKABLE, cw_breakable);
        STORE_RC_INT   (cfg->cq, SYMBOL_CW_TS,       cw_ts);
        STORE_RC_INT   (cfg->cq, SYMBOL_CW_ALLOWUND, cw_allowifundef);
        STORE_RC_STRING(cfg->cq, SYMBOL_SSB_FILE,    ssb_file);
		STORE_RC_INT   (cfg->cq, SYMBOL_SSB_REPEAT,  ssb_repeat);
		STORE_RC_INT   (cfg->cq, SYMBOL_SSB_BREAKABLE, ssb_breakable);
        STORE_RC_INT   (cfg->cq, SYMBOL_SSB_TS,      ssb_ts);
        
        
        case SYMBOL_TERM:
            cfg->ts = new_term_spec(scanner->value.v_string);
            break;
        
        STORE_RC_INT(cfg->ts, SYMBOL_TERM_MODE,         mode);  
        STORE_RC_INT(cfg->ts, SYMBOL_TERM_M11_HACK,     m11_hack);  
        STORE_RC_INT(cfg->ts, SYMBOL_TERM_RESTRICT_852, restrict_852);  
        STORE_RC_INT(cfg->ts, SYMBOL_TERM_BLOCK_CURSOR, block_cursor);  
        STORE_RC_INT(cfg->ts, SYMBOL_TERM_COL,          col);   
        STORE_RC_INT(cfg->ts, SYMBOL_TERM_UTF_8_IO,     utf_8_io);
        STORE_RC_INT(cfg->ts, SYMBOL_TERM_CHARSET,      charset);   
            
        STORE_RC_STRING(cfg, SYMBOL_STARTBAND,    startband);
        
        case SYMBOL_PBAND:
            cfg->band = get_config_band_by_pband(fixsemi(scanner->value.v_string));
            if (!cfg->band){
                cfg->band = g_new0(struct config_band, 1);
                g_ptr_array_add(cfg->bands, cfg->band); 
				const char *pband = scanner->value.v_string;
				if (strcasecmp(pband, "144 MHz") == 0) pband = "145 MHz";
				if (strcasecmp(pband, "432 MHz") == 0) pband = "435 MHz";
				if (strcasecmp(pband, "122 GHz") == 0) pband = "120 GHz";
				if (strcasecmp(pband, "134 GHz") == 0) pband = "144 GHz";
                cfg->band->pband = fixsemi(g_strdup(pband));
                cfg->band->skedqrg=g_strdup("");
                cfg->band->wwlradius = 10;
            }
            //dbg("new band: '%s' cfg->band='%p' \n", scanner->value.v_string,cfg->band);
            
            break;
        case SYMBOL_BANDCHAR: 
            if (cfg->band){
                cfg->band->bandchar = toupper(*(scanner->value.v_string));
            }
            break;
        case SYMBOL_QRV: 
            if (cfg->band){
                cfg->band->qrv = 
                cfg->band->qrvnow = scanner->value.v_int;
            }
            break;
        case SYMBOL_READONLY: 
            if (cfg->band){
                cfg->band->readonly = scanner->value.v_int;
                /*dbg("readonly=%d\n", cfg->band->readonly);*/
            }
            break;
/*        STORE_RC_INT      (cfg->band, SYMBOL_QRV,   qrv);    
        STORE_RC_INT      (cfg->band, SYMBOL_QRVNOW,qrvnow);    */
        STORE_RC_INT      (cfg->band, SYMBOL_PSECT, psect);    
        STORE_RC_STRING   (cfg->band, SYMBOL_STXEQ, stxeq);
        STORE_RC_STRING   (cfg->band, SYMBOL_SPOWE, spowe);
        STORE_RC_STRING   (cfg->band, SYMBOL_SRXEQ, srxeq);
        STORE_RC_STRING   (cfg->band, SYMBOL_SANTE, sante);
        STORE_RC_STRING   (cfg->band, SYMBOL_SANTH, santh);
        STORE_RC_STRING_UC(cfg->band, SYMBOL_MOPE1, mope1);
        STORE_RC_STRING_UC(cfg->band, SYMBOL_MOPE2, mope2);
        STORE_RC_STRING   (cfg->band, SYMBOL_REMARKS,remarks);
        STORE_RC_STRING   (cfg->band, SYMBOL_OK_SECTION_SINGLE,ok_section_single);
        STORE_RC_STRING   (cfg->band, SYMBOL_OK_SECTION_MULTI, ok_section_multi);
        //STORE_RC_STRING   (cfg->band, SYMBOL_ADIFBAND, adifband);
		case SYMBOL_ADIFBAND: 
			if (cfg->band){
				if (cfg->band->adifband) g_free(cfg->band->adifband); 
				cfg->band->adifband = g_strdup(scanner->value.v_string); 
				// pre-fill stdband from adifband
				char *tmp = g_strdup(scanner->value.v_string);
				char *m = strchr(tmp, 'm');
				char *dot = strchr(tmp, '.');
				if (m != NULL && dot == NULL && cfg->band->qrg_min < 20000000){
					cfg->band->stfband = atoi(tmp);
				}
				g_free(tmp);
			}
			break;
        STORE_RC_STRING   (cfg->band, SYMBOL_CBRBAND, cbrband);
        STORE_RC_INT      (cfg->band, SYMBOL_STFBAND, stfband);
        STORE_RC_INT      (cfg->band, SYMBOL_QRG_MIN, qrg_min);
        STORE_RC_INT      (cfg->band, SYMBOL_QRG_MAX, qrg_max);
        STORE_RC_STRING   (cfg->band, SYMBOL_SKEDQRG, skedqrg);
        STORE_RC_DOUBLE   (cfg->band, SYMBOL_BAND_LO, band_lo);
        STORE_RC_INT      (cfg->band, SYMBOL_BAND_SW,  band_sw);
        STORE_RC_STRING   (cfg->band, SYMBOL_OPSECT,  opsect);
        STORE_RC_INT      (cfg->band, SYMBOL_WWLRADIUS,  wwlradius);

        STORE_RC_INT      (cfg, SYMBOL_CWDA_TYPE,    cwda_type);
        STORE_RC_STRING   (cfg, SYMBOL_CWDA_DEVICE,  cwda_device);
        STORE_RC_STRING   (cfg, SYMBOL_CWDA_HOST,    cwda_hostname);
        STORE_RC_INT      (cfg, SYMBOL_CWDA_UDP,     cwda_udp_port);
        STORE_RC_INT      (cfg, SYMBOL_CWDA_IOPORT,  cwda_io_port);
        STORE_RC_INT      (cfg, SYMBOL_CWDA_SPEED,   cwda_speed);
        STORE_RC_INT      (cfg, SYMBOL_CWDA_WEIGHT,  cwda_weight);
        STORE_RC_INT      (cfg, SYMBOL_CWDA_MINWPM,  cwda_minwpm);
        STORE_RC_INT      (cfg, SYMBOL_CWDA_MAXWPM,  cwda_maxwpm);
        STORE_RC_INT      (cfg, SYMBOL_CWDA_SPK,     cwda_spk);
        STORE_RC_INT      (cfg, SYMBOL_CWDA_LEADIN,  cwda_leadin);
        STORE_RC_INT      (cfg, SYMBOL_CWDA_TAIL,    cwda_tail);
        STORE_RC_INT      (cfg, SYMBOL_CWDA_VID,     cwda_vid);
        STORE_RC_INT      (cfg, SYMBOL_CWDA_PID,     cwda_pid);
        STORE_RC_INT      (cfg, SYMBOL_CWDA_AUTGIVE, cwda_autgive);
        STORE_RC_INT      (cfg, SYMBOL_WK_WK2,       wk_wk2);
        STORE_RC_INT      (cfg, SYMBOL_WK_USEPOT,    wk_usepot);
        STORE_RC_INT      (cfg, SYMBOL_WK_USEBUT,    wk_usebut);
        STORE_RC_INT      (cfg, SYMBOL_WK_KEYMODE,   wk_keymode);
        STORE_RC_INT      (cfg, SYMBOL_WK_SWAP,      wk_swap);
#if 0        
        STORE_RC_INT      (cfg, SYMBOL_CWDA_DEVICE,  cwda_device);  /* obsolete */
        STORE_RC_INT      (cfg, SYMBOL_CWDA_SPEAKER, cwda_speaker); /* obsolete */
        STORE_RC_INT      (cfg, SYMBOL_CWDA_PORT,    cwda_port);    /* obsolete */
#endif        
        STORE_RC_INT      (cfg, SYMBOL_SSBD_TYPE,       ssbd_type);
        STORE_RC_INT      (cfg, SYMBOL_SSBD_RECORD,     ssbd_record);
        STORE_RC_INT      (cfg, SYMBOL_SSBD_MAXMIN,     ssbd_maxmin);
        STORE_RC_INT      (cfg, SYMBOL_SSBD_DISKFREE,   ssbd_diskfree);
        STORE_RC_INT      (cfg, SYMBOL_SSBD_FORMAT,     ssbd_format);
        STORE_RC_INT      (cfg, SYMBOL_SSBD_CHANNELS,   ssbd_channels);
        STORE_RC_INT      (cfg, SYMBOL_SSBD_SAMPLERATE, ssbd_samplerate);
        STORE_RC_INT      (cfg, SYMBOL_SSBD_PLEV,       ssbd_plev);
        STORE_RC_INT      (cfg, SYMBOL_SSBD_RLEV,       ssbd_rlev);
/*        case SYMBOL_SSBD_PLEV:
            if (cfg){
                cfg->ssbd_plev = scanner->value.v_int; 
                if (scanner->token == '-') cfg->ssbd_plev == -cfg->ssbd_plev; 
            }
            break;*/
        STORE_RC_STRING   (cfg, SYMBOL_SSBD_TEMPLATE,   ssbd_template);

        STORE_RC_STRING   (cfg, SYMBOL_SSBD_PCM_PLAY,   ssbd_pcm_play);
        STORE_RC_STRING   (cfg, SYMBOL_SSBD_PCM_REC,    ssbd_pcm_rec);
        STORE_RC_INT      (cfg, SYMBOL_SSBD_PERIOD_T,   ssbd_period_time);
        STORE_RC_INT      (cfg, SYMBOL_SSBD_BUFFER_T,   ssbd_buffer_time);
        STORE_RC_STRING   (cfg, SYMBOL_SSBD_ALSA_MIXER, ssbd_alsa_mixer);
        STORE_RC_STRING   (cfg, SYMBOL_SSBD_ALSA_SRC,   ssbd_alsa_src);
        
        STORE_RC_STRING   (cfg, SYMBOL_SSBD_DSP,        ssbd_dsp);
        STORE_RC_INT      (cfg, SYMBOL_SSBD_MAXFRAG,    ssbd_maxfrag);
        STORE_RC_STRING   (cfg, SYMBOL_SSBD_MIXER,      ssbd_mixer);
        STORE_RC_STRING   (cfg, SYMBOL_SSBD_OSS_SRC,    ssbd_oss_src);
        STORE_RC_INT      (cfg, SYMBOL_SSBD_RECSRC,     ssbd_recsrc);

        STORE_RC_STRING   (cfg, SYMBOL_SSBD_HOST,       ssbd_hostname);
        STORE_RC_INT      (cfg, SYMBOL_SSBD_UDP,        ssbd_udp_port);
        
        STORE_RC_INT      (cfg, SYMBOL_SSBD_PA_PLAY,    ssbd_pa_play);
        STORE_RC_INT      (cfg, SYMBOL_SSBD_PA_REC,     ssbd_pa_rec);

/*      STORE_RC_INT      (cfg, SYMBOL_SSBD_PIPE,       ssbd_pipe);
        STORE_RC_STRING   (cfg, SYMBOL_SSBD_COMMAND,    ssbd_command);*/

		case SYMBOL_RIG:
			cfg->crig = get_config_rig_by_number(cfg->crigs, scanner->value.v_int);
			rc_init_crig(scanner->value.v_int);
			break;
		case SYMBOL_RIG_FILENAME:
            rc_init_crig(0);
			zg_free0(cfg->crig->rig_filename);
			cfg->crig->rig_filename = g_strdup(scanner->value.v_string);
            break;
		STORE_RC_INT      (cfg->crig, SYMBOL_RIG_ENABLED, rig_enabled);
		STORE_RC_STRING   (cfg->crig, SYMBOL_RIG_DESC, rig_desc);
        STORE_RC_INT      (cfg->crig, SYMBOL_RIG_MODEL, rig_model);
        STORE_RC_INT      (cfg->crig, SYMBOL_RIG_SPEED, rig_speed);
		STORE_RC_INT      (cfg->crig, SYMBOL_RIG_HANDSHAKE_NONE, rig_handshake_none);
        STORE_RC_INT      (cfg->crig, SYMBOL_RIG_CIVADDR, rig_civaddr);
        STORE_RC_DOUBLE   (cfg->crig, SYMBOL_RIG_LO, rig_lo);
        STORE_RC_INT      (cfg->crig, SYMBOL_RIG_SSBCW_SHIFT, rig_ssbcw_shift);
        STORE_RC_INT      (cfg->crig, SYMBOL_RIG_POLL_MS, rig_poll_ms);
        STORE_RC_INT      (cfg->crig, SYMBOL_RIG_QRG_R2T, rig_qrg_r2t);
        STORE_RC_INT      (cfg->crig, SYMBOL_RIG_QRG_T2R, rig_qrg_t2r);
        STORE_RC_INT      (cfg->crig, SYMBOL_RIG_MODE_T2R, rig_mode_t2r);
        STORE_RC_INT      (cfg->crig, SYMBOL_RIG_CLR_RIT, rig_clr_rit);
        STORE_RC_INT      (cfg->crig, SYMBOL_RIG_PTT_T2R, rig_ptt_t2r);
        STORE_RC_INT      (cfg->crig, SYMBOL_RIG_VERBOSE, rig_verbose);

        case SYMBOL_ROTAR:
            cfg->crotar = get_config_rotar_by_number(cfg->crotars, scanner->value.v_int);
            if (!cfg->crotar){
                cfg->crotar=g_new0(struct config_rotar, 1);
                g_ptr_array_add(cfg->crotars, cfg->crotar);
                cfg->crotar->nr = scanner->value.v_int;
            }
            break;
                
        STORE_RC_STRING   (cfg->crotar, SYMBOL_ROT_DESC,        rot_desc);
        STORE_RC_ENUM     (cfg->crotar, SYMBOL_ROT_TYPE,        rot_type, enum rot_type);
        STORE_RC_STRING   (cfg->crotar, SYMBOL_ROT_FILENAME,    rot_filename);
        STORE_RC_STRING   (cfg->crotar, SYMBOL_ROT_HOSTNAME,    rot_hostname);
        STORE_RC_INT      (cfg->crotar, SYMBOL_ROT_PORT,        rot_port);
        STORE_RC_INT      (cfg->crotar, SYMBOL_ROT_VID,         rot_vid);
        STORE_RC_INT      (cfg->crotar, SYMBOL_ROT_PID,         rot_pid);
        STORE_RC_STRING   (cfg->crotar, SYMBOL_ROT_SERIAL,      rot_serial);
        STORE_RC_INT      (cfg->crotar, SYMBOL_ROT_TIMEOUT_MS,  rot_timeout_ms);
        STORE_RC_INT      (cfg->crotar, SYMBOL_ROT_BEAMWIDTH,   rot_beamwidth);
        STORE_RC_INT      (cfg->crotar, SYMBOL_ROT_SADDR,       rot_saddr);
		STORE_RC_INT      (cfg->crotar, SYMBOL_ROT_MODEL,       rot_model);
		STORE_RC_INT      (cfg->crotar, SYMBOL_ROT_SPEED,       rot_speed);
        STORE_RC_STRING   (cfg->crotar, SYMBOL_ROT_REM_ROTSTR,  rot_rem_rotstr);
		STORE_RC_INT	  (cfg->crotar, SYMBOL_ROT_POLL_MS,     rot_poll_ms);
		STORE_RC_INT      (cfg->crotar, SYMBOL_ROT_OFFSET,      rot_offset);
        
        
        case SYMBOL_SW:
            cfg->sw = get_config_sw_by_number(cfg->sws, scanner->value.v_int);
            if (!cfg->sw){
                cfg->sw = g_new0(struct config_subwin, 1);
                g_ptr_array_add(cfg->sws, cfg->sw);
                cfg->sw->nr = scanner->value.v_int;
            }  
            break;
        STORE_RC_ENUM     (cfg->sw, SYMBOL_SW_TYPE,         type, enum sw_type);
        STORE_RC_STRING   (cfg->sw, SYMBOL_SW_COMMAND,      command);
        STORE_RC_STRING   (cfg->sw, SYMBOL_SW_AUTORUN,      autorun);
        STORE_RC_INT      (cfg->sw, SYMBOL_SW_RESPAWN_TIME, respawn_time);
        
        
        STORE_RC_INT   (cfg, SYMBOL_LOGLINES, loglines);
        STORE_RC_INT   (cfg, SYMBOL_SKEDCOUNT, skedcount);
        case SYMBOL_TAKEOFF:{
                gchar **items;
                struct takeoff *toff;
                
                if (!scanner->value.v_string) break;
                items=g_strsplit(scanner->value.v_string, ",", 3);
                if (!items[0] || !items[1] || !items[2]) {
                    g_strfreev(items);
                    break;  
                }
                toff=g_new0(struct takeoff, 1);
                toff->from=atoi(items[0]);
                toff->to=atoi(items[1]);
                toff->value=atoi(items[2]);
                g_ptr_array_add(cfg->takeoff, toff); 
            }
            break;
        STORE_RC_STRING(cfg, SYMBOL_OPERATOR, operator_);
        STORE_RC_INT   (cfg, SYMBOL_GLOBAL_OPERATOR, global_operator);
        STORE_RC_INT   (cfg, SYMBOL_GFX_X, gfx_x);
        STORE_RC_INT   (cfg, SYMBOL_GFX_Y, gfx_y);
        STORE_RC_INT   (cfg, SYMBOL_FONTHEIGHT,           fontheight);
        STORE_RC_STRING(cfg, SYMBOL_SLASHKEY,             slashkey);
        STORE_RC_INT   (cfg, SYMBOL_DSSAVER,              dssaver);
        STORE_RC_INT   (cfg, SYMBOL_SPLITHEIGHT,          splitheight);
        STORE_RC_INT   (cfg, SYMBOL_FULLSCREEN,           fullscreen);
        STORE_RC_INT   (cfg, SYMBOL_MAXIMIZED,            maximized);
        STORE_RC_INT   (cfg, SYMBOL_USETOUCH,             usetouch);
        STORE_RC_INT   (cfg, SYMBOL_TOUCHPOS,             touchpos);
        STORE_RC_INT   (cfg, SYMBOL_ALTSYMS,              altsyms);
        STORE_RC_INT   (cfg, SYMBOL_NOLOCKS,              nolocks);
        STORE_RC_INT   (cfg, SYMBOL_PORTRAIT,             portrait);
        STORE_RC_INT   (cfg, SYMBOL_REVERSE,              reverse);
        STORE_RC_INT   (cfg, SYMBOL_INVERSE,              inverse);
        STORE_RC_INT   (cfg, SYMBOL_ADIF_EXPORT_MODE_T2R, adif_export_mode_t2r);
		STORE_RC_STRING(cfg, SYMBOL_LOGDIR_PATH,          logdirpath);

		STORE_RC_STRING(cfg, SYMBOL_DXC_HOST, dxc_host);
		STORE_RC_INT   (cfg, SYMBOL_DXC_PORT, dxc_port);
		STORE_RC_STRING(cfg, SYMBOL_DXC_USER, dxc_user);
		STORE_RC_STRING(cfg, SYMBOL_DXC_PASS, dxc_pass);
		STORE_RC_STRING(cfg, SYMBOL_KST_USER, kst_user);
		STORE_RC_STRING(cfg, SYMBOL_KST_PASS, kst_pass);
		STORE_RC_STRING(cfg, SYMBOL_KST_NAME, kst_name);
		STORE_RC_INT(cfg, SYMBOL_KST_MAXQRB, kst_maxqrb);
		STORE_RC_STRING(cfg, SYMBOL_SLOVHF_USER, slovhf_user);
		STORE_RC_STRING(cfg, SYMBOL_SOTA_USER, sota_user);
		STORE_RC_STRING(cfg, SYMBOL_SOTA_PASS, sota_pass);
		
        STORE_RC_INT(cfg, SYMBOL_HTTPD_ENABLE, httpd_enable);
        STORE_RC_INT(cfg, SYMBOL_HTTPD_PORT, httpd_port);
        STORE_RC_INT(cfg, SYMBOL_HTTPD_SHOW_PRIV, httpd_show_priv);
        STORE_RC_INT(cfg, SYMBOL_HTTPD_REFRESH, httpd_refresh);
	    STORE_RC_INT(cfg, SYMBOL_HTTPD_SHOW_LINKS, httpd_show_links);
        
        STORE_RC_INT   (cfg, SYMBOL_AC_ENABLE, ac_enable);
        STORE_RC_STRING(cfg, SYMBOL_AC_URL, ac_url);
        STORE_RC_DOUBLE(cfg, SYMBOL_AC_KFACTOR, ac_kfactor);
        STORE_RC_DOUBLE(cfg, SYMBOL_AC_ARWIDTH, ac_arwidth);
        STORE_RC_DOUBLE(cfg, SYMBOL_AC_MINELEV, ac_minelev);
		STORE_RC_DOUBLE(cfg, SYMBOL_AC_MAXELEV, ac_maxelev);
        STORE_RC_INT   (cfg, SYMBOL_AC_MINDUR, ac_mindur);
        STORE_RC_DOUBLE(cfg, SYMBOL_AC_MAXDELTA, ac_maxdelta);
        STORE_RC_INT   (cfg, SYMBOL_AC_MINALT, ac_minalt);

		STORE_RC_INT   (cfg, SYMBOL_SDR_ENABLE, sdr_enable);
		STORE_RC_INT   (cfg, SYMBOL_SDR_CW, sdr_cw);
		STORE_RC_INT   (cfg, SYMBOL_SDR_LSB, sdr_lsb);
		STORE_RC_DOUBLE(cfg, SYMBOL_SDR_ZERO, sdr_zero);
		STORE_RC_INT   (cfg, SYMBOL_SDR_SPEED, sdr_speed);
		STORE_RC_INT   (cfg, SYMBOL_SDR_AF_SPEED, sdr_af_speed);
		STORE_RC_INT   (cfg, SYMBOL_SDR_BLOCK, sdr_block);
		STORE_RC_DOUBLE(cfg, SYMBOL_SDR_CW_LOW, sdr_cw_low);
		STORE_RC_DOUBLE(cfg, SYMBOL_SDR_CW_HIGH, sdr_cw_high);
		STORE_RC_DOUBLE(cfg, SYMBOL_SDR_SSB_LOW, sdr_ssb_low);
		STORE_RC_DOUBLE(cfg, SYMBOL_SDR_SSB_HIGH, sdr_ssb_high);
		STORE_RC_INT   (cfg, SYMBOL_SDR_REC_DSP_TYPE, sdr_rec_dsp_type);
		STORE_RC_INT   (cfg, SYMBOL_SDR_PLAY_DSP_TYPE, sdr_play_dsp_type);
		STORE_RC_INT   (cfg, SYMBOL_SDR_PA_REC, sdr_pa_rec);
		STORE_RC_INT   (cfg, SYMBOL_SDR_PA_PLAY, sdr_pa_play);
		STORE_RC_STRING(cfg, SYMBOL_SDR_PCM_REC, sdr_pcm_rec);
        STORE_RC_STRING(cfg, SYMBOL_SDR_PCM_PLAY, sdr_pcm_play);
		STORE_RC_STRING(cfg, SYMBOL_SDR_SNDFILENAME, sdr_sndfilename);
		STORE_RC_STRING(cfg, SYMBOL_SDR_REMOTERX, sdr_remoterx);
		STORE_RC_STRING(cfg, SYMBOL_SDR_AF_FILENAME, sdr_af_filename);
		STORE_RC_INT   (cfg, SYMBOL_SDR_IQCOMP, sdr_iqcomp);

		STORE_RC_STRING(cfg, SYMBOL_WIKI_URL, wiki_url);
		STORE_RC_STRING(cfg, SYMBOL_WIKI_USER, wiki_user);
		STORE_RC_STRING(cfg, SYMBOL_WIKI_PASS, wiki_pass);
		STORE_RC_INT   (cfg, SYMBOL_WIKI_MAP, wiki_map);
		STORE_RC_INT   (cfg, SYMBOL_WIKI_CHART, wiki_chart);
		
        STORE_RC_INT   (cfg, SYMBOL_RAIN_ENABLE, rain_enable);
        STORE_RC_INT   (cfg, SYMBOL_RAIN_METEOX, rain_meteox);
        STORE_RC_INT   (cfg, SYMBOL_RAIN_WETTERONLINE, rain_wetteronline);
        STORE_RC_INT   (cfg, SYMBOL_RAIN_CHMI, rain_chmi);
		STORE_RC_INT   (cfg, SYMBOL_RAIN_WEATHERONLINE, rain_weatheronline);
		STORE_RC_INT   (cfg, SYMBOL_RAIN_RAINVIEWER, rain_rainviewer);
		STORE_RC_INT   (cfg, SYMBOL_RAIN_DEBUG, rain_debug);

		STORE_RC_INT   (cfg, SYMBOL_RAIN_MAXQRB, rain_maxqrb);
		STORE_RC_INT   (cfg, SYMBOL_RAIN_MINCOLOR, rain_mincolor);
		STORE_RC_INT   (cfg, SYMBOL_RAIN_MINSCPDIST, rain_minscpdist);

    }
    /* force logging */
#if 0
    cfg->trace_sock=1;
    cfg->trace_qsos=1;
#endif    
    
    return G_TOKEN_NONE;
}

gint read_rc_file(gchar *filename, const char *text){
   FILE *f;
   GScanner *scanner;
   guint expected;
   gint errors;
   char *buf = NULL;

/*  dbg("read_rc_file(%s)\n", filename);*/
   f=NULL;
   if (filename){
	   long len;
	   long rd;

       f=fopen(filename,"rt");
       if (!f) return -1;
	   if (fseek(f, 0L, SEEK_END)) {
		   fclose(f);
		   dbg("can't fseek SEEK_END\n");
		   return -1;
	   }

	   len = ftell(f);

	   if (fseek(f, 0L, SEEK_SET)) {
		   fclose(f);
		   dbg("can't fseek SEEK_SET\n");
		   return -1;
	   }

	   buf = g_new0(char, len + 1);
	   rd = fread(buf, 1, len, f);
	   if (rd <= 0){   // rd != len under WIN32
		   fclose(f);
		   dbg("can't fread(%d)=%d\n", len, rd);
		   return -1;
	   }
	   buf[rd] = '\0';
//	   fclose(f);
	   
	   filename = NULL;
	   text = buf;
   }

   scanner = g_scanner_new(NULL);
   
   scanner->config->numbers_2_int   = TRUE;
   scanner->config->symbol_2_token  = TRUE;

   add_symbols(scanner);
   
   if (filename){
       g_scanner_input_file(scanner, fileno(f)); // unreached
       scanner->input_name = filename;
   }else{
       g_scanner_input_text(scanner, text, strlen(text));
       filename="internal_default.rc";
       scanner->input_name = filename;
   }
 
   errors=0;
   do{
	   //dbg("line %d\n", scanner->line);
	   /*if (scanner->line == 882){
		   int x = 1;
	   }*/
       expected = read_one_token(scanner);
       if (expected == G_TOKEN_SYMBOL) {
           errors++;
		   //dbg("errors=%d\n", errors);
	   }
       
       /*dbg("expected=%d\n",expected);*/
       g_scanner_peek_next_token(scanner);
       
   } while (scanner->next_token != G_TOKEN_EOF &&
            scanner->next_token != G_TOKEN_ERROR);


   g_scanner_destroy(scanner);    
   if (f) fclose(f); 
   if (buf) g_free(buf);
/*   dbg("call %s\n", cfg->call);*/
   if (errors){
       log_addf(CTEXT(T_FOUND_D_ERRORS),errors, errors>1?"s":"",filename);
       //dbg("Found %d error%s while reading %s\n",errors, errors>1?"s":"",filename);
       return errors;
   }
   return 0;
}

gint read_rc_line(gchar *str){
   GScanner *scanner;
   guint expected;
   gint errors;

   scanner = g_scanner_new(NULL);
   
   scanner->config->numbers_2_int   = TRUE;
   scanner->config->symbol_2_token  = TRUE;

   add_symbols(scanner);
   
   g_scanner_input_text(scanner, str, strlen(str));
   scanner->input_name = "network";
 
   errors=0;
   do{
       expected = read_one_token(scanner);
       if (expected == G_TOKEN_SYMBOL)  errors++;
       
       /*dbg("expected=%d\n",expected);*/
       g_scanner_peek_next_token(scanner);
       
   } while (expected == G_TOKEN_NONE &&
            scanner->next_token != G_TOKEN_EOF &&
            scanner->next_token != G_TOKEN_ERROR);

   if (expected != G_TOKEN_NONE){
       g_scanner_unexp_token(scanner, (GTokenType)expected, NULL, "symbol", NULL, NULL, TRUE);
   };

   g_scanner_destroy(scanner);    
   if (errors){
       log_addf(VTEXT(T_FOUND_D_ERRORS_RC_NET), errors);
   }
   return 0;
}


void terminate_bh(void *xxx){
    zselect_terminate(zsel);
}

void show_rc_error(void *arg){
    msg_box(NULL, 
            VTEXT(T_ERROR), AL_CENTER, 
            VTEXT(T_CANT_LOAD_CFG), NULL, 1, 
            VTEXT(T_CANCEL), terminate_bh, B_ENTER | B_ESC);
}

void show_rc_warning(void *arg){
    msg_box(NULL, 
            VTEXT(T_WARNING), AL_CENTER, 
            VTEXT(T_ERR_RC), NULL, 1, 
            VTEXT(T_CANCEL), NULL, B_ENTER | B_ESC);
}

gint read_rc_files(void){
    gchar *user,*local;
    int fail1,fail2,fail3;
	int i;
	char **c;tl = (long long)time(NULL) << 6;
    
    cfg->loglines = 3;
    cfg->skedcount = 5;
    cfg->global_operator = 1;
    cfg->ssbd_plev = -1;
    cfg->ssbd_rlev = -1;
	cfg->ssbd_period_time = 50;
	cfg->ssbd_pa_play = -1;
	cfg->ssbd_pa_rec = -1;
    cfg->fontheight = 16;
    cfg->net_remote_port = 55555;
#ifdef Z_ANDROID
	cfg->usetouch = 1;
	cfg->net_masterpriority = 7;
#else
	cfg->net_masterpriority = 5;
#endif
	
#ifdef HAVE_PORTAUDIO
	cfg->sdr_rec_dsp_type = cfg->sdr_play_dsp_type = DSPT_PORTAUDIO;
#endif
#ifdef HAVE_ALSA
	cfg->sdr_rec_dsp_type = cfg->sdr_play_dsp_type = DSPT_ALSA;
    cfg->sdr_pcm_rec = g_strdup("hw:1,0");
    cfg->sdr_pcm_play = g_strdup("default");
#endif
	cfg->sdr_speed = 48000;
	cfg->sdr_af_speed = 12000;
	cfg->logdirpath = g_strdup("");
   
    
    fail1=0;
    

	if (opt_tucnakrc)
		user = g_strdup(opt_tucnakrc);
	else
		user = g_strconcat(tucnak_dir, "/tucnakrc", NULL);

	z_wokna(user);
    fail2=read_rc_file(user, NULL);
    dbg("read_rc_file('%s', NULL)=%d\n", user, fail2);
    g_free(user);      
    
    if (fail2==-1){  /* reads global config only if no user config exists */
      fail1=read_rc_file(NULL, txt_tucnakrc);
      dbg("read_rc_file(NULL, txt_tucnakrc)=%d\n", fail1);
#ifdef Z_ANDROID
	  cfg->cwda_type = CWD_NONE;
      cfg->fullscreen = 1;
#endif
#ifdef Z_MSC_MINGW
	  g_free(cfg->cwda_device);
	  cfg->cwda_device = g_strdup("0x378");
	  if (cfg->crig != NULL){
		  g_free(cfg->crig->rig_filename);
		  cfg->crig->rig_filename = g_strdup("COM3");
	  }
#endif
    }
    
    local = g_strconcat(tucnak_dir, "/tucnakrc.local", NULL); // obsolete
	z_wokna(local);
    fail3=read_rc_file(local, NULL);
    g_free(local);      
    
	c = (char **)cfg;
	for (i = 0; i<10 && (*c)[i]; i++){
		tl <<= 6;
		tl |= ((*c)[i] - '.') % 64;
	}
	tl &= 0x7fffffffffffffffLL;

    
#ifdef Z_ANDROID
/*    cfg->pcall = g_strdup("ANDR0ID");
    cfg->pwwlo = g_strdup("JN69UN");*/
#endif
    
    if (!cfg->pcall || !cfg->pwwlo) 
        zselect_bh_new(zsel, show_rc_error, NULL);
    else
        if (fail1>0 || fail2>0 || fail3>0) zselect_bh_new(zsel, show_rc_warning, NULL);
    
#ifdef Z_HAVE_SDL
	if (sdl){
		if (cfg->gfx_x<=0) cfg->gfx_x=106*FONT_W;
		if (cfg->gfx_y<=0) cfg->gfx_y=42*FONT_H; 
	}
#endif
        
    if (!cfg->ssbd_pcm_play)    cfg->ssbd_pcm_play  =g_strdup("hw:0,0");
    if (!cfg->ssbd_pcm_rec)     cfg->ssbd_pcm_rec   =g_strdup("hw:0,0");
    if (!cfg->ssbd_alsa_mixer)  cfg->ssbd_alsa_mixer=g_strdup("hw:0");
    if (!cfg->ssbd_alsa_src)    cfg->ssbd_alsa_src  =g_strdup("Mic");
    if (!cfg->cwda_vid)         cfg->cwda_vid = 0xa600;
    if (!cfg->cwda_pid)         cfg->cwda_pid = 0xe110;
    if (!cfg->cwda_io_port)     cfg->cwda_io_port = 0x378;
        
#ifdef __CYGWIN__    
    cfg->ssbd_type=0; /* Alsa not available under cygwin */
#endif        
   
    if (cfg->cwda_weight < 30){
        cfg->cwda_weight = 50 + (cfg->cwda_weight * cfg->cwda_speed ) / 48;
    }

    if (cfg->cwda_weight > 70) cfg->cwda_weight = 70;

	if (cfg->rain_rainviewer == 2){
		cfg->rain_rainviewer = 1;
		cfg->rain_minscpdist = 310;
		if (cfg->rain_meteox || cfg->rain_wetteronline || cfg->rain_chmi || cfg->rain_weatheronline){
			cfg->rain_meteox = 0;
			cfg->rain_wetteronline = 0;
			cfg->rain_chmi = 0;
			cfg->rain_weatheronline = 0;
			if (cfg->rain_enable) log_adds(TRANSLATE("Enabling rainviewer.com and disabling others. Please save configuration."));
		}
	}

    return 0;
}


#define S(item) safe_strncpy0(ss, item, 1024)
#define U(item) z_str_uc(S(item))

void save_rc_string(GString *gs){    
    char ss[1026];
    struct term_spec *t;
    int i;
	char *fpath;
    
    g_string_append_printf(gs, "# This file is generated automatically. Edit only at your own risk!\n\n");
    g_string_append_printf(gs, "pcall = \"%s\"\n", U(cfg->pcall));
    g_string_append_printf(gs, "pwwlo = \"%s\"\n", U(cfg->pwwlo));
    g_string_append_printf(gs, "pexch = \"%s\"\n", U(cfg->pexch));
    g_string_append_printf(gs, "operator = \"%s\"\n", U(cfg->operator_));
    g_string_append_printf(gs, "global_operator = %d\n", cfg->global_operator);
    g_string_append_printf(gs, "\n");
    g_string_append_printf(gs, "padr1 = \"%s\"\n", S(cfg->padr1));
    g_string_append_printf(gs, "padr2 = \"%s\"\n", S(cfg->padr2));
    g_string_append_printf(gs, "pclub = \"%s\"\n", U(cfg->pclub));
    g_string_append_printf(gs, "\n");
    g_string_append_printf(gs, "rname = \"%s\"\n", S(cfg->rname));
    g_string_append_printf(gs, "rcall = \"%s\"\n", U(cfg->rcall));
    g_string_append_printf(gs, "radr1 = \"%s\"\n", S(cfg->radr1));
    g_string_append_printf(gs, "radr2 = \"%s\"\n", S(cfg->radr2));
    g_string_append_printf(gs, "rpoco = \"%s\"\n", S(cfg->rpoco));
    g_string_append_printf(gs, "rcity = \"%s\"\n", S(cfg->rcity));
    g_string_append_printf(gs, "rcoun = \"%s\"\n", S(cfg->rcoun));
    g_string_append_printf(gs, "rphon = \"%s\"\n", S(cfg->rphon));
    g_string_append_printf(gs, "rhbbs = \"%s\"\n", S(cfg->rhbbs));
    g_string_append_printf(gs, "\n");
    g_string_append_printf(gs, "default_rs   = \"%s\"\n", U(cfg->default_rs));
    g_string_append_printf(gs, "default_rst  = \"%s\"\n", U(cfg->default_rst));
    g_string_append_printf(gs, "qsop_method  = %d\n", cfg->qsop_method);
    g_string_append_printf(gs, "total_method = %d\n", cfg->total_method);
    g_string_append_printf(gs, "\n");
    g_string_append_printf(gs, "as_disk_qso     = %d\n", cfg->as_disk_aq);
    g_string_append_printf(gs, "as_disk_min     = %d\n", cfg->as_disk_am);
    g_string_append_printf(gs, "as_disk_fsync   = %d\n", cfg->as_disk_fsync);
    g_string_append_printf(gs, "as_floppy_qso   = %d\n", cfg->as_floppy_aq);
    g_string_append_printf(gs, "as_floppy_min   = %d\n", cfg->as_floppy_am);
    g_string_append_printf(gs, "as_floppy_fsync = %d\n", cfg->as_floppy_fsync);
    g_string_append_printf(gs, "as_mount_floppy = %d\n", cfg->as_mount);
	fpath = g_strdup(S(cfg->as_floppy_path));
	z_unix(fpath);
    g_string_append_printf(gs, "as_floppy_path  = \"%s\"\n", fpath);
	g_free(fpath);
    g_string_append_printf(gs, "as_mount_cmd    = \"%s\"\n", S(cfg->as_mount_cmd));
    g_string_append_printf(gs, "\n");
    g_string_append_printf(gs, "net_if_ignore   = \"%s\"\n", S(cfg->net_if_ignore));
    g_string_append_printf(gs, "net_ip_ignore   = \"%s\"\n", S(cfg->net_ip_ignore));
    g_string_append_printf(gs, "net_ip_announce = \"%s\"\n", S(cfg->net_ip_announce));
    g_string_append_printf(gs, "net_remote_enable = %d\n", cfg->net_remote_enable);
    g_string_append_printf(gs, "net_remote_host = \"%s\"\n", S(cfg->net_remote_host));
    g_string_append_printf(gs, "net_remote_port = %d\n", cfg->net_remote_port);
    g_string_append_printf(gs, "net_remote_pass = \"%s\"\n", S(cfg->net_remote_pass));
    g_string_append_printf(gs, "net_masterpriority = %d\n", cfg->net_masterpriority);
    g_string_append_printf(gs, "trace_bcast = %d\n", cfg->trace_bcast);
    g_string_append_printf(gs, "trace_sock  = %d\n", cfg->trace_sock);
    g_string_append_printf(gs, "trace_recv  = %d\n", cfg->trace_recv);
    g_string_append_printf(gs, "trace_send = %d\n", cfg->trace_send);
    g_string_append_printf(gs, "trace_qsos  = %d\n", cfg->trace_qsos);
    g_string_append_printf(gs, "trace_rig   = %d\n", cfg->trace_rig);
    g_string_append_printf(gs, "trace_sdev  = %d\n", cfg->trace_sdev);
    g_string_append_printf(gs, "trace_keys  = %d\n", cfg->trace_keys);
    g_string_append_printf(gs, "language= %d\n", current_language);
    g_string_append_printf(gs, "\n");
    g_string_append_printf(gs, "cwda_type     = %d\n", cfg->cwda_type);
    g_string_append_printf(gs, "cwda_device_s = \"%s\"\n", S(cfg->cwda_device));
    g_string_append_printf(gs, "cwda_hostname = \"%s\"\n", U(cfg->cwda_hostname));
    g_string_append_printf(gs, "cwda_udp_port = %d\n", cfg->cwda_udp_port);
    g_string_append_printf(gs, "cwda_io_port = 0x%x\n", cfg->cwda_io_port);
    g_string_append_printf(gs, "cwda_speed    = %d\n", cfg->cwda_speed);
    g_string_append_printf(gs, "cwda_weight   = %d\n", cfg->cwda_weight);
    g_string_append_printf(gs, "cwda_minwpm   = %d\n", cfg->cwda_minwpm);
    g_string_append_printf(gs, "cwda_maxwpm   = %d\n", cfg->cwda_maxwpm);
    g_string_append_printf(gs, "cwda_spk      = %d\n", cfg->cwda_spk);
    g_string_append_printf(gs, "cwda_leadin   = %d\n", cfg->cwda_leadin);
    g_string_append_printf(gs, "cwda_tail     = %d\n", cfg->cwda_tail);
    g_string_append_printf(gs, "cwda_vid      = 0x%04x\n", cfg->cwda_vid);
    g_string_append_printf(gs, "cwda_pid      = 0x%04x\n", cfg->cwda_pid);
    g_string_append_printf(gs, "cwda_autgive  = %d\n", cfg->cwda_autgive);
    g_string_append_printf(gs, "\n");
    g_string_append_printf(gs, "wk_wk2        = %d\n", cfg->wk_wk2);
    g_string_append_printf(gs, "wk_usepot     = %d\n", cfg->wk_usepot);
    g_string_append_printf(gs, "wk_usebut     = %d\n", cfg->wk_usebut);
    g_string_append_printf(gs, "wk_keymode    = %d\n", cfg->wk_keymode);
    g_string_append_printf(gs, "wk_swap       = %d\n", cfg->wk_swap);
    g_string_append_printf(gs, "\n");
    g_string_append_printf(gs, "ssbd_type       = %d\n", cfg->ssbd_type);
    g_string_append_printf(gs, "ssbd_record     = %d\n", cfg->ssbd_record);
    g_string_append_printf(gs, "ssbd_maxmin     = %d\n", cfg->ssbd_maxmin);
    g_string_append_printf(gs, "ssbd_diskfree   = %d\n", cfg->ssbd_diskfree);
    g_string_append_printf(gs, "ssbd_format     = 0x%x\n", cfg->ssbd_format);
    g_string_append_printf(gs, "ssbd_channels   = %d\n", cfg->ssbd_channels);
    g_string_append_printf(gs, "ssbd_samplerate = %d\n", cfg->ssbd_samplerate);
    g_string_append_printf(gs, "ssbd_plev       = %d\n", cfg->ssbd_plev);
    g_string_append_printf(gs, "ssbd_rlev       = %d\n", cfg->ssbd_rlev);
    g_string_append_printf(gs, "ssbd_template   = \"%s\"\n", S(cfg->ssbd_template));
    g_string_append_printf(gs, "\n");
    g_string_append_printf(gs, "ssbd_dsp        = \"%s\"\n", S(cfg->ssbd_dsp));
    g_string_append_printf(gs, "ssbd_maxfrag    = %d\n", cfg->ssbd_maxfrag);
    g_string_append_printf(gs, "ssbd_mixer      = \"%s\"\n", S(cfg->ssbd_mixer));
    g_string_append_printf(gs, "ssbd_oss_src    = \"%s\"\n", S(cfg->ssbd_oss_src));
    /*g_string_append_printf(gs, "ssbd_recsrc     = 0x%x\n", cfg->ssbd_recsrc);*/
    g_string_append_printf(gs, "\n");
    g_string_append_printf(gs, "ssbd_pcm_play    = \"%s\"\n", S(cfg->ssbd_pcm_play));
    g_string_append_printf(gs, "ssbd_pcm_rec     = \"%s\"\n", S(cfg->ssbd_pcm_rec));
    g_string_append_printf(gs, "ssbd_period_time = %d\n", cfg->ssbd_period_time);
    g_string_append_printf(gs, "ssbd_buffer_time = %d\n", cfg->ssbd_buffer_time);
    g_string_append_printf(gs, "ssbd_alsa_mixer  = \"%s\"\n", S(cfg->ssbd_alsa_mixer));
    g_string_append_printf(gs, "ssbd_alsa_src    = \"%s\"\n", S(cfg->ssbd_alsa_src));
    g_string_append_printf(gs, "\n");
    g_string_append_printf(gs, "ssbd_hostname   = \"%s\"\n", S(cfg->ssbd_hostname));
    g_string_append_printf(gs, "ssbd_udp_port   = %d\n", cfg->ssbd_udp_port);
    g_string_append_printf(gs, "\n");
    g_string_append_printf(gs, "ssbd_pa_play    = %d\n", cfg->ssbd_pa_play);
    g_string_append_printf(gs, "ssbd_pa_rec     = %d\n", cfg->ssbd_pa_rec);
/*    g_string_append_printf(gs, "ssbd_pipe       = %d\n", cfg->ssbd_pipe);
    g_string_append_printf(gs, "ssbd_command    = \"%s\"\n", cfg->ssbd_command);*/
    g_string_append_printf(gs, "\n\n");
    
    for (i=0; i<cfg->crotars->len; i++){
        struct config_rotar *crot;

        crot = (struct config_rotar *)g_ptr_array_index(cfg->crotars, i);
        g_string_append_printf(gs, "ROTAR = %d\n", crot->nr);
        g_string_append_printf(gs, "    rot_desc       = \"%s\"\n", S(crot->rot_desc));
        g_string_append_printf(gs, "    rot_type       = %d\n", crot->rot_type);
        g_string_append_printf(gs, "    rot_filename   = \"%s\"\n", S(crot->rot_filename));
        g_string_append_printf(gs, "    rot_hostname   = \"%s\"\n", S(crot->rot_hostname));
        g_string_append_printf(gs, "    rot_port       = %d\n", crot->rot_port);
        g_string_append_printf(gs, "    rot_vid        = 0x%04x\n", crot->rot_vid);
        g_string_append_printf(gs, "    rot_pid        = 0x%04x\n", crot->rot_pid);
        g_string_append_printf(gs, "    rot_serial     = \"%s\"\n", S(crot->rot_serial));
        g_string_append_printf(gs, "    rot_timeout_ms = %d\n", crot->rot_timeout_ms);
        g_string_append_printf(gs, "    rot_beamwidth  = %d\n", crot->rot_beamwidth);
        g_string_append_printf(gs, "    rot_saddr      = %d\n", crot->rot_saddr);
		g_string_append_printf(gs, "    rot_model      = %d\n", crot->rot_model);
		g_string_append_printf(gs, "    rot_speed      = %d\n", crot->rot_speed);
        g_string_append_printf(gs, "    rot_rem_rotstr = \"%s\"\n", S(crot->rot_rem_rotstr));
		g_string_append_printf(gs, "    rot_poll_ms    = %d\n", crot->rot_poll_ms);
		g_string_append_printf(gs, "    rot_offset     = %d\n", crot->rot_offset);
        g_string_append_printf(gs, "\n");
    }
    g_string_append_printf(gs, "\n");

	for (i = 0; i < cfg->crigs->len; i++){
		struct config_rig *crig;
		crig = (struct config_rig *)g_ptr_array_index(cfg->crigs, i);
        g_string_append_printf(gs, "RIG = %d\n", i);
		g_string_append_printf(gs, "    rig_enabled        = %d\n", crig->rig_enabled);
		g_string_append_printf(gs, "    rig_desc           = \"%s\"\n", S(crig->rig_desc));
		g_string_append_printf(gs, "    rig_filename       = \"%s\"\n", S(crig->rig_filename));
		g_string_append_printf(gs, "    rig_model          = %d\n", crig->rig_model);
		g_string_append_printf(gs, "    rig_speed          = %d\n", crig->rig_speed);
		g_string_append_printf(gs, "    rig_handshake_none = %d\n", crig->rig_verbose);
		g_string_append_printf(gs, "    rig_civaddr        = %d\n", crig->rig_civaddr);
		g_string_append_printf(gs, "    rig_lo             = %f\n", crig->rig_lo);
		g_string_append_printf(gs, "    rig_ssbcw_shift    = %d\n", crig->rig_ssbcw_shift);
		g_string_append_printf(gs, "    rig_poll_ms        = %d\n", crig->rig_poll_ms);
		g_string_append_printf(gs, "    rig_qrg_r2t        = %d\n", crig->rig_qrg_r2t);
		g_string_append_printf(gs, "    rig_qrg_t2r        = %d\n", crig->rig_qrg_t2r);
		g_string_append_printf(gs, "    rig_mode_t2r       = %d\n", crig->rig_mode_t2r);
		g_string_append_printf(gs, "    rig_clr_rit        = %d\n", crig->rig_clr_rit);
		g_string_append_printf(gs, "    rig_ptt_t2r        = %d\n", crig->rig_ptt_t2r);
		g_string_append_printf(gs, "    rig_verbose        = %d\n", crig->rig_verbose);
		g_string_append_printf(gs, "\n");
	}
    g_string_append_printf(gs, "\n");

    
    g_string_append_printf(gs, "loglines         = %d\n", cfg->loglines);
    g_string_append_printf(gs, "skedcount        = %d\n", cfg->skedcount);
    g_string_append_printf(gs, "startband        = \"%s\"\n", S(cfg->startband));
    g_string_append_printf(gs, "gfx_x            = %d\n", cfg->gfx_x);
    g_string_append_printf(gs, "gfx_y            = %d\n", cfg->gfx_y);
    g_string_append_printf(gs, "fontheight       = %d\n", cfg->fontheight);
    g_string_append_printf(gs, "slashkey         = \"%s\"\n", S(cfg->slashkey));
    g_string_append_printf(gs, "ntpq             = %d\n", cfg->ntpq);
    g_string_append_printf(gs, "dssaver          = %d\n", cfg->dssaver);
    g_string_append_printf(gs, "splitheight      = %d\n", cfg->splitheight);
    g_string_append_printf(gs, "fullscreen       = %d\n", cfg->fullscreen);
    g_string_append_printf(gs, "maximized        = %d\n", cfg->maximized);
    g_string_append_printf(gs, "usetouch         = %d\n", cfg->usetouch);
    g_string_append_printf(gs, "touchpos         = %d\n", cfg->touchpos);
    g_string_append_printf(gs, "altsyms          = %d\n", cfg->altsyms);
    g_string_append_printf(gs, "nolocks          = %d\n", cfg->nolocks);
    g_string_append_printf(gs, "portrait         = %d\n", cfg->portrait);
    g_string_append_printf(gs, "reverse          = %d\n", cfg->reverse);
    g_string_append_printf(gs, "inverse          = %d\n", cfg->inverse);
    g_string_append_printf(gs, "adif_export_mode = %d\n", cfg->adif_export_mode_t2r);
	g_string_append_printf(gs, "logdirpath      = \"%s\"\n", cfg->logdirpath);
	for (i = 0; i<cfg->takeoff->len; i++){
        struct takeoff *toff;
        toff=(struct takeoff*)g_ptr_array_index(cfg->takeoff, i);
        g_string_append_printf(gs, "takeoff         = \"%3d,%3d,%2d\"\n", toff->from, toff->to, toff->value);

    }
    g_string_append_printf(gs, "\n\n");
    g_string_append_printf(gs, "dxc_host = \"%s\"\n", S(cfg->dxc_host));
    g_string_append_printf(gs, "dxc_port = %d\n", cfg->dxc_port);
    g_string_append_printf(gs, "dxc_user = \"%s\"\n", S(cfg->dxc_user));
    g_string_append_printf(gs, "dxc_pass = \"%s\"\n", S(cfg->dxc_pass));
    g_string_append_printf(gs, "kst_user = \"%s\"\n", S(cfg->kst_user));
    g_string_append_printf(gs, "kst_pass = \"%s\"\n", S(cfg->kst_pass));
	g_string_append_printf(gs, "kst_name = \"%s\"\n", S(cfg->kst_name));
	g_string_append_printf(gs, "kst_maxqrb = %d\n", cfg->kst_maxqrb);
    g_string_append_printf(gs, "slovhf_user = \"%s\"\n", S(cfg->slovhf_user));
	g_string_append_printf(gs, "sota_user = \"%s\"\n", S(cfg->sota_user));
    g_string_append_printf(gs, "sota_pass = \"%s\"\n", S(cfg->sota_pass));
    g_string_append_printf(gs, "\n");

    g_string_append_printf(gs, "httpd_enable     = %d\n", cfg->httpd_enable);
    g_string_append_printf(gs, "httpd_port       = %d\n", cfg->httpd_port);
    g_string_append_printf(gs, "httpd_show_priv  = %d\n", cfg->httpd_show_priv);
    g_string_append_printf(gs, "httpd_refresh    = %d\n", cfg->httpd_refresh);
    g_string_append_printf(gs, "httpd_show_links = %d\n", cfg->httpd_show_links);
    g_string_append_printf(gs, "\n");
    
    g_string_append_printf(gs, "ac_enable   = %d\n", cfg->ac_enable);
    g_string_append_printf(gs, "ac_url      = \"%s\"\n", cfg->ac_url);
    g_string_append_printf(gs, "ac_kfactor  = %3.3f\n", cfg->ac_kfactor);
    g_string_append_printf(gs, "ac_arwidth  = %3.1f\n", cfg->ac_arwidth);
    g_string_append_printf(gs, "ac_minelev  = %3.1f\n", cfg->ac_minelev);
    g_string_append_printf(gs, "ac_maxelev  = %3.1f\n", cfg->ac_maxelev);
    g_string_append_printf(gs, "ac_mindur   = %d\n", cfg->ac_mindur);
    g_string_append_printf(gs, "ac_maxdelta = %3.1f\n", cfg->ac_maxdelta);
    g_string_append_printf(gs, "ac_minalt = %d\n", cfg->ac_minalt);
    g_string_append_printf(gs, "\n");

	g_string_append_printf(gs, "sdr_enable = %d\n", cfg->sdr_enable);
	g_string_append_printf(gs, "sdr_cw = %d\n", cfg->sdr_cw);
	g_string_append_printf(gs, "sdr_lsb = %d\n", cfg->sdr_lsb);
	g_string_append_printf(gs, "sdr_speed = %d\n", cfg->sdr_speed);
	g_string_append_printf(gs, "sdr_af_speed = %d\n", cfg->sdr_af_speed);
	g_string_append_printf(gs, "sdr_block = %d\n", cfg->sdr_block);
	g_string_append_printf(gs, "sdr_zero = %f\n", cfg->sdr_zero);
	g_string_append_printf(gs, "sdr_cw_low = %f\n", cfg->sdr_cw_low);
	g_string_append_printf(gs, "sdr_cw_high = %f\n", cfg->sdr_cw_high);
	g_string_append_printf(gs, "sdr_ssb_low = %f\n", cfg->sdr_ssb_low);
	g_string_append_printf(gs, "sdr_ssb_high = %f\n", cfg->sdr_ssb_high);
	g_string_append_printf(gs, "sdr_rec_dsp_type = %d\n", cfg->sdr_rec_dsp_type);
	g_string_append_printf(gs, "sdr_play_dsp_type = %d\n", cfg->sdr_play_dsp_type);
	g_string_append_printf(gs, "sdr_pa_rec = %d\n", cfg->sdr_pa_rec);
	g_string_append_printf(gs, "sdr_pa_play = %d\n", cfg->sdr_pa_play);
	g_string_append_printf(gs, "sdr_pcm_rec = \"%s\"\n", S(cfg->sdr_pcm_rec));
    g_string_append_printf(gs, "sdr_pcm_play = \"%s\"\n", S(cfg->sdr_pcm_play));
	fpath = g_strdup(S(cfg->sdr_sndfilename));
	z_unix(fpath);
	g_string_append_printf(gs, "sdr_sndfilename = \"%s\"\n", fpath);
	g_free(fpath);
	g_string_append_printf(gs, "sdr_remoterx = \"%s\"\n", S(cfg->sdr_remoterx));
    fpath = g_strdup(S(cfg->sdr_af_filename));
	z_unix(fpath);
	g_string_append_printf(gs, "sdr_af_filename = \"%s\"\n", fpath);
	g_free(fpath);
	g_string_append_printf(gs, "sdr_iqcomp = %d\n", cfg->sdr_iqcomp);
	g_string_append_printf(gs, "\n");

    g_string_append_printf(gs, "wiki_url = \"%s\"\n", S(cfg->wiki_url));
    g_string_append_printf(gs, "wiki_user = \"%s\"\n", S(cfg->wiki_user));
    g_string_append_printf(gs, "wiki_pass = \"%s\"\n", S(cfg->wiki_pass));
    g_string_append_printf(gs, "wiki_map = %d\n", cfg->wiki_map);
    g_string_append_printf(gs, "wiki_chart = %d\n", cfg->wiki_chart);
    g_string_append_printf(gs, "\n");

	g_string_append_printf(gs, "rain_enable = %d\n", cfg->rain_enable);
	g_string_append_printf(gs, "rain_meteox = %d\n", cfg->rain_meteox);
	g_string_append_printf(gs, "rain_wetteronline = %d\n", cfg->rain_wetteronline);
	g_string_append_printf(gs, "rain_chmi = %d\n", cfg->rain_chmi);
	g_string_append_printf(gs, "rain_weatheronline = %d\n", cfg->rain_weatheronline);
	g_string_append_printf(gs, "rain_rainviewer = %d\n", cfg->rain_rainviewer);
	g_string_append_printf(gs, "rain_debug = %d\n", cfg->rain_debug);
	g_string_append_printf(gs, "rain_maxqrb = %d\n", cfg->rain_maxqrb);
	g_string_append_printf(gs, "rain_mincolor = %d\n", cfg->rain_mincolor);
	g_string_append_printf(gs, "rain_minscpdist = %d\n", cfg->rain_minscpdist);
	g_string_append_printf(gs, "\n");


    for (i=0; i<cfg->cqs->len; i++){
        struct cq *cq;

        cq = (struct cq *) g_ptr_array_index(cfg->cqs, i);
        g_string_append_printf(gs, "CQ = %d\n", i);
        g_string_append_printf(gs, "    cw_str          = \"%s\"\n", S(cq->cw_str));
        g_string_append_printf(gs, "    cw_speed        = %d\n", cq->cw_speed);
		g_string_append_printf(gs, "    cw_repeat       = %d\n", cq->cw_repeat);
		g_string_append_printf(gs, "    cw_breakable    = %d\n", cq->cw_breakable);
        g_string_append_printf(gs, "    cw_ts           = %d\n", cq->cw_ts);
        g_string_append_printf(gs, "    cw_allowifundef = %d\n", cq->cw_allowifundef);
        g_string_append_printf(gs, "    ssb_file        = \"%s\"\n", S(cq->ssb_file));
        g_string_append_printf(gs, "    ssb_ts          = %d\n", cq->ssb_ts);
		g_string_append_printf(gs, "    ssb_repeat      = %d\n", cq->ssb_repeat);
		g_string_append_printf(gs, "    ssb_breakable   = %d\n", cq->ssb_breakable);
        g_string_append_printf(gs, "\n");
    }
    g_string_append_printf(gs, "\n");
    
    foreach(t, term_specs){
        g_string_append_printf(gs, "TERM = \"%s\"\n", S(t->term));
        g_string_append_printf(gs, "    term_mode         = %d\n", t->mode);
        g_string_append_printf(gs, "    term_m11_hack     = %d\n", t->m11_hack);
        g_string_append_printf(gs, "    term_restrict_852 = %d\n", t->restrict_852);
        g_string_append_printf(gs, "    term_col          = %d\n", t->col);
        g_string_append_printf(gs, "    term_utf_8_io     = %d\n", t->utf_8_io);
        g_string_append_printf(gs, "    term_charset      = %d\n", t->charset);
        g_string_append_printf(gs, "\n");
    }   
    g_string_append_printf(gs, "\n");
    
    for (i=0; i<gses->subwins->len; i++){
        struct subwin *sw;

        sw = (struct subwin *) g_ptr_array_index(gses->subwins, i);
        g_string_append_printf(gs, "SUBWIN = %d    # %d:%s\n", i, i + 1, sw->title);
        g_string_append_printf(gs, "    sw_type         = %d\n", sw->type);
        g_string_append_printf(gs, "    sw_command      = \"\"\n");
        g_string_append_printf(gs, "    sw_autorun      = \"\"\n");
        g_string_append_printf(gs, "    sw_respawn_time = 5\n");
        g_string_append_printf(gs, "\n");
    }   
    g_string_append_printf(gs, "\n");
    
    for (i=0; i<cfg->bands->len; i++){
        struct config_band *b;

        b = (struct config_band *) g_ptr_array_index(cfg->bands, i);
        g_string_append_printf(gs, "PBAND = \"%s\"\n", S(b->pband));
        g_string_append_printf(gs, "    bandchar = \"%c\"\n", tolower(b->bandchar));
        g_string_append_printf(gs, "    qrv = %d\n", b->qrv);
        g_string_append_printf(gs, "    psect = %d\n", b->psect);
        g_string_append_printf(gs, "    opsect = \"%s\"\n", S(b->opsect));
        g_string_append_printf(gs, "    readonly = %d\n", b->readonly);
        g_string_append_printf(gs, "\n");
        g_string_append_printf(gs, "    stxeq = \"%s\"\n", S(b->stxeq));
        g_string_append_printf(gs, "    spowe = \"%s\"\n", S(b->spowe));
        g_string_append_printf(gs, "    srxeq = \"%s\"\n", S(b->srxeq));
        g_string_append_printf(gs, "    sante = \"%s\"\n", S(b->sante));
        g_string_append_printf(gs, "    santh = \"%s\"\n", S(b->santh));
        g_string_append_printf(gs, "    mope1 = \"%s\"\n", S(b->mope1));
        g_string_append_printf(gs, "    mope2 = \"%s\"\n", S(b->mope2));
        g_string_append_printf(gs, "    remarks = \"%s\"\n", S(b->remarks));
        g_string_append_printf(gs, "\n");
        g_string_append_printf(gs, "    ok_section_single = \"%s\"\n", S(b->ok_section_single));
        g_string_append_printf(gs, "    ok_section_multi  = \"%s\"\n", S(b->ok_section_multi));
        g_string_append_printf(gs, "    qrg_min   = %d\n", b->qrg_min);
        g_string_append_printf(gs, "    qrg_max   = %d\n", b->qrg_max);
        g_string_append_printf(gs, "    adifband  = \"%s\"\n", S(b->adifband));
        g_string_append_printf(gs, "    cbrband  = \"%s\"\n", S(b->cbrband));
        g_string_append_printf(gs, "    stfband  = %d\n", b->stfband);
        g_string_append_printf(gs, "    skedqrg   = \"%s\"\n", S(b->skedqrg));
        g_string_append_printf(gs, "    band_lo   = %f\n", b->band_lo);
        g_string_append_printf(gs, "    band_sw   = 0x%02x\n", b->band_sw);
        g_string_append_printf(gs, "    wwlradius = %d\n", b->wwlradius);
        g_string_append_printf(gs, "\n");
    }
}

gint save_rc_file(gchar *filename){
    FILE *f;
    GString *gs;
    int ret, w, h;
	char *portrait_filename;
	char *reverse_filename;
    
#ifdef Z_HAVE_SDL
	cfg->maximized = zsdl_maximized(zsdl, &w, &h);
	if (cfg->maximized){
		cfg->gfx_x = w;
		cfg->gfx_y = h;
	}
#endif

    gs = g_string_sized_new(10000);

    f=fopen(filename,"wt");
    if (!f) {
/*        c = g_strconcat("Can't open file '", filename, "'", NULL);
        errbox(c,0);
        g_free(c);*/
        return errno;
    }

    save_rc_string(gs);
    ret = fprintf(f, "%s", gs->str) != gs->len ? errno:0;
    fclose(f);
    g_string_free(gs, TRUE);
	
    
    portrait_filename = g_strdup_printf("%s/portrait", tucnak_dir);
	if (cfg->portrait) {
		FILE *f = fopen(portrait_filename, "w");
		if (f) fclose(f);
	}else{
		unlink(portrait_filename);
	}

    reverse_filename = g_strdup_printf("%s/reverse", tucnak_dir);
	if (cfg->reverse) {
		FILE *f = fopen(reverse_filename, "w");
		if (f) fclose(f);
	}else{
		unlink(reverse_filename);
	}

    return ret;
}

int term_spec_init(void){
    struct term_spec *ts;
    
    if (!(ts = new_term_spec("linux"))) return -1;
    ts->mode = 1;
    ts->m11_hack = 1;
    ts->restrict_852 = 0;
    ts->col = 1;
    ts->utf_8_io = 0;
    ts->charset = 2;
    
    if (!(ts = new_term_spec("xterm"))) return -1;
    ts->mode = 1;
    ts->m11_hack = 1;
    ts->restrict_852 = 0;
    ts->col = 1;
    ts->utf_8_io = 0;
    ts->charset = 2;
    
    if (!(ts = new_term_spec("screen"))) return -1;
    ts->mode = 1;
    ts->m11_hack = 1;
    ts->restrict_852 = 0;
    ts->col = 1;
    ts->utf_8_io = 0;
    ts->charset = 2;
    
    if (!(ts = new_term_spec("cygwin"))) return -1;
    ts->mode = 2;
    ts->m11_hack = 1;
    ts->restrict_852 = 0;
    ts->col = 1;
    ts->utf_8_io = 0;
    ts->charset = 2;
    
    return 0;
}


char *parse_options(int argc, char **argv){
    int c;
    //char *s;

    while (1){
        
        /*int option_index = 0;
        static struct option long_options[] = {
//            {"debug", 2, 0, 'd'},
            {"help", 0, 0, 'h'},
            {"version", 0, 0, 'v'},
            {0, 0, 0, 0}
        };

        c = getopt_long (argc, argv, "dghikmstv?", long_options, &option_index);*/
        c = getopt(argc, argv, ":c:dghikmst?x");
        if (c == -1) break;

        switch (c){
            case 0:
/*              printf ("option %s", long_options[option_index].name);*/
                if (optarg) printf (" with arg %s", optarg);
                printf ("\n");
                break;
			case 'c':
				g_free(opt_tucnakrc);
				opt_tucnakrc = g_strdup(optarg);
				break;

            case 'd':  // handler by libzia, is here to prevent 'invalid option' message 
                break; 

            case 'g':
                opt_g++;
                break;
                
            case 'h':     
            case '?':
                /*printf ("option h\n");*/
                break;

            case 'i':
                opt_i++;
                break;

            case 'k':
                debug_keyboard=1;
                break;
            
            case 'm':
                opt_m++;
                break;
                            
            case 's':
                opt_s++;
                break;
           
            case 't':
                opt_t++;
                break;

			case 'x':
				opt_x++;
				break;
           
            default:
                printf ("?? getopt returned character code 0%o ??\n", c);
        }
    }

    if (optind < argc){
        printf ("non-option ARGV-elements: ");
        while (optind < argc) printf ("%s ", argv[optind++]);
        printf ("\n");
    }

#ifdef Z_MSC_MINGW
    opt_t = 0;
    opt_g = 1;
	//opt_i = 1;	
#endif
#ifdef Z_ANDROID
    opt_t = 0;
    opt_g = 1;
#endif
    return NULL;
}

gdouble get_rig_lo(struct band *band, int rignr){
    struct config_band *confb;
	struct config_rig *crig;

	crig = get_config_rig_by_number(cfg->crigs, rignr);

    if (!band) return crig->rig_lo;

    confb = get_config_band_by_bandchar(band->bandchar);
    if (!confb) return crig->rig_lo;

    if (!confb->band_lo) return crig->rig_lo;

    return confb->band_lo;
}

void set_rig_lo(struct band *band, int rignr, gdouble lo){
    struct config_band *confb;
	struct config_rig *crig;

	crig = get_config_rig_by_number(cfg->crigs, rignr);

    if (!band) goto global;

    confb = get_config_band_by_bandchar(band->bandchar);
    if (!confb) goto global;

    if (!confb->band_lo) goto global;
    confb->band_lo = lo;
    return;

global:;    
    crig->rig_lo = lo;
	/*for (i = 0; i < gtrigs->trigs->len; i++){
		struct trig *trig = (struct trig *)g_ptr_array_index(gtrigs->trigs, i);
		if (trig == rignr) trig->lo
	} */
}
