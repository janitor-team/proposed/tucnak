/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

    Interface for MSVC compiler
*/

#ifdef __MSVC2
#ifndef __MSVC2_H
//#define fopen(file, mode) fopen(z_wokna(file), mode)
#endif
#endif

#ifndef __MSVC_H
#define __MSVC_H

#include "stdafx.h"
#pragma warning(disable : 4018)
#pragma warning(disable : 4101)
#pragma warning(disable : 4244)
#pragma warning(disable : 4996)

//todo remove
#pragma warning(disable : 4028)
#pragma warning(disable : 4030)

#define _CRT_SECURE_NO_WARNINGS

//WINVER
//#define WINVER 0x0500
//#define _WIN32_WINNT 0x0500

#include <WinSock2.h>
#include <Windows.h>
#include <sys/timeb.h>
#include <time.h>				  
#include <direct.h>
#include <process.h>
#include <io.h>

#include "regex_.h"


#define PACKAGE "tucnak"
#include <msvcver.h>
#define PACKAGE_NAME PACKAGE
#define PACKAGE_VERSION VERSION
#define SHAREDIR 
#define HAVE_STDINT_H
#define HAVE_UINT32_T
#define HAVE_SDL
#define HAVE_ICONV_H
#define HAVE_PNG_H
#define HAVE_LIBPNG
#define HAVE_SNDFILE
#define HAVE_PORTAUDIO
#define HAVE_LIBFFTW3
#define HAVE_HAMLIB
//#define HAVE_LIBFTDI
//#define HAVE_FTDI_NEW
#define HAVE_LIBRTLSDR
#define HAVE_SHLOBJ_H
#define HAVE_RIG_SEND_VOICE_MEM
#define HAVE_RIG_SEND_MORSE
#define HAVE_RIG_STOP_MORSE
#define PKG "nsis"
#define DDIR ""

#ifndef HAVE_MATH_H
#define HAVE_MATH_H
#endif


#define strcasecmp _stricmp
#define strncasecmp _strnicmp
#define getpid _getpid

typedef int pid_t;
typedef int speed_t;
//typedef int iconv_t;

int main(int argc, char *argv[]);
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow);



#endif
