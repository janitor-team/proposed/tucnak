/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __WIZZ_H
#define __WIZZ_H

#include "header.h"
#include "qsodb.h"

struct wizz_qsomult_item{
	struct wizz_qsomult_item *next;
	char bandchar;
	int qsomult;
};

struct wizz_item{
    gchar *tname, *pexch;
    enum tttype tttype;
    int qsoused, qsomult, qsoglob, expmode;
    int wwlused, wwlbonu, wwlmult, wwlcfm; 
    int excused, excbonu, excmult, exccfm;
    enum exctype exctype;
    gchar *excname;
    int rstused, defrstr;
    int prefmult, prefglob;
    int dxcbonu, dxcmult;
    gint  qsop_method, total_method;
    gchar *bands;
	struct wizz_qsomult_item *qsomults;
};

struct wizz{
    GPtrArray *items; /* struct wizz_item */
};

extern struct wizz *wizz;

struct wizz *init_wizz(void);
void free_wizz_item(struct wizz_item *wi);
void free_wizz(struct wizz *wizz);
struct wizz_item *find_wizz(struct wizz *wizz, gchar *tname);

int load_wizz_from_file(struct wizz *wizz, gchar *filename);
void read_wizz_files(struct wizz *wizz);

struct wizz_item *get_wizz(struct wizz *wizz, int i);
struct wizz_item *find_wizz(struct wizz *wizz, gchar *tname);

#endif
