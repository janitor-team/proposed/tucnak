/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2020 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __MAIN_H
#define __MAIN_H


#define RET_OK      0
#define RET_ERROR   1
#define RET_SIGNAL  2
#define RET_SYNTAX  3
#define RET_FATAL   4

extern int retval;
extern char *starting_sbrk;
extern int first_contest_def;
extern struct zselect *zsel;
extern struct zfiledlg *zfiledlg;
extern char *tucnak_dir;
extern char *logs_dir;
extern char *home_dir;

void unhandle_terminal_signals(void);
void unhandle_basic_signals(void);
int term_attach_terminal(int, int, int);
void handle_basic_signals(struct terminal *term);
void sig_segv(void *arg);
void info(GString *gs);
void settings(GString *gs);

#ifdef Z_ANDROID
void android_trace_keys(char *s);
#endif

void redraw(void);
void set_logs_dir(void);

#endif
