
/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __DXC_H
#define __DXC_H

struct subwin;
struct event;
struct band;
union zsockaddr;

struct spot{
    double qrg; // kHz
    gchar *callsign, *from, *text;
    int zulu;
    time_t expire, endbold;
};

struct spotband{
    ZPtrArray *freq;
    int current;	// can be -1 if array is empty
	int min_khz, max_khz;
	char bandchar; //A, B, C, ...
};

struct spotdb_t{
	ZPtrArray *bands;
    int timer_id;
	int sfi;
};

extern struct spotdb_t *spotdb;

struct spotdb_t *init_spotdb(void);
void free_spotdb(struct spotdb_t *spotdb);
struct spotband *get_spotband_by_khz(int khz);
struct spotband *get_actual_spotband(void);


void remove_spot(struct spotband *sband, int spot_i);
struct spot *dxc_parse_spot(gchar *str);
void free_spot(struct spot *spot);
int dxc_read_spot(gchar *str); // ret: -1 error, 0 correct spot, 1 correct filtered spot
int dxc_remove_expired(void);
void dxc_timer(void *);
void dxc_dump(struct spotband *sband);


int sw_dxc_kbd_func(struct subwin *sw, struct event *ev, int fw);
int sw_dxc_mouse_func(struct subwin *sw, struct event *ev, int fw);
void sw_dxc_redraw(struct subwin *sw, struct band *band, int flags);
void sw_dxc_check_bounds(struct subwin *sw);
    
struct zasyncdns;
void sw_dxc_enter(void *enterdata, gchar *str, int cq);
void sw_dxc_addrinfo(struct zasyncdns *adns, int n, int *family, int *socktype, int *protocol, int *addrlen, union zsockaddr *addr, char *errorstr);
void sw_dxc_connected_handler(void *xxx);
void sw_dxc_read_handler(void *xxx);
void sw_dxc_disconnect(struct subwin *sw);

char *sw_dxc_call_under(struct subwin *sw, int x, int y, double *out_qrg, char **out_ref);
void sw_dxc_toggle_highlight(struct subwin *sw, char *call, int val);
int sw_dxc_line_color(struct subwin *sw, char *line);
char *sw_dxc_counterp(struct subwin *sw, char *line, int *color); // thread unsafe!
int sw_dxc_color(char *call);

void sw_dxc_select(void *itdata, void *menudata);
void sw_dxc_hide(void *itdata, void *menudata);
void sw_dxc_message(void *itdata, void *menudata);
void sw_dxc_info(void *itdata, void *menudata);
void sw_dxc_use(void *itdata, void *menudata);

#ifdef HAVE_HAMLIB
void dxc_qrg_changed(freq_t qrg);
#endif

void sw_dxc_spot_from_net(struct subwin *sw, char *str);
void dxc_open_connection(void *arg, void *arg2);

void dxc_export_text(void *itdata, void *menudata);

int dxc_new_mult(const char *callsign);

#endif
