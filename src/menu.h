/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2022 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __MENU_H
#define __MENU_H

#include "header.h"
struct dialog_data;
struct dialog_item_data;
struct menu;
struct qso;
struct session;
struct wizz_item;
struct zstring;

struct refresh {
    struct window *win;
    void (*fn)(void *arg);
    void *data;
    int timer;
};


#define CONDGFREE(item) { if (item) { g_free(item); item=NULL; }}

#define STORE_STR(base,item) \
    if (base->item) g_free(base->item); \
    base->item = g_strdup(item); 
    
#define STORE_STR_UC(base,item) \
    if (base->item) g_free(base->item); \
    base->item = g_strdup(item); \
    z_str_uc(base->item);

#define STORE_STR_FS(base,item) \
    if (base->item) g_free(base->item); \
    base->item = g_strdup(item); \
    fixsemi(base->item)    
    
#define STORE_STR_FS_UC(base,item) \
    if (base->item) g_free(base->item); \
    base->item = g_strdup(item); \
    fixsemi(base->item); \
    z_str_uc(base->item);

#define STORE_STR2(dbase,sbase,item) \
    if (dbase->item) g_free(dbase->item); \
    dbase->item = g_strdup(sbase->item); 

#define STORE_STR2_UC(dbase,sbase,item) \
    if (dbase->item) g_free(dbase->item); \
    dbase->item = g_strdup(sbase->item); \
    z_str_uc(dbase->item);

#define STORE_INT(base, item) \
    base->item = item; 

#define STORE_ENUM(base, item, type) \
    base->item = (type)item; 

#define STORE_SINT(base, item) \
    base->item = atoi(item##_str); 

#define STORE_SHEX(base, item) \
    base->item = strtol(item##_str, NULL, 16); 

#define STORE_SDBL(base, item) \
    base->item = atof(item##_str); 

#define STORE_STR3(base,item,src,first) \
    if (base->item) g_free(base->item); \
    base->item = g_strdup(ztokenize(src, first)); 


#define LOAD_STR(base,item) \
    safe_strncpy0(item, base->item, sizeof(item));

#define LOAD_INT(base,item) \
    item = base->item

#define LOAD_SINT(base,item) \
    g_snprintf(item##_str, sizeof(item##_str), "%d", base->item)

#define LOAD_SHEX2(base,item) \
    g_snprintf(item##_str, sizeof(item##_str), "%02x", base->item)

#define LOAD_SHEX4(base,item) \
    g_snprintf(item##_str, sizeof(item##_str), "%04x", base->item)

#define LOAD_SDBL(base,item) \
    g_snprintf(item##_str, sizeof(item##_str), "%1.0f", base->item)



struct tmpqso;
//int fileno(FILE *stream);
void activate_bfu_technology(int);
void free_history_lists(void);
void exit_prog(void *arg);
void menu_play_last(void *arg);
void menu_break_record(void *arg);
void menu_export_edi(void *arg);
void menu_export_sota(void *arg);
void menu_export_report(void *arg);
void menu_export_html(void *arg);
void menu_export_stats(void *arg);
void menu_export_titlpage(void *arg);
void free_namelist(void);
void menu_save_all(void *arg);
void do_edit_menu(void *arg);
void menu_contest_open(void *arg);
void free_namelist(void);
void do_peer_menu( void (* func)(void *arg));
void do_peer_operators_menu(void (* func)(void *arg));
void menu_load_from_peer(void *arg);
void menu_spy_from_peer(void *arg);
void menu_discover_peers3(void *itarg, void *menuarg);
void spy3(void *);
void do_spy_peer_menu(void (* func)(void *arg));
void menu_set_language(void *arg);
void menu_contest_config_from_peer(void *arg);
void send_contest_config_request(int no);
void open_from_net3(void *arg);
void menu_language_list(void *arg);
void subwins_menu(void *arg);
void refresh(struct refresh *r);
void refresh_abort(struct dialog_data *dlg);
void contest_choose(void (*funcall)(struct menu *menu));

/* menu1.c */
void band_settings(void *arg, int from_ctest);
void menu_wizz(void *arg);
void contest_options1(char *title, int from_ctest, struct wizz_item *wi);
void contest_options1_from_menu(void *arg);
void contest_options1_from_ctest(void *arg);
void contest_def(void *pfirst);
void edi_prop(void *arg);
int dlg_edi_prop(struct dialog_data *dlg, struct dialog_item_data *di);
void menu_responsible_op(void *arg);
void refresh_contest_options1(void *xxx);
void init_ctest2(int from_dlg, int updloc, struct zstring *zs, struct wizz_item *wi);

/* menu2.c */
void edit_qso(struct qso *qso);

/* menu3.c */
void duplicate_callsign(struct qso *qso);
void menu_add_error(void *arg);
void menu_chop(void *arg);
void menu_skedqrg(void *arg);
void menu_fillop(void *arg);
void menu_recalc_qrb(void *arg);
void menu_cw_update_contest(void *arg);
void menu_cw_update_band(void *arg);
void menu_exc_update_contest(void *arg);
void menu_exc_update_band(void *arg);
void menu_import_ebw(void *arg);
void menu_autosave(void *arg);
void menu_add_subwin(void *arg);
void menu_close_subwin(void *arg);
void menu_split_subwin(void *arg);
void menu_remove_split(void *arg);
void menu_unfinished(void *arg);

/* menu4.c */
void menu_cq_cw(void *arg);
void menu_cq_ssb(void *arg);
void menu_cwda(void *arg);
void menu_ssbd(void *arg);
void menu_network(void *arg);

/* menu5.c */
void misc_opts(void *arg);
#ifdef HAVE_HAMLIB
void menu_fixqrg(void *itdata, void *menudata);
#endif
void winkey_opts(void *arg);
int dlg_winkey_opts(struct dialog_data *, struct dialog_item_data *);
void menu_rig_opts(void *arg);
void menu_rigs(void *arg);
void menu_rotars(void *arg);

/* menu6.c */
void refresh_httpd_opts(void *arg);
void menu_httpd_opts(void *arg);
void load_from_net(void *arg);
void robands(void *arg);
void menu_set_mode(void *itdata, void *menudata);
void menu_ac_opts(void *arg);
void menu_sdr_opts(void *arg);

/* menu7.c */
void menu_rain_opts(void *itdata, void *menudata);


#endif
