/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2020  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

    Interface for MSVC compiler
*/

#include "header.h"

#include <string.h>
#include <stdio.h>
#include <tchar.h>
#include <portaudio.h>

#include "tsdl.h"

#include <Windows.h>
#include <Dbghelp.h>

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow){
	WSADATA wsaData;
    char **items;
	int argc;
	char *argv[100];
	char exepath[MAX_PATH];
	GString *gs;
#ifdef HAVE_HAMLIB
	const char *hlver;
#endif

	//rig_load_all_backends();
	//RIG *tmprig = rig_init(228);

	int nCodePage = GetACP(); 
	//zfence_init(16, 0xee, 0xeeeeeeee);

	//zfence_init(16, '\0', 0xee);
	WSAStartup(MAKEWORD(2, 0), &wsaData);


	/*error(zstr_shorten("0123456789", 6));
	error(zstr_shorten("0123456789", 5));
	error(zstr_shorten("012345678", 6));
	error(zstr_shorten("012345678", 5));*/

	gs = g_string_new("------ Settings: --------\n");
	g_string_append_printf(gs, "   version: %s-%s %s\n", PACKAGE, Z_PLATFORM, VERSION);
	g_string_append_printf(gs, "     iconv: %d.%d\n", _LIBICONV_VERSION >> 8, _LIBICONV_VERSION & 0xff);
	g_string_append_printf(gs, "    libgpm: no \n");
	g_string_append_printf(gs, "libsndfile: yes %s\n", sf_version_string());
	g_string_append_printf(gs, "      alsa: no\n");
#ifdef HAVE_HAMLIB
	hlver = strchr(hamlib_version, ' ');
	if (hlver) g_string_append_printf(gs, " libhamlib: yes %s\n", hlver + 1);  
	//g_string_append_printf(gs, " libhamlib: yes 3.0.1-zia\n");  
#else
	g_string_append_printf(gs, " libhamlib: no\n");
#endif
#ifdef HAVE_LIBFFTW3
	g_string_append_printf(gs, "  libfftw3: yes\n");
//	g_string_append_printf(gs, "  libfftw3: yes %s\n", fftw_version);
#else
	g_string_append_printf(gs, "  libfftw3: no\n");
#endif
#ifdef HAVE_PORTAUDIO
	g_string_append_printf(gs, " portaudio: yes %d.%02d\n", Pa_GetVersion() / 100, Pa_GetVersion() % 100);
#else
	g_string_append_printf(gs, " portaudio: no\n");
#endif
	g_string_append_printf(gs, "    inpout: yes\n");
	g_string_append_printf(gs, "leak debug: no\n");
	g_string_append_printf(gs, "instrument: no\n");

	txt_settings = g_strdup(gs->str);
	g_string_free(gs, TRUE);
	


    items = g_strsplit(lpCmdLine, " ", 0);

	
	if (GetModuleFileName(0, exepath, MAX_PATH) != 0)
		argv[0] = exepath;
	else
		argv[0] = "tucnak.exe";

//	MessageBox(NULL, argv[0], "argv[0]", MB_OK);

    for (argc=1; items[argc-1] != 0 && argc < 100; argc++){
		if (items[argc-1] == NULL) break;
        argv[argc] = items[argc-1];
    }

    main(argc, argv);
    g_strfreev(items);
    return 0;
}

char *txt_settings;

/* ntpq */
struct ntpq *ntpq;

struct ntpq *init_ntpq(){
	//progress("Initializing NTP monitor");		

	return NULL;
}

void free_ntpq(struct ntpq *ntpq){ 
	//progress("Terminating NTP monitor");		
}


