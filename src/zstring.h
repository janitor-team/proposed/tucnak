/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2011  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/


#ifndef __ZSTRING_H
#define __ZSTRING_H
    
#include "header.h"

struct zstring {
    int tokenpos;
    char *str;
#ifdef LEAK_DEBUG_LIST
    char *file;
    int line;
#endif
};

#ifdef LEAK_DEBUG_LIST
#define zconcatesc(x...) debug_zconcatesc(__FILE__, __LINE__, x)
#define zstrdup(x) debug_zstrdup(__FILE__, __LINE__, x)
struct zstring *debug_zconcatesc(char *file, int line, char *s1, ...);
struct zstring *debug_zstrdup(char *file, int line, const char *str);
#else
struct zstring *zconcatesc(char *s1, ...);
struct zstring *zstrdup(const char *str);
#endif
char *ztokenize(struct zstring *zstr, int first);
void zfree(struct zstring *zstr);
int ztokens(struct zstring *zstr);
int zwrite(int fd, struct zstring *zs);


#endif
