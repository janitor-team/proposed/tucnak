/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2016  Ladislav Vaiz <ok1zia@nagano.cz>
    and authors of web browser Links 0.96

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __HEADER_H
#define __HEADER_H

#define VOIPx

#ifdef _MSC_VER
#include "msvc.h"
#endif

#ifdef __MINGW32__
#include "mingw.h"
#endif

#ifdef ANDROID
#include "android.h"
#endif

#ifdef HAVE_CONFIG_H
#include "../config.h"
#endif

// must be before SDL, android port undefs HAVE_ICONV_H
#ifdef HAVE_ICONV_H
#include <iconv.h> 
#endif

#include <libzia.h>

#include <glib.h>
#include <glib/gprintf.h>


#include <zconfig.h>

//#if defined(Z_HAVE_SDL) && defined(HAVE_LIBFFTW3)
#if defined(HAVE_LIBFFTW3)
#define USE_FFT
#define USE_SDR
#endif


#ifndef __USE_POSIX 
#define __USE_POSIX
#endif
#ifndef __USE_POSIX2
#define __USE_POSIX2
#endif
#ifndef __USE_MISC
#define __USE_MISC
#endif
#ifndef __USE_BSD
#define __USE_BSD
#endif

#ifdef HAVE_MATH_H
#define _USE_MATH_DEFINES // for MSVC
#include <math.h>
#endif
 
#ifdef HAVE_SYS_IOCTL_H
#include <sys/ioctl.h>
#endif

#include <stdio.h>


#include <stdlib.h>
#include <stdarg.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <ctype.h>
#include <string.h>
#include <errno.h>
#ifdef HAVE_LIMITS_H
#include <limits.h>
#endif
//#include <sys/file.h>  not in msvc10
#include <sys/types.h>
#include <sys/stat.h>

#include "os_dep.h"

#ifdef TIME_WITH_SYS_TIME
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#ifdef HAVE_TIME_H
#include <time.h>
#endif
#else
#if defined(TM_IN_SYS_TIME) && defined(HAVE_SYS_TIME_H)
#include <sys/time.h>
#elif defined(HAVE_TIME_H)
#include <time.h>
#endif
#endif

#include <sys/stat.h>
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#ifdef HAVE_DIRENT_H
#include <dirent.h>
#endif
#include <signal.h>
#ifdef HAVE_SYS_WAIT_H
#include <sys/wait.h>
#endif
#ifdef HAVE_SYS_SELECT_H
#include <sys/select.h>
#endif

#ifdef HAVE_NETINET_IN_SYSTM_H
#include <netinet/in_systm.h>
#else
#ifdef HAVE_NETINET_IN_SYSTEM_H
#include <netinet/in_system.h>
#endif
#endif
#ifdef HAVE_NETDB_H
#include <netdb.h>
#endif
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#ifdef HAVE_WINSOCK2_H
#include <winsock2.h>
#endif
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif
#ifdef HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif
#ifdef HAVE_NETINET_IP_H
#include <netinet/ip.h>
#endif
#ifdef HAVE_UTIME_H
#include <utime.h>
#endif
#ifdef HAVE_PWD_H
#include <pwd.h>
#endif
#ifdef HAVE_GRP_H
#include <grp.h>
#endif
#ifdef HAVE_REGEX_H
#include <regex.h>
#endif


#ifdef HAVE_PTY_H
#include <pty.h>
#endif

#ifdef __CYGWIN__
#include <windows.h>
#endif

#ifdef HAVE_DLFCN_H
#include <dlfcn.h>
#endif



#if defined(Z_HAVE_SDL) && !defined(Z_HAVE_SDL1) && !defined(Z_HAVE_SDL2) 
#error Undefined SDL version
#endif


#ifdef HAVE_SYS_SOUNDCARD_H
#include <sys/soundcard.h>
#define HAVE_OSS
#endif

#ifdef HAVE_STDINT_H
#include <stdint.h>
#endif

#ifdef HAVE_INTTYPES_H
#include <inttypes.h>
#endif

#ifdef HAVE_SNDFILE
#include <sndfile.h>
#endif

#ifdef HAVE_LINUX_PPDEV_H
# include <linux/ppdev.h>
# include <linux/parport.h>
#endif

#ifdef HAVE_ALSA
#define ALSA_PCM_NEW_HW_PARAMS_API
#define ALSA_PCM_NEW_SW_PARAMS_API
#include <alsa/asoundlib.h>
#endif

#ifdef HAVE_HAMLIB
#define DLL_EXPORT // for windows import of hamlib_version[]
#include <hamlib/rig.h>
#include <hamlib/rotator.h>

#ifndef HAMLIB_FILPATHLEN
#define HAMLIB_FILPATHLEN FILPATHLEN
#endif

#endif

#ifdef HAVE_LIBFFTW3
#include <fftw3.h>
#endif

#ifdef HAVE_SWRADIO
#endif

#ifdef __GLIBC__
#include <gnu/libc-version.h>
#endif


#ifndef HAVE_UINT32_T
#if SIZEOF_CHAR == 4
typedef unsigned char uint32_t;
#elif SIZEOF_SHORT == 4
typedef unsigned short uint32_t;
#elif SIZEOF_INT == 4
typedef unsigned int uint32_t;
#elif SIZEOF_LONG == 4
typedef unsigned long uint32_t;
#elif defined(HAVE_LONG_LONG) && SIZEOF_LONG_LONG == 4
typedef unsigned long long uint32_t;
#else
#error You have no 32-bit integer type. Get in touch with reality.
#endif
#endif

#ifdef HAVE_LONG_LONG
#define longlong long long
#else
#define longlong long
#endif

/* integer type same size as void *   */
/*#undef vint
#if SIZEOF_SHORT == SIZEOF_VOID_P
typedef short vint;
#elif SIZEOF_INT == SIZEOF_VOID_P
typedef int vint;
#elif SIZEOF_LONG == SIZEOF_VOID_P
typedef long vint;
#elif defined(HAVE_LONG_LONG) && SIZEOF_LONG_LONG == SIZEOF_VOID_P
typedef long long vint;
#else
#error You have no integer type of size void *
#endif*/

#ifdef HAVE_TERMIOS_H
#include <termios.h>
#endif


#ifdef Z_HAVE_LIBFTDI
#include <ftdi.h>
#endif

/* zlib.h */
#include <zglib.h>
#include <zifaces.h>
#include <zloc.h>
#include <zptrarray.h>
#include <zsun.h>

#define VERSION_STRING          VERSION

#define DEBUG

#define MAX_HISTORY_ITEMS       256
#define MENU_HOTKEY_SPACE       2

#define MODE_MASK				0x07
#define DOUBLEHT_MASK			0x10
#define DOUBLEHB_MASK			0x20

#define COL(x)              ((x)*0x100)

#define COLOR_MENU1          COL(070)
#define COLOR_MENU_FRAME        COL(070)
#define COLOR_MENU_SELECTED     COL(007)
#define COLOR_MENU_HOTKEY       COL(007)

#define COLOR_MAINMENU          COL(070)
#define COLOR_MAINMENU_SELECTED     COL(007)
#define COLOR_MAINMENU_HOTKEY       COL(070)

#define COLOR_DIALOG            COL(070)
#define COLOR_DIALOG_FRAME      COL(070)
#define COLOR_DIALOG_TITLE      COL(007)
#define COLOR_DIALOG_TEXT       COL(070)
#define COLOR_DIALOG_CHECKBOX       COL(070)
#define COLOR_DIALOG_CHECKBOX_TEXT  COL(070)
#define COLOR_DIALOG_BUTTON     COL(070)
#define COLOR_DIALOG_BUTTON_SELECTED    COL(0107)
#define COLOR_DIALOG_FIELD      COL(007)
#define COLOR_DIALOG_FIELD_TEXT     COL(007)

#define SCROLL_ITEMS            2

#define DIALOG_LEFT_BORDER      3
#define DIALOG_TOP_BORDER       1
#define DIALOG_LEFT_INNER_BORDER    2
#define DIALOG_TOP_INNER_BORDER     0
#define DIALOG_FRAME            2

#define DIALOG_LB   (DIALOG_LEFT_BORDER + DIALOG_LEFT_INNER_BORDER + 1)
#define DIALOG_TB   (DIALOG_TOP_BORDER + DIALOG_TOP_INNER_BORDER + 1)

#define COL_BG         COL(0x07)
#define COL_NORM       COL(0x07)
#define COL_INV        COL(0x38)

#define COL_DARKRED     COL(0x01) 
#define COL_DARKGREEN   COL(0x02) 
#define COL_DARKYELLOW  COL(0x03) 
#define COL_DARKBLUE    COL(0x04) 
#define COL_DARKMAGENTA COL(0x05)
#define COL_DARKCYAN    COL(0x06)
#define COL_GREY        COL(0x07)

#define COL_DARKGREY    COL(0x40)
#define COL_RED         COL(0x41) 
#define COL_GREEN       COL(0x42) 
#define COL_YELLOW      COL(0x43) 
#define COL_BLUE        COL(0x44) 
#define COL_MAGENTA     COL(0x45)
#define COL_CYAN        COL(0x46)
#define COL_WHITE       COL(0x47)

#define ESC_TIMEOUT         200

#define RESOURCE_INFO_REFRESH       300

#define NET_PORT 55555
#define MAX_INTERFACES 16

/* msec */
#define UDP_ANNOUNCE 10000
#define CONNECTION_TIMEOUT 30000
#define SEND_ACK_TIMEOUT 10000
#define ACK_TIMEOUT 20000
#define REMOVE_TIMEOUT 10000
#define LONG_TIMEOUT 20000000
#define DELAY_AFTER_REPLICATION 1000
#define DELAY_AFTER_UPDATE_ROTAR 500

/* sec */
#define NET_GLOBAL_EXPIRE 60
#define NET_MAX_SKEW_POS 120
#define NET_MAX_SKEW_NEG 60

#define IL_HIST_LEN 30
#define MAX_CQ 6


#define QSONR_WIDTH 20
#define QSONR_HEIGHT 7
#define BAND_WIDTH 16
#define ORIG_Y 1


#ifdef __CYGWIN__
/* cfmakeraw is missing in cygwin. If fails in future versions of cywin, remove it */
void cfmakeraw (struct termios *);
#endif
	

/* for BSD */
#ifndef SOUND_MIXER_LINE 
#define SOUND_MIXER_LINE 6
#endif

#ifndef SOUND_MIXER_MIC 
#define SOUND_MIXER_MIC 7
#endif




/* error.c */

#include <zdebug.h>

void check_memory_leaks(void);
void error(char *, ...);
void init_debug(void);
void free_debug(void);
void sock_debug(int sock, char *m, ...);
void dbg_str_hash(GHashTable *hash);
extern int errline;
extern char *errfile;

#ifdef LEAK_DEBUG

extern long mem_amount;
extern long last_mem_amount;

#ifndef LEAK_DEBUG_LIST
struct alloc_header {
    int size;
};
#else
#define MCHK_LEAD 8
#define MCHK_TRAIL 8
#define MCHK_PATTERN ((char)0xee)
struct alloc_header {
    struct alloc_header *next;
    struct alloc_header *prev;
    int size;
    int line;
    char *file;
    char *comment;
};
#endif

#define L_D_S ((sizeof(struct alloc_header) + MCHK_LEAD + 7) & ~7)

#endif


#ifdef LEAK_DEBUG

void set_mem_comment(void *, char *, int);
void *debug_g_new0(char *, int, size_t);
void *debug_g_new(char *, int, size_t);
gchar *debug_g_strdup(char *, int, const gchar *);
gchar *debug_g_strndup(char *, int, gchar *, gint);
gchar *debug_g_strdup_printf(char *, int, gchar *, ...);
gchar *debug_g_strdup_vprintf(char *, int, gchar *, va_list args);
gchar *debug_g_strconcat(char *, int, ...);

//#define mem_alloc(x) debug_mem_alloc(__FILE__, __LINE__, x)
//#define mem_free(x) debug_mem_free(__FILE__, __LINE__, x)
//#define mem_realloc(x, y) debug_mem_realloc(__FILE__, __LINE__, x, y)
//#undef g_new0
//#define g_new0(x, y) debug_g_new0(__FILE__, __LINE__, sizeof(x)* y)        
//#undef g_new
//#define g_new(x, y) debug_g_new0(__FILE__, __LINE__, sizeof(x)* y)        
//#define g_free(x) debug_mem_free(__FILE__, __LINE__, x)
//#define g_realloc(x, y) debug_mem_realloc(__FILE__, __LINE__, x, y)
//#undef g_renew
//#define g_renew(t, m, n) debug_mem_realloc(__FILE__, __LINE__, m, sizeof(t) * n)
#define g_strdup(x) debug_g_strdup(__FILE__, __LINE__, x) 
#define g_strndup(x,y) debug_g_strndup(__FILE__, __LINE__, x, y) 
#define g_strdup_printf(x...) debug_g_strdup_printf(__FILE__, __LINE__, x) 
#define g_strdup_vprintf(x, y) debug_g_strdup_vprintf(__FILE__, __LINE__, x, y) 
#define g_strconcat(x...) debug_g_strconcat(__FILE__, __LINE__, x) 


#else


static inline void set_mem_comment(void *p, char *c, int l) {}

#endif


static inline char *fixsemi(char *str){
    char *c;
    
    if (!str) return str;
    c=str;
    while (*c!='\0'){
        if (*c==';') *c=':';
        c++;
    }
    return str;
} 

static inline char *chomp(char *s){
    int l;

    l = strlen(s);
    if (l > 0 && s[l - 1] == '\n') {
        s[l - 1] = '\0'; 
        l--;
    }
    if (l > 0 && s[l - 1] == '\r') s[l - 1] = '\0';

    return s;
}





/* Copies at most dst_size chars into dst. Ensures null termination of dst. */

static inline char *safe_strncpy(char *dst, const char *src, size_t dst_size) {
	g_strlcpy(dst, src, dst_size);
    return dst;
}


static inline char *safe_strncpy0(char *dst, const char *src, size_t dst_size) {
    if (!src) {
        *dst='\0';
        return dst;
    }

	g_strlcpy(dst, src, dst_size);
    return dst;
}



static inline int can_write(int fd)
{
    fd_set fds;
    struct timeval tv = {0, 0};
    FD_ZERO(&fds);
    FD_SET(fd, &fds);
    return select(fd + 1, NULL, &fds, NULL, &tv);
}

static inline int can_read(int fd)
{
    fd_set fds;
    struct timeval tv = {0, 0};
    FD_ZERO(&fds);
    FD_SET(fd, &fds);
    return select(fd + 1, &fds, NULL, NULL, &tv);
}


/* language.c */

#include "language.h"
#define TRANSLATE(text) text




/* default.c */

#define MAX_STR_LEN 1024
#define EQSO_LEN 15
#define DBL_LEN 30
#define TMP_QSOS 3
#define DISP_QSOS 2


#include "txts.h"


  /* settings.c */
extern char *txt_settings;



/* cabrillo.c */

int export_all_bands_cbr(void);


/* stf.c */

int export_all_bands_stf(void);


/* list.c */

void import_list(void *xxx, char *filename);
    

/* nevim */

#define q0(item) (item)?(item):""
struct takeoff{
    int from, to;
    int value;
};


#ifdef __clang__
#pragma clang diagnostic ignored "-Winvalid-source-encoding"
#endif

#endif /* __HEADER_H */     
