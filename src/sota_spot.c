/*
    Tucnak - SOTA spot sender
    Copyright (C) 2002-2020  Ladislav Vaiz <ok1zia@nagano.cz>;
    2020 Michal OK2MUF

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "bfu.h"
#include "qsodb.h"
#include "fifo.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include <libzia.h>

#include "sota.h"
#include "sota_api.h"
#include "sota_spot.h"

/*

Used auth RFC6750

POST /api/spots
            {
                "associationCode": "W5O",
                "summitCode": "OU-013",
                "activatorCallsign": "GM4ZFZ",
                "frequency": "14.0625",
                "mode": "cw",
                "comments": "test-ignore"
            }
*/

static void sota_spot_ask_comment(struct sota_spot_data **ssd, char *qrg);
static void sota_spot_ask_password(struct sota_spot_data **ssd, char *comment);
static void sota_spot_cfm(struct sota_spot_data **ssd, char *pwd);

char *get_sota_default_qrg(char bandchar, char mode)
{
    /* Recommented frequencies for SOTA QRP */
    switch (bandchar)
    {
        case 'u': //160:
            return mode == MOD_CW_CW ? "1.810" : "1.843";
        case 'v': //80:
            return mode == MOD_CW_CW ? "3.561" : "3.691";
        case 60:
            return "5354.3";
        case 'w': //40:
            return mode == MOD_CW_CW ? "7.032" : "7.118";
        case 30:
            return "10.118";
        case 'x': //20:
            return mode == MOD_CW_CW ? "14.061" : "14.285";
        case 17:
            return mode == MOD_CW_CW ? "18.096" : "18.130";
        case 'y': //15:
            return mode == MOD_CW_CW ? "21.060" : "21.285";
        case 12:
            return mode == MOD_CW_CW ? "24.910" : "24.950";
        case 'z': //10:
            return mode == MOD_CW_CW ? "28.060" : "24.950";
        case 'c': //2:
            return mode == MOD_FM_FM ? "145.425":"144.195"; // FM/SSB
        case 'e': //70:
            return "430.150";
        case 'g': //23:
            return "1297.500";
        default:
            return "";
    }
}


void sota_spot_push(void)
{
    char *token;
    static struct sota_spot_data *ssd = NULL;
    
    //char frequency[25];
    //char comment[128];
    //char password[128];

    if (!ctest)
    {
        log_addf(TRANSLATE("No active contest."));
        return;
    }

    if (!cfg->sota_user || !strlen(cfg->sota_user))
    {
        log_addf(TRANSLATE("No SOTA user configured."));
        return;
    }

    if (!ssd)
    {
        ssd = g_new0(struct sota_spot_data, 1);
        if (!ssd)
            return;
    }
    //if (!ctest->pexch || (ssd.summitCode = strstr(ctest->pexch,'/') == NULL)
    //{
    //    log_addf("No summit code in exchange contest configuration");
    //    return
    //}
    strncpy(ssd->_summit, ctest->pexch, sizeof(ssd->_summit)-1);
     
    token = strtok(ssd->_summit, "/"); //OK
    
    if ((ssd->associationCode = token) == NULL)
    {
        log_addf("No valid summit code in exchange contest configuration");
        return;
    }
    
    token = strtok(NULL, "/"); //MO-123
    if ((ssd->summitCode = token) == NULL)
    {
        log_addf("No valid summit code in exchange contest configuration");
        return;
    }

    ssd->id = 0;
    ssd->activatorCallsign = ctest->pcall;
    ssd->callsign = ctest->pcall;
    ssd->activatorName = ctest->rname;
    ssd->mode = get_sota_mode_ps(gses->mode);

    //ssd.comment = comment;
    //ssd.frequency = frequency;
    
    /* QGR, Comment, Password */
    /* Input chain sota_spot_ask_qrg -> sota_spot_ask_comment -> sota_spot_ask_pswd -> sota_spot_ask_cfm -> sota_spot_send */
    input_field(NULL, TRANSLATE("QRG"), TRANSLATE("Operating frequency") ,
                CTEXT(T_OK), CTEXT(T_CANCEL), &ssd, 
                NULL, 15, get_sota_default_qrg(aband->bandchar, gses->mode), 0, 0, NULL,
                (void (*)(void *, char *))sota_spot_ask_comment, NULL, 0);
    
    /* Nonblocking -> continue with sota_spot_ask_comment if T_OK */
}

static void sota_spot_ask_comment(struct sota_spot_data **ssd, char *qrg)
{
    if (!ssd || !qrg)
        return;
    
    strcpy((*ssd)->frequency, qrg);
    input_field(NULL, TRANSLATE("Comment"), TRANSLATE("Spot comment") ,
                CTEXT(T_OK), CTEXT(T_CANCEL), ssd, 
                NULL, 60, "CQ", 0, 0, NULL,
                (void (*)(void *, char *))sota_spot_ask_password, NULL, 0);
}

static void sota_spot_ask_password(struct sota_spot_data **ssd, char *comment)
{
    if (!*ssd || !comment)
        return;
    
    strcpy((*ssd)->comment, comment);
    if (!cfg->sota_pass || strlen(cfg->sota_pass)==0)
    {
        input_field(NULL, CTEXT(T_PASSWORD), TRANSLATE("SOTA account password") ,
                    CTEXT(T_OK), CTEXT(T_CANCEL), ssd, 
                    NULL, MAX_STR_LEN, cfg->sota_pass, 0, 0, NULL,
                    (void (*)(void *, char *))sota_spot_cfm, NULL, 0);
    }
    else
    {
        sota_spot_cfm(ssd, cfg->sota_pass);
    }
}

static void sota_spot_cfm(struct sota_spot_data **ssd, char *pwd)
{
    char *chr;
    GString *gs = g_string_sized_new(200);

    if (!ssd || !pwd)
    {
        g_string_free(gs, TRUE);
        return;
    }

    strcpy(cfg->sota_pass, pwd);
    
    g_string_append_printf(gs, TRANSLATE("Activator:   %10s\n"), (*ssd)->callsign);
    g_string_append_printf(gs, TRANSLATE("Summit Code: %3s/%s\n"), (*ssd)->associationCode, (*ssd)->summitCode);
    g_string_append_printf(gs, TRANSLATE("Frequency:   %6s MHz\n"), (*ssd)->frequency);
    g_string_append_printf(gs, TRANSLATE("Mode:        %10s\n"), (*ssd)->mode);
    g_string_append_printf(gs, TRANSLATE("Comment:     %10s\n\n"), (*ssd)->comment);
    g_string_append(gs, TRANSLATE("Send this spot?"));  

    chr = g_strdup(gs->str);
    g_string_free(gs, FALSE);

    msg_box(getml(chr, NULL), TRANSLATE("SPOT Submit"), AL_LEFT, 
        gs->str, ssd, 2, 
        VTEXT(T_YES), sota_spot_send, B_ENTER,
        VTEXT(T_NO), NULL, B_ESC);

    //sota_spot_send(&ssd);
}

void sota_spot_send(struct sota_spot_data **ssd)
{
   /*

   https://api2.sota.org.uk/docs/index.html
   
   input_field
                "associationCode": "W5O",
                "summitCode": "OU-013",
                "activatorCallsign": "GM4ZFZ",
                "frequency": "14.0625",
                "mode": "cw",
                "comments": "test-ignore"
   */

    /* Auth */
    /* Parse token */
    /* Send spot */
    /* Logout */
    struct sota_api_data *sad = sota_api_init();
    sad->ssd = *ssd;

    if (sad)
    {
#if SOTA_API_INSECURE
    sota_api_send_spot_insecure(sad);
#else
        sota_api_get_token(sad);
#endif
    }

//   sota_api_free(sad);
//   log_addf("To be implemented");
//   g_free((void *)*ssd);
//   *ssd = NULL;

}

/*
curl \
  -d "client_id=[client id from SOTA MT]" \
  -d "client_secret=[client secret from SOTA MT]" \
  -d "username=[user name]" \
  -d "scope=openid" \
  -d "password=[password]" \
  -d "grant_type=password" \
  "https://sso.sota.org.uk/auth/realms/SOTA/protocol/openid-connect/token"
*/

