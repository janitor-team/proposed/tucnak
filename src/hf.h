/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __HF_H
#define __HF_H

#include "header.h"
struct subwin;
struct event;
struct spot;
struct band;

int  sw_hf_kbd_func(struct subwin *sw, struct event *ev, int fw);
int  sw_hf_mouse_func(struct subwin *sw, struct event *ev, int fw);
void sw_hf_redraw(struct subwin *sw, struct band *band, int flags);
void sw_hf_check_bounds(struct subwin *sw);
void sw_hf_raise(struct subwin *sw);
void get_hf_dxc(char *str);
void sw_hf_draw_spot(struct subwin *sw, int x, int y, struct spot *spot, time_t now, int acolor);

#endif
