/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __MAP_H
#define __MAP_H

#include "qrvdb.h"
#include "qsodb.h"
struct qso;
struct band;
struct rain_provider;

#define MAP_AC_INFO 1

int  sw_map_kbd_func(struct subwin *sw, struct event *ev, int fw);
int  sw_map_mouse_func(struct subwin *sw, struct event *ev, int fw);
void sw_map_redraw(struct subwin *sw, struct band *band, int flags);
void sw_map_check_bounds(struct subwin *sw);
void sw_map_raise(struct subwin *sw);
int maps_reload(void);
int maps_update_showwwls(void);
void map_clear_qso(struct qso *qso, struct band *band);
void map_add_qso(struct qso *qso, struct band *band);
#ifdef Z_HAVE_SDL
void sw_map_redraw_rect(struct subwin *sw, SDL_Rect *area, struct band *band, int flags);
void sw_map_compose(struct subwin *sw, SDL_Rect *area);
void sw_map_redraw_rotar(struct subwin *sw, SDL_Rect *area, int flags);
void sw_map_redraw_icons(struct subwin *sw, SDL_Rect *area, int flags);
void sw_map_redraw_rain(struct subwin *sw, SDL_Rect *area, int flags);
void plot_rain_provider(struct subwin *sw, SDL_Rect *aarea, struct rain_provider *provider, int ci);
void plot_rain_titles(SDL_Surface *surface, SDL_Rect *area, int flags);
void sw_map_update_rotar(struct subwin *sw);
void km2px(struct subwin *sw, int kx, int ky, int *px, int *py);
void km2px_d(struct subwin *sw, double kx, double ky, int *px, int *py);
void px2km(struct subwin *sw, int px, int py, int *kx, int *ky);
void pxkm2o(struct subwin *sw, int px, int py, int kx, int ky, int *ox, int *oy);

void km2qrbqtf(int kx, int ky, double *qrb, double *qtf);

void plot_cor(struct subwin *sw, SDL_Surface *surface, SDL_Rect *area, int flags);
void plot_wkd_wwls(struct subwin *sw, SDL_Surface *surface, SDL_Rect *area);
void plot_qth(struct subwin *sw, SDL_Rect *area);
void plot_qrv(struct subwin *sw, SDL_Surface *surface, struct band *band, struct qrv_item *qi);
void plot_qrvs(struct subwin *sw, SDL_Surface *surface, struct band *band, SDL_Rect *area);
void plot_gst(struct subwin *sw, SDL_Surface *surface, SDL_Rect *area, double gst[360], int c);
void plot_qso(struct subwin *sw, SDL_Surface *surface, struct band *band, struct qso *qso);
void plot_qsos(struct subwin *sw, SDL_Surface *surface, struct band *band, SDL_Rect *area);
void plot_nearest_qso(struct subwin *sw, struct band *band);
void plot_nearest_qrv(struct subwin *sw, struct band *band);
void plot_info_qso(struct subwin *sw, SDL_Surface *surface, struct qso *qso);
void plot_info_qrv(struct subwin *sw, SDL_Surface *surface, struct band *band, struct qrv_item *qi);
int plot_qrb_qth(struct subwin *sw, SDL_Surface *surface, int mouse_x, int mouse_y);
void plot_rotars(struct subwin *sw, SDL_Surface *surface, SDL_Rect *area, SDL_Rect *outarea);

void map_update_layout(struct subwin *sw);
void move_map(struct subwin *sw, struct band *band, int dx, int dy);
void zoom(struct subwin *sw, double factor, int centerx, int centery);

int map_update_qth(struct subwin *sw);
void map_recalc_cors(void);
struct qso *find_nearest_qso(struct band *b, int mouse_x, int mouse_y);
gchar *find_nearest_qrv(struct qrvdb *qrvdb, struct band *band, int mouse_x, int mouse_y);
void map_for_photo(struct subwin *sw, struct band *band, int flags);
void map_recalc_gst(struct subwin *sw, struct band *band);

void plot_hgts(struct subwin *sw, SDL_Rect *area);
void map_cor_callback(void);

#endif




#endif
