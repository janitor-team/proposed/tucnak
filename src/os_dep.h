
/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2006  Ladislav Vaiz <ok1zia@nagano.cz>
    and authors of web browser Links 0.96

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.
*/

#ifndef __OS_DEP_H
#define __OS_DEP_H


#if defined(__EMX__)
#define OS2
#elif defined(_WIN32)
#ifndef WIN32
#define WIN32
#endif
#elif defined(__BEOS__)
#define BEOS
#else
#define UNIX
#endif

#ifdef __EMX__
#define strcasecmp stricmp
#define strncasecmp strnicmp
#define read _read
#define write _write
#endif

#if defined(UNIX)

static inline int dir_sep(char x) { return x == '/'; }
#define NEWLINE "\n"
#include <pwd.h>
#include <grp.h>
#define SYSTEM_ID SYS_UNIX
#define SYSTEM_NAME "Unix"
#define DEFAULT_SHELL "/bin/sh"
#define GETSHELL getenv("SHELL")

#elif defined(OS2)

static inline int dir_sep(char x) { return x == '/' || x == '\\'; }
#define NEWLINE "\r\n"
#define SYSTEM_ID SYS_OS2
#define SYSTEM_NAME "OS/2"
#define DEFAULT_SHELL "cmd.exe"
#define GETSHELL getenv("COMSPEC")
#define NO_FG_EXEC
#define NO_FILE_SECURITY
#define NO_FORK_ON_EXIT

#elif defined(WIN32)

static inline int dir_sep(char x) { return x == '/' || x == '\\'; }
#define NEWLINE "\r\n"
#define SYSTEM_ID SYS_WIN32
#define SYSTEM_NAME "Win32"
#define DEFAULT_SHELL "command.com"
#define GETSHELL getenv("COMSPEC")
#define NO_FG_EXEC
#define NO_FORK_ON_EXIT

#endif


#ifdef UNIX
#define tmkdir(dir, mode) mkdir(dir, mode)
//#define z_sock_errno errno
#define sock_strerror_r strerror_r
//#define SO4
#endif

int save_screen(void);
int restore_screen(void);

gchar * get_mp(gchar *device);
int floppy_is_mounted(gchar *floppydev, gchar *path_to_floppy);
int is_in_mtab(gchar *mp);


extern int sound_pid;
int init_sound(void);
int free_sound(void);
int abort_sound(void);
int sound(int freq);







struct terminal;


int is_xterm(void);
int can_twterm(void);
int term_get_terminal_size(int, int *, int *);
void handle_terminal_resize(int, void (*)(void *));
void unhandle_terminal_resize(int);
void set_bin(int);
int get_input_handle(void);
int get_output_handle(void);
int get_ctl_handle(void);
void *handle_mouse(int, void (*)(void *,  char *, int), void *);
void unhandle_mouse(void *);
int check_file_name( char *);
int start_thread(void (*)(void *, int), void *, int);
char *get_clipboard_text(void);
void set_clipboard_text(char *);
void set_window_title( char *);
char *get_window_title(void);
int is_safe_in_shell( char);
void check_shell_security( char **);
void block_stdin(void);
void unblock_stdin(void);
int exe(char *);
int resize_window(int, int);

#endif
