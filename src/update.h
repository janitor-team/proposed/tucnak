/*
    Tucnak - VHF contest log
    Copyright (C) 2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __UPDATE_H
#define __UPDATE_H

struct zserial;

void update_tucnak(void *a, void *b);
struct zserial *update_init_process(void);
void update_info(void *arg);


#endif
