/*
    slovhfnet.h - Upload claimed live score to slovhf.net
    Copyright (C) 2017 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __SLOVHFNET_H
#define __SLOVHFNET_H

#include <libzia.h>

struct slovhfnet{
	struct zhttp *http;
	char *status;
	int bandi;
};

struct slovhfnet *init_slovhfnet(void);
void free_slovhfnet(struct slovhfnet *slovhf);

//int slovhfnet_country(char *call);

void slovhfnet_upload(void *arg);
void slovhfnet_do_upload(void *arg);
void slovhfnet_sent(struct zhttp *http);
void slovhfnet_next_band(struct slovhfnet *slovhf);

void slovhfnet_abort(void *arg);

#endif
