/*
    Tucnak - VHF contest log
    Copyright (C) 2013 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __VOIP_H
#define __VOIP_H

#include "header.h"

#ifdef HAVE_SNDFILE

#ifdef VOIP

#define VOIPMAX 512  // maximal packet size in sizeof(short)

struct ssbd;

struct voippeer {
    char *peerid;
    struct sockaddr_in sin;
};

struct voip {
	GList *jitterbuf;
	uint32_t seq;
	int dupe;
	int len;
    int sock;

    // for sender (spied)
    GPtrArray *spying; // of struct voippeer
    MUTEX_DEFINE(spying);

    // for receiver (spying)
    unsigned short udpport; 
    struct voippeer *spied;
    GThread *thread;
    int thread_break;
};

struct voippacket {
	uint32_t seq;
	uint16_t len; // in samples
	short buf[1]; // really len items
};


struct voip *init_voip(void);
void free_voip(struct voip *voip);

struct voippacket *init_voippacket(void *srcbuf, int buflen); // len is in bytes
void free_voippacket(struct voippacket *p);

int voip_add(struct voip *voip, struct voippacket *p);
void voip_test(struct voip *voip);

void voip_spy_add(struct voip *voip, char *id, char *voipaddr);
void voip_spy_update(struct voip *voip, char *id, char *voipaddr);
void voip_spy_remove(struct voip *voip, char *id);
void voip_spy_free(struct voippeer *p);
void voip_spy_dump(struct voip *voip);

void voip_spy(void *xxx);
void voip_end_spy(void *xxx, void *yyy);

void voip_distribute(struct voip *voip, struct voippacket *vp);

int voip_receive(struct ssbd *ssbd);
void *voip_thread_func(void *arg);
//void voip_thread_create(struct voip *voip);
//void voip_thread_join(struct voip *voip);
    

#endif
#endif
#endif
