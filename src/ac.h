/*
    ac.h - aircraft trace
    Copyright (C) 2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"

#include "qsodb.h"

#ifndef __AC_H
#define __AC_H

struct band;
struct qso;
struct subwin;
struct qso;
struct zhttp;
struct qrv_item;

struct ac_result{
	int crossing; // next valid only when crossing == 1
    int under;
	double startkx, startky, stopkx, stopky;
	time_t start, stop;
	// for testing
	time_t now;
	double angle; 
};

#define ACZO_LOWALT 0
#define ACZO_NORMAL 1
#define ACZO_CROSSING 2
#define ACZO_CURSOR 3

#define AC_ID_MAXLEN 7
#define AC_ICAO_MAXLEN 5


struct ac{
	char id[AC_ID_MAXLEN]; // ICAO 24-bit address
	double w, h;
	double qtfdir; // radians
    int asl; // meters
    int speed; // km/h
	time_t when; 
	
    // state
    double kx, ky; // actual position
	double pakx, paky; // predicted path after 10 minutes
	double qtf; // from me

	struct ac_result tmpres, infores, qrvres; 
	int zorder;
	gchar icao[AC_ICAO_MAXLEN];
	int wingspan; // feets
};

struct acs_shared{
	char *data; // http in main thread -> acs_thread
	GPtrArray *acs;
	double myh, myw;

    double tmpctpqrb, tmpctpqtf;
    int new_tmpctp;

    double infoctpqrb, infoctpqtf;
    int new_infoctp;

	char tracked[AC_ID_MAXLEN];
	int rotnr;
};

struct acs_thread{
	double myh, myw;
	GPtrArray *acs;
};

struct ac_counterpart{
    double qrb;
    double qtfrad; 
    double minqrb, maxqrb;
    double arm1kx, arm1ky, arm2kx, arm2ky; // middle of area
	double arkx[4], arky[4]; // area visible from both stations
	int path_valid;
    
};

struct acs{
	// only main thread
//	struct subwin *sw;
	char *url;
    int normal, active;
	GThread *thread;
	int http_timer;
	struct zhttp *http;
	struct qso infolocqso;
    time_t validity;

	// shared without lock
	int thread_break;

    struct ac_counterpart tmpqso;// TODO -> th
    struct ac_counterpart infoqso;// TODO -> th
    struct ac_counterpart qrvqso;// TODO -> th
	
	// shared
	struct acs_shared sh;
	MUTEX_DEFINE(sh);

	// only thread
	struct acs_thread th;
	GPtrArray *infos; // of ac_info
};

struct ac_info{
	gchar icao[5];
	int wingspan;
};

extern struct acs *gacs;

struct acs *init_acs(void);
void free_acs(struct acs *acs);
void acs_load(struct acs *acs, const char *data); // called in thread
void acs_load2(struct acs *acs, const char *data); // called in thread
void ac_compute(struct acs *acs, struct ac *ac, struct ac_counterpart *ctp, struct ac_result *res); // called in thread
#ifdef Z_HAVE_SDL
void plot_path(struct subwin *sw, SDL_Surface *surface, struct band *band, struct qso *qso, struct ac_counterpart *ctp);
void plot_ac(struct subwin *sw, struct ac *ac);
void plot_info_ac(struct subwin *sw, SDL_Surface *surface, struct ac *ac);

void acs_redraw(struct subwin *sw, struct acs *acs);
#endif
void ac_redraw(void);

double acs_min_asl(struct acs *acs, double qrb, struct band *b, double elev_rad);
double acs_max_qrb(struct acs *acs, double asl, struct band *b, double elev_rad);
void acs_http_timer(void *arg);
void acs_goturl(struct zhttp *http);
void acs_downloaded_callback(struct zhttp *http);
gpointer acs_thread(gpointer arg);
void acs_update_qth(struct acs *acs);
void ac_update_tmpctp(char *wwl);
void ac_update_infoctp(char *wwl);
void ac_format(struct qrv_item *qi, char *acstart, char *acint, int flags);

void init_acs_infos(struct acs *acs);
void free_acs_infos(struct acs *acs);
int acs_get_wingspan(struct acs *acs, char *icao);
struct ac *ac_under(struct acs *acs, struct subwin *sw);
void ac_track(struct acs *acs, char *id, int rotnr);

#endif
