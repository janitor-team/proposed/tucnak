/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2015  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"

#include "cwdb.h"
#include "ebw.h"
#include "fifo.h"
#include "language2.h"
#include "main.h"
#include "namedb.h"

static int nlocs = 0;
static int nnames = 0;

int load_ebw_from_file(struct cw *cw, struct namedb *namedb, char *filename){
    FILE *f;
    struct dbfhdr hdr;
    struct dbffield *fields,*field;
    int i, j;
    unsigned char *record;
    int ret=0, ofs;
	int callofs,calllen,wwlofs,wwllen,nameofs,namelen;
    gchar *call,*wwl,*name;
	
    f=fopen(filename, "rb");
    if (!f) {
        dbg("Can't open file %s\n", filename);
        return -1;
    }

    if (fread(&hdr, sizeof(struct dbfhdr), 1, f)!=1){
        fclose(f);
        return -2;
    }

    fields=g_new0(struct dbffield, hdr.hdrsize/sizeof(struct dbffield));
    if (fread(fields, hdr.hdrsize-sizeof(struct dbfhdr), 1, f)!=1){
        g_free(fields);
        fclose(f);
        return -3;
    }
    
	callofs=wwlofs=nameofs=-1;
	calllen=wwllen=namelen=0;

    for (i=0, field=fields, ofs=0;
		 ;
		 i++, field++){
        if (field->name[0]==0x0d) break;
		dbg("name='%s' len=%d\n", field->name, field->len);
        if (strcasecmp(field->name, "CALL")==0){
            callofs=ofs;
			calllen=field->len;
		}
        if (strcasecmp(field->name, "LOCATOR")==0){
            wwlofs=ofs;
			wwllen=field->len;
		}
        if (strcasecmp(field->name, "VORNAME")==0){
            nameofs=ofs;
			namelen=field->len;
		}
		
		ofs+=field->len;
    }

	if (!calllen) {
		log_addf("No CALL column if file %s\n", filename);
		goto x;
	}

	if (!wwllen || !namelen){
		log_addf("No LOCATOR or VORNAME column if file %s\n", filename);
		goto x;
	}

    fseek(f, hdr.hdrsize, SEEK_SET);
    record=g_new0(unsigned char, hdr.recsize);
    
    for (j=0; j<hdr.records; j++){
        int retx;
        retx = fread(record, hdr.recsize, 1, f); retx++;
        if (record[0]=='*') continue; /* deleted record */
      
		call=wwl=name=NULL;
		
		call=g_new0(char, calllen+1);
        memcpy(call, record+callofs, calllen);
        g_strstrip(call);
		z_str_uc(call);

		if (wwllen){
			wwl=g_new0(char, wwllen+1);
      	  	memcpy(wwl, record+wwlofs, wwllen);
       	 	g_strstrip(wwl);
			z_str_uc(wwl);
        }

		if (namelen){
			name=g_new0(char, namelen+1);
      	  	memcpy(name, record+nameofs, namelen);
       	 	g_strstrip(name);
        }

		if (wwl && strlen(wwl)==6){
			if (!find_wwl_by_call(cw, call)){  /*only unknown calls, we don't know which is newer*/
				add_cw(cw, call, wwl, 19700101, NULL);
				add_wc(cw, wwl, call, 19700101);
				nlocs++;
			}
		}
		
		if (namedb && name && strlen(name)>=2){
		  	add_namedb(namedb, call, name);   
			nnames++;
		}

		g_free(call);
		zg_free0(wwl);
		zg_free0(name);
    }
    g_free(record);
    ret=0;
x:;
    fclose(f);
    
	g_free(fields);
    return ret;
    
    
}

int read_ebw_files(struct cw *cw, struct namedb *namedb){
    gchar *s, *s1;
	int ret;
    
	nlocs = 0;
	nnames = 0;
    s = g_strconcat(tucnak_dir, "/vhf.dbf", NULL); 
	z_wokna(s);
    ret=load_ebw_from_file(cw, namedb, s);
	if (ret==0){
		log_addf(VTEXT(T_LOADED_LOCS_NAMES), nlocs, nnames, s);
        g_free(s);
		return 0;		
	}
    g_free(s);
	
#ifdef Z_UNIX
	s1 = SHAREDIR"/vhf.dbf";
	ret=load_ebw_from_file(cw, namedb, s1);
	if (ret==0){
		log_addf(VTEXT(T_LOADED_LOCS_NAMES), nlocs, nnames, s1);
		return 0;		
	}else{
		log_addf(VTEXT(T_CANT_READ), s, s1);
	}
#endif
	return ret;
}
