/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2020 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __RC_H
#define __RC_H

#include "header.h"
struct band;

struct config_band{
    gchar bandchar;         /* 'C', 'E', 'G',  ... */
    gchar *pband;           /* 144 MHz */ 
    gint psect;             /* 1=Single, 0=Multi */
	char *opsect;
    gint qrv,qrvnow,readonly;
    
    gchar *stxeq,*spowe;        /* TM-255 + gi7b, 300 */ 
    gchar *srxeq,*sante,*santh; /* TM-255, GW4PTS, 60;800 */
    gchar *mope1,*mope2;    /* OK1XDF, OK1MZM, OK1ZIA */
    gchar *remarks;

       /* a little hack for OK  */
    gchar *ok_section_single;  /* 2m=01, 70cm=03, ... */
    gchar *ok_section_multi;   /* 2m=02, 70cm=04, ... */
    gint qrg_min,qrg_max;      /* kHz */
	gchar *adifband;           /* 2m,70cm,23cm... */  
	gchar *cbrband;            /* 2M,432,1.2G... */  
    gint stfband;            /* 2, 70, 23... */  
    gchar *skedqrg;
    gdouble band_lo;
    int band_sw;
    int wwlradius;
};

struct config {   /* depending on callsign */
    gchar *pcall;
    gchar *pwwlo;
    gchar *pexch;

    gchar *padr1,*padr2,*pclub;
    gchar *rname,*rcall,*radr1,*radr2,*rpoco,*rcity,*rcoun,*rphon,*rhbbs;

    gchar *default_rs,*default_rst;
    gint qsop_method, total_method;
    
    gint as_disk_aq, as_disk_am, as_floppy_aq, as_floppy_am;
    gint as_disk_fsync, as_floppy_fsync, as_mount;
    gchar *as_floppy_path, *as_mount_cmd;

    gchar *net_if_ignore, *net_ip_ignore, *net_ip_announce;
	int net_remote_enable;
    char *net_remote_host, *net_remote_pass;
    int net_remote_port;
	int net_masterpriority;
    
    GPtrArray *bands;
    struct config_band *band;  /* active band during loading config */
    GPtrArray *cqs;            /* of struct cq */
    struct cq *cq;
    struct term_spec *ts;
    GPtrArray *sws;            /* of struct config_subwin */
    struct config_subwin *sw;
    
    gint cwda_type;  /* enum cwtype */
    gchar *cwda_device, *cwda_hostname;
    gint cwda_udp_port, cwda_io_port;
    gint cwda_speed, cwda_weight, cwda_minwpm, cwda_maxwpm;
    gint cwda_leadin, cwda_tail, cwda_spk;
    gint cwda_vid, cwda_pid, cwda_autgive;
    gint wk_wk2, wk_usepot, wk_usebut, wk_keymode, wk_swap;
    
    gint ssbd_type;
    gint ssbd_record, ssbd_maxmin, ssbd_diskfree;
    gint ssbd_format, ssbd_channels, ssbd_samplerate;
    gint ssbd_plev, ssbd_rlev;
    gchar *ssbd_template; 

    gchar *ssbd_pcm_play, *ssbd_pcm_rec;
    gint ssbd_period_time; // period time in ms
	gint ssbd_buffer_time;
    gchar *ssbd_alsa_mixer, *ssbd_alsa_src;
    int ssbd_pa_play, ssbd_pa_rec;

    gchar *ssbd_dsp;
    gint ssbd_maxfrag;
    gchar *ssbd_mixer, *ssbd_oss_src;
    gint ssbd_recsrc;

    gchar *ssbd_hostname;
    gint ssbd_udp_port;
    
    gint trace_bcast, trace_sock, trace_recv, trace_send, trace_qsos, trace_rig, trace_sdev, trace_keys;
    
	GPtrArray *crigs;	 /* of struct config_rig */
	struct config_rig *crig;
    
    GPtrArray *crotars;  /* of struct config_rotar */
    struct config_rotar *crotar;
    int loglines,skedcount;
    gchar *startband;
    GPtrArray *takeoff; /* of struct takeoff */
    int global_operator;
    gchar *operator_;
	int gfx_x, gfx_y, ntpq, dssaver, splitheight, fullscreen, maximized;
	int usetouch, touchpos, altsyms, nolocks, portrait, reverse, inverse;
	int adif_export_mode_t2r;
	char *logdirpath;

    int fontheight;
    char *slashkey;

	char *dxc_host;
	int dxc_port;
	char *dxc_user, *dxc_pass;
	char *kst_user, *kst_pass, *kst_name;
    char *sota_user, *sota_pass;
	int kst_maxqrb;
	char *slovhf_user;

    int httpd_enable;
    int httpd_port;
    int httpd_show_priv;
    int httpd_refresh;
    int httpd_show_links;

    int ac_enable;       // aircraft is enabled
    char *ac_url;        // URL for fligtradar data
    double ac_kfactor;   // troposphere k_factor
    double ac_arwidth;   // area width in km
    double ac_minelev;   // minimal elevation angle
    double ac_maxelev;   // maximal elevation angle
    int ac_mindur;       // minimal reflections duration in seconds
    double ac_maxdelta;  // max difference of incidence and reflection angles
	int ac_minalt;       // minimal AC altitude to show on map in meters

	int sdr_enable, sdr_cw, sdr_lsb, sdr_speed, sdr_af_speed, sdr_block, sdr_iqcomp;
	double sdr_zero, sdr_cw_low, sdr_cw_high, sdr_ssb_low, sdr_ssb_high;
	int sdr_rec_dsp_type, sdr_play_dsp_type;
    int sdr_pa_play, sdr_pa_rec;
	char *sdr_pcm_play, *sdr_pcm_rec, *sdr_sndfilename, *sdr_remoterx, *sdr_af_filename;

	char *wiki_url, *wiki_user, *wiki_pass, *wiki_page;
	int wiki_map, wiki_chart, wiki_overwrite;

    int rain_enable, rain_meteox, rain_wetteronline, rain_chmi, rain_weatheronline, rain_rainviewer, rain_debug;
	int rain_maxqrb, rain_mincolor, rain_minscpdist; 
};


extern struct config *cfg;
extern int opt_g, opt_i, opt_m, opt_s, opt_t, debug_keyboard, opt_x;
extern long long tl;
extern char *opt_tucnakrc;

gint init_rc(void);
void free_config_rig(struct config_rig *crig);
void free_config_rotar(struct config_rotar *crot);
gint free_rc(void);
gint read_rc_line(gchar *str);
gint read_rc_file(gchar *filename, const char *text);
gint read_rc_files(void);
void save_rc_string(GString *gs); 
gint save_rc_file(gchar *filename);
int term_spec_init(void);
char *parse_options(int argc, char **argv);
gdouble get_rig_lo(struct band *band, int rignr);
void set_rig_lo(struct band *band, int rignr, gdouble lo);




#endif
