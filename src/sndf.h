/*
    Tucnak - VHF contest log
    Copyright (C) 2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __SNDF_H
#define __SNDF_H

#include "header.h"
#include "dsp.h"

#ifdef HAVE_SNDFILE
int sndfile_open(struct dsp *dsp, int rec);
int sndfile_close(struct dsp *dsp);
int sndfile_write(struct dsp *dsp, void *data, int frames);
int sndfile_read(struct dsp *dsp, void *data, int frames);
int sndfile_reset(struct dsp *dsp);
int sndfile_sync(struct dsp *dsp);
int sndfile_set_format(struct dsp *dsp, SF_INFO *sfinfo, int rec);
int sndfile_set_sdr_format(struct dsp *dsp, int frames, int speed, int rec);

#endif

#endif
