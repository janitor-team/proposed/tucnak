/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2016 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __TRIG_H
#define __TRIG_H

// must be present also for --without-hamlib
struct config_rig{
    int nr;

	int rig_enabled;
	char *rig_desc;
    char *rig_filename;
    int rig_model, rig_civaddr;
    int rig_speed;
	int rig_handshake_none;
    double rig_lo;          /* qrg displayed on rig + rig_lo = real_qrg_on_air 
                               use get/set_rig_lo for access */
    int rig_ssbcw_shift;   /* Hz added to qrg on change ssb->cw */
    int rig_poll_ms;
    int rig_qrg_r2t;
    int rig_qrg_t2r;
    int rig_mode_t2r;
    int rig_clr_rit;
	int rig_ptt_t2r;
    int rig_verbose;
};


#ifdef HAVE_HAMLIB

#include <zthread.h>

enum trig_cmd { 
    TRIG_FREQ, TRIG_MODE, TRIG_MODE_FREQ, TRIG_RESENDFREQ, TRIG_RIT, TRIG_PTT, TRIG_TOGGLE_SPLIT_VFO, TRIG_SEND_VOICE_MEM, TRIG_SEND_MORSE, TRIG_STOP_MORSE
};

struct trig_job{
    struct trig_job *next; 
    struct trig_job *prev; 
    enum trig_cmd cmd;
    rmode_t mode;
    freq_t freq;
    shortfreq_t rit;
	ptt_t ptt;
	int ch;
	char *msg;

};

struct trig{
	// config
	int nr; // same as corresponding record in struct config_trig
	int poll_ms;
    rig_model_t model;
    char *filename;
    int speed;
	int handshake_none;
	int civaddr;
	int qrg_t2r;
	int mode_t2r;
	int ssbcw_shift;
	int clr_rit;
	int ptt_t2r;


	// state
    GThread *thread;
    int thread_break;
    //int pipe_read, pipe_write;
    struct trig_job jobs;
    MUTEX_DEFINE(jobs);

    RIG *rig;
    int locked;
    int rigerr;
};

struct trigs{
    double qrg;
    shortfreq_t rit;
	GPtrArray *trigs; // of struct trig
    int rigerr;
	vfo_t split_vfo;  // accessed from thread
};

extern struct trigs *gtrigs;
extern ZPtrArray *riglist; // of struct rig_caps



// trigs functions
void init_trigs(void);
void free_trigs(struct trigs *trigs);
void trigs_read_handler(struct trigs *trigs, int n, char **items);
void trigs_set_qrg(struct trigs *trigs, double qrg);
void trigs_set_qrg_except(struct trigs *trigs, int except_rigr, double qrg);
void trigs_set_rit_except(struct trigs *trigs, int except_rigr, double rit);
void trigs_resend_freq(struct trigs *trigs, int rignr);
void trigs_set_ptt(struct trigs *trigs, int ptt);
void trigs_toggle_split_vfo(struct trigs *trigs);
void trigs_send_voice_mem(struct trigs *trigs, int ch);
void trigs_send_morse(struct trigs *trigs, const char *msg);
void trigs_stop_morse(struct trigs *trigs);



// trig functions
struct trig *init_trig(struct config_rig *crig);
void free_trig(struct trig *trig);

void trig_job_add(struct trig *trig, struct trig_job *job);
struct trig_job *trig_job_get(struct trig *trig);

gpointer trig_main(gpointer xxx);
void trig_set_mode(struct trig *trig, rmode_t mode);
void trig_set_qrg(struct trig *trig, double qrg);
void trig_set_mode_qrg(struct trig *trig, rmode_t mode, double qrg);
void trig_resend_freq(struct trig *trig);
void trig_set_rit(struct trig *trig, shortfreq_t rit);
void trig_set_ptt(struct trig *trig, ptt_t ptt);
void trig_toggle_split_vfo(struct trig *trig);
void trig_send_voice_mem(struct trig *trig, int ch);
void trig_send_morse(struct trig *trig, const char *msg);
void trig_stop_morse(struct trig *trig);

int trig_save_riglist(const struct rig_caps *caps, void *data);
int trig_compare (const void *a, const void *b);
char *trig_short_errstr(enum rig_errcode_e error);
#endif


#endif
