
/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __WINKEY_H
#define __WINKEY_H

#include "header.h"
struct cwdaemon;

int winkey_init_serial(struct cwdaemon *);
int winkey_init_tcp   (struct cwdaemon *);
int winkey_open   (struct cwdaemon *cwda, int verbose);
int winkey_free   (struct cwdaemon *);
int winkey_reset  (struct cwdaemon *);
int winkey_cw     (struct cwdaemon *, int onoff);
int winkey_ptt    (struct cwdaemon *, int onoff);
int winkey_ssbway (struct cwdaemon *, int onoff);
int winkey_text   (struct cwdaemon *cwda, char *text);
int winkey_speed  (struct cwdaemon *cwda, int wpm);
int winkey_weight (struct cwdaemon *cwda, int weight);
int winkey_tune   (struct cwdaemon *cwda, int tune);
int winkey_back   (struct cwdaemon *cwda);

void winkey_read_handler(void *arg);
int winkey_read(struct cwdaemon *cwda, int timeout_ms);


int winkey4_init   (struct cwdaemon *);
int winkey4_free   (struct cwdaemon *);
int winkey4_reset  (struct cwdaemon *);


#endif
