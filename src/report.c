/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2020  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"
#include "bfu.h"
#include "fifo.h"
#include "qsodb.h"
#include "stats.h"
#include "tsdl.h"

int write_report(struct band *b, struct config_band *confb, FILE *f){
    int year, month, day, date;
    char s[24];
    GString *gs;
    int ret=0;
    
    gs = g_string_sized_new(1000);
    
    date = b->stats->first_date;
    day = date%100;
    month = (date/100)%100;
    year = (date/10000)%10000;
    
    g_string_append_printf(gs, "s OK1KPA\n");
    g_string_append_printf(gs, "Hlaseni ze zavodu %s %d/%d %s\n", ctest->tname, month, year, z_str_uc(safe_strncpy0(s, ctest->pcall, 21)));
    g_string_append_printf(gs, "\n\n");
    g_string_append_printf(gs, "Hlaseni ze zavodu %s %d/%d \n", ctest->tname, month, year);
    g_string_append_printf(gs, "\n");
    g_string_append_printf(gs, "Hlaseni ze zavodu:      %s  dne %02d.%02d.%2d \n", ctest->tname, day, month, year);
    g_string_append_printf(gs, "Kategorie:              %s (%s %s)\n", b->psect ? confb->ok_section_single : confb->ok_section_multi,
                b->pband, b->psect ? "Single" : "Multi");
    g_string_append_printf(gs, "Znacka v zavode:        %s \n", z_str_uc(safe_strncpy0(s, ctest->pcall, 21)));
    g_string_append_printf(gs, "Lokator:                %s \n", z_str_uc(safe_strncpy0(s, ctest->pwwlo, 7)));
    g_string_append_printf(gs, "Pocet platnych spojeni: %d \n", b->stats->nqsos);
    g_string_append_printf(gs, "Pocet bodu za spojeni:  %d \n", b->stats->nqsop);
    g_string_append_printf(gs, "Pocet nasobicu:         %d \n", g_hash_table_size(b->stats->wwls));
    g_string_append_printf(gs, "Vysledek:               %d \n", b->stats->ntotal);
    g_string_append_printf(gs, "\n");
    g_string_append_printf(gs, "Prohlasuji, ze jsem dodrzel(a) podminky zavodu, \n");
    g_string_append_printf(gs, "uvedeny vysledek odpovida skutecnosti. \n");
    g_string_append_printf(gs, "%30s\n", ctest->pcall);
    g_string_append_printf(gs, "\n");
    g_string_append_printf(gs, "/ack\n");
    g_string_append_printf(gs, "/ex\n");
    
    
    if (fprintf(f, "%s", gs->str) != gs->len) ret=1;
       
    g_string_free(gs, TRUE);
    return ret;
}


int export_all_bands_report(){
    struct band *band;
    struct config_band *confb;
    int i, err;
    gchar *filename;
    FILE *f;
    char callbuf[20];
    
    if (!ctest) return -1;
    
    for (i=0; i<ctest->bands->len; i++){
        band = g_ptr_array_index(ctest->bands, i);
        confb = get_config_band_by_bandchar(band->bandchar);

		progress(VTEXT(T_EXPORTING_S), band->bandname);

		stats_thread_join(band);
        if (band->stats->nqsos <=0) continue;
        
        filename = g_strdup_printf("%s/%s%s.txt",
                        ctest->directory,
                        band->psect ? confb->ok_section_single : confb->ok_section_multi,
                        z_get_raw_call(callbuf,ctest->pcall));
        
		z_wokna(filename);
        f=fopen(filename,"wt");
        if (!f) {
            log_addf(VTEXT(T_CANT_WRITE_S), filename);
            errbox(VTEXT(T_CANT_WRITE), errno);
            g_free(filename);
			progress(NULL);
            return -1;
        }
        err=write_report(band,confb,f);
        fclose(f);
        if (err) {
            log_addf(VTEXT(T_CANT_WRITE_S), filename);
            errbox(VTEXT(T_CANT_WRITE), 0);
            g_free(filename);
			progress(NULL);
            return -1;
        }            
        log_addf(VTEXT(T_SAVED_S), filename);
        g_free(filename);
    }
	progress(NULL);
    return 0;
}


