/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2015  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"

#include "bfu.h"
#include "chart.h"
#include "edi.h"
#include "excdb.h"
#include "fifo.h"
//#include "html.h"
#include "inputln.h"
#include "main.h"
#include "map.h"
#include "menu.h"
#include "net.h"
#include "qrvdb.h"
#include "qsodb.h"
#include "rain.h"
#include "rc.h"
#include "stats.h"
#include "terminal.h"
#include "tsdl.h"
#include "wizz.h"
    
char tname[MAX_STR_LEN],pcall[MAX_STR_LEN],pclub[MAX_STR_LEN];
char pwwlo[MAX_STR_LEN],pexch[MAX_STR_LEN],tdate[MAX_STR_LEN];
int qsoused;
char qsomult_str[MAX_STR_LEN];
int qsoglob;
int wwlused;
char wwltype_str[MAX_STR_LEN];
char wwlbonu_str[MAX_STR_LEN], wwlmult_str[MAX_STR_LEN];
int wwlcfm;
int exctype;
char exctype_str[MAX_STR_LEN];
char excbonu_str[MAX_STR_LEN], excmult_str[MAX_STR_LEN];
int exccfm;
char excname[MAX_STR_LEN];
int excused;
char prefmult_str[MAX_STR_LEN];
char dxcbonu_str[MAX_STR_LEN], dxcmult_str[MAX_STR_LEN];
int prefglob;
int rstused, defrstr;
char default_rst[MAX_STR_LEN],default_rs[MAX_STR_LEN];
static char operator_[EQSO_LEN];
int tttype;
char tttype_str[MAX_STR_LEN];
int expmode, phase;

char new_excname[MAX_STR_LEN], old_excname[MAX_STR_LEN];
/*int qsop_method=1, total_method=1;*/

/******************** BAND SETTINGS *******************************/

#define RESP_LEN 50
int qrvnow,psect,isqrv,readonly;
char opsect[RESP_LEN];
char stxeq[RESP_LEN], spowe[RESP_LEN], srxeq[RESP_LEN], sante[RESP_LEN];
char santh[RESP_LEN], mope1[RESP_LEN], mope2[RESP_LEN];
char remarks[RESP_LEN];
char ok_section_single[RESP_LEN], ok_section_multi[RESP_LEN];
char qrg_min_str[RESP_LEN], qrg_max_str[RESP_LEN];
char adifband[RESP_LEN], skedqrg[RESP_LEN], band_lo_str[RESP_LEN];
char band_sw_str[RESP_LEN];
char wwlradius_str[RESP_LEN];

//int bandfirstbutt;

void refresh_band_confb(void *xxx)
{
    struct config_band *confb;
    struct band *band;
        
/*    dbg("refresh_band_confb\n");    */
    confb=(struct config_band *)xxx;


    confb->qrvnow = qrvnow;
    confb->psect  = psect;
	STORE_STR(confb, opsect);
    confb->qrv    = isqrv;
    confb->readonly = readonly;
    dbg("refresh_band readonly=%d\n", readonly);
    STORE_STR(confb,stxeq);
/*    dbg("refresh_band confb=%p confb->stxeq='%s'\n",confb,confb->stxeq);*/
    /*b=confb;*/
    STORE_STR(confb,spowe);
    STORE_STR(confb,srxeq);
    STORE_STR(confb,sante);
    STORE_STR(confb,santh);
    STORE_STR(confb,mope1);
    STORE_STR(confb,mope2);
    STORE_STR(confb,remarks);
    STORE_STR(confb, ok_section_multi);
    STORE_STR(confb, ok_section_single);
    STORE_SINT(confb, qrg_min);
    STORE_SINT(confb, qrg_max);
    STORE_STR(confb, adifband);
    STORE_STR(confb, skedqrg);
    STORE_SDBL(confb, band_lo);
    STORE_SHEX(confb, band_sw);
    STORE_SINT(confb, wwlradius);
    
    band = find_band_by_pband(confb->pband);
    if (band != NULL){
        int oldwwlradius = band->wwlradius;
        STORE_SINT(band, wwlradius);
        if (band->wwlradius != oldwwlradius) {
            band->dirty_statsf = 1;
            recalc_statsfifo(band);
        }

    }
}

void refresh_band(void *xxx)
{
    struct band *band;
    struct zstring *zs;
    char psectstr[EQSO_LEN];
    gchar *c;
        
/*    dbg("refresh_band\n");    */
    band=(struct band *)xxx;

/*  band->qrv = qrvnow;*/ 
    band->psect  = psect;
	STORE_STR(band, opsect);
    /*band->qrv    = qrv;*/
    band->readonly = readonly;
    il_readonly(band->il, band->readonly);
    STORE_STR(band,stxeq);
/*    dbg("refresh_band band=%p band->stxeq='%s'\n",band,band->stxeq);*/
    STORE_STR(band,spowe);
    STORE_STR(band,srxeq);
    STORE_STR(band,sante);
    STORE_STR(band,santh);
    STORE_STR(band,mope1);
    STORE_STR(band,mope2);
    STORE_STR(band,remarks);
/*    STORE_STR(band, ok_section_multi);
    STORE_STR(band, ok_section_single);
    STORE_SINT(band, qrg_min);
    STORE_SINT(band, qrg_max);
    STORE_STR(band, adifband);
    STORE_STR(band, skedqrg);
    STORE_SDBL(band, band_lo);*/


    g_snprintf(psectstr, EQSO_LEN, "%d", band->psect);
    zs = zconcatesc(band->pband, psectstr, stxeq, spowe, 
            srxeq, sante, santh, mope1, 
            mope2, remarks, NULL);
    // ok_section_*, qrg_*, adifband, skedqrg a band_lo se neposilaji
    c = g_strdup_printf("BA %s\n", zs->str);
    rel_write_all(c);
    g_free(c);
    zfree(zs);
}


                                                   
/* xxx is char *band */
void band_settings(void *arg, int from_ctest)
{
    struct dialog *d;
    int i,found;
    char *pband;
    struct config_band *confb=NULL;
    struct band *band=NULL;
    static char ss[1024];

    found=0;
	pband = (char *)arg;

    if (!from_ctest)
        for (i=0;i<cfg->bands->len; i++){

            confb = (struct config_band*)g_ptr_array_index(cfg->bands,i);
            if (!confb) break;
            if (strcasecmp(pband, confb->pband)==0){
                found=1;
                break;
            }
        }
    else
        for (i=0;i<ctest->bands->len; i++){

            band = (struct band*)g_ptr_array_index(ctest->bands,i);
            if (!band) break;
            if (strcasecmp(pband, band->pband)==0){
                found=1;
                break;
            }
        }

    
    if (!found){ zinternal("band_settings() !found"); }
    
    if (confb){
        qrvnow=confb->qrvnow;
        psect=confb->psect;
		LOAD_STR(confb, opsect);
        isqrv=confb->qrv;
        readonly=confb->readonly;
        dbg("band_settings readonly=%d\n", readonly);
        safe_strncpy0(stxeq,confb->stxeq,RESP_LEN);
        safe_strncpy0(spowe,confb->spowe,RESP_LEN);
        safe_strncpy0(srxeq,confb->srxeq,RESP_LEN);
        safe_strncpy0(sante,confb->sante,RESP_LEN);
        safe_strncpy0(santh,confb->santh,RESP_LEN);
        safe_strncpy0(mope1,confb->mope1,RESP_LEN);    z_str_uc(mope1);
        safe_strncpy0(mope2,confb->mope2,RESP_LEN);    z_str_uc(mope2);
        safe_strncpy0(remarks,confb->remarks,RESP_LEN);
        safe_strncpy0(ok_section_single,confb->ok_section_single,RESP_LEN);
        safe_strncpy0(ok_section_multi,confb->ok_section_multi,RESP_LEN);
        g_snprintf(qrg_min_str, RESP_LEN, "%d", confb->qrg_min);
        g_snprintf(qrg_max_str, RESP_LEN, "%d", confb->qrg_max);
        safe_strncpy0(adifband,confb->adifband,RESP_LEN);
        safe_strncpy0(skedqrg,confb->skedqrg,RESP_LEN);
        g_snprintf(band_lo_str, RESP_LEN, "%1.1f", confb->band_lo);
        g_snprintf(band_sw_str, RESP_LEN, "0x%02x", confb->band_sw);
        g_snprintf(wwlradius_str, RESP_LEN, "%d", confb->wwlradius);
    }else{
        qrvnow=1;
        psect=band->psect;
		LOAD_STR(band, opsect);
        isqrv=1;
        readonly=band->readonly;
        safe_strncpy0(stxeq,band->stxeq,RESP_LEN);
        safe_strncpy0(spowe,band->spowe,RESP_LEN);
        safe_strncpy0(srxeq,band->srxeq,RESP_LEN);
        safe_strncpy0(sante,band->sante,RESP_LEN);
        safe_strncpy0(santh,band->santh,RESP_LEN);
        safe_strncpy0(mope1,band->mope1,RESP_LEN);    z_str_uc(mope1);
        safe_strncpy0(mope2,band->mope2,RESP_LEN);    z_str_uc(mope2);
        safe_strncpy0(remarks,band->remarks,RESP_LEN);
        /*safe_strncpy0(ok_section_single,band->ok_section_single,RESP_LEN);
        safe_strncpy0(ok_section_multi,band->ok_section_multi,RESP_LEN);
        g_snprintf(qrg_min, RESP_LEN, "%d", band->qrg_min);
        g_snprintf(qrg_max, RESP_LEN, "%d", band->qrg_max);
        safe_strncpy0(adifband,band->adifband,RESP_LEN);
        safe_strncpy0(skedqrg,band->skedqrg,RESP_LEN);
        g_snprintf(band_lo, RESP_LEN, "%1.1f", band->band_lo);
        band_sw
        wwlradius 
         */

    }

    if (!(d = (struct dialog *)g_malloc(sizeof(struct dialog) + 55 * sizeof(struct dialog_item)))) return;
    memset(d, 0, sizeof(struct dialog) + 55 * sizeof(struct dialog_item));
    d->title = ss;
	d->fn = dlg_pf_fn;
	d->y0 = 1;

    if (confb){
        g_snprintf(ss,1000,VTEXT(T_BAND_SETTINGS_SC), confb->bandchar,pband);
        d->refresh = (void (*)(void *))refresh_band_confb;
        d->refresh_data = confb;
    }else{
        g_snprintf(ss,1000,VTEXT(T_BAND_SETTINGS_SC), band->bandchar,pband);
        d->refresh = (void (*)(void *))refresh_band;
        d->refresh_data = band;
    }
    

    d->items[i=0].type = D_CHECKBOX;    /* 0 */
    d->items[i].gid  = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&isqrv;
	d->items[i].msg = CTEXT(T_QRV_ON_THIS_BAND);
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid  = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&qrvnow;
    d->items[i].msg = CTEXT(T_QRV_IN_THIS_CTEST);

    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid  = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&readonly;
    d->items[i].msg = CTEXT(T_READ_ONLY);
   	d->items[i].wrap = 1;  

    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid  = 1;
    d->items[i].gnum = 0;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&psect;
    d->items[i].msg =  CTEXT(T_MULTI);

    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid  = 1;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&psect;
    d->items[i].msg = CTEXT(T_SINGLE);

    d->items[++i].type = D_CHECKBOX;   
    d->items[i].gid  = 1;
    d->items[i].gnum = 2;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&psect;
    d->items[i].msg =  CTEXT(T_CHECK);  

    d->items[++i].type = D_CHECKBOX;   
    d->items[i].gid  = 1;
    d->items[i].gnum = 3;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&psect;
    d->items[i].msg =  CTEXT(T_OTHER);  
   		
	d->items[++i].type = D_FIELD;
    d->items[i].dlen = RESP_LEN;
	d->items[i].maxl = 15;
    d->items[i].data = opsect;
    d->items[i].msg = "";
    d->items[i].wrap = 2;
	
	d->items[++i].type = D_FIELD;
    d->items[i].dlen = RESP_LEN;
    d->items[i].data = stxeq;
    d->items[i].msg = CTEXT(T_TXEQUIP);
    d->items[i].wrap = 1;
            
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = 10;
    d->items[i].data = spowe;
	d->items[i].msg = CTEXT(T_POWER);
	//d->items[i].fn = check_spowe;
   	d->items[i].wrap = 1;

  
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = RESP_LEN;
    d->items[i].data = srxeq;
    d->items[i].msg = CTEXT(T_RXEQUIP);
   	d->items[i].wrap = 1;

    d->items[++i].type = D_FIELD;
    d->items[i].dlen = RESP_LEN;
    d->items[i].data = sante;
    d->items[i].msg = CTEXT(T_ANTENNA);
    d->items[i].wrap = 1;

    d->items[++i].type = D_FIELD;   
    d->items[i].dlen = 10;
    d->items[i].data = santh;
    d->items[i].msg = CTEXT(T_AGLASL);    
    d->items[i].wrap = 1;

    d->items[++i].type = D_FIELD;
    d->items[i].dlen = RESP_LEN;
    d->items[i].data = mope1;
    d->items[i].msg = CTEXT(T_OPS);
    d->items[i].wrap = 1;

    d->items[++i].type = D_FIELD;
    d->items[i].dlen = RESP_LEN;
    d->items[i].data = mope2;
    d->items[i].msg = CTEXT(T_SPACES1);
    d->items[i].wrap = 1;

    d->items[++i].type = D_FIELD;  
    d->items[i].dlen = RESP_LEN;
    d->items[i].data = remarks;
    d->items[i].msg = CTEXT(T_REMARKS);   
    d->items[i].wrap = 2;

    if (confb){

        d->items[++i].type = D_FIELD;
        d->items[i].dlen = 4;
        d->items[i].data = ok_section_single;
        d->items[i].msg = CTEXT(T_OK_SECTION_SINGLE); 

        d->items[++i].type = D_FIELD;
        d->items[i].dlen = 4;
        d->items[i].data = ok_section_multi;
		d->items[i].msg = CTEXT(T_OK_SECTION_MULTI); 
    	d->items[i].wrap = 1;

        d->items[++i].type = D_FIELD;
        d->items[i].dlen = 10;
        d->items[i].data = qrg_min_str;
        d->items[i].fn   = check_number;
        d->items[i].gid  = 0;
        d->items[i].gnum = 250000000;
        d->items[i].msg = CTEXT(T_QRG_MIN);

        d->items[++i].type = D_FIELD;
        d->items[i].dlen = 10;
        d->items[i].data = qrg_max_str;
        d->items[i].fn   = check_number;
        d->items[i].gid  = 0;
        d->items[i].gnum = 250000000;
		d->items[i].msg = CTEXT(T_QRG_MAX);
    	d->items[i].wrap = 1;

        d->items[++i].type = D_FIELD;
        d->items[i].dlen = 8;
        d->items[i].data = adifband;
        d->items[i].msg = CTEXT(T_ADIFBAND);

        d->items[++i].type = D_FIELD;
        d->items[i].dlen = 11;
        d->items[i].data = skedqrg;
        d->items[i].msg = CTEXT(T_SKEDQRG);
    	d->items[i].wrap = 1;

        d->items[++i].type = D_FIELD;     
        d->items[i].dlen = 16;
        d->items[i].data = band_lo_str;
        d->items[i].fn   = check_qrg;
		d->items[i].msg = CTEXT(T_BAND_LO);     
		d->items[i].wrap = 1;

        d->items[++i].type = D_FIELD;      
        d->items[i].dlen = 6;
        d->items[i].data = band_sw_str;
		d->items[i].msg = CTEXT(T_BANDSW);
	    //d->items[i].wrap = 2;

        d->items[++i].type = D_FIELD;      
        d->items[i].dlen = 6;
        d->items[i].data = wwlradius_str;
		d->items[i].msg = TRANSLATE("Stats WWL radius:");
	    d->items[i].wrap = 2;

    }
    d->items[++i].type = D_BUTTON;        
    d->items[i].gid = B_ENTER;
    d->items[i].fn = ok_dialog;
    d->items[i].text = VTEXT(T_OK);

    d->items[++i].type = D_BUTTON;
    d->items[i].gid = B_ESC;
    d->items[i].fn = cancel_dialog;
    d->items[i].text = VTEXT(T_CANCEL);
    d->items[i].align = AL_BUTTONS;
	d->items[i].wrap = 1;

    d->items[++i].type = D_END;
    do_dialog(d, getml(d, NULL));
}
                               

int dlg_band_settings_confb(struct dialog_data *dlg, struct dialog_item_data *di){
    /*dbg("dlg_band_settings %p %p \n",dlg,di);*/
    band_settings(di->item->text, 0); 
    return 0;
}

int dlg_band_settings(struct dialog_data *dlg, struct dialog_item_data *di){
    /*dbg("dlg_band_settings %p %p \n",dlg,di);*/
    band_settings(di->item->text, 1); 
    return 0;
}


/******************** RESPONSIBLE OPERATOR *******************************/

#define RESP_LEN 50
char rname[RESP_LEN], rcall[RESP_LEN], radr1[RESP_LEN], radr2[RESP_LEN];
char rpoco[RESP_LEN], rcity[RESP_LEN], rcoun[RESP_LEN];
char rphon[RESP_LEN], rhbbs[RESP_LEN];
                                                   
void refresh_responsible_op(void *xxx)
{
/*    dbg("refresh_responsible_op\n");*/
    STORE_STR(cfg,rname);
    STORE_STR(cfg,rcall);
    STORE_STR(cfg,radr1);
    STORE_STR(cfg,radr2);
    STORE_STR(cfg,rpoco);
    STORE_STR(cfg,rcity);
    STORE_STR(cfg,rcoun);
    STORE_STR(cfg,rphon);
    STORE_STR(cfg,rhbbs);
}

char *responsible_op_msg[] = {
    CTEXT(T_NAME),
    CTEXT(T_CALLSIGN),
    CTEXT(T_ADDRESS),
    CTEXT(T_ADDRESS),
    CTEXT(T_POSTCODE),
    CTEXT(T_CITY),
    CTEXT(T_COUNTRY),
    CTEXT(T_PHONE),
    CTEXT(T_BBSEMAIL),
    "", /* OK */        /* 9 */
    "", /* Cancel */
};
void responsible_op_fn(struct dialog_data *dlg)
{
    int max = 0, min = 0;
    int w, rw;
    int y = -1;

    max_group_width(term, responsible_op_msg + 0, dlg->items + 0, 1, &max);
    min_group_width(term, responsible_op_msg + 0, dlg->items + 0, 1, &min);
    max_group_width(term, responsible_op_msg + 1, dlg->items + 1, 1, &max);
    min_group_width(term, responsible_op_msg + 1, dlg->items + 1, 1, &min);
    max_group_width(term, responsible_op_msg + 2, dlg->items + 2, 1, &max);
    min_group_width(term, responsible_op_msg + 2, dlg->items + 2, 1, &min);
    max_group_width(term, responsible_op_msg + 3, dlg->items + 3, 1, &max);
    min_group_width(term, responsible_op_msg + 3, dlg->items + 3, 1, &min);
    max_group_width(term, responsible_op_msg + 4, dlg->items + 4, 1, &max);
    min_group_width(term, responsible_op_msg + 4, dlg->items + 4, 1, &min);
    max_group_width(term, responsible_op_msg + 5, dlg->items + 5, 1, &max);
    min_group_width(term, responsible_op_msg + 5, dlg->items + 5, 1, &min);
    max_group_width(term, responsible_op_msg + 6, dlg->items + 6, 1, &max);
    min_group_width(term, responsible_op_msg + 6, dlg->items + 6, 1, &min);
    max_group_width(term, responsible_op_msg + 7, dlg->items + 7, 1, &max);
    min_group_width(term, responsible_op_msg + 7, dlg->items + 7, 1, &min);
    max_group_width(term, responsible_op_msg + 8, dlg->items + 8, 1, &max);
    min_group_width(term, responsible_op_msg + 8, dlg->items + 8, 1, &min);
    
    max_buttons_width(term, dlg->items + 9, 2, &max);
    min_buttons_width(term, dlg->items + 9, 2, &min);
    
    w = dlg->win->term->x * 9 / 10 - 2 * DIALOG_LB;
    if (w > max) w = max;
    if (w < min) w = min;
    if (w > dlg->win->term->x - 2 * DIALOG_LB) w = dlg->win->term->x - 2 * DIALOG_LB;
    if (w < 1) w = 1;
    
    rw = 0;
    y ++;
    dlg_format_group(NULL, term, responsible_op_msg + 0, dlg->items + 0, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, responsible_op_msg + 1, dlg->items + 1, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, responsible_op_msg + 2, dlg->items + 2, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, responsible_op_msg + 3, dlg->items + 3, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, responsible_op_msg + 4, dlg->items + 4, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, responsible_op_msg + 5, dlg->items + 5, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, responsible_op_msg + 6, dlg->items + 6, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, responsible_op_msg + 7, dlg->items + 7, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, responsible_op_msg + 8, dlg->items + 8, 1, 0, &y, w, &rw);
    y++;
    dlg_format_buttons(NULL, term, dlg->items +9, 2, 0, &y, w, &rw, AL_LEFT);
    
    
    w = rw;
    dlg->xw = w + 2 * DIALOG_LB;
    dlg->yw = y + 2 * DIALOG_TB;

    
    center_dlg(dlg);
    draw_dlg(dlg);
    y = dlg->y + DIALOG_TB;
    y++;
    dlg_format_group(term, term, responsible_op_msg + 0, dlg->items + 0, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, responsible_op_msg + 1, dlg->items + 1, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, responsible_op_msg + 2, dlg->items + 2, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, responsible_op_msg + 3, dlg->items + 3, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, responsible_op_msg + 4, dlg->items + 4, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, responsible_op_msg + 5, dlg->items + 5, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, responsible_op_msg + 6, dlg->items + 6, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, responsible_op_msg + 7, dlg->items + 7, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, responsible_op_msg + 8, dlg->items + 8, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    y++;
    dlg_format_buttons(term, term, dlg->items +9, 2, dlg->x + DIALOG_LB, &y, w, NULL, AL_LEFT);
    
}



void responsible_op(void)
{
    struct dialog *d;
    int i;

    safe_strncpy0(rname,cfg->rname,RESP_LEN);
    safe_strncpy0(rcall,cfg->rcall,RESP_LEN); z_str_uc(rcall);
    safe_strncpy0(radr1,cfg->radr1,RESP_LEN);
    safe_strncpy0(radr2,cfg->radr2,RESP_LEN);
    safe_strncpy0(rpoco,cfg->rpoco,RESP_LEN);
    safe_strncpy0(rcity,cfg->rcity,RESP_LEN);
    safe_strncpy0(rcoun,cfg->rcoun,RESP_LEN);
    safe_strncpy0(rphon,cfg->rphon,RESP_LEN);
    safe_strncpy0(rhbbs,cfg->rhbbs,RESP_LEN); 
    

    if (!(d = g_malloc(sizeof(struct dialog) + 55 * sizeof(struct dialog_item)))) return;
    memset(d, 0, sizeof(struct dialog) + 55 * sizeof(struct dialog_item));
    d->title = VTEXT(T_RESPOP);
    d->fn = responsible_op_fn;
    d->refresh = (void (*)(void *))refresh_responsible_op;
    

    d->items[i=0].type = D_FIELD;
    d->items[i].dlen = RESP_LEN;
    d->items[i].data = rname;
    
    d->items[i=1].type = D_FIELD;
    d->items[i].dlen = RESP_LEN;
    d->items[i].data = rcall;
    
    d->items[i=2].type = D_FIELD;
    d->items[i].dlen = RESP_LEN;
    d->items[i].data = radr1;
    
    d->items[i=3].type = D_FIELD;
    d->items[i].dlen = RESP_LEN;
    d->items[i].data = radr2;
    
    d->items[i=4].type = D_FIELD;
    d->items[i].dlen = RESP_LEN;
    d->items[i].data = rpoco;
    
    d->items[i=5].type = D_FIELD;
    d->items[i].dlen = RESP_LEN;
    d->items[i].data = rcity;
    
    d->items[i=6].type = D_FIELD;
    d->items[i].dlen = RESP_LEN;
    d->items[i].data = rcoun;
    
    d->items[i=7].type = D_FIELD;
    d->items[i].dlen = RESP_LEN;
    d->items[i].data = rphon;
    
    d->items[i=8].type = D_FIELD;
    d->items[i].dlen = RESP_LEN;
    d->items[i].data = rhbbs;
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid = B_ENTER;
    d->items[i].fn = ok_dialog;
    d->items[i].text = VTEXT(T_OK);
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid = B_ESC;
    d->items[i].fn = cancel_dialog;
    d->items[i].text = VTEXT(T_CANCEL);
    
    d->items[++i].type = D_END;
    do_dialog(d, getml(d, NULL));
}
                               

int dlg_responsoble_op(struct dialog_data *dlg, struct dialog_item_data *di){
    responsible_op(); 
    return 0;
}

void menu_responsible_op(void *arg){
    responsible_op(); 
} 

/******************** QSOP METHOD, TOTAL METHOD  ******************/

#define MAX_QSOP_METHOD 23
#define MAX_TOTAL_METHOD 4

int qsop_method=1, total_method=1;
char qsop_method_str[MAX_STR_LEN],total_method_str[MAX_STR_LEN];

char *qsop_method_msg[]={
    CTEXT(T_QP0),
    CTEXT(T_QP1),
    CTEXT(T_QP2),
    CTEXT(T_QP3),
    CTEXT(T_QP4),
    CTEXT(T_QP5),
    CTEXT(T_QP6),
    CTEXT(T_QP7),
    CTEXT(T_QP8),
    CTEXT(T_QP9),
    CTEXT(T_QP10),
    CTEXT(T_QP11),
    CTEXT(T_QP12),
    CTEXT(T_QP13),
    CTEXT(T_QP14),
    CTEXT(T_QP15),
    CTEXT(T_QP16),
    CTEXT(T_QP17),
    CTEXT(T_QP18),
    CTEXT(T_QP19),
    CTEXT(T_QP20),
    CTEXT(T_QP21),
    CTEXT(T_QP22),
    CTEXT(T_QP23),
};

char *total_method_msg[]={
    CTEXT(T_TP0),
    CTEXT(T_TP1),
    CTEXT(T_TP2),
    CTEXT(T_TP3),
    CTEXT(T_TP4)
};


void qsop_method_func (void *arg){
    int active;

    active=GPOINTER_TO_INT(arg);
    if (active<1 || active>MAX_QSOP_METHOD) return;
    qsop_method = active;
    /*safe_strncpy0(qsop_method_str, qsop_method_msg[qsop_method], MAX_STR_LEN);*/
    safe_strncpy0(qsop_method_str,_(CTEXT(T_QP0+qsop_method)),MAX_STR_LEN);
    redraw_later();
}

int dlg_qsop_method(struct dialog_data *dlg, struct dialog_item_data *di){
    int i, sel;
    struct menu_item *mi;
    
    if (!(mi = new_menu(1))) return 0;
    for (i = 1; i <= MAX_QSOP_METHOD; i++) {
        add_to_menu(&mi, qsop_method_msg[i], "", "", MENU_FUNC qsop_method_func, GINT_TO_POINTER(i), 0);
    }
    sel = qsop_method-1;
    if (sel < 0) sel = 0;
    if (sel>=MAX_QSOP_METHOD) sel=0;
    do_menu_selected(mi, GINT_TO_POINTER(qsop_method), sel);
    return 0;
}

void total_method_func (void *arg){
    int active;

    active=GPOINTER_TO_INT(arg);
    if (active<1 || active>MAX_TOTAL_METHOD) return;
    total_method = active;
    safe_strncpy0(total_method_str,_(CTEXT(T_TP0+total_method)),MAX_STR_LEN);
    redraw_later();
    return;
}

int dlg_total_method(struct dialog_data *dlg, struct dialog_item_data *di){
    int i, sel;
    struct menu_item *mi;
    
    if (!(mi = new_menu(1))) return 0;
    for (i = 1; i <= MAX_TOTAL_METHOD; i++) {
        add_to_menu(&mi, total_method_msg[i], "", "", MENU_FUNC total_method_func, GINT_TO_POINTER(i), 0);
    }
    sel = total_method-1;
    if (sel < 0) sel = 0;
    if (sel>=MAX_TOTAL_METHOD) sel=0;
    do_menu_selected(mi, GINT_TO_POINTER(total_method), sel);
    return 0;
}


/****************** TTTYPE *******************/

#define MAX_TTTYPE 5

char tttype_str[MAX_STR_LEN];

char *tttype_msg[]={
    CTEXT(T_NONE),
    CTEXT(T_RSTS2),
    CTEXT(T_RSTR2),
    CTEXT(T_QSONRR2),
    CTEXT(T_EXC2)
};

void tttype_func(void *arg){
    int active;

    active=GPOINTER_TO_INT(arg);
    log_addf("tttype_func: active=%d", active);
    if (active<0 || active>=MAX_TTTYPE) return;
    tttype  = active;
    safe_strncpy0(tttype_str, get_text_translation(tttype_msg[tttype]), MAX_STR_LEN);
    redraw_later();
}

int dlg_tttype(struct dialog_data *dlg, struct dialog_item_data *di){
    int i, sel;
    struct menu_item *mi;
    
    if (!(mi = new_menu(1))) return 0;
    for (i = 0; i < MAX_TTTYPE; i++) {
        add_to_menu(&mi, tttype_msg[i], "", "", MENU_FUNC tttype_func, GINT_TO_POINTER(i), 0);
    }
    sel = tttype;
    if (sel < 1) sel = 1;
    if (sel>=MAX_TTTYPE) sel=0;
    do_menu_selected(mi, GINT_TO_POINTER(tttype), sel);
    return 0;
}                

/****************** WWLTYPE *******************/

#define MAX_WWLTYPE 5

char wwltype_str[MAX_STR_LEN];

char *wwltype_msg[]={
    CTEXT(T_UNUSED),
    CTEXT(T_USED_6),
    CTEXT(T_OPT_6),
    CTEXT(T_USED_4),
    CTEXT(T_OPT_4)
};

void wwltype_func(void *arg){
    int active;

    active=GPOINTER_TO_INT(arg);
    if (active<0 || active>=MAX_WWLTYPE) return;
    wwlused  = active;
    safe_strncpy0(wwltype_str, get_text_translation(wwltype_msg[wwlused]), MAX_STR_LEN);
    redraw_later();
}

int dlg_wwltype(struct dialog_data *dlg, struct dialog_item_data *di){
    int i, sel;
    struct menu_item *mi;
    
    if (!(mi = new_menu(1))) return 0;
    for (i = 0; i < MAX_WWLTYPE; i++) {
        add_to_menu(&mi, wwltype_msg[i], "", "", MENU_FUNC wwltype_func, GINT_TO_POINTER(i), 0);
    }
    sel = wwlused;
    if (sel < 1) sel = 1;
    if (sel>=MAX_WWLTYPE) sel=0;
    do_menu_selected(mi, GINT_TO_POINTER(wwlused), sel);
    return 0;
}                

/****************** EXCTYPE *******************/

#define MAX_EXCTYPE 6

char *exctype_msg[]={
    CTEXT(T_UNUSED),
    CTEXT(T_FREE),
    CTEXT(T_VERIFIED),
    CTEXT(T_WAZ),
    CTEXT(T_ITU),
    CTEXT(T_MULTIPLIED)
};

void exctype_func(void *arg){
    int active;

    active=GPOINTER_TO_INT(arg);
    if (active < 0 || active >= MAX_EXCTYPE) return;
    exctype  = active;
    switch(exctype){
        case EXC_WAZ: 
            strcpy(new_excname, "WAZ"); 
            break;
        case EXC_ITU: 
            strcpy(new_excname, "ITU"); 
            break;
        default:
            strcpy(new_excname, old_excname);
            break;
    }
    /*safe_strncpy0(qsop_method_str, qsop_method_msg[qsop_method], MAX_STR_LEN);*/
    safe_strncpy0(exctype_str, get_text_translation(exctype_msg[exctype]), MAX_STR_LEN);
    redraw_later();
}

int dlg_exctype(struct dialog_data *dlg, struct dialog_item_data *di){
    int i, sel = 0;
    struct menu_item *mi;
    int x[] = { 0, 1, 5, 2, 3, 4,  -1 };
    
    if (!(mi = new_menu(1))) return 0;
    for (i = 0; x[i] >= 0; i++){
        add_to_menu(&mi, exctype_msg[x[i]], "", "", MENU_FUNC exctype_func, GINT_TO_POINTER(x[i]), 0);
        if (x[i] == exctype) sel = i;
    }
    do_menu_selected(mi, GINT_TO_POINTER(exctype), sel);
    return 0;
}                



/******************** WIZZ *******************************************/

void wizz_func (void *arg){
    int active,j;
    struct wizz_item *wi;
    time_t now;
    struct tm utc;

    active=GPOINTER_TO_INT(arg);
    if (active<0 || active>=wizz->items->len) return;
    wi = get_wizz(wizz, active);

    time(&now);
    gmtime_r(&now, &utc);

    safe_strncpy0(tname, wi->tname,100);
    safe_strncpy0(pcall, cfg->pcall, 20);z_str_uc(pcall);
    safe_strncpy0(pclub, cfg->pclub, 20);z_str_uc(pclub);
    safe_strncpy0(operator_, cfg->operator_, EQSO_LEN);z_str_uc(operator_);
    safe_strncpy0(pwwlo, cfg->pwwlo, 9); z_str_uc(pwwlo);
	safe_strncpy0(pexch, wi->pexch, MAX_EXC_LEN+1); z_str_uc(pexch);
    g_snprintf(tdate, MAX_STR_LEN, "%d%02d%02d", 1900+utc.tm_year, 1+utc.tm_mon, utc.tm_mday);
    qsoused=wi->qsoused;
    g_snprintf(qsomult_str,10, "%d", wi->qsomult);
	qsoglob = wi->qsoglob;
	expmode = wi->expmode;
    wwlused=wi->wwlused;
    safe_strncpy0(wwltype_str, get_text_translation(wwltype_msg[wwlused]), MAX_STR_LEN);
    g_snprintf(wwlbonu_str,10, "%d", wi->wwlbonu);
    g_snprintf(wwlmult_str,10, "%d", wi->wwlmult);
    wwlcfm=wi->wwlcfm;
    exctype=wi->exctype;
    safe_strncpy0(exctype_str, get_text_translation(exctype_msg[exctype]),MAX_STR_LEN);
    
    safe_strncpy0(excname, wi->excname, MAX_STR_LEN);
    safe_strncpy0(new_excname, wi->excname, MAX_STR_LEN);
    safe_strncpy0(old_excname, wi->excname, MAX_STR_LEN);
    
    g_snprintf(excbonu_str,10, "%d", wi->excbonu);
    g_snprintf(excmult_str,10, "%d", wi->excmult);
    exccfm=wi->exccfm;
    excused=wi->excused;
    g_snprintf(prefmult_str,10, "%d", wi->prefmult);
    g_snprintf(dxcbonu_str,10, "%d", wi->dxcbonu);
    g_snprintf(dxcmult_str,10, "%d", wi->dxcmult);
    prefglob=wi->prefglob;
    rstused=wi->rstused;
    safe_strncpy(default_rs, cfg->default_rs, 5);    z_str_uc(default_rs);
    safe_strncpy(default_rst, cfg->default_rst, 5);  z_str_uc(default_rst);
    defrstr=wi->defrstr;
    tttype=wi->tttype;
    safe_strncpy0(tttype_str, get_text_translation(tttype_msg[tttype]), MAX_STR_LEN);
	phase = 1;

    
    qsop_method=wi->qsop_method;
    /*safe_strncpy0(qsop_method_str,qsop_method_msg[qsop_method],MAX_STR_LEN);*/
    safe_strncpy0(qsop_method_str,_(CTEXT(T_QP0+qsop_method)),MAX_STR_LEN);
    total_method=wi->total_method;
    safe_strncpy0(total_method_str,_(CTEXT(T_TP0+total_method)),MAX_STR_LEN);
    /*safe_strncpy0(total_method_str,total_method_msg[total_method],MAX_STR_LEN);*/
    //log_addf("qsop_method=%d  total_method=%d", qsop_method, total_method);
    /* wwltype */
    /* minqsop, maxqsop */
   
    if (wi->bands){
        dbg("wi->bands='%s'\n", wi->bands);
        for (j=0; j<cfg->bands->len;j++){
            struct config_band *confb;

            confb = (struct config_band*)g_ptr_array_index(cfg->bands,j);
            if (!confb) break;  /* unreached? */
            confb->qrvnow = confb->qrv;
            if (!confb->qrvnow) continue;

            if (strchr(wi->bands, confb->bandchar)==NULL) {
                dbg("Band %c is not used in contest\n", confb->bandchar); 
                confb->qrvnow=0;
            }
            
        }
    }
    contest_options1(VTEXT(T_NEW_CTEST), 0, wi);
}


struct menu_item no_wizz[] = {
    {CTEXT(T_NO_CTESTS), "", M_BAR, NULL, NULL, 0, 0},
    {NULL, NULL, 0, NULL, NULL, 0, 0}
};


void menu_wizz(void *arg){
    int i;
    struct menu_item *mi;
    struct wizz_item *wi;
    
    if (!wizz->items->len){
        do_menu(no_wizz, NULL);
        return;
    }
    
    if (!(mi = new_menu(1))) return;
    for (i = 0; i < wizz->items->len; i++) {
        wi = get_wizz(wizz, i);
        add_to_menu(&mi, wi->tname, "", "", MENU_FUNC wizz_func, GINT_TO_POINTER(i), 0);
    }
    do_menu_selected(mi, NULL, 0);
}

/******************** CONTEST OPTIONS 1 *******************************/

/*char qsop_method_str[MAX_STR_LEN],total_method_str[MAX_STR_LEN];*/
int firstbandbutt=-1, excnameidx;


/*void update_desc(struct contest *ctest){
    if (!ctest) return;
    if (!ctest->pcall) return;
    if (!ctest->tname) return;
    
    if (!ctest->pcall ||
        !ctest->tname ||
        strcasecmp(pcall, ctest->pcall) ||
        strcasecmp(tname, ctest->tname)){

        setvbuf(ctest->descfile, NULL, _IONBF, 0);
        fseek(ctest->descfile, 0, SEEK_SET);
        if (z_ftruncate(fileno(ctest->descfile), 0)){
            errbox(VTEXT(T_CANT_TRUNC), errno);
            return;
        }
        fprintf(ctest->descfile,"%s %s %s\n", ctest->cdate, z_str_uc(pcall), ctest->tname);    
    }
    
} */

void refresh_contest_options1(void *arg)
{
    int updloc;
	char *descfile;
	struct wizz_item *wi = (struct wizz_item *)arg;
    /*dbg("refresh_contest_options1\n");*/

    if (ctest){
        descfile = g_strdup_printf("%s/desc", ctest->directory);
        save_desc_to_file(descfile);
        g_free(descfile);
    }

	updloc = 1;// strcasecmp(cfg->pwwlo, pwwlo);
    
    STORE_STR_FS_UC(cfg,pcall); 
    STORE_STR_FS_UC(cfg,pclub);
    STORE_STR_FS_UC(cfg,operator_);
    STORE_STR_FS_UC(cfg,pwwlo);
    STORE_STR_FS   (cfg,default_rst);
    STORE_STR_FS   (cfg,default_rs);
    init_ctest2(1, updloc, NULL, wi);
}
 
void init_ctest2(int from_dlg, int updloc, struct zstring *zs2, struct wizz_item *wi){
    int isnew=0;
    struct zstring *zs = NULL;
    char *c;

    if (!ctest) isnew=1;
//    dbg("init_ctest2: isnew=%d zs2=%p wi=%p\n", isnew, zs2, wi);
    if (init_ctest()) return;
    if (ctest->pwwlo) updloc=strcasecmp(ctest->pwwlo, pwwlo);
    
    if (from_dlg){
        STORE_STR_FS   (ctest,tname);
        STORE_STR_FS_UC(ctest,pcall);
        STORE_STR_FS_UC(ctest,pclub);
        STORE_STR_FS_UC(ctest,pwwlo);
        STORE_STR_FS_UC(ctest,pexch);
        /* tdate is handled by argument */
//        STORE_STR_FS   (ctest,tdate);
        STORE_STR_FS   (ctest,default_rst);
        STORE_STR_FS   (ctest,default_rs);
        
        STORE_STR2   (ctest,cfg,rname);
        STORE_STR2_UC(ctest,cfg,rcall);
        STORE_STR2   (ctest,cfg,radr1);
        STORE_STR2   (ctest,cfg,radr2);
        STORE_STR2   (ctest,cfg,rpoco);
        STORE_STR2   (ctest,cfg,rcity);
        STORE_STR2   (ctest,cfg,rcoun);
        STORE_STR2   (ctest,cfg,rphon);
        STORE_STR2   (ctest,cfg,rhbbs);
        
        STORE_INT (ctest,qsoused);
        STORE_SINT(ctest,qsomult);
        STORE_INT (ctest,qsoglob);

        STORE_INT (ctest,wwlused);
        STORE_SINT(ctest,wwlbonu);
        STORE_SINT(ctest,wwlmult);
        STORE_INT (ctest,wwlcfm);
        STORE_ENUM (ctest,exctype, enum exctype);
        STORE_STR (ctest,excname);
        STORE_INT (ctest,excused);
        STORE_SINT(ctest,excbonu);
        STORE_SINT(ctest,excmult);
        STORE_INT (ctest,exccfm);
        STORE_SINT(ctest,prefmult);
        STORE_SINT(ctest,dxcbonu);
        STORE_SINT(ctest,dxcmult);
        STORE_INT (ctest,prefglob);
        STORE_INT (ctest,rstused);
        STORE_ENUM (ctest,tttype, enum tttype);
        STORE_INT (ctest,expmode);
		STORE_INT(ctest, phase);
        STORE_INT (ctest,defrstr);
        STORE_INT (ctest,qsop_method);
        STORE_INT (ctest,total_method);
    }
    if (zs2){
        zs = zstrdup(ztokenize(zs2, 1));
        dbg("ctest\t%s\n", zs->str);
        ctest_parse(zs, ctest);
        zfree(zs);

        safe_strncpy0(tdate, ctest->cdate, MAX_STR_LEN);
    }
    
    set_ctest_title();
    clear_exc(excdb);
    read_exc_files(excdb, (enum exctype)exctype, excname);

    if (!isnew){               /* contest options from menu */
        gchar *descfile;
        
        dbg("init_ctest2: from menu\n");
        /*save_all_bands_txt(0);*/

        descfile=g_strdup_printf("%s/desc", ctest->directory);
        save_desc_to_file(descfile);
        g_free(descfile);

        net_send_ac();
        net_send_operator();
        net_send_read_write_bands();
		net_send_talk();
        
        recalc_all_stats(ctest);
        if (updloc){
			update_hw();
            qrv_recalc_qrbqtf(qrv);
            qrv_recalc_gst(qrv);
#ifdef Z_HAVE_SDL
            recalc_all_qrbqtf(ctest);
            map_recalc_cors();
            maps_reload();
			maps_update_showwwls();
			rain_reload();
#endif   
            chart_reload();
			//qrv_recalc_wkd(qrvdb); probably not needed
        }
        save_all_bands_txt(0);
        return;
    }
    
    /* new contest */
    dbg("init_ctest2: new contest tdate=%s\n", tdate);
    if (new_ctest(tdate)) return;

    if (from_dlg){
        if (init_qrv_bands(wi)) return;
    }
    if (zs2){
        int i;
        for (i=0; ;i++){
            gchar *cc;

            cc = ztokenize(zs2, 0);
            if (!cc) break;
            zs = zstrdup(cc);

            dbg("band%d\t%s\n", i, zs->str);
            init_band(NULL, NULL, zs, NULL);

            zfree(zs);
        }
    }
    if (ctest->bands->len==0){
        errbox(VTEXT(T_NO_BANDS), 0);
        free_ctest();
        return;
    }

	if (zs == NULL){ 
		// called from menu, read prepared ~/tucnak/qrv
		c = g_strconcat(tucnak_dir, "/qrv", NULL);
		z_wokna(c);
		load_qrv_from_file(qrv, c);
		g_free(c);
	}
	// else QRV will be transferred over network

    save_all_bands_txt(0);
    net_send_ac();
    net_send_operator();
    net_send_read_write_bands();
	net_send_talk();

    if (cfg->startband){
        struct band *b;
        b=find_band_by_pband(cfg->startband);
        activate_band(b); /* b==NULL is handled by function */
    }
    
}

void contest_options1_fn(struct dialog_data *dlgd)
{
    int numofbands;
    
    /*dbg("contest_options1_fn()\n");*/
//    firstbandbutt=26;
    for (numofbands=0; numofbands<26; numofbands++ ) {
        if (! dlgd->items[firstbandbutt+numofbands].item) break;
        if (dlgd->items[firstbandbutt+numofbands].item->type!= D_BUTTON) break;
        if (dlgd->items[firstbandbutt+numofbands].item->gid== B_ENTER) break;
    }
/*    dbg("first=%d, num=%d\n",firstbandbutt,numofbands);*/
            
    
    if (strcmp(excname, new_excname)!=0){
        strcpy(excname, new_excname);
        safe_strncpy0(dlgd->items[excnameidx].cdata, excname, EQSO_LEN);
        dlgd->items[excnameidx].cpos = strlen(dlgd->items[excnameidx].cdata);
    }
            
#if 0
    int ph;
    for (ph = 0; ph < 3; ph++){
        dlg_pf_init(ph, dlgd);
        dlgd->yy++;
        dlg_pf_group(ph, dlgd, 1);
        dlg_pf_group(ph, dlgd, 2);
        dlg_pf_group(ph, dlgd, 3);
        dlg_pf_group(ph, dlgd, 1); // tttype
        dlg_pf_group(ph, dlgd, 3); // qso
        dlg_pf_group(ph, dlgd, 4); // wwl
        dlg_pf_group(ph, dlgd, 4); // exc
        dlg_pf_group(ph, dlgd, 2); // exc
        dlg_pf_group(ph, dlgd, 2);
        dlg_pf_group(ph, dlgd, 4);
        dlg_pf_group(ph, dlgd, 2);
        dlgd->yy ++;
        dlg_pf_group(ph, dlgd, numofbands);
        dlgd->yy ++;
        //dlg_pf_buttons(ph, dlgd, 4);
        dlg_pf_group(ph, dlgd, 4);
        dlgd->yy ++;
        dlgd->yy ++;
    }
#endif
}

    
void contest_options1(char *title, int from_ctest, struct wizz_item *wi)
{
    struct dialog *d;
    int i,j;

    /*dbg("contest_options1\n");*/

#ifdef Z_ANDROID
    if (!from_ctest){
        double h, w;
        int state;
        //SDL_ANDROID_GetHW(&h, &w, &state);
        zandroid_get_location(&h, &w, &state);
        //log_addf("contest_options1: h=%f w=%f\n", h, w);
        if (h > -1000 && h < 1000){ // todo v zavislosti na state
            hw2loc(pwwlo, h, w, 6);
        }
    }
#endif
    
    if (!(d = (struct dialog *)g_malloc(sizeof(struct dialog) + 60 * sizeof(struct dialog_item)))) return;
    memset(d, 0, sizeof(struct dialog) + 60 * sizeof(struct dialog_item));
    d->title = title;
    d->fn = dlg_pf_fn;
    d->fn2 = contest_options1_fn;
    d->refresh = (void (*)(void *))refresh_contest_options1;
	d->refresh_data = wi;
    d->y0 = 1;
    
    
    d->items[i=0].type = D_FIELD;
    d->items[i].dlen = MAX_STR_LEN;
    d->items[i].data = tname;
    d->items[i].maxl = 45;
    d->items[i].msg = CTEXT(T_NAME_OF_CTEST);
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = 20;
    d->items[i].data = pcall;
    d->items[i].maxl = 15;
    d->items[i].msg = CTEXT(T_CALL_OF_CTEST);
    
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = 20;
    d->items[i].data = pclub;
    d->items[i].maxl = 15;
    d->items[i].msg = CTEXT(T_CLUB_ST);
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = 9;
    d->items[i].data = pwwlo;
    d->items[i].msg = CTEXT(T_YOURWWL);
    
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = MAX_EXC_LEN+1;
    d->items[i].data = pexch;
    d->items[i].msg = CTEXT(T_YOUREXC);
    
    d->items[++i].type = D_FIELD;     /* 5 */
    d->items[i].dlen = 9;
    d->items[i].data = tdate;
    d->items[i].msg = CTEXT(T_DATE_OF_CTEST);
    d->items[i].wrap = 1;
    
	d->items[++i].type = D_CHECKBOX;
	d->items[i].gid = 0;
	d->items[i].gnum = 1;
	d->items[i].dlen = sizeof(int);
	d->items[i].data = (char *)&expmode;
	d->items[i].msg = CTEXT(T_EXPMODE);
	d->items[i].wrap = 2;



	d->items[++i].type = D_TEXT;
	d->items[i].msg = TRANSLATE("    Band options:");
	d->items[i].wrap = 1;

	firstbandbutt = i + 1;
	/*    dbg("firstband=%d\n",i+1);*/
	if (!from_ctest){
		for (j = 0; j<26; j++){
			struct config_band *confb;

			if (j >= cfg->bands->len) break; /* no config file */

			confb = (struct config_band*)g_ptr_array_index(cfg->bands, j);
			if (!confb) break;  /* unreached? */
			if (!confb->qrvnow) continue;

			d->items[++i].type = D_BUTTON;
			d->items[i].gid = 0;
			d->items[i].fn = dlg_band_settings_confb;
			d->items[i].text = confb->pband;
		}
	}
	else{
		for (j = 0; j<ctest->bands->len; j++){
			struct band *band;

			band = (struct band*)g_ptr_array_index(ctest->bands, j);
			if (!band) break;  /* unreached? */

			d->items[++i].type = D_BUTTON;
			d->items[i].gid = 0;
			d->items[i].fn = dlg_band_settings;
			d->items[i].text = band->pband;
		}
	}
	/*    dbg("lastband=%d\n",i);*/
	d->items[i].wrap = 2;




	d->items[++i].type = D_TEXT;
	d->items[i].msg = TRANSLATE("    Advanced options:");
	d->items[i].wrap = 1;

    d->items[++i].type = D_BUTTON;     
    d->items[i].gid  = 0;
    d->items[i].fn   = dlg_tttype;
    d->items[i].text = tttype_str;
    d->items[i].msg = CTEXT(T_TTTYPE);
    
    
    d->items[++i].type = D_CHECKBOX3;  
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data=(char *)&qsoused;
    d->items[i].msg = CTEXT(T_QSOUSED);
    
    d->items[++i].type = D_FIELD;
    d->items[i].data = qsomult_str;
    d->items[i].dlen = 8;
    d->items[i].maxl = 3;
    d->items[i].fn   = check_number;
    d->items[i].gid  = 0;
    d->items[i].gnum = 999999;
    d->items[i].msg = CTEXT(T_QSOPM);
    
    d->items[++i].type = D_CHECKBOX3;  
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data=(char *)&qsoglob;
    d->items[i].msg = CTEXT(T_QSOGLOB);
    d->items[i].wrap = 1;
    
    
/*    d->items[++i].type = D_CHECKBOX3;
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data=(char *)&wwlused;
    d->items[i].msg = CTEXT(T_WWLUSED);*/
    
    d->items[++i].type = D_BUTTON;     
    d->items[i].gid  = 0;
    d->items[i].fn   = dlg_wwltype;
    d->items[i].text = wwltype_str;
    d->items[i].msg = CTEXT(T_WWL2);
    
    
    d->items[++i].type = D_FIELD;      /* 10 */
    d->items[i].data = wwlbonu_str;
    d->items[i].dlen = 8;
    d->items[i].maxl = 4;
    d->items[i].fn   = check_number;
    d->items[i].gid  = 0;
    d->items[i].gnum = 999999;
    d->items[i].msg = CTEXT(T_WWLB);
    
    d->items[++i].type = D_FIELD;      
    d->items[i].data = wwlmult_str;
    d->items[i].dlen = 8;
    d->items[i].maxl = 3;
    d->items[i].fn   = check_number;
    d->items[i].gid  = 0;
    d->items[i].gnum = 999999;
    d->items[i].msg = CTEXT(T_WWLM);
    
    d->items[++i].type = D_CHECKBOX;   
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data=(char *)&wwlcfm;
    d->items[i].msg = CTEXT(T_CONFIRM_WWL);
    d->items[i].wrap = 1;


    d->items[++i].type = D_CHECKBOX3;    
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data=(char *)&excused;
    d->items[i].msg = CTEXT(T_EXCUSED);

    d->items[++i].type = D_FIELD;
    d->items[i].data = excbonu_str;
    d->items[i].dlen = 8;
    d->items[i].maxl = 4;
    d->items[i].fn   = check_number;
    d->items[i].gid  = 0;
    d->items[i].gnum = 999999;
    d->items[i].msg = CTEXT(T_EXCB);
    
    d->items[++i].type = D_FIELD;         /* 15 */  
    d->items[i].data = excmult_str;
    d->items[i].dlen = 8;
    d->items[i].maxl = 3;
    d->items[i].fn   = check_number;
    d->items[i].gid  = 0;
    d->items[i].gnum = 999999;
    d->items[i].msg = CTEXT(T_EXCM);
    
    d->items[++i].type = D_CHECKBOX;      
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data=(char *)&exccfm;
    d->items[i].msg = CTEXT(T_CONFIRM_EXC);
    d->items[i].wrap = 1;
    
    
    d->items[++i].type = D_BUTTON;     
    d->items[i].gid  = 0;
    d->items[i].fn   = dlg_exctype;
    d->items[i].text = exctype_str;
    d->items[i].msg = CTEXT(T_EXC2);
    
    
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = 15;
    d->items[i].data = excname;
    d->items[i].maxl = 7;
    d->items[i].msg = CTEXT(T_EXCNAME);
    d->items[i].wrap = 1;
    excnameidx = i;
    
    
    d->items[++i].type = D_FIELD; 
    d->items[i].data = prefmult_str;
    d->items[i].dlen = 8;
    d->items[i].maxl = 3;
    d->items[i].fn   = check_number;
    d->items[i].gid  = 0;
    d->items[i].gnum = 999999;
    d->items[i].msg = CTEXT(T_PREFM);
    
    d->items[++i].type = D_FIELD; 
    d->items[i].data = dxcbonu_str;
    d->items[i].dlen = 8;
    d->items[i].maxl = 4;
    d->items[i].fn   = check_number;
    d->items[i].gid  = 0;
    d->items[i].gnum = 999999;
    d->items[i].msg = CTEXT(T_DXCB);
    
    d->items[++i].type = D_FIELD;        
    d->items[i].data = dxcmult_str;
    d->items[i].dlen = 8;
    d->items[i].maxl = 3;
    d->items[i].fn   = check_number;
    d->items[i].gid  = 0;
    d->items[i].gnum = 999999;
    d->items[i].msg = CTEXT(T_DXCM);
    
    d->items[++i].type = D_CHECKBOX;   
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(gint);
    d->items[i].data=(char *)&prefglob;
    d->items[i].msg = CTEXT(T_PREFGLOB);
    d->items[i].wrap = 1;
    
    
    d->items[++i].type = D_CHECKBOX3;    
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(gint);
    d->items[i].data=(char *)&rstused;
    d->items[i].msg = CTEXT(T_RSTUSED);
    
    d->items[++i].type = D_FIELD;       
    d->items[i].data = default_rs;
    d->items[i].dlen = 5;
    d->items[i].msg = CTEXT(T_DEFRS);
    
    d->items[++i].type = D_FIELD;      
    d->items[i].data = default_rst;
    d->items[i].dlen = 5;
    d->items[i].msg = CTEXT(T_DEFRST);
    
    d->items[++i].type = D_CHECKBOX;   
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(gint);
    d->items[i].data=(char *)&defrstr;
    d->items[i].msg = CTEXT(T_ALSORSTR);
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_BUTTON;          
    d->items[i].gid  = 0;
    d->items[i].fn   = dlg_qsop_method;
    d->items[i].text = qsop_method_str;
    d->items[i].msg = CTEXT(T_QP_CALC);
	d->items[i].wrap = 1;

    d->items[++i].type = D_BUTTON;     
    d->items[i].gid  = 0;
    d->items[i].fn   = dlg_total_method;
    d->items[i].text = total_method_str;
    d->items[i].msg = CTEXT(T_TO_CALC);
    d->items[i].wrap = 2;



	d->items[++i].type = D_BUTTON;
    d->items[i].gid = B_ENTER;
    d->items[i].fn = ok_dialog;
    d->items[i].text = VTEXT(T_OK);
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid = B_ESC;
    d->items[i].fn = cancel_dialog;
    d->items[i].text = VTEXT(T_CANCEL);
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid = 0;                         
    d->items[i].fn = dlg_edi_prop;
    d->items[i].text = VTEXT(T_EDI_PROP);
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid = 0;
    d->items[i].fn = dlg_responsoble_op;
    d->items[i].text = VTEXT(T_RESPOP2);
    d->items[i].align = AL_CENTER | AL_BUTTONS;
    d->items[i].wrap = 1;
    
    d->items[++i].type = D_END;
    dbg("last item=%d\n", i);
    do_dialog(d, getml(d, NULL));
}


void contest_options1_from_menu(void *arg)
{
    time_t now;
    struct tm utc;

    time(&now);
    gmtime_r(&now, &utc);

    /*dbg("contest_options1\n");*/
    safe_strncpy0(tname,"",100);
    safe_strncpy0(pcall, cfg->pcall, 20);z_str_uc(pcall);
    safe_strncpy0(pclub, cfg->pclub, 20);z_str_uc(pclub);
    safe_strncpy0(pwwlo, cfg->pwwlo, 9); z_str_uc(pwwlo);
    safe_strncpy0(pexch, cfg->pexch, MAX_EXC_LEN+1); z_str_uc(pexch);
    g_snprintf(tdate, MAX_STR_LEN, "%d%02d%02d", 1900+utc.tm_year, 1+utc.tm_mon, utc.tm_mday);
    qsoused=1;
    safe_strncpy(qsomult_str,"1", 10);
    qsoglob=0;
    wwlused=1;
	safe_strncpy0(wwltype_str, get_text_translation(wwltype_msg[wwlused]),MAX_STR_LEN);
    safe_strncpy(wwlbonu_str,"0",10);
    safe_strncpy(wwlmult_str,"0",10);
    wwlcfm=1;
    exctype=0; 
    safe_strncpy0(exctype_str,get_text_translation(exctype_msg[exctype]),MAX_STR_LEN);
    safe_strncpy(excbonu_str,"0",10);
    safe_strncpy(excmult_str,"0",10);
    exccfm=1;
    safe_strncpy(excname, "", 10);
    safe_strncpy(new_excname, "", 10);
    safe_strncpy(old_excname, "", 10);
    excused=0;
    safe_strncpy(prefmult_str,"0",10);
    safe_strncpy(dxcbonu_str,"0",10);
    safe_strncpy(dxcmult_str,"0",10);
    rstused=1;
    safe_strncpy0(default_rs, cfg->default_rs, 5);    z_str_uc(default_rs);
    defrstr=0;
    safe_strncpy0(default_rst, cfg->default_rst, 5);  z_str_uc(default_rst);
    qsop_method=cfg->qsop_method % (MAX_QSOP_METHOD + 1);
    safe_strncpy0(qsop_method_str,_(CTEXT(T_QP0+qsop_method)),MAX_STR_LEN);
    /*safe_strncpy0(qsop_method_str,qsop_method_msg[qsop_method],MAX_STR_LEN);*/
    total_method=cfg->total_method % MAX_TOTAL_METHOD;
    safe_strncpy0(total_method_str,_(CTEXT(T_TP0+total_method)),MAX_STR_LEN);
    /*safe_strncpy0(total_method_str,total_method_msg[total_method],MAX_STR_LEN);*/
    tttype=TT_RSTS;
    safe_strncpy0(tttype_str, get_text_translation(tttype_msg[tttype]), MAX_STR_LEN);
    expmode=0;
	phase = 1;
    contest_options1(VTEXT(T_NEW_CTEST), 0, NULL);
}
                               

void contest_options1_from_ctest(void *arg)
{
    /*dbg("contest_options1\n");*/
    safe_strncpy0(tname, ctest->tname,100);
    safe_strncpy0(pcall, ctest->pcall, 20);z_str_uc(pcall);
    safe_strncpy0(pclub, ctest->pclub, 20);z_str_uc(pclub);
    safe_strncpy0(pwwlo, ctest->pwwlo, 9); z_str_uc(pwwlo);
    safe_strncpy0(pexch, ctest->pexch, MAX_EXC_LEN+1); z_str_uc(pexch);
    safe_strncpy0(tdate, "", MAX_STR_LEN); 
    qsoused=ctest->qsoused;
    g_snprintf(qsomult_str,10, "%d", ctest->qsomult);
    qsoglob=ctest->qsoglob;
    wwlused=ctest->wwlused;
    safe_strncpy0(wwltype_str, get_text_translation(wwltype_msg[wwlused]), MAX_STR_LEN);
    g_snprintf(wwlbonu_str,10, "%d", ctest->wwlbonu);
    g_snprintf(wwlmult_str,10, "%d", ctest->wwlmult);
    wwlcfm=ctest->wwlcfm;
    exctype=ctest->exctype;
    safe_strncpy0(exctype_str, get_text_translation(exctype_msg[exctype]), MAX_STR_LEN);
    
    safe_strncpy0(excname, ctest->excname, MAX_STR_LEN);
    safe_strncpy0(new_excname, ctest->excname, MAX_STR_LEN);
    safe_strncpy0(old_excname, ctest->excname, MAX_STR_LEN);
    
    g_snprintf(excbonu_str,10, "%d", ctest->excbonu);
    g_snprintf(excmult_str,10, "%d", ctest->excmult);
    exccfm=ctest->exccfm;
    excused=ctest->excused;
    g_snprintf(prefmult_str,10, "%d", ctest->prefmult);
    g_snprintf(dxcbonu_str,10, "%d", ctest->dxcbonu);
    g_snprintf(dxcmult_str,10, "%d", ctest->dxcmult);
    rstused=ctest->rstused;
    safe_strncpy0(default_rs, cfg->default_rs, 5);    z_str_uc(default_rs);
    safe_strncpy0(default_rst, cfg->default_rst, 5);  z_str_uc(default_rst);
    
    qsop_method=ctest->qsop_method;
    safe_strncpy0(qsop_method_str,_(CTEXT(T_QP0+qsop_method)),MAX_STR_LEN);
    total_method=ctest->total_method;
    safe_strncpy0(total_method_str,_(CTEXT(T_TP0+total_method)),MAX_STR_LEN);
    defrstr=ctest->defrstr;
    tttype=ctest->tttype;
    safe_strncpy0(tttype_str,get_text_translation(tttype_msg[tttype]),MAX_STR_LEN);
    expmode=ctest->expmode;
	phase = ctest->phase;
    contest_options1(VTEXT(T_CONTEST_OP), 1, NULL);
}
                               

/******************** CONTEST DEFAULTS *******************************/

/*
char tname[MAX_STR_LEN],pcall[MAX_STR_LEN],pclub[MAX_STR_LEN];
char pwwlo[MAX_STR_LEN],pexch[MAX_STR_LEN];
char default_rst[MAX_STR_LEN],default_rs[MAX_STR_LEN];
*/  
char padr1[MAX_STR_LEN],padr2[MAX_STR_LEN];
int global_operator;
char dxc_host[MAX_STR_LEN], dxc_port_str[EQSO_LEN];
char dxc_user[MAX_STR_LEN], dxc_pass[MAX_STR_LEN];
char kst_user[MAX_STR_LEN], kst_pass[MAX_STR_LEN], kst_name[MAX_STR_LEN], slovhf_user[MAX_STR_LEN];
char sota_user[MAX_STR_LEN], sota_pass[MAX_STR_LEN];
char contest_def_is_first = 0;

void refresh_contest_def(void *xxx)
{
    int updloc;
/*    dbg("refresh_contest_def\n");*/
    updloc=strcmp(cfg->pwwlo, pwwlo);
    STORE_STR_UC(cfg,pcall);
    STORE_STR_UC(cfg,pclub);
    STORE_STR_UC(cfg,operator_);
    STORE_INT(cfg, global_operator);
    STORE_STR_UC(cfg,pwwlo);
    STORE_STR(cfg,padr1);
    STORE_STR(cfg,padr2);
    STORE_STR_UC(cfg,pexch);
    STORE_STR_UC(cfg,default_rs);
    STORE_STR_UC(cfg,default_rst);

	STORE_STR(cfg, dxc_host);
	STORE_SINT(cfg, dxc_port);
	STORE_STR(cfg, dxc_user);
	STORE_STR(cfg, dxc_pass);
	STORE_STR(cfg, kst_user);
	STORE_STR(cfg, kst_pass);
	STORE_STR(cfg, kst_name);
    STORE_STR(cfg, sota_user);
    STORE_STR(cfg, sota_pass);
	STORE_STR(cfg, slovhf_user);

	if (contest_def_is_first){
		g_free(cfg->operator_);		cfg->operator_ = g_strdup(pcall);
		cfg->global_operator = 1;
		g_free(cfg->dxc_user);  	cfg->dxc_user = g_strdup(pcall);	z_str_lc(cfg->dxc_user);
		g_free(cfg->kst_user);		cfg->kst_user = g_strdup(pcall);	z_str_lc(cfg->kst_user);
		g_free(cfg->sota_user);		cfg->sota_user = g_strdup(pcall);	z_str_lc(cfg->sota_user);
	}


	update_hw();

#ifdef Z_HAVE_SDL
	if (updloc) {
		update_hw();
		maps_reload();
	}
	maps_update_showwwls();
	rain_reload();

#endif
	if (first_contest_def){
		if (strlen(cfg->pcall)==0 || strlen(cfg->pwwlo)==0){
/*			zselect_bh_new(zsel, contest_def, NULL);*/
            //zselect_bh_new(zsel, destroy_terminal, NULL);
			zselect_terminate(zsel);

		}else{
			first_contest_def=0;
			menu_save_rc(NULL);
		}
	}
    net_send_operator();
    net_send_read_write_bands();
	net_send_talk();
}


int contest_def_language(struct dialog_data *dlgd, struct dialog_item_data *dlgid){
	menu_language_list(NULL);
	return 0;
}

#ifdef Z_HAVE_SDL
int contest_def_plus(struct dialog_data *dlgd, struct dialog_item_data *dlgid){
	tsdl_change_font('+');
	return 0;
}

int contest_def_minus(struct dialog_data *dlgd, struct dialog_item_data *dlgid){
	tsdl_change_font('-');
	return 0;
}
#endif

void contest_def(void *pfirst)
{
    struct dialog *d;
    int i, first;

	first = GPOINTER_TO_INT(pfirst);
	contest_def_is_first = first;
	if (first) {
#ifdef Z_HAVE_SDL
		zsdl_maximize(zsdl, 1); 
		tsdl_optimal_font();
#endif
	}

    safe_strncpy0(pcall,cfg->pcall,MAX_STR_LEN); z_str_uc(pcall);
    safe_strncpy0(pclub,cfg->pclub,MAX_STR_LEN); z_str_uc(pclub);
    safe_strncpy0(operator_,cfg->operator_,EQSO_LEN); z_str_uc(operator_);
    global_operator=cfg->global_operator;
#ifdef Z_ANDROID
    if (first){
        double h, w;
        SDL_ANDROID_GetHW(&h, &w);
        log_addf("contest_def: h=%f w=%f\n", h, w);
        if (h > -1000 && h < 1000){
            hw2loc(pwwlo, h, w, 6);
        }
    }else
#endif
    {
        safe_strncpy0(pwwlo,cfg->pwwlo,MAX_STR_LEN); z_str_uc(pwwlo);
    }

    safe_strncpy0(padr1,cfg->padr1,MAX_STR_LEN);
    safe_strncpy0(padr2,cfg->padr2,MAX_STR_LEN);
	safe_strncpy0(pexch, cfg->pexch, MAX_EXC_LEN+1);
    safe_strncpy0(default_rs, cfg->default_rs,MAX_STR_LEN);  z_str_uc(default_rs);
    safe_strncpy0(default_rst,cfg->default_rst,MAX_STR_LEN); z_str_uc(default_rst);

	safe_strncpy0(dxc_host, cfg->dxc_host, MAX_STR_LEN);
	g_snprintf(dxc_port_str, EQSO_LEN, "%d", cfg->dxc_port);
	safe_strncpy0(dxc_user, cfg->dxc_user, MAX_STR_LEN);
	safe_strncpy0(dxc_pass, cfg->dxc_pass, MAX_STR_LEN);
	safe_strncpy0(kst_user, cfg->kst_user, MAX_STR_LEN);
	safe_strncpy0(kst_pass, cfg->kst_pass, MAX_STR_LEN);
	safe_strncpy0(kst_name, cfg->kst_name, MAX_STR_LEN);
	safe_strncpy0(sota_user, cfg->sota_user, MAX_STR_LEN);
	safe_strncpy0(sota_pass, cfg->sota_pass, MAX_STR_LEN);
	safe_strncpy0(slovhf_user, cfg->slovhf_user, MAX_STR_LEN);


    if (!(d = (struct dialog *)g_new0(struct dialog, 30))) return;
    
    d->title = CTEXT(T_CTEST_DEF);
    d->fn = dlg_pf_fn;
    d->refresh = (void (*)(void *))refresh_contest_def;
	d->refresh_data = NULL;
	d->y0 = 1;
	i = -1;
    
	if (first){
	    d->title = CTEXT(T_WELCOME);

		d->items[++i].type = D_TEXT;
		d->items[i].msg = CTEXT(T_WELCOME1);
		d->items[i].wrap = 1;

		d->items[++i].type = D_TEXT;
		d->items[i].msg = CTEXT(T_WELCOME2);
		d->items[i].wrap = 2;
	}

    d->items[++i].type = D_FIELD;
    d->items[i].dlen = MAX_STR_LEN;
    d->items[i].data = pcall;
    d->items[i].maxl = 15;
	d->items[i].msg = CTEXT(T_CALLSIGN2);
	d->items[i].wrap = 1;
    
	if (!first){
		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = pclub;
		d->items[i].maxl = 15;
		d->items[i].msg = CTEXT(T_CLUB);
		d->items[i].wrap = 1;
	
		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = operator_;
		d->items[i].maxl = 15;
		d->items[i].msg = CTEXT(T_OPERATOR);
    
		d->items[++i].type = D_CHECKBOX;  
		d->items[i].gid = 0;
		d->items[i].gnum = 1;
		d->items[i].dlen = sizeof(int);
		d->items[i].data=(char *)&global_operator;
		d->items[i].msg = CTEXT(T_GLOBAL_OPERATOR2);
		d->items[i].wrap = 1;
	}

    d->items[++i].type = D_FIELD;
    d->items[i].dlen = MAX_STR_LEN;
    d->items[i].data = pwwlo;
    d->items[i].maxl = 15;
	d->items[i].msg = CTEXT(T_YOURWWL);
	d->items[i].wrap = 1;

	if (first){
		d->items[i].wrap++;
		d->items[++i].type = D_BUTTON;
		d->items[i].gid = 0;
		d->items[i].fn = contest_def_language;
		d->items[i].text = CTEXT(T_LANGUAGE);
		d->items[i].wrap = 1;
#ifdef Z_HAVE_SDL
		if (sdl){
			d->items[++i].type = D_TEXT;
			d->items[i].msg = CTEXT(T_FONT_SIZE);

			d->items[++i].type = D_BUTTON;
			d->items[i].gid = 0;
			d->items[i].fn = contest_def_plus;
			d->items[i].text = "+";

			d->items[++i].type = D_BUTTON;
			d->items[i].gid = 0;
			d->items[i].fn = contest_def_minus;
			d->items[i].text = "-";
			d->items[i].wrap = 1;
		}
#endif
	}
    
    if (!first){
		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = padr1;
		d->items[i].maxl = 25;
		d->items[i].msg = CTEXT(T_PADR1);
		d->items[i].wrap = 1;
    
		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = padr2;
		d->items[i].maxl = 25;
		d->items[i].msg = CTEXT(T_PADR2);
		d->items[i].wrap = 1;
    
		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = pexch;
		d->items[i].maxl = 15;
		d->items[i].msg = CTEXT(T_YOUREXC);
		d->items[i].wrap = 1;
    
		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = default_rs;
		d->items[i].maxl = 5;
		d->items[i].msg = CTEXT(T_RS);
		d->items[i].wrap = 1;
    
		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = default_rst;
		d->items[i].maxl = 5;
		d->items[i].msg = CTEXT(T_RST);
		d->items[i].wrap = 2;

		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = dxc_host;
		d->items[i].maxl = 15;
		d->items[i].msg = CTEXT(T_DXC_HOSTNAME);

		d->items[++i].type = D_FIELD;
		d->items[i].dlen = EQSO_LEN;
		d->items[i].data = dxc_port_str;
		d->items[i].maxl = 6;
		d->items[i].fn   = check_number;
		d->items[i].gid  = 0;
		d->items[i].gnum = 65535;
		d->items[i].msg = CTEXT(T_TCP_PORT);
		d->items[i].wrap = 1;

		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = dxc_user;
		d->items[i].maxl = 15;
		d->items[i].msg = CTEXT(T_DXC_USERNAME);

		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = dxc_pass;
		d->items[i].maxl = 15;
		d->items[i].msg = CTEXT(T_PASSWORD);
		d->items[i].wrap = 1;

		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = sota_user;
		d->items[i].maxl = 14;
		d->items[i].msg = CTEXT(T_SOTA_USERNAME);

		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = sota_pass;
		d->items[i].maxl = 15;
		d->items[i].msg = CTEXT(T_PASSWORD);
		d->items[i].wrap = 1;

		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = kst_user;
		d->items[i].maxl = 15;
		d->items[i].msg = CTEXT(T_KST_USERNAME);

		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = kst_pass;
		d->items[i].maxl = 15;
		d->items[i].msg = CTEXT(T_PASSWORD);
		d->items[i].wrap = 1;

		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = kst_name;
		d->items[i].maxl = 15;
		d->items[i].msg = CTEXT(T_KST_YOURNAME);

		d->items[++i].type = D_FIELD;
		d->items[i].dlen = MAX_STR_LEN;
		d->items[i].data = slovhf_user;
		d->items[i].maxl = 15;
		d->items[i].msg = CTEXT(T_SLOVHF_USER);
		d->items[i].wrap = 1;
	}

	d->items[i].wrap++;
    
    d->items[++i].type = D_BUTTON;   
    d->items[i].gid = B_ENTER;
    d->items[i].fn = ok_dialog;
    d->items[i].text = CTEXT(T_OK);
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid = B_ESC;
	if (first_contest_def){
    	d->items[i].fn = ok_dialog;
	}else{	
	    d->items[i].fn = cancel_dialog;
	}
    d->items[i].text = CTEXT(T_CANCEL);
	d->items[i].align = AL_BUTTONS;
  	d->items[i].wrap = 1;

    d->items[++i].type = D_END;
    do_dialog(d, getml(d, NULL));
}
                               

/******************** EDI PROPERTIES *******************************/

/*
char padr1[MAX_STR_LEN],padr2[MAX_STR_LEN];
*/

void refresh_edi_prop(void *xxx)
{
/*    dbg("refresh_edi_prop\n");*/
    STORE_STR(cfg,padr1);
    STORE_STR(cfg,padr2);
}


void edi_prop(void *arg)
{
    struct dialog *d;
    int i;

    safe_strncpy0(padr1,cfg->padr1,MAX_STR_LEN);
    safe_strncpy0(padr2,cfg->padr2,MAX_STR_LEN);
    
    if (!(d = g_malloc(sizeof(struct dialog) + 20 * sizeof(struct dialog_item)))) return;
    memset(d, 0, sizeof(struct dialog) + 20 * sizeof(struct dialog_item));
    d->title = VTEXT(T_CTEST_DEF);
    d->fn = dlg_pf_fn;
    d->refresh = (void (*)(void *))refresh_edi_prop;
    d->y0 = 1;
    

    d->items[i=0].type = D_FIELD;
    d->items[i].dlen = MAX_STR_LEN;
    d->items[i].data = padr1;
    d->items[i].maxl = 25;
    d->items[i].msg = CTEXT(T_PADR1);
    d->items[i].wrap = 1;
    
    d->items[i=1].type = D_FIELD;
    d->items[i].dlen = MAX_STR_LEN;
    d->items[i].data = padr2;
    d->items[i].maxl = 25;
    d->items[i].msg = CTEXT(T_PADR2);
    d->items[i].wrap = 2;
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid = B_ENTER;
    d->items[i].fn = ok_dialog;
    d->items[i].text = VTEXT(T_OK);
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid = B_ESC;
    d->items[i].fn = cancel_dialog;
    d->items[i].text = VTEXT(T_CANCEL);
    d->items[i].align = AL_BUTTONS;
    d->items[i].wrap = 1;
    
    
    d->items[++i].type = D_END;
    do_dialog(d, getml(d, NULL));
}
                               
int dlg_edi_prop(struct dialog_data *dlg, struct dialog_item_data *di){
    edi_prop(NULL); 
    return 0;
}

