#!/usr/bin/perl -w
use Data::Dumper;

open(COR, ">>tucnakcor") or die;

$t=128;
while ($s=<>){
    chomp($s);
    next if (length($s)==0);
    ($w,$h)=split /\s+/,$s;
    print Dumper($w,$h);
    next unless defined $w;
    next unless defined $h;
    $iw = int($w * 100);
    $ih = int($h * 100);
    print Dumper($iw, $ih);
    $bin=pack("ccccc", $ih % 256, $ih / 256, $iw % 256, $iw / 256, $t);
    $t=0;
    print COR $bin;
}
close(COR);
