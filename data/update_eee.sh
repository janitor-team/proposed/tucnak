#!/bin/sh

echo "*** This is upgrade script for Tucnak"
echo "*** Please follow instructions bellow"
echo ""
      
cd $HOME
echo "tar xvzf '$2' tucnak/tucnak"
tar xvzf '$2' tucnak/tucnak

if test x"$?" = x"0" ; then
    echo "*** Seems OK. Please restart Tucnak"
    exit 0
else
    echo "*** tar returns $?, giving up"
fi


