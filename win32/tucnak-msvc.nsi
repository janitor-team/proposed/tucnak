; tucnak-msvc.nsi
;
; Install script for Nullsoft Scriptable Install System (NSIS), http://nsis.sf.net 
; 

;--------------------------------

LoadLanguageFile "${NSISDIR}\Contrib\Language files\English.nlf"

; The name of the installer
Name "Tucnak 4.36"

; The file to write
OutFile "Tucnak-4.36.exe"

; The default installation directory
InstallDir "$PROGRAMFILES\Tucnak"

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\OK1ZIA\Tucnak" "Install_Dir"

Icon "tucnak.ico"
UninstallIcon "tucnak.ico"

Crccheck on
XPStyle on
AutoCloseWindow true

;--------------------------------

; Pages

Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------


; The stuff to install
Section "Install Tucnak"

    SectionIn 1 2 RO

    ; Set output path to the installation directory.
    SetOutPath $INSTDIR

    ; Put file there
    File "..\Release\tucnak.exe"
    File "..\Release\tucnak.pdb"
;    File "..\Release\libftdi.dll"
;    File "..\Release\libpng.dll"
;    File "..\Release\libzia.dll"

;    File "iconv.dll"
    File "inpout32.dll"
    File "intl.dll"
    File "libfftw3-3.dll"
    File "libglib-2.0-0.dll"
    File "libgthread-2.0-0.dll"
    File "libhamlib-4.dll"
    File "libgcc_s_sjlj-1.dll"
    File "libwinpthread-1.dll"
    File "libiconv2.dll"
    File "libsndfile-1.dll"
    File "libusb-1.0.dll"
    File "libpng.dll"
    File "msvcr120.dll"
    File "portaudio_x86.dll"
    File "SDL2.dll"
    File "tucnak.ico"
    File "zlib1.dll"
    File "libhogweed-6.dll"
    File "libgmp-10.dll"
    File "libgnutls-30.dll"
    File "libnettle-8.dll"
    File "rigctl.exe"
    File "rigctld.exe"
    File "rotctl.exe"


    ;SetOutPath "$INSTDIR\libusb"
    ;File "libusb\Davac4.inf"
    ;File "libusb\Digital_analyser.inf"
    ;File "libusb\HdKeyb.inf"
    ;File "libusb\Rotar4.inf"
    ;File "libusb\FUNcube_Dongle.inf"
    ;File "libusb\Installer_x64.exe"
    ;File "libusb\Installer_x86.exe"
    ;File "libusb\testlibusb.exe"
    ;File "libusb\testlibusb-win.exe"
    
    ;SetOutPath "$INSTDIR\libusb\amd64"
    ;File "libusb\amd64\libusb0.dll"
    ;File "libusb\amd64\libusb0.sys"

    ;SetOutPath "$INSTDIR\libusb\ia64"
    ;File "libusb\ia64\libusb0.dll"
    ;File "libusb\ia64\libusb0.sys"

    ;SetOutPath "$INSTDIR\libusb\x86"
    ;File "libusb\x86\libusb0_x86.dll"
    ;File "libusb\x86\libusb0.sys"


    ; Write the installation path into the registry
    WriteRegStr HKLM Software\OK1ZIA\tucnak "Install_Dir" "$INSTDIR"

    ; Write the uninstall keys for Windows
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Tucnak" "DisplayName" "Tucnak"
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Tucnak" "UninstallString" '"$INSTDIR\uninstall.exe"'
    WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Tucnak" "NoModify" 1
    WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Tucnak" "NoRepair" 1
    WriteUninstaller "uninstall.exe"
    SetOutPath $INSTDIR
  
SectionEnd

; Optional section (can be disabled by the user)
Section "Create shortcut in menu Start"
    SetShellVarContext all
    CreateDirectory "$SMPROGRAMS\OK1ZIA"
    CreateShortCut "$SMPROGRAMS\OK1ZIA\Tucnak.lnk" "$INSTDIR\tucnak.exe" "" "$INSTDIR\tucnak.ico" 0
    CreateShortCut "$SMPROGRAMS\OK1ZIA\Uninstall Tucnak.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
    SetShellVarContext current
    CreateShortCut "$SMPROGRAMS\OK1ZIA\Tucnak data.lnk" "$WINDIR\explorer.exe" '/e="$APPDATA\Tucnak"' "$INSTDIR\tucnak.ico" 0
SectionEnd

Section "Create shortcuts in Desktop"
    SetShellVarContext current
    CreateShortCut "$DESKTOP\Tucnak.lnk" "$INSTDIR\tucnak.exe" "" "$INSTDIR\tucnak.ico" 0
    CreateShortCut "$DESKTOP\Tucnak data.lnk" "$WINDIR\explorer.exe" '/e="$APPDATA\Tucnak"' "$INSTDIR\tucnak.ico" 0
SectionEnd

;Section "Create symbolic links in C:\"
;    System::Call "advapi32::GetUserName(t .r0, *i ${NSIS_MAX_STRLEN} r1) i.r2"
;    ExecWait '"mklink" /d "c:\Tucnak-$0" "$APPDATA\Tucnak"' $0
;    DetailPrint "some program returned $0"
;    ExecWait '"mklink" "c:\Tucnak-Program" "$INSTDIR\tucnak.exe"' $0
;    DetailPrint "some program returned $0"
;SectionEnd

Section "Run Tucnak"
    Exec "$INSTDIR\tucnak.exe"
SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"

    ; Remove registry keys
    DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Tucnak"
    DeleteRegValue HKLM "Software\OK1ZIA\Tucnak" "Install_Dir"
    DeleteRegKey /ifempty HKLM "Software\OK1ZIA\Tucnak"


    ; Remove files and uninstaller
    Delete "$INSTDIR\tucnak.exe"
    Delete "$INSTDIR\tucnak.pdb"
    Delete "$INSTDIR\tucnak.dmp"    ; can be copied here for crash analyse
;    Delete "$INSTDIR\libftdi.dll"  
;    Delete "$INSTDIR\libzia.dll"  

    Delete "$INSTDIR\iconv.dll"         
    Delete "$INSTDIR\inpout32.dll"         
    Delete "$INSTDIR\intl.dll"         
    Delete "$INSTDIR\libfftw3-3.dll"       
    Delete "$INSTDIR\libhamlib-2.dll"
    Delete "$INSTDIR\libhamlib-4.dll"
    Delete "$INSTDIR\hamlib-*.dll"
    Delete "$INSTDIR\libgcc_s_sjlj-1.dll"
    Delete "$INSTDIR\libwinpthread-1.dll"
    Delete "$INSTDIR\libglib-2.0-0.dll"    
    Delete "$INSTDIR\libgthread-1.0-0.dll" 
    Delete "$INSTDIR\libgthread-2.0-0.dll" 
    Delete "$INSTDIR\libiconv2.dll"        
    Delete "$INSTDIR\libsndfile-1.dll"     
    Delete "$INSTDIR\libusb0.dll"          
    Delete "$INSTDIR\libusb-1.0.dll"          
    Delete "$INSTDIR\libpng.dll"  
    Delete "$INSTDIR\msvcr100.dll"          
    Delete "$INSTDIR\msvcr100d.dll"          
    Delete "$INSTDIR\msvcr120.dll"          
    Delete "$INSTDIR\portaudio_x86.dll"    
    Delete "$INSTDIR\SDL2.dll"              
    Delete "$INSTDIR\SDL.dll"              
    Delete "$INSTDIR\tucnak.ico"           
    Delete "$INSTDIR\uninstall.exe"
    Delete "$INSTDIR\zlib1.dll"            
    Delete "$INSTDIR\rigctl.exe"            
    Delete "$INSTDIR\rigctld.exe"            
    Delete "$INSTDIR\rotctl.exe"            

    Delete "$INSTDIR\libusb\Davac4.inf"
    Delete "$INSTDIR\libusb\Digital_analyser.inf"
    Delete "$INSTDIR\libusb\HdKeyb.inf"
    Delete "$INSTDIR\libusb\Rotar4.inf"
    Delete "$INSTDIR\libusb\FUNcube_Dongle.inf"
    Delete "$INSTDIR\libusb\Installer_x64.exe"
    Delete "$INSTDIR\libusb\Installer_x86.exe"
    Delete "$INSTDIR\libusb\testlibusb.exe"
    Delete "$INSTDIR\libusb\testlibusb-win.exe"
    Delete "$INSTDIR\libusb\amd64\libusb0.dll"
    Delete "$INSTDIR\libusb\amd64\libusb0.sys"
    Rmdir  "$INSTDIR\libusb\amd64"
    Delete "$INSTDIR\libusb\ia64\libusb0.dll"
    Delete "$INSTDIR\libusb\ia64\libusb0.sys"
    Rmdir  "$INSTDIR\libusb\ia64"
    Delete "$INSTDIR\libusb\x86\libusb0_x86.dll"
    Delete "$INSTDIR\libusb\x86\libusb0.sys"
    Rmdir  "$INSTDIR\libusb\x86"
    Rmdir  "$INSTDIR\libusb"


    SetShellVarContext all
    Delete "$SMPROGRAMS\OK1ZIA\Tucnak.lnk"
    Delete "$SMPROGRAMS\OK1ZIA\Uninstall Tucnak.lnk" 
    SetShellVarContext current
    Delete "$SMPROGRAMS\OK1ZIA\Tucnak data.lnk" 

    SetShellVarContext current
    Delete "$DESKTOP\Tucnak.lnk"
    Delete "$DESKTOP\Tucnak data.lnk" 

    ; Remove directories used
    RMDir "$SMPROGRAMS\OK1ZIA"
    RMDir "$INSTDIR"

SectionEnd
