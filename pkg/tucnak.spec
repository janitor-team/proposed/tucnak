# Note that this is NOT a relocatable package
%define ver      4.36
%define rel      1
%define prefix   /usr

Summary:	VHF and microwave contest log
Name:		4.36
Version:	%ver
Release:	%rel
License:	GPL
Group:		Applications/Hamradio
Source:		tucnak-%{PACKAGE_VERSION}.tar.gz
URL:		http://tucnak.nagano.cz/
BuildRoot:	/tmp/tucnak-%{PACKAGE_VERSION}-root
# fill this by your name and email (then uncomment)
#Packager:	Mike Karas OM4AA <zoliqe@gmail.com>
Requires:	glib

%description
Tucnak is a amaterur radio VHF and above contest logging program
with some useful features as networking, cw keying, ssb voicer,
sound recorder and more. User interface is based on Taclog.


%prep
%setup
CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%prefix --sysconfdir=/etc --with-pkg=rpm
make

%install
make DESTDIR=$RPM_BUILD_ROOT install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%config /etc/*
%{prefix}/*

%changelog
* Tue Feb 14 2006 OM4AA
- fixed legacy tag
- updated to 1.24
* Wed Apr 13 2005 OK1ZIA
- Packages for Mandrake made by PE2SVN
* Mon Nov 22 2004 OM4AA
- updated to version 1.16, for news in this version see ChangeLog
* Tue Nov 11 2004 OM4AA
- updated .spec file for tucnak2 (and newer) package name
* Fri Dec 13 2002 OK1ZIA
- created .spec file

